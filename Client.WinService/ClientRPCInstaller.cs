﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Client.WinService
{
    [RunInstaller(true)]
    public partial class ClientRPCInstaller : System.Configuration.Install.Installer
    {
        public ClientRPCInstaller()
        {
            InitializeComponent();
        }

        private void RPCClientInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            using (ServiceController sc = new ServiceController(RPCClientInstaller.ServiceName))
            {
                sc.Start();
            }
        }
    }
}
