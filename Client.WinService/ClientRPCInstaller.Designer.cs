﻿namespace Client.WinService
{
    partial class ClientRPCInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RPCClientProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.RPCClientInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // RPCClientProcessInstaller
            // 
            this.RPCClientProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.RPCClientProcessInstaller.Password = null;
            this.RPCClientProcessInstaller.Username = null;
            // 
            // RPCClientInstaller
            // 
            this.RPCClientInstaller.Description = "Client service of RemotePowerController";
            this.RPCClientInstaller.DisplayName = "RPCClient";
            this.RPCClientInstaller.ServiceName = "RPCClient";
            this.RPCClientInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.RPCClientInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.RPCClientInstaller_AfterInstall);
            // 
            // ClientRPCInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.RPCClientProcessInstaller,
            this.RPCClientInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller RPCClientProcessInstaller;
        private System.ServiceProcess.ServiceInstaller RPCClientInstaller;
    }
}