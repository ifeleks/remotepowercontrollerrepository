﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceProcess;
using System.Threading;
using Cassia;
using Client.Service;
using Common.Logger;
using Common.Logger.Impl;

namespace Client.WinService
{
    public partial class ClientRPCService : ServiceBase
    {
        public ServiceHost PowerManagementHost = null;
        private Thread _loginCheckThread;
        private ILogger _logger;
        private volatile bool _isRunning;

        public ClientRPCService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _logger = LoggerFactory.Current.GetLogger<ClientRPCService>();
            _isRunning = true;

            _loginCheckThread = new Thread(loginCheck);
            _loginCheckThread.Start();
            
            _logger.Trace("PowerManagementWinService started");
        }

        private void loginCheck()
        {
            while (_isRunning)
            {
                //Close service if client app is closed manually
                if (IsSomeoneLoggedIn() && !IsProcessAvailble())
                {
                    if (PowerManagementHost != null) PowerManagementHost.Close();
                    PowerManagementHost = null;
                }
                else
                {
                    if (PowerManagementHost == null)
                    {
                        PowerManagementHost = new ServiceHost(typeof (PowerManagementWinService));
                        PowerManagementHost.Open();
                    }
                }
                Thread.Sleep(1000);
            }
        }

        protected override void OnStop()
        {
            try
            {
                if (PowerManagementHost != null) PowerManagementHost.Close();
                PowerManagementHost = null;
                _logger.Trace("PowerManagementWinService closed");
                _loginCheckThread.Abort();
            }
            catch (Exception ex)
            {
                _logger.Error("Can't stop WCF-service", ex);
                this.Stop();
            }
        }

        private bool IsSomeoneLoggedIn()
        {
            ITerminalServicesManager manager = new TerminalServicesManager();
            var loginedUsers = new List<string>();
            using (ITerminalServer server = manager.GetLocalServer())
            {
                server.Open();
                foreach (ITerminalServicesSession session in server.GetSessions())
                {
                    var account = session.UserAccount;
                    if (account != null)
                    {
                        loginedUsers.Add(session.UserName);
                    }
                }
            }
            return loginedUsers.Count != 0;
        }

        private bool IsProcessAvailble()
        {
            var a = Process.GetProcessesByName("RPCClient");
            return a.Any();
        }
    }
}
