﻿namespace RemotePowerControllerTestCore.Helpers
{
    public class AdminDataHelper
    {
        public static int GetAdminId()
        {
            return 6;
        }

        public static int GetAdminGroupAllId()
        {
            return 1;
        }

        public static string GetAdminRoleName
        {
            get
            {
                return "admin";
            }
        }
    }
}
