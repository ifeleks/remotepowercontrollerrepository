﻿using System;

namespace RemotePowerControllerTestCore.Helpers
{
    public class TimeZoneHelper
    {
        private static string timeZoneId = "FLE Standard Time";
        public static readonly TimeZoneInfo CurrentTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
    }
}
