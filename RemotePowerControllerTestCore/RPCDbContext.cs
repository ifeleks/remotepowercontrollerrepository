﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using RemotePowerControllerTestCore.Entities;

namespace RemotePowerControllerTestCore
{
    public class RPCDbContext : DbContext
    {
        public RPCDbContext()
            : base("RPCDbContext")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientSettings> ClientSettingses { get; set; }
        public DbSet<ClientState> ClientStates { get; set; }
        public DbSet<ClientStateAudit> ClientStateAudits { get; set; }
        public DbSet<Favorite> Favorites { get; set; }
        public DbSet<DomainAuthAttempt> DomainAuthAttempts { get; set; }
        public DbSet<Server> Servers { get; set; }
        public DbSet<CommandAudit> CommandAudits { get; set; }
        public DbSet<Group> Groups {get; set;}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<User>().ToTable("User");

            modelBuilder.Entity<Client>().HasOptional(cs => cs.ClientSettings).WithRequired(c => c.Client);

            modelBuilder.Entity<Client>().HasOptional(cs => cs.ClientState).WithRequired(c => c.Client);

            modelBuilder.Entity<Favorite>().HasKey(pk => new { pk.UserId, pk.ClientId });
        }
    }
}
