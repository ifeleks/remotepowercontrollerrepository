﻿using System;
using RemotePowerControllerTestCore.Entities;
using RemotePowerControllerTestCore.Repositories.Implementations;
using RemotePowerControllerTestCore.Repositories.Interfaces;

namespace RemotePowerControllerTestCore
{
    public class RPCUnitOfWork : IDisposable
    {
        private RPCDbContext context = new RPCDbContext();
        private IGenericRepository<User> _userRepository;
        private IGenericRepository<Client> _clientRepository;
        private IGenericRepository<ClientSettings> _clientSettingsRepository;
        private IGenericRepository<ClientState> _clientStateRepository;
        private IGenericRepository<ClientStateAudit> _clientStateAuditRepository;
        private IGenericRepository<Server> _serverRepository;
        private IGenericRepository<Group> _groupRepository;
        private IGenericRepository<Favorite> _favoriteRepository;

        public IGenericRepository<User> UserRepository
        {
            get { return _userRepository ?? (_userRepository = new UOWGenericRepository<User>(context)); }
        }

        public IGenericRepository<Client> ClientRepository
        {
            get { return _clientRepository ?? (_clientRepository = new UOWGenericRepository<Client>(context)); }
        }

        public IGenericRepository<ClientSettings> ClientSettingsRepository
        {
            get { return _clientSettingsRepository ?? (_clientSettingsRepository = new UOWGenericRepository<ClientSettings>(context)); }
        }

        public IGenericRepository<ClientState> ClientStateRepository
        {
            get { return _clientStateRepository ?? (_clientStateRepository = new UOWGenericRepository<ClientState>(context)); }
        }

        public IGenericRepository<ClientStateAudit> ClientStateAuditRepository
        {
            get { return _clientStateAuditRepository ?? (_clientStateAuditRepository = new UOWGenericRepository<ClientStateAudit>(context));}
        }

        public IGenericRepository<Server> ServerRepository
        {
            get { return _serverRepository ?? (_serverRepository = new UOWGenericRepository<Server>(context)); }
        }

        public IGenericRepository<Group> GroupRepository
        {
            get { return _groupRepository ?? (_groupRepository = new UOWGenericRepository<Group>(context)); }
        }

        public IGenericRepository<Favorite> FavoriteRepository
        {
            get { return _favoriteRepository ?? (_favoriteRepository = new UOWGenericRepository<Favorite>(context)); }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing) context.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
