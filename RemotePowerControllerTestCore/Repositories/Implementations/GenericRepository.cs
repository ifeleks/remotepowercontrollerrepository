﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using RemotePowerControllerTestCore.Repositories.Interfaces;

namespace RemotePowerControllerTestCore.Repositories.Implementations
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            using (var context = new RPCDbContext())
            {
                IQueryable<TEntity> query = context.Set<TEntity>();

                query = CreateQuery(filter, includeProperties, query);

                var result = orderBy != null ? orderBy(query).ToList() : query.ToList();

                return result;
            }
        }

        private static IQueryable<TEntity> CreateQuery(Expression<Func<TEntity, bool>> filter, string includeProperties, IQueryable<TEntity> query)
        {
            if (filter != null)
            {
                query = query.Where(filter);
            }

            IQueryable<TEntity> result = query;
            foreach (string s in includeProperties.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries))
                result = result.Include(s);
            return result;
        }

        public TEntity GetByID(int id)
        {
            using (var context = new RPCDbContext())
            {
                return context.Set<TEntity>().Find(id);
            }
        }

        public TEntity Insert(TEntity entity)
        {
            using (var context = new RPCDbContext())
            {
                context.Set<TEntity>().Add(entity);
                context.SaveChanges();

                return entity;
            }
        }

        public void Delete(int id)
        {
            using (var context = new RPCDbContext())
            {
                TEntity entityToDelete = context.Set<TEntity>().Find(id);
                if (context.Entry(entityToDelete).State == EntityState.Detached)
                    context.Set<TEntity>().Attach(entityToDelete);
                context.Set<TEntity>().Remove(entityToDelete);
                context.SaveChanges();
            }
        }

        public void Delete(TEntity entityToDelete)
        {
            using (var context = new RPCDbContext())
            {
                if (context.Entry(entityToDelete).State == EntityState.Detached)
                    context.Set<TEntity>().Attach(entityToDelete);
                context.Set<TEntity>().Remove(entityToDelete);
                context.SaveChanges();
            }
        }

        public void Update(TEntity entityToUpdate)
        {
            using (var context = new RPCDbContext())
            {
                context.Entry(entityToUpdate).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
