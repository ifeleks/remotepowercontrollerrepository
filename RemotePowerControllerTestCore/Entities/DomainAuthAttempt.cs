﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.DTO;

namespace RemotePowerControllerTestCore.Entities
{
    public class DomainAuthAttempt
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DomainAuthAttemptId { get; set; }

        [Index("IX_User_Username", 1, IsUnique = true)]
        [StringLength(255, MinimumLength = 1)]
        public string Username { get; set; }

        [StringLength(255, MinimumLength = 6)]
        public string Password { get; set; }

        public bool? IsAuth { get; set; }

        public static AuthentificationAttemptTO ConvertToTO(DomainAuthAttempt domainAuthAttempt)
        {
            return new AuthentificationAttemptTO
            {
                Id = domainAuthAttempt.DomainAuthAttemptId,
                UserName = domainAuthAttempt.Username,
                PasswordCrypt = domainAuthAttempt.Password,
                IsValid = domainAuthAttempt.IsAuth
            };
        }

        public static DomainAuthAttempt ConvertFromTO(AuthentificationAttemptTO authentificationAttemptTo)
        {
            return new DomainAuthAttempt
            {
                DomainAuthAttemptId = authentificationAttemptTo.Id,
                Username = authentificationAttemptTo.UserName,
                Password = authentificationAttemptTo.PasswordCrypt,
                IsAuth = authentificationAttemptTo.IsValid
            };
        }
    }
}
