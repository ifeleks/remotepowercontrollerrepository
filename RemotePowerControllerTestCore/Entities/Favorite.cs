﻿namespace RemotePowerControllerTestCore.Entities
{
    public class Favorite
    {
        public int UserId { get; set; }
        public int ClientId { get; set; }
    }
}
