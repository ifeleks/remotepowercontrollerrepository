﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.DTO;
using RemotePowerControllerTestCore.Repositories.Implementations;

namespace RemotePowerControllerTestCore.Entities
{
    public class Client
    {
        public Client()
        {
            Groups = new HashSet<Group>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClientId { get; set; }

        [StringLength(255, MinimumLength = 2)]
        public string NetworkName { get; set; }

        [StringLength(255, MinimumLength = 0)]
        public string FullName { get; set; }

        [Index("IX_Client_MACAddress", 1, IsUnique = true)]
        [StringLength(12, MinimumLength = 12)]
        public string MACAddress { get; set; }

        [StringLength(15, MinimumLength = 7)]
        public string ClientIP { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime InstallationDate { get; set; }

        public int ServerId { get; set; }

        public int UserId { get; set; }

        public virtual Server Server { get; set;  }
        
        public virtual ClientSettings ClientSettings { get; set; }
        
        public virtual ClientState ClientState { get; set; }

        [InverseProperty("Clients")]
        public virtual ICollection<Group> Groups { get; set; }

        public static ClientInfoTO ConvertToTO(Client client)
        {
            return new ClientInfoTO
            {
                ClientName = client.NetworkName,
                ClientFullName = client.FullName,
                MacAddress = client.MACAddress,
                ClientIp = client.ClientIP,
                ServerSignature = client.GetServerNameById(client.ServerId),
                State = client.ClientState.CurrentState,
                Command = client.ClientState.Command
            };
        }

        public static Client ConvertFromTO(ClientInfoTO clientTO)
        {
            return new Client
            {
                NetworkName = clientTO.ClientName,
                FullName = clientTO.ClientFullName,
                MACAddress = clientTO.MacAddress,
                ClientIP = clientTO.ClientIp,
                InstallationDate = clientTO.InstallationDate,
            };
        }

        private string GetServerNameById(int id)
        {
            var serverRepo = new GenericRepository<Server>();
            var server = serverRepo.GetByID(id);
            return server == null ? String.Empty : server.ServerName;
        }
    }
}
