﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace RemotePowerControllerTestCore.Entities
{
    public class Server
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ServerId { get; set; }

        public string ServerName { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime LastActivity { get; set; }
    }
}
