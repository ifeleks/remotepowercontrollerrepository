﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.DTO;
using Common.DTO.Enums;

namespace RemotePowerControllerTestCore.Entities
{
    public class ClientState
    {
        [Key]
        [ForeignKey("Client")]
        public int ClientId { get; set; }

        public State CurrentState { get; set; }

        public Command Command { get; set; }

        public virtual Client Client { get; set; }

        public static ClientInfoTO ConvertToTO(ClientState clientState)
        {
            return new ClientInfoTO
            {
                State = clientState.CurrentState,
                Command = clientState.Command
            };
        }

        public static ClientState ConvertFromTO(ClientInfoTO clientInfo)
        {
            return new ClientState
            {
                CurrentState = clientInfo.State,
                Command = clientInfo.Command
            };
        }

        public static ClientState DefaultState()
        {
            return new ClientState
            {
                CurrentState = State.On,
                Command = Command.DoNothing
            };
        }
    }
}
