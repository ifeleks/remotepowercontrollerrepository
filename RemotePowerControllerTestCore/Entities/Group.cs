﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RemotePowerControllerTestCore.Entities
{
    public class Group
    {
        public Group()
        {
            Clients = new HashSet<Client>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GroupId { get; set; }

        public string GroupName { get; set; }

        public int UserId { get; set; }

        [InverseProperty("Groups")]
        public virtual ICollection<Client> Clients { get; set; }
    }
}
