﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.DTO;
using Common.DTO.Enums;

namespace RemotePowerControllerTestCore.Entities
{
    public class ClientSettings
    {
        [Key]
        [ForeignKey("Client")]
        public int ClientId { get; set; }

        public TimeSpan StartWorkTime { get; set; }
        public TimeSpan EndWorkTime { get; set; }
        public TimeSpan IdleTime { get; set; }

        public DefaultShutdownType DefaultEndWorkType { get; set; }

        public virtual Client Client { get; set; }

        public static SettingsTO ConvertToTO(ClientSettings clientSettings)
        {
            return new SettingsTO
            {
                WorkTimeStart = clientSettings.StartWorkTime,
                WorkTimeEnd = clientSettings.EndWorkTime,
                IdleTime = clientSettings.IdleTime.Minutes,
                ShutDownMode = clientSettings.DefaultEndWorkType
            };
        }

        public static ClientSettings ConvertFromTO(SettingsTO settingsTo)
        {
            return new ClientSettings
            {
                StartWorkTime = settingsTo.WorkTimeStart,
                EndWorkTime = settingsTo.WorkTimeEnd,
                IdleTime = new TimeSpan(0, settingsTo.IdleTime, 0),
                DefaultEndWorkType = settingsTo.ShutDownMode
            };
        }
    }
}
