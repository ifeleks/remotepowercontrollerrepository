﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Common.DTO.Enums;

namespace RemotePowerControllerTestCore.Entities
{
    public class CommandAudit
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommandAuditId { get; set; }

        public DateTime DateTime { get; set; }

        public int ClientId { get; set; }

        public Command Command { get; set; }

        public int UserId { get; set; }

        public string UserIP { get; set; }
    }
}
