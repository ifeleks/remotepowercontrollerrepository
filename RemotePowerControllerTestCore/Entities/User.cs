﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.DTO;

namespace RemotePowerControllerTestCore.Entities
{
    public class User
    {
        public User()
        {
            Groups = new HashSet<Group>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        [Index("IX_User_Username", 1, IsUnique = true)]
        [StringLength(255, MinimumLength = 1)]
        public string Username { get; set; }

        [StringLength(128, MinimumLength = 6)]
        public string Password { get; set; }

        public bool IsDomainAuth { get; set; }

        public virtual ICollection<Group> Groups { get; set; }

        public static User ConvertFromTO(UserInfoTO userTO)
        {
            return new User
            {
                Username = userTO.Login,
                Password = userTO.Password,
                IsDomainAuth = userTO.IsDomain
            };
        }

        public static UserInfoTO ConvertToTO(User user)
        {
            return new UserInfoTO
            {
                Login = user.Username,
                Password = user.Password,
                IsDomain = user.IsDomainAuth
            };
        }
    }
}
