﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Common.DTO.Enums;

namespace RemotePowerControllerTestCore.Entities
{
    public class ClientStateAudit
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClientStateAuditId { get; set; }

        public DateTime DateTime { get; set; }

        public int ClientId { get; set; }

        public State State { get; set; }

        public virtual Client Client { get; set; }

        public static ClientStateAudit DefaultStateAudit()
        {
            return new ClientStateAudit {DateTime = DateTime.UtcNow, State = State.On};
        }
    }
}
