﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Security;
using Common.DTO.Enums;
using RemotePowerControllerTestCore.Entities;
using RemotePowerControllerTestCore.Helpers;
using WebMatrix.WebData;

namespace RemotePowerControllerTestCore
{
    public class RPCDbInitializer : CreateDatabaseIfNotExists<RPCDbContext>
    {
        protected override void Seed(RPCDbContext context)
        {
            var users = new List<User>
            {
                new User{Username="andriy.pukalyak",    Password= PasswordHash.CreateHash("ap_password"),   IsDomainAuth = false},
                new User{Username="andriy.leshchuk",    Password= PasswordHash.CreateHash("al_password"),   IsDomainAuth = false},
                new User{Username="oleksandr.kravchuk", Password= PasswordHash.CreateHash("ok_password"),   IsDomainAuth = false},
                new User{Username="dmytro.kuznecov",    Password= PasswordHash.CreateHash("dk_password"),   IsDomainAuth = false},
                new User{Username="myhaylo.romaniuk",   Password= PasswordHash.CreateHash("mr_password"),   IsDomainAuth = false},
                new User{Username = "rpc_admin",        Password = PasswordHash.CreateHash("rpc_password"), IsDomainAuth = false}
            };
            users.ForEach(u => context.Users.Add(u));
            context.SaveChanges();

            var servers = new List<Server>
            {
                new Server{ServerName = "if_eleks", LastActivity = DateTime.UtcNow}
            };
            servers.ForEach(s=>context.Servers.Add(s));
            context.SaveChanges();

            var clients = new List<Client>
            {
                new Client{NetworkName= "WS-IF-CP0080",     FullName="WS-IF-CP0080.eleks-software.local",  MACAddress = "001CC04B2DA3", ClientIP = "172.20.0.78",  InstallationDate = DateTime.Now, ServerId = 1, UserId = 1},
                new Client{NetworkName= "Andriy_Laptop",    FullName="Andriy_Laptop.eleks-software.local", MACAddress = "3C970ECA81ED", ClientIP = "172.20.0.148", InstallationDate = DateTime.Now, ServerId = 1, UserId = 2},
                new Client{NetworkName= "Santinel",         FullName="Santinel.eleks-software.local",      MACAddress = "3C970E28E724", ClientIP = "172.20.0.120", InstallationDate = DateTime.Now, ServerId = 1, UserId = 3},
                new Client{NetworkName= "Dima_Kuznecov",    FullName="Dima_Kuznecov.eleks-software.local", MACAddress = "8434971371BC", ClientIP = "172.20.0.75",  InstallationDate = DateTime.Now, ServerId = 1, UserId = 4},
                new Client{NetworkName= "RomaniukPC",       FullName="RomaniukPC.eleks-software.local",    MACAddress = "0002447CF299", ClientIP = "172.20.0.175", InstallationDate = DateTime.Now, ServerId = 1, UserId = 5},
                new Client{NetworkName= "ANDRIY2",          FullName="ANDRIY2.eleks-software.local",       MACAddress = "60EB69375499", ClientIP = "172.20.0.166", InstallationDate = DateTime.Now, ServerId = 1, UserId = 1},
            };
            clients.ForEach(c => context.Clients.Add(c));
            context.SaveChanges();

            var clientSettings = new List<ClientSettings>
            {
                
                new ClientSettings{ClientId = 1, StartWorkTime = new TimeSpan(9, 0, 0),     EndWorkTime = new TimeSpan(19, 0, 0), IdleTime = new TimeSpan(0, 15, 0),    DefaultEndWorkType = DefaultShutdownType.Off},
                new ClientSettings{ClientId = 2, StartWorkTime = new TimeSpan(10, 0, 0),    EndWorkTime = new TimeSpan(20, 0, 0), IdleTime = new TimeSpan(0, 10, 0),    DefaultEndWorkType = DefaultShutdownType.Off},
                new ClientSettings{ClientId = 3, StartWorkTime = new TimeSpan(8, 0, 0),     EndWorkTime = new TimeSpan(18, 0, 0), IdleTime = new TimeSpan(0, 5, 0),     DefaultEndWorkType = DefaultShutdownType.Sleep},
                new ClientSettings{ClientId = 4, StartWorkTime = new TimeSpan(11, 0, 0),    EndWorkTime = new TimeSpan(21, 0, 0), IdleTime = new TimeSpan(0, 7, 0),     DefaultEndWorkType = DefaultShutdownType.Sleep},
                new ClientSettings{ClientId = 5, StartWorkTime = new TimeSpan(12, 0, 0),    EndWorkTime = new TimeSpan(15, 0, 0), IdleTime = new TimeSpan(0, 6, 0),     DefaultEndWorkType = DefaultShutdownType.Off},
                new ClientSettings{ClientId = 6, StartWorkTime = new TimeSpan(12, 0, 0),    EndWorkTime = new TimeSpan(15, 0, 0), IdleTime = new TimeSpan(0, 6, 0),     DefaultEndWorkType = DefaultShutdownType.Off},          
            };
            clientSettings.ForEach(cs => context.ClientSettingses.Add(cs));
            context.SaveChanges();

            var clientStates = new List<ClientState>
            {
                new ClientState{ClientId = 1, CurrentState = State.Off, Command = Command.DoNothing},
                new ClientState{ClientId = 2, CurrentState = State.Off, Command = Command.DoNothing},
                new ClientState{ClientId = 3, CurrentState = State.Off, Command = Command.DoNothing},
                new ClientState{ClientId = 4, CurrentState = State.Off, Command = Command.DoNothing},
                new ClientState{ClientId = 5, CurrentState = State.Off, Command = Command.DoNothing},
                new ClientState{ClientId = 6, CurrentState = State.Off, Command = Command.DoNothing},
            };

            clientStates.ForEach(cs => context.ClientStates.Add(cs));
            context.SaveChanges();

            var clientStateAudit = new List<ClientStateAudit>
            {
                new ClientStateAudit{ClientId = 1, DateTime = DateTime.UtcNow, State = State.Off},
                new ClientStateAudit{ClientId = 2, DateTime = DateTime.UtcNow, State = State.Off},
                new ClientStateAudit{ClientId = 3, DateTime = DateTime.UtcNow, State = State.Off},
                new ClientStateAudit{ClientId = 4, DateTime = DateTime.UtcNow, State = State.Off},
                new ClientStateAudit{ClientId = 5, DateTime = DateTime.UtcNow, State = State.Off},
                new ClientStateAudit{ClientId = 6, DateTime = DateTime.UtcNow, State = State.Off},
            };

            clientStateAudit.ForEach(csa => context.ClientStateAudits.Add(csa));
            context.SaveChanges();

            var favorites = new List<Favorite>
            {
                new Favorite{UserId = 6, ClientId = 1},
                new Favorite{UserId = AdminDataHelper.GetAdminId(), ClientId = 2},
                new Favorite{UserId = 1, ClientId = 3},
            };
            favorites.ForEach(f => context.Favorites.Add(f));
            context.SaveChanges();

            var groups = new List<Group>
            {
                new Group{GroupName = "All",        UserId = AdminDataHelper.GetAdminId()},
                new Group{GroupName = "Favorites",  UserId = 1},
                new Group{GroupName = "Favorites",  UserId = 2},
                new Group{GroupName = "Favorites",  UserId = 3},
                new Group{GroupName = "Favorites",  UserId = 4},
                new Group{GroupName = "Favorites",  UserId = 5},
                new Group{GroupName = "Favorites",  UserId = AdminDataHelper.GetAdminId()},
                new Group{GroupName = "Test", UserId = 1},
                new Group{GroupName = "Test", UserId = AdminDataHelper.GetAdminId()},
            };
            groups.ForEach(g=>context.Groups.Add(g));
            context.SaveChanges();
            
            //add clients to admin all group
            for (var i = 0; i < clients.Count; i++)
            {
                groups[0].Clients.Add(clients[i]);
            }
            context.SaveChanges();

            //add client Andriy_2 to test group
            groups[7].Clients.Add(clients[5]);
            context.SaveChanges();

            //add WS-IF to admin test group
            groups[8].Clients.Add(clients[0]);
            context.SaveChanges();

            if (!WebSecurity.Initialized)
            {
                WebSecurity.InitializeDatabaseConnection("RPCDbContext", "User", "UserId", "Username", true);
            }
            if (!Roles.RoleExists(AdminDataHelper.GetAdminRoleName))
            {
                Roles.CreateRole(AdminDataHelper.GetAdminRoleName);
            }
            if (!Roles.GetRolesForUser("rpc_admin").Contains(AdminDataHelper.GetAdminRoleName))
            {
                Roles.AddUsersToRoles(new[] { "rpc_admin" }, new[] { AdminDataHelper.GetAdminRoleName });
            }
        } 
    }
}