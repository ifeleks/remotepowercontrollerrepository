﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using RemotePowerControllerTestCore.Entities;

namespace WinFormTestClient
{
    public class PostSender
    {
        public static string Send(string username, string password)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:50952/api/users");
            httpWebRequest.ContentType = "text/json";
            httpWebRequest.Method = "POST";
            var json = String.Empty;
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                json = "{\"UserID\":\"233\"," + "\"Username\":\"" + username + "\",\"Password\":\"" + password +"\"}";
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (HttpWebResponse) httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                }
            }

            return json;
        }
    }
}
