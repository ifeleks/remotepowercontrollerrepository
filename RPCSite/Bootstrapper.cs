using System.Web.Mvc;
using Microsoft.Practices.Unity;
using RemotePowerControllerTestCore.Repositories.Implementations;
using RemotePowerControllerTestCore.Repositories.Interfaces;
using Unity.Mvc4;

namespace RPCSite
{
    public static class Bootstrapper
    {
        public static IUnityContainer Initialise()
        {
            IUnityContainer container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            RegisterTypes(container);

            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType(typeof(IGenericRepository<>), typeof(GenericRepository<>));
        }
    }
}