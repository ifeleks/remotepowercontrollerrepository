﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using RemotePowerControllerTestCore;
using RemotePowerControllerTestCore.Helpers;
using RPCSite.Models;
using WebMatrix.WebData;
using Group = RemotePowerControllerTestCore.Entities.Group;

namespace RPCSite.Controllers
{
    public class GroupController : Controller
    {
        //
        // GET: /Group/

        public ActionResult Index()
        {
            var groupModels = new List<Models.Group>();
            using (var uow = new RPCUnitOfWork())
            {
                var groupFavorites = uow.GroupRepository.Get(gr=>gr.UserId == WebSecurity.CurrentUserId && gr.GroupName == GroupDataHelper.FavoritesGroupName).First();
                var adminGroupAllId = AdminDataHelper.GetAdminGroupAllId();
                var groupAll = uow.GroupRepository.GetByID(adminGroupAllId);
                var networkClientModels = new List<NetworkClient>();
                foreach (var client in groupAll.Clients)
                {
                    var clientOwner = uow.UserRepository.GetByID(client.UserId);
                    var server = uow.ServerRepository.GetByID(client.ServerId);
                    var clientDetailsEntities = uow.ClientStateAuditRepository.Get(csa => csa.ClientId == client.ClientId).ToList();
                    var networkClientModel = NetworkClient.CreateFromEntity(client, clientOwner, server, clientDetailsEntities);
                    if (Roles.IsUserInRole(AdminDataHelper.GetAdminRoleName)) networkClientModel.IsCurrentUserClient = true;
                    else if (client.UserId == WebSecurity.CurrentUserId) networkClientModel.IsCurrentUserClient = true;
                    networkClientModels.Add(networkClientModel);
                }
                var groups = new List<Group> {groupFavorites, groupAll};
                IEnumerable<Group> otherGroups;
                if (Roles.IsUserInRole(AdminDataHelper.GetAdminRoleName))
                    otherGroups = uow.GroupRepository.Get(gr => gr.UserId == WebSecurity.CurrentUserId && gr.GroupId != adminGroupAllId && gr.GroupName != GroupDataHelper.FavoritesGroupName,
                        o=>o.OrderBy(gr=>gr.GroupName));
                else
                    otherGroups = uow.GroupRepository.Get(gr => gr.UserId == WebSecurity.CurrentUserId && gr.GroupName != GroupDataHelper.FavoritesGroupName,
                        o => o.OrderBy(gr => gr.GroupName));
                groups.AddRange(otherGroups);
                foreach (var @group in groups)
                {
                    var tempNetworkClientModels = new List<NetworkClient>();
                    foreach (var client in @group.Clients)
                    {
                        var tempNetworkClientModel = networkClientModels.Find(nc => nc.NetworkClientId == client.ClientId);
                        if(tempNetworkClientModel != null) tempNetworkClientModels.Add(tempNetworkClientModel);
                    }
                    var groupModel = Models.Group.ConvertFromEntity(@group, tempNetworkClientModels);
                    groupModels.Add(groupModel);
                }
            }
            return View(groupModels);
        }

        //
        // GET: /Group/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Group/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Group/Create

        [HttpPost]
        public ActionResult Create(string groupName)
        {
            try
            {
                using (var uow = new RPCUnitOfWork())
                {
                    uow.GroupRepository.Insert(new Group{GroupName = groupName, UserId = WebSecurity.CurrentUserId});
                    uow.Save();
                }
                // TODO: Add insert logic here
                return Json(new {success = true});
            }
            catch
            {
                return Json(new { success = false });
            }
        }

        //
        // GET: /Group/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Group/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, string newGroupName)
        {
            try
            {
                using (var uow = new RPCUnitOfWork())
                {
                    var groupToEdit = uow.GroupRepository.GetByID(id);
                    groupToEdit.GroupName = newGroupName;
                    uow.GroupRepository.Update(groupToEdit);
                    uow.Save();
                }
                // TODO: Add update logic here

                return Json(new {success = true});
            }
            catch
            {
                return Json(new { success = false });
            }
        }

        //
        // GET: /Group/Delete/5

        /*public ActionResult Delete(int id)
        {
            return View();
        }*/

        //
        // POST: /Group/Delete/5

        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                using (var uow = new RPCUnitOfWork())
                {
                    uow.GroupRepository.Delete(id);
                    uow.Save();
                }
                // TODO: Add delete logic here

                return Json(new { success = true });
            }
            catch
            {
                return Json(new { success = false });
            }
        }

        [HttpPost]
        public ActionResult DeleteClientFromGroup(int groupId, int clientId)
        {
            using (var uow = new RPCUnitOfWork())
            {
                var group = uow.GroupRepository.GetByID(groupId);
                var client = uow.ClientRepository.GetByID(clientId);
                group.Clients.Remove(client);
                uow.Save();
            }
            return Json(new {success = true});
        }

        [HttpPost]
        public ActionResult MoveClientToGroup(int clientId, int groupId, int targetGroupId)
        {
            try
            {
                using (var uow = new RPCUnitOfWork())
                {
                    var client = uow.ClientRepository.GetByID(clientId);
                    if (groupId != AdminDataHelper.GetAdminGroupAllId())
                    {
                        var groupToRemoveClientFrom = uow.GroupRepository.GetByID(groupId);
                        groupToRemoveClientFrom.Clients.Remove(client);
                    }
                    var group = uow.GroupRepository.GetByID(targetGroupId);
                    group.Clients.Add(client);
                    uow.Save();
                }
                return Json(new { success = true });
            }
            catch (Exception)
            {
                return Json(new { success = false });
            }
        }

        [HttpPost]
        public ActionResult GetGroupList(int clientId)
        {
            using (var uow = new RPCUnitOfWork())
            {
                var transferGroups = new List<Models.TransferGroup>();
                var adminAllGroupId = AdminDataHelper.GetAdminGroupAllId();
                var groups = uow.GroupRepository.Get(gr => gr.UserId == WebSecurity.CurrentUserId && gr.GroupId != adminAllGroupId);
                foreach (var @group in groups)
                {
                    var client = @group.Clients.SingleOrDefault(cl => cl.ClientId == clientId);
                    var transferGroup = TransferGroup.CreateFromEntity(group);
                    if (client != null) transferGroup.IsClientInGroup = true;
                    transferGroups.Add(transferGroup);
                }
                return Json(data: transferGroups);
            }
        }

        [HttpPost]
        public ActionResult MoveClientToGroups(FormCollection fc)
        {
            var clientId = Int32.Parse(fc["clientId"]);
            using (var uow = new RPCUnitOfWork())
            {
                var adminAllGroupId = AdminDataHelper.GetAdminGroupAllId();
                var groups = uow.GroupRepository.Get(gr => gr.UserId == WebSecurity.CurrentUserId && gr.GroupId != adminAllGroupId);
                foreach (var @group in groups)
                {
                    var client = @group.Clients.SingleOrDefault(cl => cl.ClientId == clientId);
                    var wasClientInGroup = client != null;
                    var isClientInGroup = Boolean.Parse(fc[@group.GroupId.ToString()]);
                    if (wasClientInGroup == isClientInGroup) continue;
                    if (wasClientInGroup && !isClientInGroup)
                    {
                        @group.Clients.Remove(client);
                    }
                    if (!wasClientInGroup && isClientInGroup)
                    {
                        var clientToAdd = uow.ClientRepository.GetByID(clientId);
                        @group.Clients.Add(clientToAdd);
                    }
                    uow.Save();
                }
            }
            return Json(new {success = true});
        }
    }
}
