﻿using System;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using Common.Ciphers;
using Common.CustomUrlCoder;
using Common.Logger;
using Common.Logger.Impl;
using RemotePowerControllerTestCore;
using RemotePowerControllerTestCore.Entities;
using RemotePowerControllerTestCore.Repositories.Interfaces;
using WebMatrix.WebData;

namespace RPCSite.Controllers
{
    public class HomeController : Controller
    { 
        private IGenericRepository<User> _userRepository;
        private IGenericRepository<DomainAuthAttempt> _domainAuthAttemptRepo;
        private ILogger _logger;

        public HomeController(IGenericRepository<User> userRepository, IGenericRepository<DomainAuthAttempt> domainAuthAttemptRepo)
        {
            _userRepository = userRepository;
            _domainAuthAttemptRepo = domainAuthAttemptRepo;
            _logger = LoggerFactory.Current.GetLogger<HomeController>();
        }
        //
        // GET: /Home/
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult About()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Models.User model)
        {
            var isModelValid = ModelState.IsValid;
            var isDomainAuth = Models.User.IsDomainAuth(model);
            if (isModelValid && isDomainAuth)
            {
                _domainAuthAttemptRepo.Insert(Models.User.CreateDomainAuthAttempt(model));
                var domainAuthAttemptEntity = _domainAuthAttemptRepo.Get(daa => daa.Username == model.Username).First();
                var i = 0;
                while ((i++)<10)
                {
                    Thread.Sleep(1000);
                    domainAuthAttemptEntity = _domainAuthAttemptRepo.GetByID(domainAuthAttemptEntity.DomainAuthAttemptId);
                    if (!domainAuthAttemptEntity.IsAuth.HasValue) continue;
                    if (domainAuthAttemptEntity.IsAuth == false)
                    {
                        _domainAuthAttemptRepo.Delete(domainAuthAttemptEntity);
                        return Json(new { success = false });
                    }
                    if (IsLogIn(model))
                    {
                        _domainAuthAttemptRepo.Delete(domainAuthAttemptEntity);
                        _logger.Info(String.Format("\tUser: {0}\t has loged in", model.Username));
                        return Json(new { success = true });
                    }                        
                }
                _domainAuthAttemptRepo.Delete(domainAuthAttemptEntity);
                return Json(new { success = false });
            }
            var isUserValid = Models.User.IsValid(model); 
            if (isUserValid && isModelValid)
            {
                if (IsLogIn(model))
                {
                    _logger.Info(String.Format("\tUser: {0}\t has loged in", model.Username));
                    return Json(new {success = true});
                }
            }
            ViewBag.Error = "The user name or password provided is incorrect.";
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return Json(new {success=false});
        }

        public ActionResult LogOut()
        {
            _logger.Info(String.Format("\tUser:  v {0}\t{1}\t has loged out", WebSecurity.CurrentUserId, WebSecurity.CurrentUserName));
            WebSecurity.Logout();
            return RedirectToAction("Index", "Home");
        }

        private bool IsLogIn(Models.User model)
        {
            var userEntity = _userRepository.Get(u => u.Username == model.Username).First();
            if (!WebSecurity.IsConfirmed(userEntity.Username))
                WebSecurity.CreateAccount(userEntity.Username, userEntity.Password, false);
            return WebSecurity.Login(userEntity.Username, userEntity.Password, persistCookie: model.RememberMe);
        }

        public ActionResult TurnOnAndRD(int clientId, string param1, string param2)
        {
            var decodedUserName = CustomUrlCoder.Decode(param1);
            var userName = StringCipher.Decrypt(decodedUserName, Common.Keys.Passwords.UserNameCipherKey);
            var decodedUserPassword = CustomUrlCoder.Decode(param2);
            var userPass = StringCipher.Decrypt(decodedUserPassword, Common.Keys.Passwords.PasswordCipherKey);
            var userModel = new RPCSite.Models.User {Username = userName, Password = userPass};
            var isModelValid = Models.User.IsValid(userModel);
            if(isModelValid) IsLogIn(userModel); //throw exception if isLogin false
            using (var uow = new RPCUnitOfWork())
            {
                var client = uow.ClientRepository.GetByID(clientId);
                if (client != null)
                {
                    ViewBag.Name = client.NetworkName;
                }
            }
            ViewBag.Id = clientId;
            return View();
        }
    }
}
