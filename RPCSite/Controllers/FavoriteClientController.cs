﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RemotePowerControllerTestCore;
using RemotePowerControllerTestCore.Entities;
using RemotePowerControllerTestCore.Repositories.Interfaces;
using RPCSite.Models;
using WebMatrix.WebData;
using Client = RemotePowerControllerTestCore.Entities.Client;
using User = RemotePowerControllerTestCore.Entities.User;

namespace RPCSite.Controllers
{
    public class FavoriteClientController : Controller
    {
        private IGenericRepository<User> _userRepository;
        private IGenericRepository<Client> _clientRepository;
        private IGenericRepository<ClientState> _clientStateRepository;
        private IGenericRepository<ClientStateAudit> _clientStateAuditRepository;
        private IGenericRepository<Favorite> _favoriteRepository;

        public FavoriteClientController(IGenericRepository<User> userRepository, IGenericRepository<Client> clientRepository, 
            IGenericRepository<ClientState> clientStateRepository, IGenericRepository<Favorite> favoriteRepository, IGenericRepository<ClientStateAudit> clientStateAuditRepository)
        {
            _userRepository = userRepository;
            _clientRepository = clientRepository;
            _clientStateRepository = clientStateRepository;
            _favoriteRepository = favoriteRepository;
            _clientStateAuditRepository = clientStateAuditRepository;
        }

        //
        // GET: /FavoriteClient/
        [Authorize]
        public ActionResult Index(string query)
        {
            var favoriteClientIds = _favoriteRepository.Get(f => f.UserId == WebSecurity.CurrentUserId).ToList();
            var favoriteClients = new List<Client>();
            foreach (var favoriteClientId in favoriteClientIds)
            {
                var favoriteClient = _clientRepository.GetByID(favoriteClientId.ClientId);
                favoriteClients.Add(favoriteClient);
            }
            var result = new List<Models.FavoriteClient>();
            if (String.IsNullOrWhiteSpace(query))
                foreach (var favoriteClient in favoriteClients)
                {
                    var favoriteClientOwner = _userRepository.GetByID(favoriteClient.UserId);
                    var favoriteClientState = _clientStateRepository.GetByID(favoriteClient.ClientId);
                    var favoriteClientStateAudit =  _clientStateAuditRepository.Get(csa => csa.ClientId == favoriteClient.ClientId).ToList();
                    result.Add(FavoriteClient.CreateFromEntity(favoriteClient, favoriteClientOwner, favoriteClientState, favoriteClientStateAudit));
                }
            else
            {
                foreach (var favoriteClient in favoriteClients)
                {
                    var favoriteClientOwner = _userRepository.GetByID(favoriteClient.UserId);
                    var isSearchedClientName = favoriteClient.NetworkName.IndexOf(query, 0, StringComparison.InvariantCultureIgnoreCase) >= 0;
                    var isSearchedClientOwner = favoriteClientOwner.Username.IndexOf(query, 0, StringComparison.InvariantCultureIgnoreCase) >= 0;
                    if (!isSearchedClientName && !isSearchedClientOwner) continue;
                    var favoriteClientState = _clientStateRepository.GetByID(favoriteClient.ClientId);
                    var favoriteClientStateAudit = _clientStateAuditRepository.Get(csa => csa.ClientId == favoriteClient.ClientId).ToList();
                    result.Add(FavoriteClient.CreateFromEntity(favoriteClient, favoriteClientOwner, favoriteClientState, favoriteClientStateAudit));
                }
            }

            return View(result);
        }

        //
        // GET: /FavoriteClient/Details/5

        /*public ActionResult Details(int id)
        {
            return View();
        }*/

        //
        // GET: /FavoriteClient/Create
        [Authorize]
        public ActionResult Add(string query)
        {
            var notCurrentUserClients = _clientRepository.Get(c => c.UserId != WebSecurity.CurrentUserId).ToList();
            var currentUserFavoriteClientIds = _favoriteRepository.Get(f => f.UserId == WebSecurity.CurrentUserId).ToList();
            var result = new List<Models.NotUserClient>();
            if (String.IsNullOrWhiteSpace(query))
                foreach (var notCurrentUserClient in notCurrentUserClients)
                {
                    var notUserClientOwner = _userRepository.GetByID(notCurrentUserClient.UserId);
                    var notUserClientStateAudit = _clientStateAuditRepository.Get(csa=>csa.ClientId == notCurrentUserClient.ClientId).ToList();
                    var isFavorite = false;
                    foreach (var currentUserFavoriteClientId in currentUserFavoriteClientIds)
                        if (notCurrentUserClient.ClientId == currentUserFavoriteClientId.ClientId) isFavorite = true;
                    result.Add(NotUserClient.ConvertFromEntity(notCurrentUserClient, notUserClientOwner, isFavorite, notUserClientStateAudit));
                }
            else
                foreach (var notCurrentUserClient in notCurrentUserClients)
                {
                    var notUserClientOwner = _userRepository.GetByID(notCurrentUserClient.UserId);
                    var isSearchedClientName = notCurrentUserClient.NetworkName.IndexOf(query, 0, StringComparison.InvariantCultureIgnoreCase) >= 0;
                    var isSearchedClientOwner = notUserClientOwner.Username.IndexOf(query, 0, StringComparison.InvariantCultureIgnoreCase) >= 0;
                    if(!isSearchedClientName && !isSearchedClientOwner) continue;
                    var notUserClientStateAudit = _clientStateAuditRepository.Get(csa => csa.ClientId == notCurrentUserClient.ClientId).ToList();
                    var isFavorite = false;
                    foreach (var currentUserFavoriteClientId in currentUserFavoriteClientIds)
                    {
                        if (notCurrentUserClient.ClientId == currentUserFavoriteClientId.ClientId) isFavorite = true;
                    }
                    result.Add(NotUserClient.ConvertFromEntity(notCurrentUserClient, notUserClientOwner, isFavorite, notUserClientStateAudit));
                }
            return View(result);
        }

        //
        // POST: /FavoriteClient/Create

        [HttpPost]
        public ActionResult Add(int id)
        {
            try
            {
                // TODO: Add insert logic here
                var favoritePair = new Favorite
                {
                    UserId = WebSecurity.CurrentUserId,
                    ClientId = id,
                };
                _favoriteRepository.Insert(favoritePair);
                return RedirectToAction("Add");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /FavoriteClient/Edit/5

        /*public ActionResult Edit(int id)
        {
            return View();
        }*/

        //
        // POST: /FavoriteClient/Edit/5

        /*[HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }*/

        //
        // GET: /FavoriteClient/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                var favoritePair = new Favorite
                {
                    UserId = WebSecurity.CurrentUserId,
                    ClientId = id,
                };
                _favoriteRepository.Delete(favoritePair);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /FavoriteClient/Delete/5

        //[HttpPost]
        /*public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                var favoritePair = new Favorite
                {
                    UserId = WebSecurity.CurrentUserId,
                    ClientId = id,
                };
                _favoriteRepository.Delete(favoritePair);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }*/
    }
}
