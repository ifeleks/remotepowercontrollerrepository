﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web.Mvc;
using System.Web.Security;
using Common.DTO.Enums;
using Common.Logger;
using Common.Logger.Impl;
using Microsoft.Ajax.Utilities;
using RemotePowerControllerTestCore;
using RemotePowerControllerTestCore.Entities;
using RemotePowerControllerTestCore.Helpers;
using RemotePowerControllerTestCore.Repositories.Interfaces;
using RPCSite.Models;
using RPCSite.RDP;
using WebGrease.Css.Extensions;
using WebMatrix.WebData;
using Group = RemotePowerControllerTestCore.Entities.Group;
using Client = RemotePowerControllerTestCore.Entities.Client;
using User = RemotePowerControllerTestCore.Entities.User;

namespace RPCSite.Controllers
{
    public class ClientController : Controller
    {
        private readonly IGenericRepository<Client> _clientRepository;
        private readonly IGenericRepository<ClientState> _clientStateRepository;
        private readonly IGenericRepository<ClientSettings> _clienSettingsRepository;
        private readonly IGenericRepository<ClientStateAudit> _clientStateAuditRepo;
        private readonly IGenericRepository<User> _userRepository;
        private readonly IGenericRepository<Server> _serverRepository;
        private readonly IGenericRepository<CommandAudit> _commandAuditRepository;
        private readonly IGenericRepository<Group> _groupRepository;
        private readonly ILogger _logger;

        public ClientController(
            IGenericRepository<Client> clientRepository, 
            IGenericRepository<ClientState> clientStateRepository, 
            IGenericRepository<ClientSettings> clientSettingsRepository, 
            IGenericRepository<ClientStateAudit> clientStateAuditRepo, 
            IGenericRepository<User> userRepository,
            IGenericRepository<Server> serveRepository, 
            IGenericRepository<CommandAudit> commandAuditRepository,
            IGenericRepository<Group> groupRepository)
        {
            _clientRepository = clientRepository;
            _clientStateRepository = clientStateRepository;
            _clienSettingsRepository = clientSettingsRepository;
            _clientStateAuditRepo = clientStateAuditRepo;
            _userRepository = userRepository;
            _serverRepository = serveRepository;
            _commandAuditRepository = commandAuditRepository;
            _groupRepository = groupRepository;
            _logger = LoggerFactory.Current.GetLogger<ClientController>();
        }

        //
        // GET: /Client/
        [Authorize]
        public ActionResult Index(string query)
        {
            var clientModels = new List<Models.Client>();
            using (var uow = new RPCUnitOfWork())
            {
                IEnumerable<Client> clients;
                clients = Roles.IsUserInRole(AdminDataHelper.GetAdminRoleName) ? uow.ClientRepository.Get(orderBy:o=>o.OrderByDescending(c=>c.NetworkName)) :
                    uow.ClientRepository.Get(c => c.UserId == WebSecurity.CurrentUserId, orderBy: o => o.OrderByDescending(c => c.NetworkName));
                foreach (var client in clients)
                {
                    var clientOwner = uow.UserRepository.GetByID(client.UserId);
                    var clientDetailsEntities = uow.ClientStateAuditRepository.Get(csa => csa.ClientId == client.ClientId).ToList();
                    var clientModel = Models.Client.CreateFromEntity(clientOwner, client, clientDetailsEntities);
                    clientModels.Add(clientModel);
                }
            }
            return View(clientModels);
        }

        [HttpPost]
        public ActionResult CheckServerActivity()
        {
            var servers = _serverRepository.Get().ToList();
            foreach (var server in servers)
            {
                var timeDiff = DateTime.UtcNow - server.LastActivity;
                var totalSeconds = timeDiff.TotalSeconds;
                if (totalSeconds >= 5)
                {
                    var clients = _clientRepository.Get(c => c.ServerId == server.ServerId).ToList();
                    foreach (var client in clients)
                    {
                        var clientState = _clientStateRepository.GetByID(client.ClientId);
                        if (clientState != null && clientState.CurrentState != State.ServerInaccessible)
                        {
                            clientState.CurrentState = State.ServerInaccessible;
                            _clientStateRepository.Update(clientState);
                        }
                    }
                }
            }
            return Json(new {success = true});
        }

        [HttpPost]
        public ActionResult SwitchCommand(int id, string command)
        {
            Command enumCommand;
            switch (command)
            {
                case "turn_on":
                    enumCommand = Command.TurnOn;
                    break;
                case "shut_down":
                    enumCommand = Command.TurnOff;
                    break;
                case "sleep":
                    enumCommand = Command.GoToSleep;
                    break;
                default:
                    enumCommand = Command.DoNothing;
                    break;
            };
            
            var clientStateToSwitchCommand = _clientStateRepository.GetByID(id);
            clientStateToSwitchCommand.Command = enumCommand;
            _clientStateRepository.Update(clientStateToSwitchCommand);
            string userIP = Request.UserHostAddress;
            var commandAudit = new CommandAudit
            {
                DateTime = DateTime.UtcNow,
                ClientId = id,
                Command = enumCommand,
                UserId = WebSecurity.CurrentUserId,
                UserIP = userIP
            };
            _commandAuditRepository.Insert(commandAudit);
            _logger.Info(String.Format("\tUser: {0}\t{1}\t send command: {2}\t to client:{3}", 
                WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, command, id ));
            return Json(new {success = true});
        }

        [HttpPost]
        public ActionResult CheckStates()
        {
            var clientStates = _clientStateRepository.Get().ToList();
            var result = new List<Models.Client>();
            foreach (var clientState in clientStates)
            {

                result.Add(Models.Client.CreateFromClientStateEntity(clientState));
            }
            return Json(data: result);
        }

        //
        // GET: /Client/Details/5
        [Authorize]
        public ActionResult Details(int id, string sortOrder)
        {
            var client = _clientRepository.GetByID(id);
            var user = _userRepository.GetByID(client.UserId);
            var clientStateAuditEntities = _clientStateAuditRepo.Get(csa => csa.ClientId == id, orderBy:q=>q.OrderByDescending(csa=>csa.DateTime)).ToList();
            var result = new List<ClientDetails>();
            foreach (var clientStateAuditEntity in clientStateAuditEntities)
            {
                result.Add(ClientDetails.ConvertFromEntity(user.Username, client, clientStateAuditEntity));
            }

            var stats = new Stats.StatHandler(result);
            result[0].LongestWorkTime = stats.GetLongestWorkTime();
            result[0].TotalTrackingTime = stats.GetTotalTrackingTime();
            result[0].TotalWorkTime = stats.GetTotalWorkTime();
            
            /*ViewBag.DateTime = String.IsNullOrEmpty(sortOrder) ? "DateTime_desc" : "";
            ViewBag.State = sortOrder == "State" ? "State_desc" : "State";
            switch (sortOrder)
            {
                case "DateTime_desc":
                    result = result.OrderBy(csa => csa.DateTime).ToList();
                    break;
                case "State_desc":
                    result = result.OrderByDescending(csa => csa.State).ThenByDescending(csa => csa.DateTime).ToList();
                    break;
                case "State":
                    result = result.OrderBy(csa => csa.State).ThenByDescending(csa => csa.DateTime).ToList();
                    break;
                default:
                    result = result.OrderByDescending(csa => csa.DateTime).ToList();
                    break;
            }*/
            return View(result);
        }

        //
        // GET: /Client/Create

        /*public ActionResult Create()
        {
            return View();
        }*/

        //
        // POST: /Client/Create

        /*[HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }*/

        //
        // GET: /Client/Edit/5

        /*public ActionResult Edit(int id)
        {
            return View();
        }*/

        //
        // POST: /Client/Edit/5

        /*[HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }*/

        //
        // GET: /Client/Delete/5

        /*public ActionResult Delete(int id)
        {
            var clientEntity = _clientRepository.Get(c => c.ClientId == id, includeProperties: "ClientState").FirstOrDefault();
            var clientModel = Models.Client.CreateFromEntity(clientEntity);
            return View(clientModel);
        }*/

        //
        // POST: /Client/Delete/5

        [HttpPost]
        public ActionResult Delete(int clientId)
        {
            try
            {
                using (var uow = new RPCUnitOfWork())
                {
                    var clientToRemove = uow.ClientRepository.GetByID(clientId);
                    uow.ClientSettingsRepository.Delete(clientId);
                    uow.ClientStateRepository.Delete(clientId);
                    var clientStateAuditsToRemove = uow.ClientStateAuditRepository.Get(csa => csa.ClientId == clientId);
                    foreach (var clientStateAuditToRemove in clientStateAuditsToRemove)
                    {
                        uow.ClientStateAuditRepository.Delete(clientStateAuditToRemove);    
                    }
                    var favoritesToRemove = uow.FavoriteRepository.Get(f => f.ClientId == clientId);
                    foreach (var favorite in favoritesToRemove)
                    {
                        uow.FavoriteRepository.Delete(favorite);
                    }
                    foreach (var clientToRemoveGroup in clientToRemove.Groups)
                    {
                        clientToRemoveGroup.Clients.Remove(clientToRemove);
                    }
                    uow.ClientRepository.Delete(clientId);
                    
                    uow.Save();
                    _logger.Info(String.Format("\tUser: {0}\t{1}\t delete client:{2}", WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, clientId));
                    return Json(new { success = true });
                }
            }
            catch
            {
                return Json(new { success = false });
            }
        }

        public ActionResult TurningOn(int clientId)
        {
            using (var uow = new RPCUnitOfWork())
            {
                var client = uow.ClientRepository.GetByID(clientId);
                if (client != null)
                {
                    ViewBag.Name = client.NetworkName;
                }
            }
            ViewBag.Id = clientId;
            return View();
        }

        [HttpPost]
        public ActionResult CheckClientState(int id)
        {
            var clientState = _clientStateRepository.GetByID(id);
            var clientModel = Models.Client.CreateFromClientStateEntity(clientState);
            return Json(clientModel);
        }

        [Authorize]
        public ActionResult DownloadRDP(int id)
        {
            var clientToConnect = _clientRepository.GetByID(id);
            var rdpBuilder = new RDPFileBuilder();
            if (clientToConnect.FullName == string.Empty)
            {
                //todo: message that not-domain client is only accesable from local network
                rdpBuilder.getRDPFileForClient(clientToConnect.ClientIP);
            }
            else
            {
                rdpBuilder.getRDPFileForClient(clientToConnect.FullName);
            }
            return new EmptyResult();
        }

        public ActionResult Search(string pcname, string[] states, string user, string server)
        {
            var networkClientModels = new List<Models.NetworkClient>();
            ViewBag.States = new MultiSelectList(Models.Client.GetStateEnumStringNames());
            using (var uow = new RPCUnitOfWork())
            {
                var clients = uow.ClientRepository.Get();
                if (!String.IsNullOrWhiteSpace(pcname)) clients = clients.Where(cl => cl.NetworkName.IndexOf(pcname, StringComparison.OrdinalIgnoreCase) >= 0);
                if (!String.IsNullOrWhiteSpace(user))
                {
                    var users = uow.UserRepository.Get();
                    var filteredUsers = users.Where(u => u.Username.IndexOf(user, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                    clients = from filteredUser in filteredUsers
                              from client in clients
                              where client.UserId == filteredUser.UserId
                              select client;
                }
                if (!String.IsNullOrWhiteSpace(server)) clients = clients.Where(cl => cl.Server.ServerName.IndexOf(server, StringComparison.OrdinalIgnoreCase) >= 0);
                var resultClients = new List<Client>();
                if(states!= null && states.Length > 0)
                    foreach (var state in states)
                    {
                        var enumState = Models.Client.GetStateFromString(state);
                        var tempClients = clients.Where(cl => cl.ClientState.CurrentState == enumState);
                        resultClients.AddRange(tempClients);
                    }
                else resultClients.AddRange(clients);
                foreach (var resultClient in resultClients)
                {
                    var clientOwner = uow.UserRepository.GetByID(resultClient.UserId);
                    var clientServer = uow.ServerRepository.GetByID(resultClient.ServerId);
                    var clientDetailsEntities = uow.ClientStateAuditRepository.Get(csa => csa.ClientId == resultClient.ClientId).ToList();
                    var networkClientModel = NetworkClient.CreateFromEntity(resultClient, clientOwner, clientServer, clientDetailsEntities);
                    if (Roles.IsUserInRole(AdminDataHelper.GetAdminRoleName)) networkClientModel.IsCurrentUserClient = true; // if user is admin IsCurrentUserClient always true
                    else if (resultClient.UserId == WebSecurity.CurrentUserId) networkClientModel.IsCurrentUserClient = true;
                    networkClientModels.Add(networkClientModel);
                }
            }
            return View(networkClientModels);
        }
    }
}
