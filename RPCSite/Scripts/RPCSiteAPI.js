﻿// Group handling section
var isGroupNameValid = function (groupName) {
    var errorText = "Group name cannot be {0}", isValid = true;
    var invalidNames = ["All", "Favorites", ""];
    for (var i = 0; i < invalidNames.length; i++) {
        var placeholder = invalidNames[i];
        if (groupName.toUpperCase() !== placeholder.toUpperCase()) continue;
        isValid = false;
        if (invalidNames[i] == "") placeholder = "empty";
        errorText = errorText.replace("{0}", placeholder);
        break;
    }
    return { "isValid": isValid, "errorText": errorText };
};

var addCreateGroupButtonToPanel = function (selector) {
    var firstGroupPanel = $(selector).first();
    var firstGroupPanelId = firstGroupPanel.attr("id").split("-")[1];
    //todo: another way to store buttons id
    $("#btnCreateGroup-" + firstGroupPanelId).show();
    $("#btnEditGroup-" + firstGroupPanelId).hide();
    $("#btnDeleteGroup-" + firstGroupPanelId).hide();
};

var groupCollapseHandler = function (triggerSelector, groupCollapsedPartSelector) {
    $(triggerSelector).on("click", function () {
        var groupId = $(this).attr("data-target").split("-")[1];
        var groupCollapsedPartId = groupCollapsedPartSelector + groupId;
        var isCollapsed = $(groupCollapsedPartId).hasClass("in") ? true : false;
        if (isCollapsed) {
            $(this).find("span.glyphicon").removeClass("glyphicon-chevron-down");
            $(this).find("span.glyphicon").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-right");
        } else {
            $(this).find("span.glyphicon").removeClass("glyphicon-chevron-right");
            $(this).find("span.glyphicon").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-down");
        }
        localStorage.setItem(groupCollapsedPartId, isCollapsed);
    });
};

var restoreGroupCollapsedPartStates = function (groupSelector, groupCollapsedPartSelector, groupPanelSelector, triggerSelector) {
    $(groupSelector).each(function () {
        var arrInfo = $(this).attr("id").split("-");
        var groupId = arrInfo[1];
        var groupCollapsedPartId = groupCollapsedPartSelector + groupId;
        var groupPanelId = groupPanelSelector + groupId;
        if (localStorage.getItem(groupCollapsedPartId) == "true") {
            $(groupPanelId).find(triggerSelector + " > span > span.glyphicon").removeClass("glyphicon-chevron-down");            
            $(groupPanelId).find(triggerSelector + " > span > span.glyphicon").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-right");
            $(groupCollapsedPartId).removeClass("in");
        }
    });
};

var createEditGroupModalDefaultState = function (modalSelector) {
    $(modalSelector).on('show.bs.modal', function () {
        $(this).find("label.error-label").text("");
        $(this).find("div.form-group").removeClass("has-error");
        $(this).one("shown.bs.modal", function () {
            $(this).find('input:first').focus();
        });
    });
};

var modalShow = function (actionLinkSelector, modalSelector) {
    $(actionLinkSelector).on("click", function () {
        $(modalSelector).modal("show");
    });
};
//

//Client handling section
var clientCollapseHandler = function () {
    $("a.client-collapse-trigger").on("click", function () {
        var clientCollapsedPartId = $(this).attr("data-target");
        var isCollapsed = $(clientCollapsedPartId).hasClass("in") ? true : false;
        localStorage.setItem(clientCollapsedPartId, isCollapsed);
    });
};

var restoreClientCollapsedPartStates = function () {
    $("div.client-panel-collapse").each(function () {
        if (localStorage.getItem("#" + $(this).attr("id")) == "true") {
            $(this).removeClass("in");
        }
    });
};

var setAddClientToNetworkGroupModaltToDefaultState = function (addClientToGroupModalSelector) {
    $(addClientToGroupModalSelector).find("div.modal-body > p").text("Add client {0} to group {1}?");
};

var commandHandler = function (url) {
    $("button.command").on("click", function () {
        console.log("clicked");
        var clickedButtonId = "#" + $(this).attr("id");
        var arrInfo = $(this).attr("id").split("-");
        var itemId = arrInfo[2];
        var command = arrInfo[1];
        if (command == "shut_down") {
            $("#shutDownConfiramtion").modal("show");
            $("#btnShutDownClient").on("click", function () {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: { 'id': itemId, 'command': command },
                    datatype: "json",
                    success: function (data) {
                        if (data.success) {
                            $("#shutDownConfiramtion").modal("hide");
                            $(clickedButtonId).attr("disabled", "disabled");
                        }
                    }
                });
                $("#btnShutDownClient").off("click");
            });
        } else {
            $(this).attr("disabled", "disabled");
            $.ajax({
                url: url,
                type: "POST",
                data: { 'id': itemId, 'command': command },
                datatype: "json",
                success: function (data) {
                }
            });
        }
        $("#shutDownConfiramtion").on("hide.bs.modal", function () {
            console.log("hide");
            $("#btnShutDownClient").off("click");
            $("#shutDownConfiramtion").off("hide.bs.modal");
        });
    });
};

var moveClientToGroups = function(url) {
    $("a.move-client").on("click", function() {
        var clientId = $(this).attr("id").split("-")[1];
        $("#groupListModal").find("div.modal-body").html('<p class="' + clientId + '"></p>');
        $.ajax({
            url: url,
            type: "post",
        data: { "clientId": clientId },
        datatype: "json",
        success: function(data) {
            $.each(data, function(i, obj) {
                if (obj.IsClientInGroup == true) {
                    $("#groupListModal").find("div.modal-body > p").append('<div class="checkbox"><label><input name="' + obj.TransferGroupId + '" type="checkbox" value="" checked>' + obj.Name + '</label></div>');
                } else {
                    $("#groupListModal").find("div.modal-body > p").append('<div class="checkbox"><label><input name="' + obj.TransferGroupId + '" type="checkbox" value="">' + obj.Name + '</label></div>');
                }
                $("#groupListModal").modal("show");
            });
        }
    });
});
};

var moveClientToGroupsHadler = function (url) {
    $("#btnSaveClientsInGroups").on("click", function () {
        var clientId = $("#groupListModal").find("div.modal-body > p").attr("class");
        var arrayToSubmit = [];
        arrayToSubmit.push({ name: "clientId", value: clientId });
        $("#groupListModal").find("input[type=checkbox]").each(function () {
            var isChecked = $(this).is(':checked');
            if (isChecked) {
                $(this).val(true);
            } else {
                $(this).val(false);
            }
            arrayToSubmit.push({ name: this.name, value: $(this).val() });
        });
        var serializedArr = $.param(arrayToSubmit);
        $.ajax({
            url: url,
            type: "post",
            data: serializedArr,
            success: function() {
                location.reload();
            }
        });
        $("#groupListModal").modal("hide");
    });
};
//

//Learn how
var doSomething = function (event) {
    console.log(event.data.name);
};
//write Karl
var testFunction = function () {
    $("a.create-group").on("click", { name: "Karl" }, doSomething);
};
//