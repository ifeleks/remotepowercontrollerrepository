﻿using System;
using System.IO;
using System.Text;
using System.Web;

namespace RPCSite.RDP
{
    public class RDPFileBuilder
    {
        private const string FileName = "Default.rdp";
        private HttpContext context = HttpContext.Current;

        public string getRDPFileForClient(string fullAdress)
        {
            string filePath = context.Server.MapPath("~/Downloads/" + FileName);

            UpdateRDPFile(fullAdress, filePath);

            DownLoadFile(fullAdress, filePath);

            return filePath;
        }

        private static void UpdateRDPFile(string fullAdress, string filePath)
        {
            var newFile = new StringBuilder();
            var temp = String.Empty;
            var fileToUpdate = File.ReadAllLines(filePath);
            foreach (var line in fileToUpdate)
            {
                temp = line;

                if (line.Contains("full address:s:"))
                {
                    temp = String.Empty;
                    temp = temp.Insert(0, "full address:s:" + fullAdress);
                }

                if (fullAdress.Contains(".0.")) //todo: if parsed to IP
                {
                    if (line.Contains("gatewayhostname:s:tsg.eleks.com"))
                    {
                        temp = String.Empty;
                        temp = temp.Insert(0, "gatewayhostname:s:");
                        //temp = line.Replace("gatewayhostname:s:tsg.eleks.com", "gatewayhostname:s:");
                    }
                }
                else
                {
                    if (line.Contains("gatewayhostname:s:"))
                    {
                        temp = String.Empty;
                        temp = temp.Insert(0, "gatewayhostname:s:tsg.eleks.com");
                        //temp = line.Replace("gatewayhostname:s:", "gatewayhostname:s:tsg.eleks.com");
                    }
                }

                newFile.Append(temp + "\r\n");
            }
            File.WriteAllText(filePath, newFile.ToString());
        }

        private void DownLoadFile(string fullAdress, string filePath)
        {
            var file = new FileInfo(filePath);
            // -- if the file exists on the server
            if (file.Exists)
            {
                // set appropriate headers
                context.Response.ClearHeaders();
                context.Response.ClearContent();
                context.Response.Clear();
                context.Response.ContentType = "application/rdp";
                context.Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}.rdp", fullAdress));
                //context.Response.AppendHeader("Content-Length", file.Length.ToString());
                context.Response.WriteFile(file.FullName);
                context.Response.Flush();
                context.Response.End();
                // if file does not exist
            }
            else
            {
                HttpContext.Current.Response.Write("This file does not exist.");
            }
        }
    }
}