﻿using System;
using System.Collections.Generic;
using System.Linq;
using RemotePowerControllerTestCore.Helpers;
using RPCSite.Models;

namespace RPCSite.Stats
{
    public class StatHandler
    {
        private TimeSpan _totalTrackingTime = TimeSpan.Zero;
        private TimeSpan _totalWorkTime = TimeSpan.Zero;
        private TimeSpan _longestWorkTime = TimeSpan.Zero;
        private decimal _workPercentage;
        private List<TimeSpan> _workingSessions = new List<TimeSpan>();

        public StatHandler(List<ClientDetails> details)
        {
            if (details.Count == 0) return;
            var currentTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneHelper.CurrentTimeZone);
            details = details.OrderBy(d => d.DateTime).ToList();
            _totalTrackingTime = currentTime.Subtract(details.First().DateTime);

            var startSessionTime = new DateTime();
            var endSessionTime = new DateTime();
            var startFounded = false;
            foreach (var clientDetails in details)
            {
                if ((clientDetails.State == "On" || clientDetails.State == "On (Client is not running)")
                    && !startFounded)
                {
                    startFounded = true;
                    startSessionTime = clientDetails.DateTime;
                } else if (clientDetails.State == "Off")
                {
                    if (startFounded)
                    {
                        startFounded = false;
                        endSessionTime = clientDetails.DateTime;
                        _workingSessions.Add(endSessionTime.Subtract(startSessionTime));
                    }
                }
            }
            if (startSessionTime > endSessionTime) //means that current session is up
            {
                endSessionTime = currentTime;
                _workingSessions.Add(endSessionTime.Subtract(startSessionTime));
            }

            _totalWorkTime = new TimeSpan();
            _totalWorkTime = _workingSessions.Aggregate(_totalWorkTime,
                (current, workingSession) => current.Add(workingSession));

            _longestWorkTime = _workingSessions.Count != 0 ? _workingSessions.Max() : new TimeSpan();

            var totalTicks = _totalTrackingTime.Ticks;
            var workTicks = _totalWorkTime.Ticks;
            var workPercent = _totalWorkTime > TimeSpan.Zero ? ((double) workTicks/(double) totalTicks)*100 : 0;
            _workPercentage = Convert.ToDecimal(workPercent);
        }

        public TimeSpan GetTotalTrackingTime()
        {
            return _totalTrackingTime;
        }

        public TimeSpan GetTotalWorkTime()
        {
            return _totalWorkTime;
        }

        public TimeSpan GetLongestWorkTime()
        {
            return _longestWorkTime;
        }

        public decimal GetWorkPercentage()
        {
            return _workPercentage;
        }

        public int GetTotalSwitches()
        {
            return _workingSessions.Count*2;
        }
    }
}