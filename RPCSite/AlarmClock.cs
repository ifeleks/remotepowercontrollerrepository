﻿using System;
using System.Linq;
using System.Timers;
using Common.DTO;
using Common.DTO.Enums;
using RemotePowerControllerTestCore.Entities;
using RemotePowerControllerTestCore.Repositories.Implementations;
using Web.Services;

namespace RPCSite
{
    public class AlarmClock
    {
        private TimeSpan timeZoneDiff;

        public AlarmClock(int checkFrequency)
        {
            timeZoneDiff = new TimeSpan(10, 3, 0); //Diff between somee.com and Kyiv

            var turnOnTimer = new Timer(checkFrequency);
            turnOnTimer.AutoReset = true;
            turnOnTimer.Elapsed += AlarmClockTick;
            turnOnTimer.Enabled = true;

            turnOnTimer.Start();
        }

        private void AlarmClockTick(object sender, EventArgs e)
        {
            var settingsService = new SettingsService();
            var clientService = new ClientInfoService();
            var clients = clientService.GetAllServerClients("all");
            foreach (var client in clients)
            {
                var currentClientSettings = settingsService.GetClientSettings(client);
                var acceptableDiff = new TimeSpan(0, 1, 30);
                if (IsItTimeToWakeUp(client, currentClientSettings.WorkTimeStart, acceptableDiff))
                {
                    SetTurnOnCommand(client);
                }
            }            
        }

        private bool IsItTimeToWakeUp(ClientInfoTO client, TimeSpan clientTurnOnTime, TimeSpan acceptableDiff)
        {
            if (client.Command == Command.TurnOn || client.State != State.Off) return false;
            var serverTurnOnTime = clientTurnOnTime.Add(timeZoneDiff);
            var timeDiff = DateTime.Now.TimeOfDay.Subtract(serverTurnOnTime);
            return timeDiff < acceptableDiff;
        }

        private void SetTurnOnCommand(ClientInfoTO client)
        {
            var clientRepo = new GenericRepository<Client>();
            var clientToTurnOn = clientRepo.Get(c => c.MACAddress == client.MacAddress).First();
            var _clientStateRepository = new GenericRepository<ClientState>();
            var clientStateToSwitchCommand = _clientStateRepository.GetByID(clientToTurnOn.ClientId);
            clientStateToSwitchCommand.Command = Command.TurnOn;
            _clientStateRepository.Update(clientStateToSwitchCommand);
        }
    }
}
