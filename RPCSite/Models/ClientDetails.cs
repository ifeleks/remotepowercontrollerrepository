﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using RemotePowerControllerTestCore;
using RemotePowerControllerTestCore.Entities;
using RemotePowerControllerTestCore.Helpers;
using RPCSite.Stats;

namespace RPCSite.Models
{
    public class ClientDetails
    {
        public string Username { get; set; }

        public string Networkname { get; set; }

        [DisplayFormat(DataFormatString = "{0: d MMM yyyy HH:mm:ss}")]
        public DateTime DateTime { get; set; }

        public string State { get; set; }

        public TimeSpan TotalTrackingTime { get; set; }

        public TimeSpan TotalWorkTime { get; set; }

        public TimeSpan LongestWorkTime { get; set; }
        

        public static ClientDetails ConvertFromEntity(string username, RemotePowerControllerTestCore.Entities.Client clientEntity, ClientStateAudit clientStateAuditEntity)
        {
            return new ClientDetails
            {
                Username = username,
                Networkname = clientEntity.NetworkName,
                DateTime = TimeZoneInfo.ConvertTimeFromUtc(clientStateAuditEntity.DateTime, TimeZoneHelper.CurrentTimeZone),
                State = Models.Client.GetStringCurrentState(clientStateAuditEntity.State)
            };
        }

        public static ClientDetails ConvertFromEntity(ClientStateAudit clientStateAuditEntity)
        {
            return new ClientDetails
            {
                DateTime = TimeZoneInfo.ConvertTimeFromUtc(clientStateAuditEntity.DateTime, TimeZoneHelper.CurrentTimeZone),
                State = Models.Client.GetStringCurrentState(clientStateAuditEntity.State)
            };
        }

        public static string GetWorkTimePercentage(List<RemotePowerControllerTestCore.Entities.ClientStateAudit> clientDetailsEntities)
        {
            var clientDetailsModel = new List<ClientDetails>();
            foreach (var clientDetailsEntity in clientDetailsEntities)
            {
                clientDetailsModel.Add(ConvertFromEntity(clientDetailsEntity));
            }
            var workTimePercentage = 100 - (new StatHandler(clientDetailsModel)).GetWorkPercentage();
            return (workTimePercentage <= 1) ? string.Format("0{0}", workTimePercentage.ToString("#.##")) : workTimePercentage.ToString("#.##");
        }
    }
}