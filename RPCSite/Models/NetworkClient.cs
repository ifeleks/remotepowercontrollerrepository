﻿using System.Collections.Generic;
using RemotePowerControllerTestCore.Entities;

namespace RPCSite.Models
{
    public class NetworkClient
    {
        public int NetworkClientId { get; set; }

        public string NetworkName { get; set; }

        public string WorkTimePercentage { get; set; }

        public string Username { get; set; }

        public string Server { get; set; }

        public string TurnOnLink { get; set; }

        public string TurnOnAndRDLink { get; set; }

        public bool IsCurrentUserClient { get; set; }

        public static NetworkClient CreateFromEntity(RemotePowerControllerTestCore.Entities.Client client, RemotePowerControllerTestCore.Entities.User clientOwner, 
            Server server, List<ClientStateAudit> clientDetailsEntities)
        {
            return new NetworkClient
            {
                NetworkClientId = client.ClientId,
                NetworkName = client.NetworkName,
                WorkTimePercentage = ClientDetails.GetWorkTimePercentage(clientDetailsEntities),
                Username = clientOwner.Username,
                Server = server.ServerName,
                TurnOnLink = Client.GetTurnOnLink(client.ClientId, clientOwner.Username, clientOwner.Password),
                TurnOnAndRDLink = Client.GetTurnOnWithRDLink(client.ClientId, clientOwner.Username, clientOwner.Password),
            };
        }
    }
}