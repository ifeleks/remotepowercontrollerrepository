﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using Common.Ciphers;
using Common.CustomUrlCoder;
using Common.DTO.Enums;
using RPCSite.Stats;

namespace RPCSite.Models
{
    public class Client
    {
        public int ClientId { get; set; }

        [DisplayName("Networkname")]
        public string NetworkName { get; set; }

        [DisplayName("Current state")]
        public string CurrentState { get; set; }

        public string Command { get; set; }

        public string TurnOnLink { get; set; }

        public string TurnOnWithRDLink { get; set; }

        public string WorkTimePercentage { get; set; }

        public static string GetStringCurrentState(State state)
        {
            switch (state)
            {
                case State.InProcess:
                    return "Process...";
                case State.Off:
                    return "Off";
                case State.On:
                    return "On";
                case State.Sleep:
                    return "Sleep";
                case State.OnWithoutClient:
                    return "On (Client is not running)";
                case State.ServerInaccessible:
                    return "Server Inaccessible";
                default:
                    return "Process...";
            }
        }

        public static State GetStateFromString(string state)
        {
            state = state.Trim();
            switch (state)
            {
                case "Process...":
                    return State.InProcess;
                case "Off":
                    return State.Off;
                case "On":
                    return State.On;
                case "Sleep":
                    return State.Sleep;
                case "On (Client is not running)":
                    return State.OnWithoutClient;
                case "Server Inaccessible":
                    return State.ServerInaccessible;
                default:
                    return State.InProcess;
            }
        }

        private static string GetStringCommand(Command command)
        {
            switch (command)
            {
                case Common.DTO.Enums.Command.TurnOn:
                    return "Turn On";
                case Common.DTO.Enums.Command.TurnOff:
                    return "Shut Down";
                case Common.DTO.Enums.Command.GoToSleep:
                    return "Sleep";
                case Common.DTO.Enums.Command.DoNothing:
                    return "0";
                case Common.DTO.Enums.Command.InProcess:
                    return "Process...";
                default:
                    return "Process...";
            }
        }

        public static string[] GetStateEnumStringNames()
        {
            var states = Enum.GetNames(typeof (State));
            var result = new string[states.Length];
            for (var i = 0; i < states.Length; i++)
            {
                result[i] = GetStringCurrentState((State) i);
            }
            return result;
        }

        public static Client CreateFromEntity(RemotePowerControllerTestCore.Entities.User user, RemotePowerControllerTestCore.Entities.Client entityClient ,
            List<RemotePowerControllerTestCore.Entities.ClientStateAudit> clientDetailsEntities)
        {
            return new Client
            {
                ClientId = entityClient.ClientId,
                NetworkName = entityClient.NetworkName,
                CurrentState = GetStringCurrentState(entityClient.ClientState.CurrentState),
                Command = GetStringCommand(entityClient.ClientState.Command),
                WorkTimePercentage = ClientDetails.GetWorkTimePercentage(clientDetailsEntities),
                TurnOnLink = GetTurnOnLink(entityClient.ClientId, user.Username, user.Password),
                TurnOnWithRDLink = GetTurnOnWithRDLink(entityClient.ClientId, user.Username, user.Password)
            };
        }

        public static Client CreateFromClientStateEntity(RemotePowerControllerTestCore.Entities.ClientState entityClientState)
        {
            return new Client
            {
                ClientId = entityClientState.ClientId,
                CurrentState = GetStringCurrentState(entityClientState.CurrentState),
                Command = GetStringCommand(entityClientState.Command)
            };
        }

        public static string GetTurnOnLink(int clientId, string username, string userPassword)
        {
            var encryptedUsername = StringCipher.Encrypt(username, Common.Keys.Passwords.UserNameCipherKey);
            var encodedUsername = CustomUrlCoder.Encode(encryptedUsername);
            var encryptedUserPassword = StringCipher.Encrypt(userPassword, Common.Keys.Passwords.PasswordCipherKey);
            var encodedUserPassword = CustomUrlCoder.Encode(encryptedUserPassword);
            return String.Format("http://{0}/APIClient.svc/TurnON={1}/{2}/{3}", ConfigurationManager.AppSettings["RPCLocalUrl"], clientId, encodedUsername, encodedUserPassword);
        }

        public static string GetTurnOnWithRDLink(int clientId, string username, string userPassword)
        {
            var encryptedUsername = StringCipher.Encrypt(username, Common.Keys.Passwords.UserNameCipherKey);
            var encodedUsername = CustomUrlCoder.Encode(encryptedUsername);
            var encryptedUserPassword = StringCipher.Encrypt(userPassword, Common.Keys.Passwords.PasswordCipherKey);
            var encodedUserPassword = CustomUrlCoder.Encode(encryptedUserPassword);
            return String.Format("http://{0}/APIClient.svc/CreateRDPConnection={1}/{2}/{3}", ConfigurationManager.AppSettings["RPCLocalUrl"], clientId, encodedUsername, encodedUserPassword);
        }
    }
}