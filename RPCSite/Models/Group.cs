﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace RPCSite.Models
{
    public class Group
    {
        public Group()
        {
            NetworkClients = new List<NetworkClient>();
        }

        public int GroupId { get; set; }

        public string GroupName { get; set; }

        public string GroupWorkTimePercentage { get; set; }

        public List<NetworkClient> NetworkClients { get; set; }

        public static Group ConvertFromEntity(RemotePowerControllerTestCore.Entities.Group groupEntity, List<NetworkClient> networkClientModels)
        {
            return new Group
            {
                GroupId = groupEntity.GroupId,
                GroupName = groupEntity.GroupName,
                NetworkClients = networkClientModels,
                GroupWorkTimePercentage = GetGroupWorkTimePercentage(networkClientModels),
            };
        }

        private static string GetGroupWorkTimePercentage(IReadOnlyCollection<NetworkClient> clientModels)
        {
            var groupWorkTimePercentage = clientModels
                .Select(clientModel => clientModel
                    .WorkTimePercentage)
                .Select(fl => float.Parse(fl, CultureInfo.InvariantCulture))
                .Sum();
            if (clientModels.Count > 0)
            {
                groupWorkTimePercentage /= clientModels.Count;
            }
            else
            {
                groupWorkTimePercentage = 0;
            }
            
            return (groupWorkTimePercentage<=1) ? "0" : groupWorkTimePercentage.ToString("#.##");
        }
    }

    public class TransferGroup
    {
        public int TransferGroupId { get; set; }

        public string Name { get; set; }

        public bool IsClientInGroup { get; set; }

        public static TransferGroup CreateFromEntity(RemotePowerControllerTestCore.Entities.Group group)
        {
            return new TransferGroup
            {
                TransferGroupId = group.GroupId,
                Name = group.GroupName
            };
        }
    }
}