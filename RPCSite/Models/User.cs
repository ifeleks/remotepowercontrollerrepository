﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Common.Logger.Impl;
using RemotePowerControllerTestCore.Entities;
using RemotePowerControllerTestCore.Helpers;
using RemotePowerControllerTestCore.Repositories.Implementations;

namespace RPCSite.Models
{
    public class User
    {
        public User()
        {
            Clients = new HashSet<Client>();
        }

        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }

        public virtual ICollection<Client> Clients { get; set; }

        public static User CreateFromEntity(RemotePowerControllerTestCore.Entities.User entityUser)
        {
            var user = new User
                {
                    UserId = entityUser.UserId,
                    Username = entityUser.Username,
                    Password = entityUser.Password
                };
            return user;
        }

        public static DomainAuthAttempt CreateDomainAuthAttempt(Models.User user)
        {
            var rsa = new RSACryptoServiceProvider();
            try
            {
                rsa.FromXmlString(File.ReadAllText(HttpContext.Current.Server.MapPath("~/domainAuth_public.xml")));
            }
            catch (Exception ex)
            {
                var logger = LoggerFactory.Current.GetLogger<User>();
                logger.Error("Error with RSA pass encrypting", ex);
                throw;
            }
            return new DomainAuthAttempt
            {
                Username = user.Username,               
                Password = Convert.ToBase64String(rsa.Encrypt(Encoding.UTF8.GetBytes(user.Password), false)),
                IsAuth = null
            };
        }

        public static RemotePowerControllerTestCore.Entities.User ConverToEntity(User user)
        {
            var entityUser = new RemotePowerControllerTestCore.Entities.User
            {
                Username = user.Username,
                Password = user.Password
            };
            return entityUser;
        }

        public static bool IsValid(Models.User user)
        {
            var userRepo = new GenericRepository<RemotePowerControllerTestCore.Entities.User>();
            var userEntity = userRepo.Get(u => u.Username == user.Username).FirstOrDefault();
            return userEntity != null && PasswordHash.ValidatePassword(user.Password, userEntity.Password);
        }

        public static bool IsDomainAuth(Models.User user)
        {
            var userRepo = new GenericRepository<RemotePowerControllerTestCore.Entities.User>();
            var userEntity = userRepo.Get(u=>u.Username == user.Username).FirstOrDefault();
            return userEntity != null && userEntity.IsDomainAuth;
        }
    }
}