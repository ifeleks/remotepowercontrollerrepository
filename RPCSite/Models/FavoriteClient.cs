﻿using System.Collections.Generic;
using RPCSite.Stats;

namespace RPCSite.Models
{
    public class FavoriteClient
    {
        public int ClientId { get; set; }
        public string Username { get; set; }
        public string ClientNetworkName { get; set; }

        public string CurrentState { get; set; }

        public string WorkTimePercentage { get; set; }

        public static FavoriteClient CreateFromEntity(RemotePowerControllerTestCore.Entities.Client client, RemotePowerControllerTestCore.Entities.User user, 
            RemotePowerControllerTestCore.Entities.ClientState state, List<RemotePowerControllerTestCore.Entities.ClientStateAudit> clientStateAudits)
        {
            var favoriteClientDetailsModel = new List<Models.ClientDetails>();
            foreach (var clientStateAudit in clientStateAudits)
            {
                favoriteClientDetailsModel.Add(ClientDetails.ConvertFromEntity(clientStateAudit));
            }
            var workTimePercentage = 100 - (new StatHandler(favoriteClientDetailsModel)).GetWorkPercentage();
            return new FavoriteClient
            {
                ClientId = client.ClientId,
                Username = user.Username,
                ClientNetworkName = client.NetworkName,
                CurrentState = Models.Client.GetStringCurrentState(state.CurrentState),
                WorkTimePercentage = (workTimePercentage <= 1) ? "0.00" : workTimePercentage.ToString("#.##"),
            };
        }
    }

    public class NotUserClient
    {
        public int ClientId { get; set; }
        public string Username { get; set; }
        public string ClientNetworkName { get; set; }
        public bool IsFavorite { get; set; }
        public string WorkTimePercentage { get; set; }

        public static NotUserClient ConvertFromEntity(RemotePowerControllerTestCore.Entities.Client client, RemotePowerControllerTestCore.Entities.User user, bool isFavorite,
            List<RemotePowerControllerTestCore.Entities.ClientStateAudit> clientStateAudits)
        {
            var notUserClientDetailsModel = new List<Models.ClientDetails>();
            foreach (var clientStateAudit in clientStateAudits)
            {
                notUserClientDetailsModel.Add(ClientDetails.ConvertFromEntity(clientStateAudit));
            }
            var workTimePercentage = 100 - (new StatHandler(notUserClientDetailsModel)).GetWorkPercentage();
            return new NotUserClient
            {
                ClientId = client.ClientId,
                Username = user.Username,
                ClientNetworkName = client.NetworkName,
                IsFavorite = isFavorite,
                WorkTimePercentage = (workTimePercentage <= 1) ? "0.00" : workTimePercentage.ToString("#.##"),
            };
        }
    }
}