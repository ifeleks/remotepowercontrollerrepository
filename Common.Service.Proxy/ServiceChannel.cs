﻿using System;
using System.Configuration;
using System.ServiceModel;
using Common.Logger.Impl;

namespace Common.Service.Proxy
{
    public static class ServiceChannel<T>
    {
        public static void For(EndpointAddress endpointAddress, Action<T> execute)
        {
            T client;
            ChannelFactory<T> factory;

            try
            {
                var serviceName = typeof (T).Name;
                var endpointConfigurationName = ConfigurationManager.AppSettings[serviceName];

                factory = new ChannelFactory<T>(endpointConfigurationName, endpointAddress);
                client = factory.CreateChannel();

                try
                {
                    execute(client);
                }
                catch (Exception ex)
                {
                    var _logger = LoggerFactory.Current.GetLogger<T>();
                    _logger.Error("Problem with command execution", ex);
                }
                finally
                {
                    DisposeSafely(client as ICommunicationObject);
                    DisposeSafely(factory);
                }
            }
            catch (Exception ex)
            {
                var _logger = LoggerFactory.Current.GetLogger<T>();
                _logger.Error("Cannot create connection to server", ex);
            }
        }

        public static void For(Action<T> execute)
        {
            T client;
            ChannelFactory<T> factory;

            try
            {
                var serviceName = typeof (T).Name;
                var endpointConfigurationName = ConfigurationManager.AppSettings[serviceName];

                factory = new ChannelFactory<T>(endpointConfigurationName);
                client = factory.CreateChannel();

                try
                {
                    execute(client);
                }
                catch (Exception ex)
                {
                    var logger = LoggerFactory.Current.GetLogger<T>();
                    logger.Error("Exception from web-level caughted", ex);
                }
                finally
                {
                    DisposeSafely(client as ICommunicationObject);
                    DisposeSafely(factory);
                }
            }
            catch (Exception ex)
            {
                var logger = LoggerFactory.Current.GetLogger<T>();
                logger.Error("Possibly wrong endpoint", ex);
            }
        }

        private static void DisposeSafely(ICommunicationObject client)
        {
            if (client == null)
                return;

            var success = false;
            try
            {
                if (client.State == CommunicationState.Faulted)
                    return;
                client.Close();
                success = true;
            }
            finally
            {
                if (!success)
                {
                    client.Abort();
                }
            }
        }
    }
}
