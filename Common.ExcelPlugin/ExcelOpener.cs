﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Common.PluginsInterface;
using Microsoft.Office.Interop.Excel;

namespace Common.ExcelPlugin
{
    [Export(typeof(IOpenerPlugin))]
    public class ExcelOpener : IOpenerPlugin
    {
        private readonly List<string> _applicationNames;
        private readonly List<string> _supportedExtensions;

        ExcelOpener()
        {
            _applicationNames = new List<string>
            {
                "EXCEL", 
                "excel"
            };
            _supportedExtensions = new List<string>
            {
                "xls", "xlsb", "xlsm",
                "xlsx", "xlt", "xltx"
            };
        }

        public bool IsSuitableForProcess(string processName)
        {
            return _applicationNames.Any(processName.Equals);
        }

        public IEnumerable<string> GetSupportedExtensions()
        {
            return _supportedExtensions;
        }

        public string GetProcessLine(Process process)
        {
            var docList = new List<string>();
            try
            {
                Application oExcelApp;

                oExcelApp = (Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");

                docList.AddRange(from Workbook wb in oExcelApp.Workbooks select wb.FullName);

                oExcelApp = null;
            }
            catch
            {
                // No documents opened
            }


            var result = new StringBuilder();
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(process.MainModule.FileName);
            result.Append(fileVersionInfo.FileDescription + "\t" + process.ProcessName +
                "\t" + process.MainModule.FileName + "\t" + true + "\t");
            foreach (var supportedExtension in _supportedExtensions)
            {
                result.Append("." + supportedExtension);
            } 
            foreach (var doc in docList)
            {
                result.Append("\t" + doc);
            }
            return result.ToString();
        }

        public void OpenFiles(string processPath, List<string> filePaths)
        {
            if (filePaths.Any())
            {
                var arguments = filePaths.Aggregate(string.Empty,
                    (current, filePath) => current + ("\t\"" + filePath + "\""));
                Process.Start(processPath, arguments);
            }
            else
            {
                Process.Start(processPath);
            }
        }
    }
}
