﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Automation;
using Common.PluginsInterface;

namespace Common.ChromePlugin
{
    [Export(typeof(IOpenerPlugin))]
    public class ChromeOpener : IOpenerPlugin
    {
        private readonly List<string> _applicationNames;
        private readonly List<string> _supportedExtensions;

        ChromeOpener()
        {
            _applicationNames = new List<string> {"chrome", "chromium"};
            _supportedExtensions = new List<string> {"html", "asax"};
        }

        public bool IsSuitableForProcess(string processName)
        {
            return _applicationNames.Any(processName.Equals);
        }

        public IEnumerable<string> GetSupportedExtensions()
        {
            return _supportedExtensions;
        }

        public string GetProcessLine(Process process)
        {
            var result = new StringBuilder();
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(process.MainModule.FileName);
            result.Append(fileVersionInfo.FileDescription + "\t" + process.ProcessName +
                "\t" + process.MainModule.FileName + "\t" + true + "\t");
            foreach (var supportedExtension in _supportedExtensions)
            {
                result.Append("." + supportedExtension);
            }

            if (process.MainWindowHandle == IntPtr.Zero)
            {
                return result.ToString();
            }

            // find the automation element
            var elm = AutomationElement.FromHandle(process.MainWindowHandle);

            // manually walk through the tree, searching using TreeScope.Descendants is too slow (even if it's more reliable)
            AutomationElement elmUrlBar = null;
            try
            {
                // walking path found using inspect.exe (Windows SDK) for Chrome 31.0.1650.63 m (currently the latest stable)
                var elm1 = elm.FindFirst(TreeScope.Children, new PropertyCondition(AutomationElement.NameProperty, "Google Chrome"));
                if (elm1 == null) { return result.ToString(); } // not the right chrome.exe
                // here, you can optionally check if Incognito is enabled:
                //bool bIncognito = TreeWalker.RawViewWalker.GetFirstChild(TreeWalker.RawViewWalker.GetFirstChild(elm1)) != null;
                var elm2 = TreeWalker.RawViewWalker.GetLastChild(elm1); // I don't know a Condition for this for finding :(
                var elm3 = elm2.FindFirst(TreeScope.Children, new PropertyCondition(AutomationElement.NameProperty, ""));
                var elm4 = elm3.FindFirst(TreeScope.Children, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.ToolBar));
                elmUrlBar = elm4.FindFirst(TreeScope.Children, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Custom));
            }
            catch
            {
                // Chrome has probably changed something, and above walking needs to be modified. :(
                // put an assertion here or something to make sure you don't miss it
                return result.ToString();
            }

            // make sure it's valid
            if (elmUrlBar == null)
            {
                // it's not..
                return result.ToString();
            }

            // elmUrlBar is now the URL bar element. we have to make sure that it's out of keyboard focus if we want to get a valid URL
            if ((bool)elmUrlBar.GetCurrentPropertyValue(AutomationElement.HasKeyboardFocusProperty))
            {
                return result.ToString();
            }

            // there might not be a valid pattern to use, so we have to make sure we have one
            var patterns = elmUrlBar.GetSupportedPatterns();
            if (patterns.Length == 1)
            {
                var ret = string.Empty;
                try
                {
                    ret = ((ValuePattern)elmUrlBar.GetCurrentPattern(patterns[0])).Current.Value;
                }
                catch { }
                if (ret != "")
                {
                    // must match a domain name (and possibly "https://" in front)
                    if (Regex.IsMatch(ret, @"^(https:\/\/)?[a-zA-Z0-9\-\.]+(\.[a-zA-Z]{2,4}).*$"))
                    {
                        // prepend http:// to the url, because Chrome hides it if it's not SSL
                        if (!ret.StartsWith("http"))
                        {
                            ret = "http://" + ret;
                        }
                        Console.WriteLine("Open Chrome URL found: '" + ret + "'");
                    }
                    result.Append("\t" + ret);
                }
                return result.ToString();
            }
            return result.ToString();
        }

        public void OpenFiles(string processPath, List<string> filePaths)
        {
            if (filePaths.Any())
            {
                var arguments = filePaths.Aggregate(string.Empty,
                    (current, filePath) => current + ("\t" + filePath));
                Process.Start(processPath, arguments);
            }
            else
            {
                Process.Start(processPath, "--restore-last-session");
                //Process.Start(processPath, "--user-data-dir=$(mktemp -d) --restore-last-session");
            }
        }
    }
}
