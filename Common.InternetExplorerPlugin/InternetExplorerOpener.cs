﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Common.PluginsInterface;

namespace Common.InternetExplorerPlugin
{
    [Export(typeof(IOpenerPlugin))]
    public class InternetExplorerOpener : IOpenerPlugin
    {
        private readonly List<string> _applicationNames;
        private readonly List<string> _supportedExtensions;

        InternetExplorerOpener()
        {
            _applicationNames = new List<string> {"iexplore", "internetexplorer"};
            _supportedExtensions = new List<string> { "html", "asax" };
        }

        public bool IsSuitableForProcess(string processName)
        {
            return _applicationNames.Any(processName.Equals);
        }

        public IEnumerable<string> GetSupportedExtensions()
        {
            return _supportedExtensions;
        }

        public string GetProcessLine(Process process)
        {
            var shellWindows = new SHDocVw.ShellWindows();
            var urls = new List<string>();
            foreach (SHDocVw.InternetExplorer ie in shellWindows)
            {
                var filename = Path.GetFileNameWithoutExtension(ie.FullName).ToLower();
                if (!IsSuitableForProcess(filename)) continue;
                urls.Add(ie.LocationURL);
            }
            var result = new StringBuilder();
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(process.MainModule.FileName);
            result.Append(fileVersionInfo.FileDescription + "\t" + process.ProcessName +
                "\t" + process.MainModule.FileName + "\t" + true + "\t");
            foreach (var supportedExtension in _supportedExtensions)
            {
                result.Append("." + supportedExtension);
            } 
            foreach (var url in urls)
            {
                result.Append("\t" + url);
            }
            return result.ToString();
        }

        public void OpenFiles(string processPath, List<string> filePaths)
        {
            if (filePaths.Any())
            {
                var javaScript = string.Format("var navOpenInBackgroundTab = 0x1000;" +
                                               "\nvar objIE = new ActiveXObject(\"InternetExplorer.Application\");" +
                                               "\nobjIE.Navigate2(\"{0}\");", filePaths[0]);
                if (filePaths.Count > 1)
                {
                    for (var i = 1; i < filePaths.Count(); i++)
                    {
                        javaScript += string.Format("\nobjIE.Navigate2(\"{0}\", navOpenInBackgroundTab);",
                            filePaths[i]);
                    }
                }
                javaScript += "\nobjIE.Visible = true;";
                const string jsFileName = "startIE.js";
                if (!File.Exists(jsFileName)) File.Create(jsFileName);
                File.WriteAllText(jsFileName, javaScript);
                Process.Start("wscript", jsFileName);
                //File.Delete(jsFileName);
            }
            else
            {
                Process.Start(processPath);
            }
        }
    }
}
