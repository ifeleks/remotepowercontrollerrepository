﻿using System.Collections.Generic;
using System.ServiceModel;
using Common.DTO;

namespace Web.Interfaces
{
    [ServiceContract]
    public interface IDomainService
    {
        [OperationContract]
        List<AuthentificationAttemptTO> GetDomainAuthentificationCommands(string serverSignature);

        [OperationContract]
        void SetAuthentificationResult(AuthentificationAttemptTO user);
    }
}
