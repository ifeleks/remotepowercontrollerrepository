﻿using System.Collections.Generic;
using System.ServiceModel;
using Common.DTO;
using Common.DTO.Enums;

namespace Web.Interfaces
{
    [ServiceContract]
    public interface IClientInfoService
    {
        [OperationContract]
        void AddClientForUser(UserInfoTO userInfo, ClientInfoTO clientInfo, SettingsTO settings);

        [OperationContract]
        List<ClientInfoTO> GetClientsWithCommands(string serverName);

        [OperationContract]
        List<ClientInfoTO> GetAllServerClients(string serverName);

        [OperationContract]
        void UpdateClientState(State newState, ClientInfoTO linkedClient);

        [OperationContract]
        void ChangeCommand(Command command, ClientInfoTO linkedClient);
        
        [OperationContract]
        bool IsMacExist(string macAddress, UserInfoTO userCredentials);
    }
}
