﻿using System;
using System.ServiceModel;
using Common.DTO;

namespace Web.Interfaces
{
    [ServiceContract]
    public interface ISettingsService
    {
        [OperationContract]
        void AddSettings(SettingsTO settings, ClientInfoTO client);

        [OperationContract]
        void UpdateSettings(string clientMACAddress, SettingsTO settings);

        [OperationContract]
        SettingsTO GetDefaultSettings();

        [OperationContract]
        SettingsTO GetClientSettings(ClientInfoTO client);

        [OperationContract]
        void AlarmClock(TimeSpan serverTime, TimeSpan acceptableDelay);
    }
}
