﻿using System.ServiceModel;
using Common.DTO;

namespace Web.Interfaces
{
    [ServiceContract]
    public interface IUserInfoService
    {
        [OperationContract]
        void RegisterNewUser(UserInfoTO userInfo, ClientInfoTO clientInfo, SettingsTO settings);

        [OperationContract]
        bool  IsLoginExist(UserInfoTO userInfo);

        [OperationContract]
        bool LogIn(UserInfoTO userInfo);

        [OperationContract]
        void ChangePassword(string newPassword, UserInfoTO userInfo);

        [OperationContract]
        void ChangeLogin(string newLogin, UserInfoTO userInfo);

        [OperationContract]
        UserInfoTO GetUserById(int id);
    }
}
