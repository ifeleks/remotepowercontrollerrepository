﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Windows.Forms;
using Client.Interface;
using Common.DTO.Enums;
using Common.Service.Proxy;
using RPCServer.Interfaces;
using Shared.Audio;
using Shared.ProcessRestoring;

namespace Client.Service
{
    public class PowerManagementService : IPowerManagementService
    {
        public void TurnOff()
        {
            if (RPCClient.Properties.Settings.Default.IsCommandInProcess) return;
            if (RPCClient.Properties.Settings.Default.IsVoiceMessagesOn) SoundNotificationFactory.Current.PlayTurnOff();
            ShowMessageBeforeAction("Turn off", RPCClient.Properties.Settings.Default.ConfirmPowerActionTime, () => Process.Start("shutdown","/s /f /t 0")); 
        }

        public void Sleep()
        {
            if (RPCClient.Properties.Settings.Default.IsCommandInProcess) return;
            if (RPCClient.Properties.Settings.Default.IsVoiceMessagesOn) SoundNotificationFactory.Current.PlayLullaby();
            ShowMessageBeforeAction("Go to sleep", RPCClient.Properties.Settings.Default.ConfirmPowerActionTime, () => Application.SetSuspendState(PowerState.Suspend, false, false)); 
        }

        private void ShowMessageBeforeAction(string actionName, int timeout, Action powerAction)
        {
            RPCClient.Properties.Settings.Default.IsCommandInProcess = true;
            SetCommandState(Command.InProcess);
            
            var mbCaption = "It's a sign from above! - RemotePowerController";
            var mbResult = DialogResult.No;
            if (RPCClient.Properties.Settings.Default.IsConfirmPowerActionOn)
            {
                var signalForm = new PowerActionMessage(mbCaption, actionName, timeout) {TopMost = true, TopLevel = true};
                mbResult = signalForm.ShowDialog();
            }

            RPCClient.Properties.Settings.Default.IsCommandInProcess = false;
            if (mbResult == DialogResult.No)
            {
                var restoringEnabled = RPCClient.Properties.Settings.Default.IsRestoringOn &&
                                       !RPCClient.Properties.Settings.Default.IsRestoringCustom;
                if (restoringEnabled)
                {
                    var processRestorer = new FileProcessRestorer();
                    processRestorer.CreateSavePoint();
                }

                powerAction();
            }
            else
            {
                SetCommandState(Command.DoNothing);
            }
        }

        private void SetCommandState(Command command)
        {
            var serverEndpoint = new EndpointAddress(
                    string.Format("http://{0}:{1}/ClientInfoService",
                        RPCClient.Properties.Settings.Default.ServerIP,
                        RPCClient.Properties.Settings.Default.ServerPort));
            ServiceChannel<IClientInfoService>.For(serverEndpoint, service =>
                service.ChangeCommand(command, RPCClient.Properties.Settings.Default.ClientInfo));
        }
    }
}
