﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using Cassia;
using Client.Interface;
using Common.Service.Proxy;

namespace Client.Service
{
    public class PowerManagementWinService : IPowerManagementService
    {
        public void TurnOff()
        {
            if (IsProcessAvailble())
            {
                ServiceChannel<IPowerManagementService>.For(service =>
                    service.TurnOff());
            }
            else if (!IsSomeoneLoggedIn())
            {
                Process.Start("shutdown", "/s /f /t 0");
            }
        }

        public void Sleep()
        {           
            if (IsProcessAvailble())
            {
                ServiceChannel<IPowerManagementService>.For(service =>
                    service.Sleep());
            }
            else if (!IsSomeoneLoggedIn())
            {
                Application.SetSuspendState(PowerState.Suspend, false, false);
            }
        }

        private bool IsProcessAvailble()
        {
            return Process.GetProcessesByName("RPCClient").Any();
        }

        private bool IsSomeoneLoggedIn()
        {
            ITerminalServicesManager manager = new TerminalServicesManager();
            var loginedUsers = new List<string>();
            using (ITerminalServer server = manager.GetLocalServer())
            {
                server.Open();
                foreach (ITerminalServicesSession session in server.GetSessions())
                {
                    var account = session.UserAccount;
                    if (account != null)
                    {
                        loginedUsers.Add(session.UserName);
                    }
                }
            }
            return loginedUsers.Count != 0;
        }
    }
}
