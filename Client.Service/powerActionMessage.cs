﻿using System;
using System.Windows.Forms;

namespace Client.Service
{
    public partial class PowerActionMessage : Form
    {
        private System.Timers.Timer _closeTimer;
        private int _seconds;
        private string _actionName;
        private string _caption;

        public static PowerActionMessage Instance;

        public PowerActionMessage(string caption, string actionName, int seconds)
        {
            Instance = this;
            _caption = caption;
            _seconds = seconds;
            _actionName = actionName;
            InitializeComponent();
            _closeTimer = new System.Timers.Timer(1000);
            _closeTimer.Enabled = true;
            _closeTimer.Elapsed += delegate
            {
                _seconds--; 
                lblMessage.Invoke((MethodInvoker) delegate
                {
                    lblMessage.Text = string.Format("Signal from server received.\n{0} in {1} sec." +
                                                    "\nWould you confirm action \"{0}\"?", _actionName, _seconds);
                });
                this.Invoke((MethodInvoker) delegate
                {
                    TopMost = false;
                    TopMost = true;
                });
                if (_seconds == 0)
                {
                    _closeTimer.Stop();
                    _closeTimer.Dispose();
                    this.Invoke((MethodInvoker) delegate
                    {
                        this.DialogResult = DialogResult.No;
                        this.Close();
                    });
                }   
            };
            _closeTimer.Start();
            WindowState = FormWindowState.Normal;
            TopMost = false;
            TopMost = true;
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            _closeTimer.Stop();
            _closeTimer.Dispose();
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            _closeTimer.Stop();
            _closeTimer.Dispose();
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void PowerActionMessage_Load(object sender, EventArgs e)
        {
            this.Text = _caption;
            lblMessage.Text = string.Format("Signal from server received.\n{0} in {1} sec." +
                                            "\nWould you confirm action \"{0}\"?", _actionName, _seconds);
            TopMost = true;
        }
    }
}
