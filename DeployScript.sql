--
--DROPING EXISTING DATABASE
--
USE master
GO
IF EXISTS(SELECT * FROM sys.databases WHERE name='RemotePowerControllerDB')
DROP DATABASE RemotePowerControllerDBFromScript
GO
--
--DROPING EXISTING DATABASE
--
--CREATING DATABASE
--
USE master
GO
CREATE DATABASE RemotePowerControllerDBFromScript
GO
--
--END CREATING DATABASE
--
--CREATING TABLES
--
USE RemotePowerControllerDBFromScript
GO
CREATE TABLE [User] 
	(
	 [UserID]	INT				IDENTITY(1, 1)	NOT NULL	CONSTRAINT PK_User			PRIMARY KEY([UserID])
	,[Username] NVARCHAR(255)					NOT NULL	CONSTRAINT UK_User_Username	UNIQUE
	,[Password] NVARCHAR(64)					NOT NULL
	)
GO

USE RemotePowerControllerDBFromScript
GO
CREATE TABLE [Client] 
	(
	 [ClientID]			INT				IDENTITY(1, 1)	NOT NULL	CONSTRAINT PK_Client	PRIMARY KEY([ClientID])
	,[NetworkName]		NVARCHAR(255)					NOT NULL	
	,[MACAddress]		CHAR(12)						NOT NULL
	,[ClientIP]			VARCHAR(15)						NOT NULL	
	,[GetawayIP]		VARCHAR(15)						NOT NULL
	,[ServerIP]			VARCHAR(15)
	,[InstallationDate]	DATETIME						NOT NULL	CONSTRAINT DF_Client_InstallationDate
																	DEFAULT GETDATE()
	)
GO

USE RemotePowerControllerDBFromScript
GO
CREATE TABLE [ClientSettings] 
	(
	 [ClientID]					INT		NOT NULL	CONSTRAINT PK_Computer	PRIMARY KEY([ClientID])
	,[CurrentState]				TINYINT	NOT NULL	CONSTRAINT CK_ClientSettings_CurrentState
													CHECK([CurrentState] >= 0 AND [CurrentState] <= 3)
	,[CurrentStateDescription]	AS
	(
		CASE
			WHEN [CurrentState] = (0) then 'OFF'
			WHEN [CurrentState] = (1) then 'ON'
			WHEN [CurrentState] = (2) then 'Shutting Down'
			WHEN [CurrentState] = (2) then 'Turning On'
			ELSE 'Undefined'
		END
	)
	,[StartWorkTime]			TIME(0)	NOT NULL	CONSTRAINT DF_ClientSettings_StartWorkTime 
													DEFAULT '09:00:00'
	,[EndWorkTime]				TIME(0)	NOT NULL	CONSTRAINT DF_ClientSettings_EndWorkTime 
													DEFAULT '19:00:00'
	,[IdleTime]					TIME(0)	NOT NULL	CONSTRAINT DF_ClientSettings_IdleTime 
													DEFAULT '00:15:00'
	,CONSTRAINT	FK_ClientSettings_Client	FOREIGN KEY([ClientID])	REFERENCES [Client]([ClientID])																	
	)
GO

--USE RemotePowerControllerDB
--CREATE TABLE [Server] 
--	(
--	 [ServerID]		INT				IDENTITY(1, 1)	NOT NULL	CONSTRAINT PK_Server	PRIMARY KEY([ServerID])
--	,[Name]			NVARCHAR(255)					NOT NULL	
--	,[ServerIP]		VARCHAR(15)	
--	)
--GO

USE RemotePowerControllerDBFromScript
GO
CREATE TABLE [ClientUser] 
	(
	 [ClientID]		INT NOT NULL	CONSTRAINT FK_ClientUser_Client 
									FOREIGN KEY([ClientID])	REFERENCES [Client]([ClientID])
	,[UserID]		INT NOT NULL	CONSTRAINT FK_ClientUser_User 
									FOREIGN KEY([UserID])	REFERENCES [User]([UserID])
	,CONSTRAINT		PK_ClientUser	PRIMARY KEY(ClientID, UserID)
	)
GO
--
--CREATING TABLES
--
--INSERTING TEST DATA
--
USE RemotePowerControllerDBFromScript
GO
INSERT INTO [User] ([Username], [Password])
			VALUES ('andriy.pukalyak', 'a.p_password')
GO

USE RemotePowerControllerDBFromScript
GO
INSERT INTO [Client] ([NetworkName], [MACAddress], [ClientIP], [GetawayIP], [ServerIP])
			VALUES	 ('WS-IF-CP0080', '001CC04B2DA3', '172.20.0.78', '172.20.0.1', '172.255.255.255')
GO

USE RemotePowerControllerDBFromScript
GO
INSERT INTO [ClientSettings] ([ClientID], [CurrentState])
			VALUES			 (1, 1)
GO

USE RemotePowerControllerDBFromScript
GO
INSERT INTO [ClientUser] ([ClientID], [UserID])
			VALUES		 (1, 1)
GO
--
--END INSERTING TEST DATA
--
--CREATING STORED PROCEDURES
--
USE RemotePowerControllerDBFromScript
GO
CREATE PROCEDURE uspAddUser(@Username NVARCHAR(255), @Password NVARCHAR(64)) AS 
INSERT INTO [User] ([Username], [Password])
			VALUES (@Username,  @Password)
GO

USE RemotePowerControllerDBFromScript
GO
CREATE PROCEDURE uspGetAllUsers AS
SELECT [UserID], [Username], [Password]
FROM [User]
GO
--
--END CREATING STORED PROCEDURES
--



