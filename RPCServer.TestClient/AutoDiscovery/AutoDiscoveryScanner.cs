﻿using System;
using System.ComponentModel;

namespace RPCServer.TestClient.AutoDiscovery
{
    public class AutoDiscoveryScanner : IDisposable
    {
        public bool IsServerFound;
        public bool IsSearchCompleted;
        private string _serverIp = null;
        private string _serverPort = null;

        private BackgroundWorker _workerAD;
        private AutoDiscoveryClient _svcAutoDiscoveryClient;

        public string getServerIp()
        {
            return _serverIp;
        }

        public string getServerPort()
        {
            return _serverPort;
        }

        public AutoDiscoveryScanner(byte[] identifyBytes, int autoDiscoveryPort, int attempts, int timeout)
        {
            IsSearchCompleted = false;
            _workerAD = new BackgroundWorker();
            _workerAD.WorkerReportsProgress = true;
            _workerAD.WorkerSupportsCancellation = true;

            _svcAutoDiscoveryClient = new AutoDiscoveryClient(ref _workerAD, identifyBytes,
                autoDiscoveryPort, attempts, timeout);
            _workerAD.DoWork += _svcAutoDiscoveryClient.ServerSearch;
            _workerAD.ProgressChanged += logWorkers_ProgressChanged;
            _workerAD.RunWorkerAsync();
        }

        private void logWorkers_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
            {
                var result = e.UserState.ToString();
                var serverAdress = result.Split(':');
                _serverIp = serverAdress[0];
                _serverPort = serverAdress[1];
                IsServerFound = true;
            }
            else
            {
                IsServerFound = false;
            }
            _svcAutoDiscoveryClient.Stop();
            IsSearchCompleted = true;
        }

        public void TryAgain()
        {
            if (_svcAutoDiscoveryClient == null) return;
            IsSearchCompleted = false;
            _svcAutoDiscoveryClient.Resume();
        }

        public void Dispose()
        {
            if (_svcAutoDiscoveryClient == null) return;
            _svcAutoDiscoveryClient.Stop();
            _workerAD.Dispose();
        }
    }
}
