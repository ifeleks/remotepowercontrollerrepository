﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace RPCServer.TestClient.AutoDiscovery
{
    public class TCPClient
    {
        private static bool _isConnectionSuccessful;
        private static Exception _socketexception;
        private static ManualResetEvent TimeoutObject = new ManualResetEvent(false);

        public static TcpClient Connect(IPEndPoint remoteEndPoint, int timeoutMSec)
        {
            TimeoutObject.Reset();
            _socketexception = null;

            var serverip = Convert.ToString(remoteEndPoint.Address);
            var serverport = remoteEndPoint.Port;
            var tcpclient = new TcpClient();

            tcpclient.BeginConnect(serverip, serverport,
                CallBackMethod, tcpclient);

            if (TimeoutObject.WaitOne(timeoutMSec, false))
            {
                if (_isConnectionSuccessful)
                {
                    return tcpclient;
                }
                throw _socketexception;
            }
            tcpclient.Close();
            throw new TimeoutException("TimeOut Exception");
        }
        private static void CallBackMethod(IAsyncResult asyncresult)
        {
            try
            {
                _isConnectionSuccessful = false;
                var tcpclient = asyncresult.AsyncState as TcpClient;

                if (tcpclient.Client != null)
                {
                    tcpclient.EndConnect(asyncresult);
                    _isConnectionSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                _isConnectionSuccessful = false;
                _socketexception = ex;
            }
            finally
            {
                TimeoutObject.Set();
            }
        }
    }
}
