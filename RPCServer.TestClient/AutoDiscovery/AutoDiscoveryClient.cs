﻿using System;
using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Common.Logger;
using Common.Logger.Impl;

namespace RPCServer.TestClient.AutoDiscovery
{
    public class AutoDiscoveryClient
    {
        private bool _isServerFound;
        private bool _isRunning;

        private int _autoDiscoveryPort;
        private int _attempts;
        private int _autoDiscoveryTimeout;

        private byte[] _identifyBytesPacket;

        public string ServerAddress;
        public int ServerPort;

        private BackgroundWorker _worker;
        private ILogger _logger;

        public AutoDiscoveryClient(ref BackgroundWorker worker, byte[] identifyBytes, int autoDiscoveryPort, int attempts, int timeout)
        {
            _logger = LoggerFactory.Current.GetLogger<AutoDiscoveryClient>();
            _worker = worker;
            _attempts = attempts;
            _identifyBytesPacket = identifyBytes;
            _autoDiscoveryPort = autoDiscoveryPort;
            _autoDiscoveryTimeout = timeout;
            _isRunning = true;
            _logger.Trace("AutoDiscovery started at {0}", _autoDiscoveryPort);
        }

        public void ServerSearch(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                while (_isRunning)
                {
                    try
                    {
                        for (var i = 0; i < _attempts; i++)
                        {
                            if (_isServerFound) continue;
                            _logger.Trace("Looking for server..", _autoDiscoveryPort);
                            sendBroadcastSearchPacket();
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Error with sending broadcast request", ex);
                    }
                }
            }
        }

        //private bool ServerIsReachable(string host, int port)
        //{
        //    var serverEndpoint = new EndpointAddress(
        //        string.Format("http://{0}:{1}/ClientInfoService",
        //            host, port));
        //    try
        //    {
        //        ServiceChannel<IClientInfoService>.For(serverEndpoint,
        //            s => { var a = Command.DoNothing; }); //Do nothing, just check for availbleness
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Server is not reachable", ex);
        //        return false;
        //    }
        //}

        private void sendBroadcastSearchPacket()
        {
            _isServerFound = false;
            var udp = new UdpClient();
            udp.EnableBroadcast = true;
            udp.Client.ReceiveTimeout = _autoDiscoveryTimeout;
            var groupEndPoint = new IPEndPoint(IPAddress.Parse("255.255.255.255"), _autoDiscoveryPort);

            try
            {
                udp.Send(_identifyBytesPacket, _identifyBytesPacket.Length, groupEndPoint);

                var receiveBytes = udp.Receive(ref groupEndPoint);

                var returnData = Encoding.ASCII.GetString(receiveBytes, 0, receiveBytes.Length);
                switch (returnData.Substring(0, 3))
                {
                    case "ACK":
                    {
                        var splitRcvd = returnData.Split(' ', ':');
                        _logger.Trace("Response Server is {0}:{1}", splitRcvd[1], splitRcvd[2]);

                        //ServiceIsReachable function takes time. Lets do it without check at first
                        ServerAddress = splitRcvd[1];
                        ServerPort = Convert.ToInt32(splitRcvd[2]);
                        _isServerFound = true;
                        _logger.Trace("Founded server is RemotePowerController Server. Application start");
                        _worker.ReportProgress(1, string.Format("{0}:{1}", splitRcvd[1], splitRcvd[2]));
                    }
                        break;
                    case "NAK":
                        _logger.Trace("INVALID REQUEST");
                        break;
                    default:
                        _logger.Trace("Garbage Received?");
                        break;
                }
                _logger.Trace("Sleeping. No work to do");
            }
            catch (SocketException ex)
            {
                _logger.Error("UDP timeout", ex);
                _worker.ReportProgress(1);//link to main thread. Report about error
            }
            udp.Close();
        }

        public void Stop()
        {
            _isRunning = false;
        }

        public void Resume()
        {
            _isRunning = true;
        }

    }
}
