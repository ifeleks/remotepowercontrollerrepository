﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.ActiveDirectory;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Security.Principal;
using System.ServiceModel;
using Common.DTO;
using Common.Service.Proxy;
using Web.Interfaces;


namespace RPCServer.TestClient
{
    class Program
    {
        private static void Main()
        {
            //var clientEndpoint = new EndpointAddress(string.Format("http://172.20.0.120:12541/PowerManagementService/"));

            //Exception except;
            //ServiceChannel<IClientInfoService>.For(service => 
            //    service.GetAllServerClients("if_eleks"));

            //for (int i = 0; i < 256; i++)
            //{
            //    if (IsServiceAvailble(string.Format("172.20.0.{0}", i), 200))
            //    {
            //        Console.Write("Busted: 172.20.0.{0}", i);
            //    }

            //}

            //Console.WriteLine("Input your domain credentials:");
            //var name = Console.ReadLine();
            //var pass = Console.ReadLine();
            //var isExist = IsLoginExistInDomain(name);
            //var isValid = CheckCredentials(name, pass);

            //Console.WriteLine("User exist: {0}", isExist);
            //Console.WriteLine("Credentials valid: {0}", isValid);

            //CreateSavePoint(Process.GetProcesses().ToList());

            var isMacExist = false;
            ServiceChannel<IClientInfoService>.For(service =>
                isMacExist = service.IsMacExist("3C970E28E724", new UserInfoTO { Login = "andriy.leshchuk", IsDomain = false, Password = "al_password" }));

            Console.Write("Mac exist? - {0}", isMacExist);

            Console.ReadLine();
        }

        public static void CreateSavePoint(List<Process> processes)
        {
            var lines = new List<string>();
            foreach (var process in processes)
            {
                try
                {
                    lines.Add(+process.Id + "\t" + process.ProcessName + "\t\t" + process.MainModule.FileName);
                }catch{}
            }
            File.WriteAllLines("processList.txt", lines);
        }

        public static bool CheckCredentials(string name, string pass)
        {
            var isValid = false;
            if (name.Contains("\\"))
            {
                var split = name.Split('\\');
                name = split[1];
            }
            try
            {
                using (var context = new PrincipalContext(ContextType.Domain))
                {
                    isValid = context.ValidateCredentials(name, pass,
                        ContextOptions.Negotiate);
                }
            }
            catch
            {
                Console.WriteLine("No domain on this PC");
            }
            return isValid;
        }

        public static bool IsLoginExistInDomain(string userDomainName)
        {
            var domainLoginExist = false;
            try
            {
                using (var domainContext = new PrincipalContext(ContextType.Domain))
                {
                    using (var foundUser = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName,
                            userDomainName))
                    {
                        if (foundUser != null)
                        {
                            domainLoginExist = true;
                        }
                    }
                }
            }
            catch
            {
                Console.WriteLine("No domain on this PC");
            }
            return domainLoginExist;
        }

        private static List<string> GetDomains()
        {
            var domains = new List<string>();

            // Querying the current Forest for the domains within.
            foreach (Domain d in Forest.GetCurrentForest().Domains)
                domains.Add(d.Name);

            return domains;
        }

        public IPAddress[] GetAllUnicastAddresses_Old()
        {
            // By passing an empty string to GetHostEntry we receive all the IP addresses on the local machine
            // This breaks on Mono
            IPHostEntry LocalEntry = Dns.GetHostEntry("");
            return LocalEntry.AddressList;
        }

        public IPAddress[] GetAllUnicastAddresses_New()
        {
            // This works on both Mono and .NET , but there is a difference: it also
            // includes the LocalLoopBack so we need to filter that one out
            List<IPAddress> Addresses = new List<IPAddress>();
            // Obtain a reference to all network interfaces in the machine
            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface adapter in adapters)
            {
                IPInterfaceProperties properties = adapter.GetIPProperties();
                foreach (IPAddressInformation uniCast in properties.UnicastAddresses)
                {
                    // Ignore loop-back addresses & IPv6
                    if (!IPAddress.IsLoopback(uniCast.Address) && uniCast.Address.AddressFamily != AddressFamily.InterNetworkV6)
                        Addresses.Add(uniCast.Address);
                }

            }
            return Addresses.ToArray();
        }

        private static void PrintDomainInfo()
        {
            WindowsIdentity currentIdentity = WindowsIdentity.GetCurrent();
            var environmentDomainName = Environment.UserDomainName;
            var machineName = Environment.MachineName;
            var applicationDomain = AppDomain.CurrentDomain;
            var userPrincipalName = string.Empty;
            try
            {
                using (var context = new UserPrincipal(new PrincipalContext(ContextType.Domain, currentIdentity.Name)))
                {
                    userPrincipalName = context.GivenName;
                }
            }
            catch { }

            var doms = new List<string>();
            try
            {
                doms = GetDomains();
            }
            catch
            {
            }

            String strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            var fullName = ipEntry.HostName;

            Console.WriteLine("winIdentityName: " + currentIdentity.Name);
            Console.WriteLine("environmentDomainName: " + environmentDomainName);
            Console.WriteLine("machineName: " + machineName);
            Console.WriteLine("userPrincipal: " + userPrincipalName);
            Console.WriteLine("HostName: " + fullName);
            try
            {
                Console.WriteLine("ActiveDirectory current domain:" + Domain.GetComputerDomain());
            }
            catch
            {
                Console.WriteLine("ActiveDirectory current domain not found");
            }
            Console.WriteLine("Domains: ");
            if (doms.Count != 0)
            {
                foreach (var dom in doms)
                {
                    Console.WriteLine("       " + dom);
                }
            }
            else
            {
                Console.WriteLine("Current PC is not in domain");
            }
        }
    }
}
