﻿namespace Common.Logger.Impl
{
    public static class LoggerFactory
    {
        static LoggerFactory()
        {
            Current = new NLogFactory();
        }
        public static ILoggerFactory Current { get; private set; }
    }
}