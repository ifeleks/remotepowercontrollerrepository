using System;

namespace Common.Logger.Impl
{
    internal class NLogger : ILogger
    {
        private readonly NLog.Logger _logger;

        public NLogger(NLog.Logger logger)
        {
            _logger = logger;
        }

        public void Info(string message, params object[] args)
        {
            _logger.Info(message, args);
        }

        public void Warn(string message, params object[] args)
        {
            _logger.Warn(message, args);
        }

        public void Trace(string message, params object[] args)
        {
            _logger.Trace(message, args);
        }

        public void Info(string message)
        {
            _logger.Info(message);
        }

        public void Warn(string message)
        {
            _logger.Warn(message);
        }

        public void Error(string message, Exception error)
        {
            if (error == null)
            {
                _logger.Error(String.Format("{0}", message));
            }
            else
            {
                string inner = null;
                if (error.InnerException != null)
                    inner = String.Format("INNER: {0}\n{1}\n", error.InnerException.Message, error.InnerException.StackTrace);

                _logger.Error(message == null
                                  ? String.Format("{0}\n{1}", error.Message, error.StackTrace)
                                  : String.Format("{0}\n{1}\n{2}\n{3}", message, error.Message, error.StackTrace, inner ?? ""));


            }
        }

        public void Trace(string message)
        {
            _logger.Trace(message);
        }
    }
}