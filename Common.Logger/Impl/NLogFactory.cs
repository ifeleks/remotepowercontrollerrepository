using NLog;

namespace Common.Logger.Impl
{
    internal class NLogFactory : ILoggerFactory
    {        
        public ILogger GetLogger(string className)
        {            
            var logger = LogManager.GetLogger(className.Aling(32));
            var nLogger = new NLogger(logger);
            return nLogger;
        }

        public ILogger GetLogger<T>()
        {
            return GetLogger(typeof(T).Name);
        }
    }
}