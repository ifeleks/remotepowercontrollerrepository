﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace Common.Logger.Impl
{
    public static class LoggerExtension
    {
        public static string Aling(this string msg, int len)
        {
            var pattern = "{0," + len + "}";
            return String.Format(pattern, msg.Substring(0, Math.Min(len, msg.Length)));
        }

        public static IDisposable LogTrace(this ILogger logger, MethodBase methodBase)
        {
            return LogTrace(logger, methodBase.Name);
        }

        public static IDisposable LogTrace(this ILogger logger, string message)
        {
            return new TraceLogger(logger, message);
        }

        public static IDisposable LogTrace(this ILogger logger, string message, params object[] args)
        {
            return new TraceLogger(logger, message, args);
        }

        public static IDisposable LogTime(this ILogger logger, MethodBase methodBase)
        {
            return LogTime(logger, methodBase.Name);
        }

        public static IDisposable LogTime(this ILogger logger, string message)
        {
            return new TimeLogger(logger, message);
        }

        public static IDisposable LogTime(this ILogger logger, string message, params object[] args)
        {
            return new TimeLogger(logger, message, args);
        }
    }

    public class TimeLogger : IDisposable
    {
        private readonly ILogger _logger;
        private readonly string _message;
        private readonly object[] _args;
        private readonly long start;

        public TimeLogger(ILogger logger, string message, params object[] args)
        {
            _logger = logger;
            _message = message;
            _args = args;
            start = Stopwatch.GetTimestamp();
        }

        public void Dispose()
        {
            var stop = Stopwatch.GetTimestamp();
            _logger.Trace(" took => " + _message + String.Format(" {0} ms. ", (stop - start) / 1000), _args);
        }
    }

    public class TraceLogger : IDisposable
    {
        private readonly ILogger _logger;
        private readonly string _message;
        private readonly object[] _args;

        public TraceLogger(ILogger logger, string message, params object[] args)
        {
            _logger = logger;
            _message = message;
            _args = args;
            _logger.Trace("begin => " + _message, _args);
        }

        public void Dispose()
        {
            _logger.Trace("  end => " + _message, _args);
        }
    }
}