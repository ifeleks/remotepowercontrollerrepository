﻿
namespace Common.Logger
{
    public interface ILoggerFactory
    {
        ILogger GetLogger(string className);
        ILogger GetLogger<T>();
    }    
}