using System;

namespace Common.Logger
{
    public interface ILogger
    {
        void Info(string message);
        void Warn(string message);
        void Trace(string message);
        void Info(string message, params object[] args);
        void Warn(string message, params object[] args);
        void Trace(string message, params object[] args);
        void Error(string message=null, Exception error = null);        
    }

    public class StubLogger : ILogger
    {
        public void Info(string message)
        {
        
        }

        public void Warn(string message)
        {
        
        }

        public void Trace(string message)
        {
        
        }

        public void Info(string message, params object[] args)
        {
        
        }

        public void Warn(string message, params object[] args)
        {
        
        }

        public void Trace(string message, params object[] args)
        {
        
        }

        public void Error(string message = null, Exception error = null)
        {
        
        }
    }
}