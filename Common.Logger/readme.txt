ILogger Logger = LoggerFactory.Current.GetLogger<Program>();


Logger.Trace("\n\n======================================================================================================\n\n");
Logger.Info("ICustomerService:{0}", customerServiceEndpointName);

using (Logger.LogTime("ICustomerService.ctor")) {
customerService = ServiceLocator.Current.GetInstance<ICustomerService>();
}


using (Logger.LogTrace("ICustomerService.ctor")) {
customerService = ServiceLocator.Current.GetInstance<ICustomerService>();
}