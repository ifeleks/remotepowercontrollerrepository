﻿using System;
using System.Net;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Common.Availableness
{
    public class ServiceAvailability
    {
        public static bool IsOnline(string uri)
        {
            var request = WebRequest.Create(uri);
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse) request.GetResponse();
            }
            catch
            {
            }

            request.Abort(); //very important (CLR dont re-create requests, it use object from previous call)

            var result = response != null && response.StatusCode == HttpStatusCode.OK;
            if (response == null) return false;
            response.Close();
            response.Dispose();
            return result;
        }

        public static bool IsOnline(string uri, int timeout)
        {
            var request = WebRequest.Create(uri);
            request.Timeout = timeout; 
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch
            {
            }

            request.Abort();

            var result = response != null && response.StatusCode == HttpStatusCode.OK;
            if (response == null) return false;
            response.Close();
            response.Dispose();
            return result;
        }

        public static void DoWorkSafely(string uri, Action work)
        {
            DialogResult mbResult;
            do
            {
                if (IsOnline(uri))
                {
                    work();

                    mbResult = DialogResult.OK;
                }
                else
                {
                    mbResult = XtraMessageBox.Show("Server is not responding. Try again?\n(Click No to exit, Cancel to continue in offline mode)",
                        "Warning!",
                        MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Error);

                    if (mbResult == DialogResult.No) Environment.Exit(1);
                }
            } while (mbResult == DialogResult.Yes);
        }

        public static void DoWorkSafely(string uri, int timeout, Action work)
        {
            DialogResult mbResult;
            do
            {
                if (IsOnline(uri, timeout))
                {
                    work();

                    mbResult = DialogResult.OK;
                }
                else
                {
                    mbResult = XtraMessageBox.Show("Server is not responding. Try again?\n(Click No to exit, Cancel to continue in offline mode)",
                        "Warning!",
                        MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Error);

                    if (mbResult == DialogResult.No) Environment.Exit(1);
                }
            } while (mbResult == DialogResult.Yes);
        }
    }
}
