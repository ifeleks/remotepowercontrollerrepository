﻿using System.ServiceModel;
using Common.DTO;
using Common.DTO.Enums;

namespace RPCServer.Interfaces
{
    [ServiceContract]
    public interface IClientInfoService
    {
        [OperationContract]
        void AddNewClientInfoForUser(UserInfoTO user, ClientInfoTO clientInfo, SettingsTO settings);

        [OperationContract]
        void UpdateClientState(State newState, ClientInfoTO clientInfo);

        [OperationContract]
        void ConfirmPowerAction(ClientInfoTO client, DefaultShutdownType powerAction);

        [OperationContract]
        void ChangeCommand(Command command, ClientInfoTO client);

        [OperationContract]
        bool IsMacExist(string macAddress, UserInfoTO userCredentials);
    }
}
