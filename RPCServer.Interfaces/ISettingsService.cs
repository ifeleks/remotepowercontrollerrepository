﻿using System.ServiceModel;
using Common.DTO;

namespace RPCServer.Interfaces
{
    [ServiceContract]
    public interface ISettingsService
    {
        [OperationContract]
        void AddNewSettings(SettingsTO settings, ClientInfoTO client);

        [OperationContract]
        void UpdateSettings(ClientInfoTO client, SettingsTO settings);

        [OperationContract]
        SettingsTO GetClientSettings(ClientInfoTO client);
    }
}
