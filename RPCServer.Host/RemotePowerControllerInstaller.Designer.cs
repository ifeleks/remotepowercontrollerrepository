﻿namespace RPCServer.Host
{
    partial class RemotePowerControllerInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RPCInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.RPCServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // RPCInstaller
            // 
            this.RPCInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.RPCInstaller.Password = null;
            this.RPCInstaller.Username = null;
            // 
            // RPCServiceInstaller
            // 
            this.RPCServiceInstaller.Description = "RPCService";
            this.RPCServiceInstaller.ServiceName = "RemotePowerController";
            this.RPCServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // RemotePowerControllerInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.RPCInstaller,
            this.RPCServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller RPCInstaller;
        private System.ServiceProcess.ServiceInstaller RPCServiceInstaller;
    }
}