﻿using System;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using Common.Logger;
using Common.Logger.Impl;
using Common.Service.Proxy;
using Web.Interfaces;

namespace RPCServer.Host.Threads_workers
{
    public class DomainChecker
    {
        volatile bool _isRunning;
        ILogger _logger;
        Thread _domainCheckerThread;
        object _domainCheckerLocker = new object();
        int _sleepInterval;

        public DomainChecker(int sleepInterval)
        {
            _logger = LoggerFactory.Current.GetLogger<DomainChecker>();
            _sleepInterval = sleepInterval;
            _isRunning = true;

            _domainCheckerThread = new Thread(CheckForAuthenticationRequest);
            _domainCheckerThread.Start();
        }

        private void CheckForAuthenticationRequest()
        {
            _logger.Trace("Domain checker thread started");
            #region privateKeyXML
            var rsaXml =
                "<RSAKeyValue><Modulus>q7ywp+Yu0DCUJn6L1/rZ1QjHCwyvT1BjkaROEluwvR8pC5SLBS9N1+sfNbA3wZo3DivW7OK+G6f0TB/LhO8BmtoH2QCzltrE33KOqMDqanwy7C+GG/nVr1IDgvLKMudsmtB3FIknLLuB5Um4tT4ps+cPjnYkboZKJTkuADi/D8c=</Modulus><Exponent>AQAB</Exponent><P>2s+qJhNkBuRWcZXyccl3eS0WLfH4rn0g+ujJ4dXUp2YyXFzV/P33QwoHqjmSXtbu1cjBlvKz8s+1enK1BZKWew==</P><Q>yOzcSYOdG6pb5nKxUm6hGcLrURtaryFTFXOkNCDagW6dqB1/2mhnz3IyMMKRALefJmyE89iKfHbALjD53bTwJQ==</Q><DP>Ma4007BTZb0yY1fIVE2J8tlpz8TA0d1XP6DbNYt3XQq3JIFvlKGVIoOEegHYWfAzChcq3Tr94IOIkOS6dSlzQQ==</DP><DQ>BQAYD2BOJaUcumR6U0HbkoeOUatrm1Yp+iuxl3aF2ZjGJavuRaI5iykdZKdC8pCfTlZ/DI6EC8rsx3mCjmj1XQ==</DQ><InverseQ>i/5ImxlA3N7TiLFgZzOMrzc1KyOwveyr83H22D/XJ9vXOP22J9kud36ere5FQk07FuI6r2En8fL4TTGMWVjy8A==</InverseQ><D>dcgkVBJvGsOmZ7w0hrz5Nr0dXbumZDwa/aD1FmwiC6LaUKHgsn6d4a7UqcqggrwUdDim37Imdes7hhXsS9BrhTuSbx8svpYnOcnODo+McNcM2M6NF/DjhFVjlNBalClcC27ZeaOJOsNoKwn4dAZ624d1iwujEHNISNlVs7BtGfk=</D></RSAKeyValue>";
            #endregion privateKeyXML

            while (_isRunning)
            {
                lock (_domainCheckerLocker)
                {
                    ServiceChannel<IDomainService>.For(webService =>
                    {
                        var authentificationAttempts = webService.GetDomainAuthentificationCommands(
                            ConfigurationManager.AppSettings["ServerSignature"]);

                        try
                        {
                            using (var context = new PrincipalContext(ContextType.Domain))
                            {
                                foreach (var attempt in authentificationAttempts)
                                {
                                    _logger.Trace("Get to check user {0} with hash {1}", attempt.UserName,
                                        attempt.PasswordCrypt);

                                    var rsa = new RSACryptoServiceProvider();
                                    rsa.FromXmlString(rsaXml);
                                    var password = Encoding.UTF8.GetString(rsa.Decrypt(
                                        Convert.FromBase64String(attempt.PasswordCrypt), false));

                                    attempt.IsValid = CheckCredentials(attempt.UserName, password);
                                    _logger.Trace("User {0} with hash {1} validation: {2}",
                                        attempt.UserName, attempt.PasswordCrypt, attempt.IsValid);

                                    webService.SetAuthentificationResult(attempt);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error("Error occured in domain checker", ex);
                        }
                    });
                }
                Thread.Sleep(_sleepInterval);
            }
        }

        public bool CheckCredentials(string name, string pass)
        {
            var isValid = false;
            if (name.Contains("\\"))
            {
                var split = name.Split('\\');
                name = split[1];
            }
            try
            {
                using (var context = new PrincipalContext(ContextType.Domain))
                {
                    isValid = context.ValidateCredentials(name, pass,
                        ContextOptions.Negotiate);
                    _logger.Trace("Validation of {0} ended", name);
                }
            }
            catch
            {
                _logger.Warn("No domain on this PC. Cant validate credentials");
            }
            return isValid;
        }

        public void Dispose()
        {
            _isRunning = false;
            _domainCheckerThread.Abort();
        }
    }
}
