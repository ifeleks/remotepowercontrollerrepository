﻿using System;
using System.Configuration;
using System.Threading;
using Common.Logger;
using Common.Logger.Impl;
using Common.Service.Proxy;
using Web.Interfaces;

namespace RPCServer.Host.Threads_workers
{
    public class CommandGetter : IDisposable
    {
        volatile bool _isRunning;
        ILogger _logger;
        Thread _commandGetThread;
        object _commandGetterLocker = new object();
        int _sleepInterval;

        CommandExecutor _executor;

        public CommandGetter(CommandExecutor executor, int sleepInterval)
        {
            _logger = LoggerFactory.Current.GetLogger<CommandGetter>();
            _executor = executor;
            _sleepInterval = sleepInterval;
            _isRunning = true;

            _commandGetThread = new Thread(CommandRequestSend);
            _commandGetThread.Start();
        }

        private void CommandRequestSend()
        {
            _logger.Trace("Command getter thread started");

            while (_isRunning)
            {
                lock (_commandGetterLocker)
                {
                    ServiceChannel<IClientInfoService>.For(webService =>
                    {
                        var webCommands = webService.GetClientsWithCommands(
                            ConfigurationManager.AppSettings["ServerSignature"]);

                        //var webCommands = new List<ClientInfoTO>();
                        //webCommands.Add(new ClientInfoTO{ClientName = "Client#1", Command = Command.TurnOn});
                        //webCommands.Add(new ClientInfoTO { ClientName = "Client#2", Command = Command.TurnOff });
                        //webCommands.Add(new ClientInfoTO { ClientName = "Client#3", Command = Command.GoToSleep });

                        _executor.EnqueueTask(webCommands);
                    });
                }
                Thread.Sleep(_sleepInterval);
            }
        }

        public void Dispose()
        {
            _isRunning = false;
            _commandGetThread.Abort();
        }
    }
}
