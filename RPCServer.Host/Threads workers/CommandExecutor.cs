﻿using System;
using System.Collections.Generic;
using System.Threading;
using Common.DTO;
using Common.DTO.Enums;
using Common.Logger;
using Common.Logger.Impl;
using Common.Service.Proxy;
using RPCServer.Host.Network;
using RPCServer.Host.Repositories;
using Web.Interfaces;

namespace RPCServer.Host.Threads_workers
{
    public class CommandExecutor : IDisposable // where T : ClientInfoTO
    {
        volatile bool _isRunning;
        ILogger _logger;
        object locker = new object();
        Thread[] executants;

        ICommandRepository _commandsRepo = new CommandListRepository();

        public CommandExecutor(int executorsCount)
        {
            _isRunning = true;
            executants = new Thread[executorsCount];        

            // Create and start a separate thread for each command
            for (int i = 0; i < executorsCount; i++)
            {
                (executants[i] = new Thread(Execute)).Start();
            }

            _logger = LoggerFactory.Current.GetLogger<CommandExecutor>();

            _logger.Trace("CommandExecutors threads started");
        }

        public void Dispose()
        {
            _isRunning = false;
            // Enqueue one null task per worker to make each exit.
            foreach (Thread executant in executants) EnqueueTask(null);
            foreach (Thread executant in executants) executant.Abort();
        }

        public void EnqueueTask(List<ClientInfoTO> commands)
        {
            lock (locker)
            {
                _commandsRepo.AddCommands(commands);
                Monitor.PulseAll(locker);
            }
        }

        void Execute()
        {
            while (_isRunning)
            {
                ClientInfoTO commandClient;
                lock (locker)
                {
                    while (_commandsRepo.IsEmpty())
                        Monitor.Wait(locker);
                    commandClient = _commandsRepo.GetCommand();
                }
                if (commandClient == null) 
                    return;         // This signals our exit

                try
                {
                    _logger.Trace("{0} to {1}({2}) is taken for execution", commandClient.Command, commandClient.ClientName, commandClient.ClientIp);
                    ServiceChannel<IClientInfoService>.For(webService =>
                    {
                        webService.ChangeCommand(Command.DoNothing, commandClient); //todo: change name of method and signature for REST
                        DeterminAndExecuteCommand(commandClient);
                    });
                }
                catch{}
                
            }
        }

        void DeterminAndExecuteCommand(ClientInfoTO commandClient)
        {
            switch (commandClient.Command)
            {
                case Command.TurnOn:
                    ClientManager.WakeUp(commandClient);
                    break;
                case Command.TurnOff:
                    ClientManager.TurnOff(commandClient);
                    break;
                case Command.GoToSleep:
                    ClientManager.Lullaby(commandClient);
                    break;
                default:
                    _logger.Trace("Wrong command recorded: {0}", commandClient.Command);
                    break;
            }
        }


    }
}
