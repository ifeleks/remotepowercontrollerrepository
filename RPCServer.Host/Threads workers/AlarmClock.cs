﻿using System;
using System.Timers;
using Common.Logger.Impl;
using Common.Service.Proxy;
using Web.Interfaces;

namespace RPCServer.Host
{
    public class AlarmClock : IDisposable
    {
        private Timer _alarmTimer;

        public AlarmClock(int interval)
        {
            _alarmTimer = new Timer(interval);
            _alarmTimer.AutoReset = true;
            _alarmTimer.Elapsed += AlarmTimerOnElapsed;
            _alarmTimer.Enabled = true;
            _alarmTimer.Start();
            var logger = LoggerFactory.Current.GetLogger<AlarmClock>();
            logger.Trace("AlarmClockWork started");
        }

        private void AlarmTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            var acceptableDelay = new TimeSpan(0, 5, 0);
            ServiceChannel<ISettingsService>.For(service => 
                service.AlarmClock(DateTime.Now.TimeOfDay, acceptableDelay));
        }

        public void Dispose()
        {
            _alarmTimer.Stop();
            _alarmTimer.Dispose();
            var logger = LoggerFactory.Current.GetLogger<AlarmClock>();
            logger.Trace("AlarmClockWork stopped");
        }
    }
}
