﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using Common.DTO;
using Common.DTO.Enums;
using Common.Logger;
using Common.Logger.Impl;
using Common.Service.Proxy;
using RPCServer.Host.Network;
using RPCServer.Host.Repositories;
using Web.Interfaces;

namespace RPCServer.Host.Threads_workers
{
    public class AsyncOnlineChecker : IDisposable
    {
        private const int PingTimeout = 300;
        const int PingRepeats = 2; //to reduce lost ping errors. Try 'twice' if fault
        private IClientsRepository _clientsRepo;
        volatile bool _isRunning;
        ILogger _logger;
        Thread _onlineCheckThread;
        object _onlineCheckLocker = new object();
        int _sleepInterval;

        public AsyncOnlineChecker(int sleepInterval)
        {
            _logger = LoggerFactory.Current.GetLogger<AsyncOnlineChecker>();
            _sleepInterval = sleepInterval;

            _isRunning = true;

            _onlineCheckThread = new Thread(OnlineCheck);
            _onlineCheckThread.Start();
        }

        public void OnlineCheck()
        {
            _logger.Trace("Online check thread started");

            _clientsRepo = new ClientsRepository();
            var serverSignature = ConfigurationManager.AppSettings["ServerSignature"];
            while (_isRunning)
            {
                lock (_onlineCheckLocker)
                {
                    ServiceChannel<IClientInfoService>.For(
                        webService => CheckAndUpdateStates(webService, serverSignature));
                    //_logger.Trace("OnlineStatus of {0} clients updated", serverSignature);
                }
                Thread.Sleep(_sleepInterval);
            }
        }

        private void CheckAndUpdateStates(IClientInfoService webService, string serverSignature)
        {
            var serverClients = webService.GetAllServerClients(serverSignature);

            //safe updating
            _clientsRepo.UpdateClientsCollection(serverClients);

            var clientsToCheck = _clientsRepo.GetClientsCollection();

            //Saving old states:
            var states = clientsToCheck.Select(client => client.State).ToList();

            var updatedClients = StatusCheck.IsOnlineAsync(clientsToCheck, PingTimeout);

            for (var i = 0; i < clientsToCheck.Count; i++)
            {
                //reducing fault ping:
                if (states[i] != updatedClients[i].State)
                {
                    var faultPing = (updatedClients[i].State == State.Off && states[i] == State.On || states[i] == State.OnWithoutClient) ||
                        (updatedClients[i].State == State.OnWithoutClient && states[i] == State.On);
                    if (faultPing)
                    {
                        updatedClients[i].FaultPingCount++;
                        if (updatedClients[i].FaultPingCount <= PingRepeats) continue;
                        webService.UpdateClientState(updatedClients[i].State, updatedClients[i]);
                        webService.ChangeCommand(Command.DoNothing, updatedClients[i]);
                        updatedClients[i].FaultPingCount = 0;
                    }
                    else
                    {
                        webService.UpdateClientState(updatedClients[i].State, updatedClients[i]);
                        webService.ChangeCommand(Command.DoNothing, updatedClients[i]);
                        updatedClients[i].FaultPingCount = 0;
                    }
                }
            }

            _clientsRepo.UpdateClientsCollection(updatedClients);
        }

        public void Dispose()
        {
            _isRunning = false;
            _onlineCheckThread.Abort();
        }
    }
}
