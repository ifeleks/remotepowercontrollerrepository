﻿using System;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.ComponentModel;
using Common.Logger;
using Common.Logger.Impl;

namespace RPCServer.Host.AutoDiscovery
{
    public class AutoDiscoveryServer
    {
        private int AutoDiscoveryPort;
        private byte[] IdentifyBytesPacket;
        private BackgroundWorker workerUDP;
        
        public IPAddress AddrDaemonListenIP;
        public int BroadCastDaemonPort;        

        private bool _disposing;
        private ILogger _logger;

        public AutoDiscoveryServer(ref BackgroundWorker workerUDP, string serverIP, int serverPort,
            int autoDiscoveryPort, byte[] identifyBytesPacket)
        {
            _logger = LoggerFactory.Current.GetLogger<AutoDiscoveryServer>();

            this.workerUDP = workerUDP;
            BroadCastDaemonPort = serverPort;
            AddrDaemonListenIP = IPAddress.Parse(serverIP);
            AutoDiscoveryPort = autoDiscoveryPort;
            IdentifyBytesPacket = identifyBytesPacket;
            //new byte[] {0x1, 0x2, 0x3}
        }

        public void Start(object sender, DoWorkEventArgs e)
        {
            try
            {                
                _logger.Trace("UDP Listening on {0} port started", AutoDiscoveryPort);
                var ReceivedData = new byte[1024];

                var localEndPoint = new IPEndPoint(IPAddress.Any, AutoDiscoveryPort);
                var remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);

                var newsock = new UdpClient(localEndPoint);

                ReceivedData = newsock.Receive(ref remoteEndPoint);

                var incomingIP = remoteEndPoint;

                while (!_disposing)
                {
                    if (ReceivedData.SequenceEqual(IdentifyBytesPacket))
                    {
                        _logger.Trace("Discovery founded. From {0}", incomingIP);

                        var packetBytesAck = Encoding.ASCII.GetBytes(string.Format("ACK {0}:{1}", AddrDaemonListenIP, BroadCastDaemonPort)); // Acknowledged

                        newsock.Send(packetBytesAck, packetBytesAck.Length, remoteEndPoint);

                        _logger.Trace("Answering(ACK) {0} bytes to {1}", IdentifyBytesPacket.Length, incomingIP);
                    }
                    else
                    {
                        // Unknown packet type received:
                        _logger.Trace("Answering(NAK) {0} bytes to {1}", IdentifyBytesPacket.Length, incomingIP);

                        var packetBytesNak = Encoding.ASCII.GetBytes("NAK"); // Not Acknowledged

                        newsock.Send(packetBytesNak, packetBytesNak.Length, remoteEndPoint);
                    }
                    ReceivedData = newsock.Receive(ref remoteEndPoint);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("UDP Listening error", ex);
            }
        }

        public void Stop()
        {
            _disposing = true;
        }
    }
}
