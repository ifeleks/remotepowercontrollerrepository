﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Common.Logger;
using Common.Logger.Impl;

namespace RPCServer.Host.AutoDiscovery
{
    public class AutoDiscoveryListener : IDisposable
    {
        ILogger _logger;
        BackgroundWorker _workerAD;
        AutoDiscoveryServer _svcAutoDiscovery;

        public AutoDiscoveryListener(int discoveryPort, byte[] identifyBytes)
        {
            _logger = LoggerFactory.Current.GetLogger<AutoDiscoveryListener>();
            _workerAD = new BackgroundWorker();

            _workerAD.WorkerReportsProgress = true;
            _workerAD.WorkerSupportsCancellation = true;

            var serverIP = GetHostIp();
            var serverPort = Convert.ToInt32(ConfigurationManager.AppSettings["ServerPort"]);
            _svcAutoDiscovery = new AutoDiscoveryServer(ref _workerAD, 
                serverIP, serverPort, discoveryPort, identifyBytes);
            
            _workerAD.DoWork += _svcAutoDiscovery.Start;
            _workerAD.ProgressChanged += logWorkers_ProgressChanged;
            _workerAD.RunWorkerAsync();
        }

        private void logWorkers_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //_logger.Trace("{0}",e.UserState.ToString());
        }

        private string GetHostIp()
        {
            var hostIP = "";
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork))
            {
                hostIP = ip.ToString();
            }
            return hostIP;
        }

        public void Dispose()
        {
            _svcAutoDiscovery.Stop();
            _workerAD.CancelAsync();
            _workerAD.Dispose();
        }
    }
}
