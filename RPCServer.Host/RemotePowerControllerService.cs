﻿using System;
using System.ServiceModel;
using System.ServiceProcess;
using Common.Logger;
using Common.Logger.Impl;
using RPCServer.Host.AutoDiscovery;
using RPCServer.Host.Threads_workers;
using RPCServer.Services;

namespace RPCServer.Host
{
    public partial class RemotePowerControllerService : ServiceBase
    {
        public ServiceHost UserInfoHost = null;
        public ServiceHost ClientInfoHost = null;
        public ServiceHost SettingsHost = null;

        private ILogger _logger;

        private AutoDiscoveryListener _discoveryListener;
        private CommandGetter _getter;
        private CommandExecutor _executor;
        private AsyncOnlineChecker _checker;
        private AlarmClock _alarmClock;
        private DomainChecker _domainChecker;

        public RemotePowerControllerService()
        {
            InitializeComponent();
            ServiceName = "RPCService";
        }

        protected override void OnStart(string[] args)
        {
            _logger = LoggerFactory.Current.GetLogger<RemotePowerControllerService>();
            _logger.Trace("RemotePowerController service started");

            OpenServices();

            var serverIdentify = new byte[] {0x1, 0x2, 0x3};
            _discoveryListener = new AutoDiscoveryListener(18500, serverIdentify);

            _executor = new CommandExecutor(10);
            _getter = new CommandGetter(_executor, 100);
            _checker = new AsyncOnlineChecker(100);
            _alarmClock = new AlarmClock(180000); //3 minutes
            _domainChecker = new DomainChecker(500);
        }

        protected override void OnStop()
        {
            _discoveryListener.Dispose();
            _getter.Dispose();
            _checker.Dispose();
            _alarmClock.Dispose();
            _executor.Dispose();
            _domainChecker.Dispose();

            CloseServices();

            _logger.Trace("RemotePowerController service stopped");
        }

        private void OpenServices()
        {
            try
            {
                if (UserInfoHost != null) UserInfoHost.Close();
                if (ClientInfoHost != null) ClientInfoHost.Close();
                if (SettingsHost != null) SettingsHost.Close();

                UserInfoHost = new ServiceHost(typeof (UserInfoService));
                UserInfoHost.Open();
                _logger.Trace("UserInfoService opened");

                ClientInfoHost = new ServiceHost(typeof (ClientInfoService));
                ClientInfoHost.Open();
                _logger.Trace("ClientInfoService opened");

                SettingsHost = new ServiceHost(typeof (SettingsService));
                SettingsHost.Open();
                _logger.Trace("SettingsService opened");
            }
            catch (Exception ex)
            {
                _logger.Error("Problem with starting WCF-service..", ex);
                this.Stop();
            }
        }

        private void CloseServices()
        {
            try
            {
                if (UserInfoHost != null) UserInfoHost.Close();
                UserInfoHost = null;
                _logger.Trace("UserInfoService closed");

                if (ClientInfoHost != null) ClientInfoHost.Close();
                ClientInfoHost = null;
                _logger.Trace("ClientInfoService closed");

                if (SettingsHost != null) SettingsHost.Close();
                SettingsHost = null;
                _logger.Trace("SettingsService closed");
            }
            catch (Exception ex)
            {
                _logger.Error("Can't stop WCF-service", ex);
                this.Stop();
            }
        }
    }
}
