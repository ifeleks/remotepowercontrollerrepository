﻿using System.Collections.Generic;
using System.Linq;
using Common.DTO;

namespace RPCServer.Host.Repositories
{
    public class CommandListRepository:ICommandRepository
    {
        private static List<ClientInfoTO> _collectionInstance;

        //public static Dictionary<Command, ClientInfoTO> CommandsInstance{get { return _collectionInstance; }}

        public CommandListRepository()
        {
            if (_collectionInstance == null)
            {
                _collectionInstance = new List<ClientInfoTO>();
            }
        }

        public void AddCommands(List<ClientInfoTO> commands)
        {
            if (commands == null) return;
            foreach (var command in commands)
            {
                _collectionInstance.Add(command);
            }
        }

        public ClientInfoTO GetCommand()
        {
            if (_collectionInstance.Count == 0) return null;
            var firstCommand = _collectionInstance.First();
            _collectionInstance.Remove(firstCommand);
            return firstCommand;
        }

        public bool IsEmpty()
        {
            return _collectionInstance.Count == 0;
        }

        public void Clear()
        {
            _collectionInstance.Clear();
        }
    }
}
