﻿using System.Collections.Generic;
using System.Linq;
using Common.DTO;

namespace RPCServer.Host.Repositories
{
    public class ClientsRepository : IClientsRepository
    {
        private static List<ClientInfoTO> _collectionInstance;

        public ClientsRepository()
        {
            if (_collectionInstance == null)
            {
                _collectionInstance = new List<ClientInfoTO>();
            }
        }
        
        public void AddClient(ClientInfoTO client)
        {
            _collectionInstance.Add(client);
        }

        public void UpdateClientsCollection(List<ClientInfoTO> clients)
        {
            //saving old values of pingCounter and StateWasChanged
            foreach (var instanceClient in _collectionInstance)
            {
                foreach (var client in clients.Where(client => client.MacAddress == instanceClient.MacAddress))
                {
                    client.FaultPingCount = instanceClient.FaultPingCount;
                }
            }
            _collectionInstance = clients;
        }

        public List<ClientInfoTO> GetClientsCollection()
        {
            return _collectionInstance;
        }
    }
}
