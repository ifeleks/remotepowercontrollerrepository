﻿using System.Collections.Generic;
using Common.DTO;

namespace RPCServer.Host.Repositories
{
    public interface IClientsRepository
    {
        void AddClient(ClientInfoTO client);
        void UpdateClientsCollection(List<ClientInfoTO> clients);
        List<ClientInfoTO> GetClientsCollection();
    }
}
