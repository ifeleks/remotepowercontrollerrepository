﻿using System.Collections.Generic;
using Common.DTO;

namespace RPCServer.Host.Repositories
{
    public interface ICommandRepository
    {
        void AddCommands(List<ClientInfoTO> commands);
        ClientInfoTO GetCommand();
        bool IsEmpty();
        void Clear();
    }
}