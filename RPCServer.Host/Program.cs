﻿using System;
using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;

namespace RPCServer.Host
{
    static class Program
    {

        static void Main(string[] args)
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new RemotePowerControllerService() 
            };

            if (Environment.UserInteractive)
            {
                var parameter = string.Concat(args);
                switch (parameter)
                {
                    case "--install":
                        ManagedInstallerClass.InstallHelper(new string[]
                        {Assembly.GetExecutingAssembly().Location});
                        break;
                    case "--uninstall":
                        ManagedInstallerClass.InstallHelper(new string[]
                        {"/u", Assembly.GetExecutingAssembly().Location});
                        break;
                }
            }
            else
            {
#if DEBUG
                System.Diagnostics.Debugger.Launch();
#endif
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
