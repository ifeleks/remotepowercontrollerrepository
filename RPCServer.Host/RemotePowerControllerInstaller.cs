﻿using System.ComponentModel;

namespace RPCServer.Host
{
    [RunInstaller(true)]
    public partial class RemotePowerControllerInstaller : System.Configuration.Install.Installer
    {
        public RemotePowerControllerInstaller()
        {
            InitializeComponent();
        }
    }
}
