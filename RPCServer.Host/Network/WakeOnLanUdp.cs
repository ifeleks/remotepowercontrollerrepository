﻿using System.Globalization;
using System.Net;
using System.Net.Sockets;

namespace RPCServer.Host.Network
{
    public class WakeOnLanUdp : UdpClient
    {
        public void SetClientToBrodcastMode()
            {
                if (Active)
                    Client.SetSocketOption(SocketOptionLevel.Socket,
                                              SocketOptionName.Broadcast, 0);
            }

            public static void WakeUp(string macAdress)
            {
                WakeOnLanUdp client = new WakeOnLanUdp();
                client.Connect(new IPAddress(0xffffffff), 0x0009); //Using port = 9
                client.SetClientToBrodcastMode();
                int counter = 0;
                //sending buffer
                byte[] bytes = new byte[1024];
                //First 6 bits is 0xFF
                for (int y = 0; y < 6; y++)
                    bytes[counter++] = 0xFF;
                //Repeat MACAdress 16 times
                for (int y = 0; y < 16; y++)
                {
                    int i = 0;
                    for (int z = 0; z < 6; z++)
                    {
                        bytes[counter++] = byte.Parse(macAdress.Substring(i, 2), NumberStyles.HexNumber);
                        i += 2;
                    }
                }

                //Sending magic packet
                int returnedValue = client.Send(bytes, 1024);
            }
    }
}
