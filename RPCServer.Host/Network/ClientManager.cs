﻿using System.Configuration;
using System.ServiceModel;
using Client.Interface;
using Common.DTO;
using Common.Logger.Impl;
using Common.Service.Proxy;
using RPCServer.Host.Threads_workers;

namespace RPCServer.Host.Network
{
    public static class ClientManager
    {

        public static void WakeUp(ClientInfoTO client)
        {
            WakeOnLanUdp.WakeUp(client.MacAddress);
            var logger = LoggerFactory.Current.GetLogger<CommandExecutor>();
            logger.Trace("Magic packet was send to {0}", client.MacAddress);
        }

        public static void TurnOff(ClientInfoTO client)
        {
            var clientEndpoint = new EndpointAddress(
                string.Format("http://{0}:{1}/PowerManagementService",
                    client.ClientIp, ConfigurationManager.AppSettings["ClientPort"]));

            ServiceChannel<IPowerManagementService>.For(clientEndpoint,
                powerManagementService => powerManagementService.TurnOff());
        }

        public static void Lullaby(ClientInfoTO client)
        {
            var clientEndpoint = new EndpointAddress(
                string.Format("http://{0}:{1}/PowerManagementService",
                    client.ClientIp, ConfigurationManager.AppSettings["ClientPort"]));

            ServiceChannel<IPowerManagementService>.For(clientEndpoint,
                powerManagementService => powerManagementService.Sleep());
        }
    }
}
