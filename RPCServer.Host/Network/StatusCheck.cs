﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Net.NetworkInformation;
using Common.Availableness;
using Common.DTO;
using Common.DTO.Enums;

namespace RPCServer.Host.Network
{
    public static class StatusCheck
    {
        public static State IsOnline(ClientInfoTO client)
        {
            var ping = new Ping();
            var reply = ping.Send(client.ClientIp);
            switch (reply.Status)
            {
                case IPStatus.Success:
                    return State.On;
                default:
                    return State.Off;
            }
        }

        public static List<State> IsOnline(List<ClientInfoTO> clients)
        {
            var onlineList = new List<State>();
            foreach (var client in clients)
            {
                onlineList.Add(IsOnline(client));
            }
            return onlineList;
        }

        public static List<ClientInfoTO> IsOnlineAsync(List<ClientInfoTO> clients, int pingTimeout)
        {
            var pingTasks = clients.Select(client =>
                SendPingAsync(client, pingTimeout)).ToList();

            Task.WaitAll(pingTasks.ToArray());

            for (var i = 0; i < clients.Count; i++)
            {
                clients[i].State = pingTasks[i].Result;
            }

            return clients;
        }

        static Task<State> SendPingAsync(ClientInfoTO client, int timeout)
        {
            var tcs = new TaskCompletionSource<State>();
            Ping ping = new Ping();
            ping.PingCompleted += (obj, sender) =>
            {
                if (sender.Reply.Status == IPStatus.Success)
                {
                    if (client.State == State.Off)
                    {
                        tcs.SetResult(State.OnWithoutClient);
                    }
                    else
                    {
                        tcs.SetResult(IsServiceAvailble(client.ClientIp, timeout) ?
                            State.On : State.OnWithoutClient);
                    }
                }
                else
                {
                    tcs.SetResult(State.Off);
                }
                //var Status == IPStatus.Success ? State.On : State.Off
                //tcs.SetResult(sender.Reply);               
            };
            
            ping.SendAsync(client.ClientIp, timeout, new object());
            return tcs.Task;
        }

        private static bool IsServiceAvailble(string clientAddress, int timeout)
        {
            var clientEndpoint = string.Format("http://{0}:{1}/PowerManagementService",
                    clientAddress, ConfigurationManager.AppSettings["ClientPort"]);

            return ServiceAvailability.IsOnline(clientEndpoint, timeout);
        }
    }
}
