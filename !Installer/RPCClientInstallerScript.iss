#define MyAppName "Remote Power Controller"
#define MyAppVersion "1.0.29"
#define MyAppPublisher "Eleks Software"
#define MyAppCopyright "Copyright (C) 1999-2014 ELEKS"
#define MyAppURL "http://www.eleks.com/"
#define MyAppExeName "RPCClient.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId=4119F9C1-0D01-4CC2-87A6-3EB8DD627E08
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
AppCopyright={#MyAppCopyright}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
AllowNoIcons=yes
OutputDir=D:\C# Development\Remote Power Controller\Remote Power Controller Installer
OutputBaseFilename=RPCClient_setup
SetupIconFile=D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\power.ico
Compression=lzma
SolidCompression=yes
PrivilegesRequired=admin

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"

[Files]
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\RPCClient.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\AddExceptions.bat"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\Cassia.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\Client.Interface.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\Client.Service.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\Common.Availableness.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\Common.DTO.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\Common.Keys.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\Common.Logger.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\Common.PluginsInterface.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\Common.Service.Proxy.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\DeleteExceptions.bat"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\DevExpress.Data.v14.1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\DevExpress.Utils.v14.1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\DevExpress.XtraEditors.v14.1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\DevExpress.XtraGrid.v14.1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\NLog.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\NLog.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\power.ico"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\RPCClient.exe.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\RPCServer.Interfaces.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\Shared.Audio.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\Shared.Keyboard.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\Shared.ProcessRestoring.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\plugins\Common.ChromePlugin.dll"; DestDir: "{app}\plugins"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\plugins\Common.ExcelPlugin.dll"; DestDir: "{app}\plugins"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\plugins\Common.FirefoxPlugin.dll"; DestDir: "{app}\plugins"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\plugins\Common.InternetExplorerPlugin.dll"; DestDir: "{app}\plugins"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\plugins\Common.OperaPlugin.dll"; DestDir: "{app}\plugins"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\plugins\Common.PluginsInterface.dll"; DestDir: "{app}\plugins"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\plugins\Common.VisualStudioPlugin.dll"; DestDir: "{app}\plugins"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\plugins\Common.WordPlugin.dll"; DestDir: "{app}\plugins"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\bin\Release\plugins\NDde.dll"; DestDir: "{app}\plugins"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\RPCClient\Properties\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\Cassia.dll"; DestDir: "{app}\WinService"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\Client.Interface.dll"; DestDir: "{app}\WinService"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\Client.Service.dll"; DestDir: "{app}\WinService"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\Client.WinService.exe"; DestDir: "{app}\WinService"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\Client.WinService.exe.config"; DestDir: "{app}\WinService"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\Common.DTO.dll"; DestDir: "{app}\WinService"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\Common.Logger.dll"; DestDir: "{app}\WinService"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\Common.PluginsInterface.dll"; DestDir: "{app}\WinService"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\Common.Service.Proxy.dll"; DestDir: "{app}\WinService"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\NLog.config"; DestDir: "{app}\WinService"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\NLog.dll"; DestDir: "{app}\WinService"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\RPCServer.Interfaces.dll"; DestDir: "{app}\WinService"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\Shared.Audio.dll"; DestDir: "{app}\WinService"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\Shared.Keyboard.dll"; DestDir: "{app}\WinService"; Flags: ignoreversion
Source: "D:\C# Development\Remote Power Controller\remotepowercontrollerrepository\Client.WinService\bin\Release\Shared.ProcessRestoring.dll"; DestDir: "{app}\WinService"; Flags: ignoreversion

[Icons]
Name: "{commonstartup}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Registry]   
Root: "HKLM"; Subkey: "SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers\"; ValueType: String; ValueName: "{app}\RPCClient.exe"; ValueData: "RUNASADMIN"; Flags: uninsdeletekeyifempty uninsdeletevalue; MinVersion: 0,6.1
Root: "HKCU"; Subkey: "SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers\"; ValueType: String; ValueName: "{app}\RPCClient.exe"; ValueData: "RUNASADMIN"; Flags: uninsdeletekeyifempty uninsdeletevalue; MinVersion: 0,6.1

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
Filename: "{sys}\netsh.exe"; Parameters: "firewall add allowedprogram ""{app}\RPCClient.exe"" ""Remote Power Controller"" ENABLE ALL";     StatusMsg: "Addidng to firewall exception..."; Flags: runhidden; MinVersion: 0,5.01.2600sp2;
Filename: "{app}\AddExceptions.bat"; Flags: runhidden
Filename: "{app}\WinService\Client.WinService.exe"; Parameters: "--install"

[UninstallRun]   
Filename: "{cmd}"; Parameters: "/C ""taskkill /im RPCClient.exe /f /t"
Filename: "{app}\WinService\Client.WinService.exe"; Parameters: "--uninstall"
Filename: "{app}\DeleteExceptions.bat"; Flags: runhidden
Filename: "{sys}\netsh.exe"; Parameters: "firewall delete allowedprogram program=""{app}\RPCClient.exe"""; Flags: runhidden;     MinVersion: 0,5.01.2600sp2;
          
[UninstallDelete]
Type: filesandordirs; Name: "{localappdata}\RPCClient"

[CustomMessages]
OptionsFormCaption=Setup options...
RepairButtonCaption=Repair
UninstallButtonCaption=Uninstall

[Code]
const
  mrRepair = 100;
  mrUninstall = 101;

function ShowOptionsForm: TModalResult;
var
  OptionsForm: TSetupForm;
  RepairButton: TNewButton;
  UninstallButton: TNewButton;
begin
  Result := mrNone;
  OptionsForm := CreateCustomForm;
  try
    OptionsForm.Width := 220;
    OptionsForm.Caption := ExpandConstant('{cm:OptionsFormCaption}');
    OptionsForm.Position := poScreenCenter;

    RepairButton := TNewButton.Create(OptionsForm);
    RepairButton.Parent := OptionsForm;
    RepairButton.Left := 8;
    RepairButton.Top := 8;
    RepairButton.Width := OptionsForm.ClientWidth - 16;
    RepairButton.Caption := ExpandConstant('{cm:RepairButtonCaption}');
    RepairButton.ModalResult := mrRepair;

    UninstallButton := TNewButton.Create(OptionsForm);
    UninstallButton.Parent := OptionsForm;
    UninstallButton.Left := 8;
    UninstallButton.Top := RepairButton.Top + RepairButton.Height + 8;
    UninstallButton.Width := OptionsForm.ClientWidth - 16;
    UninstallButton.Caption := ExpandConstant('{cm:UninstallButtonCaption}');
    UninstallButton.ModalResult := mrUninstall;

    OptionsForm.ClientHeight := RepairButton.Height + UninstallButton.Height + 24;
    Result := OptionsForm.ShowModal;
  finally
    OptionsForm.Free;
  end;
end;

function GetUninstallerPath: string;
var
  RegKey: string;
begin
  Result := '';
  RegKey := Format('%s\%s_is1', ['Software\Microsoft\Windows\CurrentVersion\Uninstall', 
    '{#emit SetupSetting("AppId")}']);
  if not RegQueryStringValue(HKEY_LOCAL_MACHINE, RegKey, 'UninstallString', Result) then
    RegQueryStringValue(HKEY_CURRENT_USER, RegKey, 'UninstallString', Result);
end;

function InitializeSetup: Boolean;
var
  UninstPath: string;
  ResultCode: Integer;  
begin
  Result := True;
  UninstPath := RemoveQuotes(GetUninstallerPath);
  if UninstPath <> '' then
  begin
    case ShowOptionsForm of
      mrRepair: Result := True;
      mrUninstall: 
      begin
        Result := False;
        if not Exec(UninstPath, '', '', SW_SHOW, ewNoWait, ResultCode) then
          MsgBox(FmtMessage(SetupMessage(msgUninstallOpenError), [UninstPath]), mbError, MB_OK);
      end;
    else
      Result := False;
    end;
  end;
end;