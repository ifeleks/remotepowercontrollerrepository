﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using EnvDTE80;

public class CurrentIde
{
    [DllImport("ole32.dll")]
    private static extern void CreateBindCtx(int reserved, out IBindCtx ppbc);

    [DllImport("ole32.dll")]
    private static extern void GetRunningObjectTable(int reserved, out IRunningObjectTable prot);

    internal static DTE2 GetCurrent(Process currentDevenv)
    {
        //rot entry for visual studio running under current process.
        var rotEntry1 = "!VisualStudio.DTE.";
        var rotEntry2 = String.Format(".0:{0}", currentDevenv.Id);
        IRunningObjectTable rot;
        GetRunningObjectTable(0, out rot);
        IEnumMoniker enumMoniker;
        rot.EnumRunning(out enumMoniker);
        enumMoniker.Reset();
        IntPtr fetched = IntPtr.Zero;
        IMoniker[] moniker = new IMoniker[1];
        while (enumMoniker.Next(1, moniker, fetched) == 0)
        {
            IBindCtx bindCtx;
            CreateBindCtx(0, out bindCtx);
            string displayName;
            moniker[0].GetDisplayName(bindCtx, null, out displayName);
            if (displayName.Contains(rotEntry1) && displayName.Contains(rotEntry2))
            {
                object comObject;
                rot.GetObject(moniker[0], out comObject);
                return (EnvDTE80.DTE2) comObject;
            }
        }
        return null;
    }
}