﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Common.PluginsInterface;
using EnvDTE;
using Process = System.Diagnostics.Process;

namespace Common.VisualStudioPlugin
{
    [Export(typeof(IOpenerPlugin))]
    public class VisualStudioOpener : IOpenerPlugin
    {
        private readonly List<string> _applicationNames;
        private readonly List<string> _supportedExtensions;

        VisualStudioOpener()
        {
            _applicationNames = new List<string>
            {
                "devenv", 
                "DEVENV"
            };
            _supportedExtensions = new List<string>
            {
                "sln", "cs",
                "csproj", "js"
            };
        }

        public bool IsSuitableForProcess(string processName)
        {
            return _applicationNames.Any(processName.Equals);
        }

        public IEnumerable<string> GetSupportedExtensions()
        {
            return _supportedExtensions;
        }

        public string GetProcessLine(Process process)
        {
            //EnvDTE.DTE dte = (EnvDTE.DTE)System.Runtime.InteropServices.Marshal.GetActiveObject("VisualStudio.DTE");
            //string solutionDir = System.IO.Path.GetDirectoryName(dte.Solution.FullName);

            var IDE = CurrentIde.GetCurrent(process);
            var solutionDir = IDE.Solution.FullName;
            var result = new StringBuilder();
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(process.MainModule.FileName);
            result.Append(fileVersionInfo.FileDescription + "\t" + process.ProcessName +
                "\t" + process.MainModule.FileName + "\t" + true + "\t");
            foreach (var supportedExtension in _supportedExtensions)
            {
                result.Append("." + supportedExtension);
            }
            result.Append("\t" + solutionDir);

            return result.ToString();
        }

        public void OpenFiles(string processPath, List<string> filePaths)
        {
            if (filePaths.Any())
            {
                var arguments = filePaths.Aggregate(string.Empty,
                    (current, filePath) => current + ("\t\"" + filePath + "\""));
                Process.Start(processPath, arguments);
            }
            else
            {
                Process.Start(processPath);
            }
        }
    }
}
