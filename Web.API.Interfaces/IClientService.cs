﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using Common.DTO;
using Common.DTO.Enums;

namespace Web.API.Interfaces
{
    [ServiceContract]
    public interface IClientService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ClientInfoTO> GetUserClients(UserInfoTO user);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetServerClients/{serverSignature}")]
        List<ClientInfoTO> GetServerClients(string serverSignature);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        void DeleteClient(ClientInfoTO client);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<string> GetDetails(ClientInfoTO client);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        void ExecuteCommand(Command command, ClientInfoTO client, UserInfoTO author);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json, UriTemplate = "TurnON={clientId}/{cryptedUserName}/{cryptedUserPassword}")]
        void SendMagicPacket(string clientId, string cryptedUserName, string cryptedUserPassword);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json, UriTemplate = "CreateRDPConnection={clientId}/{cryptedUserName}/{cryptedUserPassword}")]
        void CreateRDPConnection(string clientId, string cryptedUserName, string cryptedUserPassword);
    }
}
