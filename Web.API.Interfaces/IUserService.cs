﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using Common.DTO;

namespace Web.API.Interfaces
{
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool LogIn(UserInfoTO user);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        void AddToFavorites(UserInfoTO user, ClientInfoTO favoriteClient);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        void DeleteFavorite(UserInfoTO user, ClientInfoTO favoriteClient);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ClientInfoTO> GetFavorites(UserInfoTO user);
    }
}
