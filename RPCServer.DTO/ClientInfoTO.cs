﻿using System;
using System.Runtime.Serialization;
using Common.DTO.Enums;

namespace Common.DTO
{
    [DataContract]
    public class ClientInfoTO
    {
        [DataMember]
        public string ClientName { get; set; }
        [DataMember]
        public string ClientFullName { get; set; }
        [DataMember]
        public string MacAddress { get; set; }
        [DataMember]
        public string ClientIp { get; set; }
        [DataMember]
        public string GetawayIp { get; set; }
        [DataMember]
        public string ServerSignature { get; set; }
        [DataMember]
        public DateTime InstallationDate { get; set; }
        [DataMember]
        public State State { get; set; }
        [DataMember]
        public Command Command { get; set; }
        public int FaultPingCount { get; set; }
    }
}
