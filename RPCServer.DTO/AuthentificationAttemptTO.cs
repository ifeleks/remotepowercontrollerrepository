﻿using System.Runtime.Serialization;

namespace Common.DTO
{
    [DataContract]
    public class AuthentificationAttemptTO
    {
        [DataMember]
        public int Id;

        [DataMember]
        public string UserName;

        [DataMember]
        public string PasswordCrypt;

        [DataMember]
        public bool? IsValid;

        [DataMember]
        public int ServerId;
    }
}
