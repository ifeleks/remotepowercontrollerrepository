﻿using System;
using System.Runtime.Serialization;
using Common.DTO.Enums;

namespace Common.DTO
{
    [DataContract]
    public class SettingsTO
    {
        [DataMember]
        public TimeSpan WorkTimeStart { get; set; }
        [DataMember]
        public TimeSpan WorkTimeEnd { get; set; }
        [DataMember]
        public int IdleTime { get; set; }
        [DataMember]
        public DefaultShutdownType ShutDownMode { get; set; }
    }
}
