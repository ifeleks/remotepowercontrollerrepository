﻿namespace Common.DTO.Enums
{
    public enum State
    {
        Off, On, OnWithoutClient, Sleep, InProcess, ServerInaccessible
    }
}
