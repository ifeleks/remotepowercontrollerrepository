﻿namespace Common.DTO.Enums
{
    public enum Command
    {
        DoNothing, InProcess, TurnOn, TurnOff, GoToSleep
    }
}
