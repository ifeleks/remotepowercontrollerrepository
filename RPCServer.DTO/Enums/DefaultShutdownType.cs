﻿namespace Common.DTO.Enums
{
    public enum DefaultShutdownType
    {
        Off, Sleep
    }
}
