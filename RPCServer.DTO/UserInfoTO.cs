﻿using System.Runtime.Serialization;

namespace Common.DTO
{
    [DataContract]
    public class UserInfoTO
    {
        [DataMember]
        public string Login { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public bool IsDomain { get; set; }
    }
}
