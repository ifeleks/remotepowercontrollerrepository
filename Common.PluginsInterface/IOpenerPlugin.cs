﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Common.PluginsInterface
{
    public interface IOpenerPlugin
    {
        bool IsSuitableForProcess(string processName);
        IEnumerable<string> GetSupportedExtensions(); 
        string GetProcessLine(Process process);
        void OpenFiles(string processPath, List<string> filePaths);
    }
}
