﻿using System.Collections.Generic;
using System.Linq;
using Common.DTO;
using RemotePowerControllerTestCore.Entities;
using RemotePowerControllerTestCore.Repositories.Implementations;
using Web.API.Interfaces;

namespace Web.API.Services
{
    public class UserService : IUserService
    {
        public bool LogIn(UserInfoTO userTO)
        {
            var userRepo = new GenericRepository<User>();
            if (!IsLoginValid(userTO)) return false;
            var user = userRepo.Get(u => u.Username == userTO.Login).First();
            return user.Password == userTO.Password;
        }

        public bool IsLoginValid(UserInfoTO userInfo)
        {
            var userRepo = new GenericRepository<User>();
            var user = userRepo.Get(u => u.Username == userInfo.Login).FirstOrDefault();
            return user != null;
        }

        public void AddToFavorites(UserInfoTO user, ClientInfoTO favoriteClientTO)
        {
            var clientRepo = new GenericRepository<Client>();
            var favoriteClient = clientRepo.Get(c => c.MACAddress == favoriteClientTO.MacAddress).First();
            var userRepo = new GenericRepository<User>();
            var currentUser = userRepo.Get(u => u.Username == user.Login).First();
            var favoriteRepo = new GenericRepository<Favorite>();

            favoriteRepo.Insert(new Favorite {UserId = currentUser.UserId, ClientId = favoriteClient.ClientId});
        }

        public void DeleteFavorite(UserInfoTO user, ClientInfoTO favoriteClientTO)
        {
            var clientRepo = new GenericRepository<Client>();
            var favoriteClient = clientRepo.Get(c => c.MACAddress == favoriteClientTO.MacAddress).First();
            var userRepo = new GenericRepository<User>();
            var currentUser = userRepo.Get(u => u.Username == user.Login).First();
            var favoriteRepo = new GenericRepository<Favorite>();

            favoriteRepo.Delete(new Favorite {UserId = currentUser.UserId, ClientId = favoriteClient.ClientId});
        }

        public List<ClientInfoTO> GetFavorites(UserInfoTO user)
        {
            var userRepo = new GenericRepository<User>();
            var currentUser = userRepo.Get(u => u.Username == user.Login).First();
            var favoriteRepo = new GenericRepository<Favorite>();
            var favoriteList = favoriteRepo.Get(f => f.UserId == currentUser.UserId);
            var clientRepo = new GenericRepository<Client>();
            return favoriteList.Select(favorite => clientRepo.GetByID(favorite.ClientId))
                .Select(Client.ConvertToTO).ToList();
        }
    }
}
