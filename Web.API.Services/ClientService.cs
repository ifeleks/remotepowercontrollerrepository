﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;
using Common.Ciphers;
using Common.CustomUrlCoder;
using Common.DTO;
using Common.DTO.Enums;
using RemotePowerControllerTestCore.Entities;
using RemotePowerControllerTestCore.Repositories.Implementations;
using Web.API.Interfaces;

namespace Web.API.Services
{
    public class ClientService : IClientService
    {
        const string RpcAddress = "localhost:50952";
        const string LocalAddress = "rpc.somee.com";

        public List<ClientInfoTO> GetUserClients(UserInfoTO user)
        {
            var userRepo = new GenericRepository<User>();
            var currentUser = userRepo.Get(u => u.Username == user.Login).First();
            var clientRepo = new GenericRepository<Client>();
            var userClients = clientRepo.Get(c => c.UserId == currentUser.UserId);
            return userClients.Select(Client.ConvertToTO).ToList();
        }

        public List<ClientInfoTO> GetServerClients(string serverSignature)
        {
            var serverRepo = new GenericRepository<Server>();
            var server = serverRepo.Get(s => s.ServerName == serverSignature).FirstOrDefault();
            if (server!=null)
            {
                var clientRepo = new GenericRepository<Client>();
                var serverClients = clientRepo.Get(c => c.ServerId == server.ServerId).ToList();
                return serverClients.Select(Client.ConvertToTO).ToList();
            }
            return new List<ClientInfoTO>();
        }

        public void DeleteClient(ClientInfoTO client)
        {
            var clientRepo = new GenericRepository<Client>();
            var currentClient = clientRepo.Get(c => c.MACAddress == client.MacAddress).First();
            clientRepo.Delete(currentClient.ClientId);
        }

        public List<string> GetDetails(ClientInfoTO client)
        {
            throw new NotImplementedException();
        }

        public void ExecuteCommand(Command command, ClientInfoTO client, UserInfoTO author)
        {
            //todo: add author signature for command
            var clientRepo = new GenericRepository<Client>();
            var currentClient = clientRepo.Get(c => c.MACAddress == client.MacAddress).First();
            var clientStateRepo = new GenericRepository<ClientState>();
            var clientStateToSwitchCommand = clientStateRepo.GetByID(currentClient.ClientId);
            clientStateToSwitchCommand.Command = command;
            clientStateRepo.Update(clientStateToSwitchCommand);
        }

        public void SendMagicPacket(string clientId, string cryptedUserName, string cryptedUserPassword)
        {
            var decodedUserName = CustomUrlCoder.Decode(cryptedUserName);
            var userName = StringCipher.Decrypt(decodedUserName, Common.Keys.Passwords.UserNameCipherKey); 
            var decodedUserPassword = CustomUrlCoder.Decode(cryptedUserPassword);
            var userPass = StringCipher.Decrypt(decodedUserPassword, Common.Keys.Passwords.PasswordCipherKey);          
            var isLoginCorrect = (new UserService()).LogIn(new UserInfoTO {Login = userName, Password = userPass});
            if (isLoginCorrect)
            {
                var clientRepo = new GenericRepository<Client>();
                var currentClient = clientRepo.GetByID(Convert.ToInt32(clientId));
                var clientStateRepo = new GenericRepository<ClientState>();
                var clientStateToSwitchCommand = clientStateRepo.GetByID(currentClient.ClientId);
                clientStateToSwitchCommand.Command = Command.TurnOn;
                clientStateRepo.Update(clientStateToSwitchCommand);

                RedirectToSuccessPage(clientId);
            }
            else
            {
                RedirectToFailPage();
            }
        }

        private void RedirectToFailPage()
        {
            try
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Redirect;
                WebOperationContext.Current.OutgoingResponse.Location = "http://rpc.somee.com/";
            }
            catch { } //means that there is no HttpContext and someone use this API not grom browser
        }

        private void RedirectToSuccessPage(string clientId)
        {
            try
            {
                var targetUrl = String.Format("http://{0}/Client/TurningOn?clientId={1}", LocalAddress, clientId);
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Redirect;
                WebOperationContext.Current.OutgoingResponse.Location = targetUrl;
            } catch{} //means that there is no HttpContext and someone use this API not from browser
        }

        public void CreateRDPConnection(string clientId, string cryptedUserName, string cryptedUserPassword)
        {
            var decodedUserName = CustomUrlCoder.Decode(cryptedUserName);
            var userName = StringCipher.Decrypt(decodedUserName, Common.Keys.Passwords.UserNameCipherKey);
            var decodedUserPassword = CustomUrlCoder.Decode(cryptedUserPassword);
            var userPass = StringCipher.Decrypt(decodedUserPassword, Common.Keys.Passwords.PasswordCipherKey);
            var isLoginCorrect = (new UserService()).LogIn(new UserInfoTO { Login = userName, Password = userPass });
            if (isLoginCorrect)
            {
                var clientRepo = new GenericRepository<Client>();
                var currentClient = clientRepo.GetByID(Convert.ToInt32(clientId));
                var clientStateRepo = new GenericRepository<ClientState>();
                var clientStateToSwitchCommand = clientStateRepo.GetByID(currentClient.ClientId);
                clientStateToSwitchCommand.Command = Command.TurnOn;
                clientStateRepo.Update(clientStateToSwitchCommand);
                
                /*using (var wb = new WebClient())
                {
                    var data = new NameValueCollection();
                    data["Username"] = userName;
                    data["Password"] = userPass;

                    var response = wb.UploadValues(string.Format("http://{0}/Home/CustomLogin", LocalAddress), "POST", data);
                }*/

                //todo: redirect to page with timer
                var targetUrl = String.Format("http://{0}/Home/TurnOnAndRD?clientId={1}&param1={2}&param2={3}", LocalAddress, clientId, cryptedUserName, cryptedUserPassword);
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Redirect;
                WebOperationContext.Current.OutgoingResponse.Location = targetUrl;
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Redirect;
                WebOperationContext.Current.OutgoingResponse.Location = "http://rpc.somee.com/";
            }
        }
    }
}
