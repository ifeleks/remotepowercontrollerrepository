﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Common.PluginsInterface;
using Photoshop;

namespace Common.PhotoshopPlugin
{
    [Export(typeof(IOpenerPlugin))]
    public class PhotoshopOpener : IOpenerPlugin
    {
        private readonly List<string> _applicationNames;

        PhotoshopOpener()
        {
            _applicationNames = new List<string>
            {
                "Photoshop"
            };
        }

        public bool IsSuitableForProcess(string processName)
        {
            return _applicationNames.Any(processName.Equals);
        }

        public string GetProcessLine(Process process)
        {
            dynamic app = Activator.CreateInstance(Type.GetTypeFromProgID("Photoshop.Application"));
            //var psApp = new ApplicationClass();
            
            //var docs = psApp.Documents;
            //var docA = psApp.ActiveDocument.FullName;
            
            var result = new StringBuilder();
            result.Append(process.Id + "\t" + process.ProcessName + "\t" + process.MainModule.FileName);

            //foreach (Document doc in docs)
            //{
            //    result.Append("\t" + doc.FullName);
            //}

            return result.ToString();
        }

        public void OpenFiles(List<string> processParams)
        {
            if (processParams.Count() > 2)
            {
                var arguments = string.Empty;
                for (var i = 3; i < processParams.Count; i++)
                {
                    arguments += " \"" + processParams[i] + "\"";
                }
                Process.Start(processParams[2], arguments);
            }
            else
            {
                Process.Start(processParams[2]);
            }
        }
    }
}
