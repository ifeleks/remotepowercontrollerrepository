﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Shared.ProcessRestoring
{
    public static class ProcessManager
    {
        public static IEnumerable<Process> GetCurrentAppsSession()
        {
            var processes = Process.GetProcesses().ToList();
            var processesThatCurrentlyRunning = new List<Process>();
            foreach (var process in processes)
            {
                if (string.IsNullOrEmpty(process.MainWindowTitle)) continue; //user apps filter
                try
                {
                    var tryToGetName = process.MainModule.FileName;
                    processesThatCurrentlyRunning.Add(process);
                }
                catch
                {
                }
            }
            return processesThatCurrentlyRunning;
        }

        public static string GetAppName(string filePath)
        {
            try
            {
                var fileVersionInfo = FileVersionInfo.GetVersionInfo(filePath);
                return fileVersionInfo.FileDescription;
            }
            catch
            {
                return "Corrupted file";
            }
        }
    }
}
