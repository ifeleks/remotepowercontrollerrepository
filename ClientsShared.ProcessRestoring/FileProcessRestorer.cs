﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Common.Logger.Impl;
using Common.PluginsInterface;
using DevExpress.XtraPrinting.Native;
using Shared.ProcessRestoring.InformForms;

namespace Shared.ProcessRestoring
{
    public class FileProcessRestorer : IProcessRestorer
    {
        [ImportMany]
        private IOpenerPlugin[] Openers { get; set; }

        private const string FilePath = "save/processList.txt";

        public void CreateSavePoint()
        {
            var processes = ProcessManager.GetCurrentAppsSession();
            ComposePlugins(); //Load plugins to "Openers"
            var lines = processes.Select(GetProcessLine).ToList();

            if (!Directory.Exists("save"))
                Directory.CreateDirectory("save");
            File.WriteAllLines(FilePath, lines);
        }

        public void CreateSavePoint(IEnumerable<string> processesWithParameters)
        {
            if (!Directory.Exists("save"))
                Directory.CreateDirectory("save");
            File.WriteAllLines(FilePath, processesWithParameters);
        }

        public void LoadLastPoint()
        {      
            ComposePlugins(); //Load plugins to "Openers"
            var processesToRestore = GetProcessesToRestore().ToList();

            ShowProgressBar(processesToRestore, 30);
            RunLoadedPrograms(processesToRestore);
        }

        private void ShowProgressBar(List<string> processesToRestore, int timeoutInSec)
        {
            if (processesToRestore.IsEmpty()) return;
            var progressForm = new LoadSessionProgressBar(processesToRestore, timeoutInSec);
            progressForm.Show();
        }

        private IEnumerable<string> GetProcessesToRestore()
        {
            var resultList = new List<string>();
            if (!IsLoadPossible() || !ConfirmLoading()) return resultList;

            var savedProcesses = GetProcessList();
            var currentAppsSession = ProcessManager.GetCurrentAppsSession();

            resultList.AddRange(
                from fileLine in savedProcesses
                let existCounter = 
                (from process in currentAppsSession
                 let processFileName = fileLine.Split('\t')[2]
                 where processFileName.Equals(process.MainModule.FileName)
                 select process).Count()
                where existCounter == 0
                select fileLine);
            return resultList;
        }

        public IEnumerable<string> GetProcessList()
        {
            return !IsLoadPossible() ? null : File.ReadAllLines(FilePath);
        }

        public List<string> GetExtensions(string processName)
        {
            ComposePlugins();
            return Openers.Where(openerPlugin => openerPlugin
                .IsSuitableForProcess(processName))
                .Select(openerPlugin => openerPlugin
                    .GetSupportedExtensions()
                    .ToList())
                .FirstOrDefault();
        }

        private string GetProcessLine(Process process)
        {
            foreach (var openerPlugin in Openers.Where(openerPlugin =>
                openerPlugin.IsSuitableForProcess(process.ProcessName)))
            {
                try
                {
                    return openerPlugin.GetProcessLine(process);
                }
                catch
                {
                } //goto defaultGetter
            }

            return string.Format("{0}\t{1}\t{2}\t{3}",
                ProcessManager.GetAppName(process.MainModule.FileName),
                process.ProcessName, process.MainModule.FileName,
                false);
        }

        private void ComposePlugins()
        {
            var catalog = new DirectoryCatalog(Environment.CurrentDirectory + "\\plugins");
            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);
        }

        private void RunLoadedPrograms(IEnumerable<string> processesToRestore)
        {
            foreach (var processLine in processesToRestore)
            {
                var isPluginExists = false;
                var processProperties = processLine.Split('\t');
                List<string> filePaths = null;
                if (processProperties.Count() > 4)
                {
                    filePaths = processProperties.ToList()
                        .GetRange(5, processProperties.Count() - 5);
                }

                foreach (var openerPlugin in Openers.Where(openerPlugin =>
                    openerPlugin.IsSuitableForProcess(processProperties[1])))
                {
                    try
                    {
                        openerPlugin.OpenFiles(processProperties[2], filePaths);
                        isPluginExists = true;
                    }
                    catch (Exception ex)
                    {
                        var logger = LoggerFactory.Current.GetLogger<FileProcessRestorer>();
                        logger.Error("Error with running process " + processProperties[1], ex);
                    }
                }
                if (!isPluginExists) DeafultProcessOpener(processProperties[2], filePaths);
            }
        }

        private void DeafultProcessOpener(string processPath, List<string> filePaths)
        {
            try
            {
                if (filePaths != null)
                {
                    var arguments = filePaths.Aggregate(string.Empty,
                        (current, filePath) => current + ("\t" + filePath));
                    Process.Start(processPath, arguments);
                }
                else
                {
                    var startInfo = new ProcessStartInfo(processPath);
                    startInfo.UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                    Process.Start(processPath);
                }
            }
            catch (Exception ex)
            {
                var logger = LoggerFactory.Current.GetLogger<FileProcessRestorer>();
                logger.Error("Error with running process " + processPath[1], ex);
            }
        }

        private bool IsLoadPossible()
        {
            if (!Directory.Exists("save")) Directory.CreateDirectory("save");
            if (!File.Exists(FilePath))
            {
                File.Create(FilePath);
                return false;
            }
            var lines = File.ReadAllLines(FilePath);
            return lines.Any();
        }

        private bool ConfirmLoading()
        {
            var restoringSessionMessageForm = new LoadSessionMessage("Restoring session. - RemotePowerController", 10);
            var ans = restoringSessionMessageForm.ShowDialog();
            return ans != DialogResult.Yes;
        }
    }
}


