﻿using System.Collections.Generic;

namespace Shared.ProcessRestoring
{
    public interface IProcessRestorer
    {
        void CreateSavePoint();
        void CreateSavePoint(IEnumerable<string> processesWithParameters);
        void LoadLastPoint();
        IEnumerable<string> GetProcessList();
        List<string> GetExtensions(string processName);
    }
}
