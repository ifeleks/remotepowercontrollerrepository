﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using Timer = System.Timers.Timer;

namespace Shared.ProcessRestoring.InformForms
{
    public partial class LoadSessionProgressBar : DevExpress.XtraEditors.XtraForm
    {
        private List<string> _filesToRestore;
        private int _waitSeconds;
        private int _loadingTimeout;

        public LoadSessionProgressBar(List<string> processesToRestore, int timeout)
        {
            InitializeComponent();
            //Set start position:
            Height = 165;
            var x = Screen.PrimaryScreen.WorkingArea.Right - this.Width;
            var y = Screen.PrimaryScreen.WorkingArea.Bottom - this.Height;
            this.Location = new Point(x, y);
            prBarSessionRestoring.Position = 0;
            _filesToRestore = new List<string>();
            foreach (var process in processesToRestore)
            {
                var fileName = process.Split('\t')[2];
                _filesToRestore.Add(fileName);
            }

            _waitSeconds = 0;
            _loadingTimeout = timeout;

            var loadingTimer = new Timer(1000) { AutoReset = true, Enabled = true};
            loadingTimer.Elapsed += delegate { _waitSeconds++; };
            loadingTimer.Start();
        }

        private void LoadSessionProgressBar_Shown(object sender, System.EventArgs e)
        {
            Update();
            ProgressBarUpdateWork();
        }

        public void ProgressBarUpdateWork()
        {
            var restoredProcess = new List<string>();
            var processToRestoreCount = _filesToRestore.Count;

            while (_waitSeconds < _loadingTimeout && restoredProcess.Count < processToRestoreCount)
            {
                var currentSession = GetCurrentAppsSession();
                foreach (var currentSessionProcess in currentSession)
                {
                    var processFileName = currentSessionProcess.MainModule.FileName;
                    if (!_filesToRestore.Contains(processFileName) 
                        && !string.IsNullOrEmpty(currentSessionProcess.MainWindowTitle)) continue;
                    restoredProcess.Add(processFileName);
                    _filesToRestore.Remove(processFileName);
                }

                prBarSessionRestoring.Position = 100 / processToRestoreCount * restoredProcess.Count;
                prBarSessionRestoring.Update();
            }

            if (restoredProcess.Count < _filesToRestore.Count)
            {
                XtraMessageBox.Show(UserLookAndFeel.Default, "Not all processes restored succesfully =(");
            }
            else
            {
                XtraMessageBox.Show(UserLookAndFeel.Default, "All processes restored succesfull");
            }

            Close();
        }

        private List<Process> GetCurrentAppsSession()
        {
            var processes = Process.GetProcesses().ToList();
            var processesThatCurrentlyRunning = new List<Process>();
            foreach (var process in processes)
            {
                if (string.IsNullOrEmpty(process.MainWindowTitle)) continue; //user apps filter
                try
                {
                    var tryToGetName = process.MainModule.FileName;
                    processesThatCurrentlyRunning.Add(process);
                }
                catch
                {
                }
            }
            return processesThatCurrentlyRunning;
        }
    }
}