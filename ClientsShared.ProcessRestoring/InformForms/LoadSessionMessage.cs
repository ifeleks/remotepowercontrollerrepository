﻿using System;
using System.Windows.Forms;

namespace Shared.ProcessRestoring.InformForms
{
    public partial class LoadSessionMessage : Form
    {
        private System.Timers.Timer _closeTimer;
        private int _seconds;
        private string _caption;

        public static LoadSessionMessage Instance;

        public LoadSessionMessage(string caption, int seconds)
        {
            Instance = this;
            _caption = caption;
            _seconds = seconds;
            InitializeComponent();
            _closeTimer = new System.Timers.Timer(1000);
            _closeTimer.Enabled = true;
            _closeTimer.Elapsed += delegate
            {
                _seconds--; 
                lblMessage.Invoke((MethodInvoker) delegate
                {
                    lblMessage.Text = string.Format("Saved session will start loading in {0} sec." +
                                                    "\nWould you like to confirm action?", _seconds);
                });
                if (_seconds == 0)
                {
                    _closeTimer.Stop();
                    _closeTimer.Dispose();
                    this.Invoke((MethodInvoker) delegate
                    {
                        this.DialogResult = DialogResult.No;
                        this.Close();
                    });
                }   
            };
            _closeTimer.Start();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            _closeTimer.Stop();
            _closeTimer.Dispose();
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            _closeTimer.Stop();
            _closeTimer.Dispose();
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void PowerActionMessage_Load(object sender, EventArgs e)
        {
            this.Text = _caption;
            lblMessage.Text = string.Format("Saved session will start loading in {0} sec." +
                                            "\nWould you like to confirm action?", _seconds);
        }
    }
}
