﻿namespace Shared.ProcessRestoring.InformForms
{
    partial class LoadSessionProgressBar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.prBarSessionRestoring = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblRestoredCount = new DevExpress.XtraEditors.LabelControl();
            this.lblCurrentlyRestored = new DevExpress.XtraEditors.LabelControl();
            this.lblCurrentlyRestoredProcessName = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.prBarSessionRestoring.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // prBarSessionRestoring
            // 
            this.prBarSessionRestoring.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.prBarSessionRestoring.EditValue = "50";
            this.prBarSessionRestoring.Location = new System.Drawing.Point(12, 72);
            this.prBarSessionRestoring.Name = "prBarSessionRestoring";
            this.prBarSessionRestoring.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.prBarSessionRestoring.ShowProgressInTaskBar = true;
            this.prBarSessionRestoring.Size = new System.Drawing.Size(300, 45);
            this.prBarSessionRestoring.TabIndex = 0;
            // 
            // lblRestoredCount
            // 
            this.lblRestoredCount.Location = new System.Drawing.Point(74, 12);
            this.lblRestoredCount.Name = "lblRestoredCount";
            this.lblRestoredCount.Size = new System.Drawing.Size(183, 13);
            this.lblRestoredCount.TabIndex = 1;
            this.lblRestoredCount.Text = "Already restored {0} of {1} processes";
            // 
            // lblCurrentlyRestored
            // 
            this.lblCurrentlyRestored.Location = new System.Drawing.Point(118, 31);
            this.lblCurrentlyRestored.Name = "lblCurrentlyRestored";
            this.lblCurrentlyRestored.Size = new System.Drawing.Size(95, 13);
            this.lblCurrentlyRestored.TabIndex = 2;
            this.lblCurrentlyRestored.Text = "Currently restoring:";
            // 
            // lblCurrentlyRestoredProcessName
            // 
            this.lblCurrentlyRestoredProcessName.Location = new System.Drawing.Point(133, 50);
            this.lblCurrentlyRestoredProcessName.Name = "lblCurrentlyRestoredProcessName";
            this.lblCurrentlyRestoredProcessName.Size = new System.Drawing.Size(64, 13);
            this.lblCurrentlyRestoredProcessName.TabIndex = 3;
            this.lblCurrentlyRestoredProcessName.Text = "ProcessName";
            // 
            // LoadSessionProgressBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 121);
            this.ControlBox = false;
            this.Controls.Add(this.lblCurrentlyRestoredProcessName);
            this.Controls.Add(this.lblCurrentlyRestored);
            this.Controls.Add(this.lblRestoredCount);
            this.Controls.Add(this.prBarSessionRestoring);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoadSessionProgressBar";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Session restoring is in progress - RemotePowerController";
            this.TopMost = true;
            this.Shown += new System.EventHandler(this.LoadSessionProgressBar_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.prBarSessionRestoring.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ProgressBarControl prBarSessionRestoring;
        private DevExpress.XtraEditors.LabelControl lblRestoredCount;
        private DevExpress.XtraEditors.LabelControl lblCurrentlyRestored;
        private DevExpress.XtraEditors.LabelControl lblCurrentlyRestoredProcessName;
    }
}