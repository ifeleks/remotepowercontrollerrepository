﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Common.PluginsInterface;
using NDde.Client;

namespace Common.OperaPlugin
{
    [Export(typeof(IOpenerPlugin))]
    public class OperaOpener : IOpenerPlugin
    {
        private readonly List<string> _applicationNames;
        private readonly List<string> _supportedExtensions;

        OperaOpener()
        {
            _applicationNames = new List<string> {"opera", "орега"};
            _supportedExtensions = new List<string> {"html", "asax"};
        }

        public bool IsSuitableForProcess(string processName)
        {
            return _applicationNames.Any(processName.Equals);
        }

        public IEnumerable<string> GetSupportedExtensions()
        {
            return _supportedExtensions;
        }

        public string GetProcessLine(Process process)
        {
            var dde = new DdeClient(process.ProcessName, "WWW_GetWindowInfo");
            dde.Connect();
            var fullUrl = dde.Request("URL", int.MaxValue);
            var text = fullUrl.Split(new[] { "\",\"" }, StringSplitOptions.RemoveEmptyEntries);
            var url = text[0].Substring(1);

            var result = new StringBuilder();
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(process.MainModule.FileName);
            result.Append(fileVersionInfo.FileDescription + "\t" + process.ProcessName +
                "\t" + process.MainModule.FileName + "\t" + true + "\t");
            foreach (var supportedExtension in _supportedExtensions)
            {
                result.Append("." + supportedExtension);
            } 
            result.Append("\t" + url);
            return result.ToString();
        }

        public void OpenFiles(string processPath, List<string> filePaths)
        {
            if (filePaths.Any())
            {
                var arguments = filePaths.Aggregate(string.Empty,
                    (current, filePath) => current + ("\t" + filePath));
                Process.Start(processPath, arguments);
            }
            else
            {
                Process.Start(processPath);
            }
        }
    }
}
