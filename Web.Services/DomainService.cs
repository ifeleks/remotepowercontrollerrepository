﻿using System.Collections.Generic;
using System.Linq;
using Common.DTO;
using RemotePowerControllerTestCore.Entities;
using RemotePowerControllerTestCore.Repositories.Implementations;
using Web.Interfaces;

namespace Web.Services
{
    public class DomainService : IDomainService
    {
        public List<AuthentificationAttemptTO> GetDomainAuthentificationCommands(string serverSignature)
        {
            var domainAuthAttemptRepo = new GenericRepository<DomainAuthAttempt>();
            var domainAuthAttempts = domainAuthAttemptRepo.Get(/*a => a.ServerSignature == serverSignature*/).ToList();
            var domainAuthAttemptsTO = new List<AuthentificationAttemptTO>();
            foreach (var domainAuthAttemp in domainAuthAttempts)
            {
                domainAuthAttemptsTO.Add(DomainAuthAttempt.ConvertToTO(domainAuthAttemp));
            }
            return domainAuthAttemptsTO;
        }

        public void SetAuthentificationResult(AuthentificationAttemptTO user)
        {
            var domainAuthAttemptRepo = new GenericRepository<DomainAuthAttempt>();
            var domainAuthAttemptEntity = domainAuthAttemptRepo.GetByID(user.Id);
            domainAuthAttemptEntity.IsAuth = user.IsValid;
            domainAuthAttemptRepo.Update(domainAuthAttemptEntity);
        }
    }
}
