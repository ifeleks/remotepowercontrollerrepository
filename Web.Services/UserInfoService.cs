﻿using System;
using System.Linq;
using Common.DTO;
using RemotePowerControllerTestCore;
using RemotePowerControllerTestCore.Entities;
using RemotePowerControllerTestCore.Helpers;
using RemotePowerControllerTestCore.Repositories.Implementations;
using Web.Interfaces;

namespace Web.Services
{
    public class UserInfoService : IUserInfoService
    {
        public void RegisterNewUser(UserInfoTO userInfo, ClientInfoTO clientInfo, SettingsTO settings)
        {
            //todo: validate and encode userInfo
            if (IsLoginExist(userInfo)) throw new Exception("Login already exists");
            if (!IsMacAddressValid(clientInfo)) throw new Exception("MACAddres already exists");
            var userToInsert = User.ConvertFromTO(userInfo);
            userToInsert.Password = PasswordHash.CreateHash(userToInsert.Password);
            var userRepo = new GenericRepository<User>();
            userRepo.Insert(userToInsert);
            CreateGroupFavoritesIfNotExists(userToInsert);
            var clientService = new ClientInfoService();
            var server = clientService.CreateServerIfNotExists(clientInfo.ServerSignature);
            var clientToInsert = Client.ConvertFromTO(clientInfo);
            var settingsToInsert = ClientSettings.ConvertFromTO(settings);
            clientToInsert.UserId = userToInsert.UserId;
            clientToInsert.ServerId = server.ServerId;
            clientService.InsertClient(clientToInsert, settingsToInsert, userToInsert);
        }

        private void CreateGroupFavoritesIfNotExists(User user)
        {
            using (var uow = new RPCUnitOfWork())
            {
                var groupFavorites = uow.GroupRepository.Get(g => g.GroupName == GroupDataHelper.FavoritesGroupName && g.UserId == user.UserId).FirstOrDefault();
                if(groupFavorites != null) return;
                groupFavorites = new Group
                {
                    GroupName = GroupDataHelper.FavoritesGroupName,
                    UserId = user.UserId
                };
                uow.GroupRepository.Insert(groupFavorites);
                uow.Save();
            }
        }

        public bool IsLoginExist(UserInfoTO userInfo)
        {
            //todo: validate and encode userInfo
            var userRepo = new GenericRepository<User>();
            var user = userRepo.Get(u => u.Username == userInfo.Login).FirstOrDefault();
            return user != null;
        }

        public bool LogIn(UserInfoTO userInfo)
        {
            var userRepo = new GenericRepository<User>();
            if (!IsLoginValid(userInfo)) return false;
            var user = userRepo.Get(u => u.Username == userInfo.Login).First();
            var result = PasswordHash.ValidatePassword(userInfo.Password, user.Password);
            return result;
        }

        public bool IsMacAddressValid(ClientInfoTO clientInfo)
        {
            var clientRepo = new GenericRepository<Client>();
            var client = clientRepo.Get(c => c.MACAddress == clientInfo.MacAddress).FirstOrDefault();
            return client == null;
        }

        public bool IsLoginValid(UserInfoTO userInfo)
        {
            var userRepo = new GenericRepository<User>();
            var user = userRepo.Get(u => u.Username == userInfo.Login).FirstOrDefault();
            return user != null;
        }

        public void ChangePassword(string newPassword, UserInfoTO userInfo)
        {
            //todo: validate and encode userInfo, new password
            var userRepo = new GenericRepository<User>();
            var user = userRepo.Get(u => u.Username == userInfo.Login && u.Password == userInfo.Password).FirstOrDefault();
            if (user == null) return;
            user.Password = newPassword;
            userRepo.Update(user);
        }

        public void ChangeLogin(string newLogin, UserInfoTO userInfo)
        {
            //todo: validate and encode userInfo, newLogin
            var userRepo = new GenericRepository<User>();
            var user = userRepo.Get(u => u.Username == userInfo.Login && u.Password == userInfo.Password).FirstOrDefault();
            if (user == null) return;
            user.Username = newLogin;
            userRepo.Update(user);
        }

        public UserInfoTO GetUserById(int id)
        {
            var userRepo = new GenericRepository<User>();
            var user = userRepo.GetByID(id);
            return user != null ? User.ConvertToTO(user) : null;
        }
    }
}
