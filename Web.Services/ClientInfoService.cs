﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using Common.DTO;
using Common.DTO.Enums;
using RemotePowerControllerTestCore;
using RemotePowerControllerTestCore.Entities;
using RemotePowerControllerTestCore.Helpers;
using RemotePowerControllerTestCore.Repositories.Implementations;
using Web.Interfaces;

namespace Web.Services
{
    public class ClientInfoService : IClientInfoService
    {
        public void AddClientForUser(UserInfoTO userInfo, ClientInfoTO clientInfo, SettingsTO settings)
        {
            var userRepo = new GenericRepository<User>();
            var userToAddClient = userRepo.Get(u => u.Username == userInfo.Login).First();

            var clientRepo = new GenericRepository<Client>();
            var existingClient = clientRepo.Get(c => c.MACAddress == clientInfo.MacAddress).FirstOrDefault();
            if (existingClient != null) return;

            var clientServer = CreateServerIfNotExists(clientInfo.ServerSignature);
            var clientToInsert = Client.ConvertFromTO(clientInfo);
            clientToInsert.UserId = userToAddClient.UserId;
            clientToInsert.ServerId = clientServer.ServerId;
            var settingsToInsert = ClientSettings.ConvertFromTO(settings);
            InsertClient(clientToInsert, settingsToInsert, userToAddClient);
        }

        public Server CreateServerIfNotExists(string serverSignature)
        {
            var serverRepo = new GenericRepository<Server>();
            var server = serverRepo.Get(s => s.ServerName == serverSignature).FirstOrDefault();
            if (server != null) return server;
            server = new Server { ServerName = serverSignature, LastActivity = DateTime.UtcNow };
            serverRepo.Insert(server);
            return server;
        }

        public void InsertClient(Client client, ClientSettings settings, User user)
        {
            var clientRepo = new GenericRepository<Client>();
            clientRepo.Insert(client);
            settings.ClientId = client.ClientId;
            var settingsRepo = new GenericRepository<ClientSettings>();
            settingsRepo.Insert(settings);
            InsertDefaultStateAndAuditForClient(client);
            AddClientToAdminAllGroup(client);
        }

        private void InsertDefaultStateAndAuditForClient(Client client)
        {
            var clientStateToInsert = ClientState.DefaultState();
            clientStateToInsert.ClientId = client.ClientId;
            var clientStateRepo = new GenericRepository<ClientState>();
            clientStateRepo.Insert(clientStateToInsert);
            var clientStateAuditToInsert = ClientStateAudit.DefaultStateAudit();
            clientStateAuditToInsert.ClientId = client.ClientId;
            var clientStateAuditRepo = new GenericRepository<ClientStateAudit>();
            clientStateAuditRepo.Insert(clientStateAuditToInsert);
        }

        public void AddClientToAdminAllGroup(Client client)
        {
            using (var uow = new RPCUnitOfWork())
            {
                var adminGroupAll = uow.GroupRepository.GetByID(AdminDataHelper.GetAdminGroupAllId());
                var clientToAdd = uow.ClientRepository.GetByID(client.ClientId);
                adminGroupAll.Clients.Add(clientToAdd);
                uow.Save();
            }
        }

        public List<ClientInfoTO> GetClientsWithCommands(string serverName)
        {
            var serverRepo = new GenericRepository<Server>();
            var server = serverRepo.Get(s=>s.ServerName == serverName).FirstOrDefault();
            var result = new List<ClientInfoTO>();

            if (server == null) return result;

            var clientRepo = new GenericRepository<Client>();
            var serverClients = clientRepo.Get(c => c.ServerId == server.ServerId, includeProperties: "ClientState");

            if (serverClients == null) return result;

            var clientsToSwitchState = serverClients.Where(c => c.ClientState.Command == Command.GoToSleep ||
                                                                c.ClientState.Command == Command.TurnOff ||
                                                                c.ClientState.Command == Command.TurnOn).ToList();
            
            foreach (var client in clientsToSwitchState)
                result.Add(Client.ConvertToTO(client));

            //Say about server activity:
            server.LastActivity = DateTime.UtcNow;
            serverRepo.Update(server);

            return result;
        }

        public List<ClientInfoTO> GetAllServerClients(string serverName)
        {
            var clientRepo = new GenericRepository<Client>();
            var result = new List<ClientInfoTO>();
            IEnumerable<Client> serverClients;
            if (serverName == "all")
            {
                serverClients = clientRepo.Get(includeProperties: "ClientState").ToList();
            }
            else
            {
                var serverRepo = new GenericRepository<Server>();
                var server = serverRepo.Get(s => s.ServerName == serverName).FirstOrDefault();
                if (server == null) return result;
                serverClients = clientRepo.Get(c => c.ServerId == server.ServerId, includeProperties: "ClientState").ToList();
            }

            foreach (var client in serverClients)
                result.Add(Client.ConvertToTO(client));
            return result;
        }

        public void UpdateClientState(State newState, ClientInfoTO linkedClient)
        {
            var clientRepo = new GenericRepository<Client>();
            var clientToUpdateState = clientRepo.Get(c => c.MACAddress == linkedClient.MacAddress).FirstOrDefault();
            if (clientToUpdateState == null) return;
            var clientStatesRepo = new GenericRepository<ClientState>();
            var clientStateToUpdate = clientStatesRepo.GetByID(clientToUpdateState.ClientId);
            if (clientStateToUpdate == null) return;
            if (clientStateToUpdate.CurrentState == newState) return;
            clientStateToUpdate.CurrentState = newState;
            //clientStateToUpdate.Command = Command.DoNothing;
            clientStatesRepo.Update(clientStateToUpdate);
            var clientStateAuditRepo = new GenericRepository<ClientStateAudit>();
            clientStateAuditRepo.Insert(new ClientStateAudit { DateTime = DateTime.UtcNow, ClientId = clientStateToUpdate.ClientId, State = clientStateToUpdate.CurrentState });
        }

        public void ChangeCommand(Command command, ClientInfoTO linkedClient)
        {
            var clientRepo = new GenericRepository<Client>();
            var clientToSetInProcess = clientRepo.Get(c => c.MACAddress == linkedClient.MacAddress).FirstOrDefault();
            if (clientToSetInProcess == null) return;
            var clientStatesRepo = new GenericRepository<ClientState>();
            var clientStateToUpdate = clientStatesRepo.GetByID(clientToSetInProcess.ClientId);
            if (clientStateToUpdate == null) return;
            if (clientStateToUpdate.Command == command) return;
            clientStateToUpdate.Command = command;
            clientStatesRepo.Update(clientStateToUpdate);
        }

        public bool IsMacExist(string macAddress, UserInfoTO userCredentials)
        {
            Client existingClient;
            try
            {
                var clientRepo = new GenericRepository<Client>();
                existingClient = clientRepo.Get(c => c.MACAddress == macAddress).FirstOrDefault();
            }
            catch
            {
                existingClient = null;
            }

            User userToAddClient;
            try
            {
                var userRepo = new GenericRepository<User>();
                userToAddClient = userRepo.Get(u => u.Username == userCredentials.Login).First();
            }
            catch
            {
                userToAddClient = null;
            }

            bool badRegistration = existingClient != null && userToAddClient == null;
            bool badlogin = existingClient != null && userToAddClient != null && existingClient.UserId != userToAddClient.UserId;

            return badRegistration || badlogin;
        }
    }
}
