﻿using System;
using System.Linq;
using Common.DTO;
using Common.DTO.Enums;
using RemotePowerControllerTestCore.Entities;
using RemotePowerControllerTestCore.Repositories.Implementations;
using Web.Interfaces;

namespace Web.Services
{
    public class SettingsService : ISettingsService
    {
        public void AddSettings(SettingsTO settings, ClientInfoTO client)
        {
            throw new NotImplementedException();
        }

        public void UpdateSettings(string clientMACAddress, SettingsTO settings)
        {
            var clientRepo = new GenericRepository<Client>();
            var clientToUpdateSettings = clientRepo.Get(c => c.MACAddress == clientMACAddress).FirstOrDefault();
            if (clientToUpdateSettings == null) return;
            var clientSettingsRepo = new GenericRepository<ClientSettings>();
            var clientSettings = ClientSettings.ConvertFromTO(settings);
            clientSettings.ClientId = clientToUpdateSettings.ClientId;
            clientSettingsRepo.Update(clientSettings);
        }

        public SettingsTO GetDefaultSettings()
        {
            throw new NotImplementedException();
        }

        public SettingsTO GetClientSettings(ClientInfoTO client)
        {
            var clientRepo = new GenericRepository<Client>();
            var clientEntity = clientRepo.Get(c => c.MACAddress == client.MacAddress).FirstOrDefault();
            if (clientEntity == null) return null;
            var clientSettingsRepo = new GenericRepository<ClientSettings>();
            var clientSettings = clientSettingsRepo.GetByID(clientEntity.ClientId);
            return ClientSettings.ConvertToTO(clientSettings);
        }

        #region alarmClock
        public void AlarmClock(TimeSpan serverTime, TimeSpan acceptableDelay)
        {
            var settingsService = new SettingsService();
            var clientService = new ClientInfoService();
            var clients = clientService.GetAllServerClients("all");
            foreach (var client in clients)
            {
                var currentClientSettings = settingsService.GetClientSettings(client);
                if (IsItTimeToWakeUp(client, currentClientSettings.WorkTimeStart,
                    acceptableDelay, serverTime))
                {
                    SetTurnOnCommand(client);
                }
            }
        }

        private bool IsItTimeToWakeUp(ClientInfoTO client, TimeSpan clientTurnOnTime,
            TimeSpan acceptableDiff, TimeSpan serverTime)
        {
            if (client.Command == Command.TurnOn || client.State != State.Off) return false;
            var timeDiff = serverTime.Subtract(clientTurnOnTime);
            var zeroHour = new TimeSpan(0, 0, 0);
            return timeDiff < acceptableDiff && timeDiff > zeroHour;
        }

        private void SetTurnOnCommand(ClientInfoTO client)
        {
            var clientRepo = new GenericRepository<Client>();
            var clientToTurnOn = clientRepo.Get(c => c.MACAddress == client.MacAddress).First();
            var _clientStateRepository = new GenericRepository<ClientState>();
            var clientStateToSwitchCommand = _clientStateRepository.GetByID(clientToTurnOn.ClientId);
            clientStateToSwitchCommand.Command = Command.TurnOn;
            _clientStateRepository.Update(clientStateToSwitchCommand);
        }
        #endregion alarmClock
    }
}
