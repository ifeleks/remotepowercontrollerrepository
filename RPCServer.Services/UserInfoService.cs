﻿using System;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using Common.DTO;
using Common.Logger.Impl;
using Common.Service.Proxy;
using IUserInfoService = Web.Interfaces.IUserInfoService;

namespace RPCServer.Services
{
    public class UserInfoService : Interfaces.IUserInfoService
    {
        public void RegisterNewUser(UserInfoTO userInfo, ClientInfoTO clientInfo, SettingsTO settings)
        {
            var logger = LoggerFactory.Current.GetLogger<ClientInfoService>();
            logger.Trace("Registering new user {0} on client {1}", userInfo.Login, clientInfo.ClientName);
            var ex = new Exception("Web-server is unavailable");
            clientInfo.ServerSignature = ConfigurationManager.AppSettings["ServerSignature"];
            clientInfo.GetawayIp = "172.20.0.1";
            ServiceChannel<IUserInfoService>.For(
                service =>
                {
                    try
                    {
                        service.RegisterNewUser(userInfo, clientInfo, settings);
                        ex = new NotImplementedException();
                    }
                    catch (Exception exception)
                    {
                        ex = exception;
                    }
                });
            if (ex is NotImplementedException) return;
            throw ex;
        }

        public bool IsLoginExist(UserInfoTO userInfo)
        {
            bool isCorrect = false;
            ServiceChannel<IUserInfoService>.For(
                service => isCorrect = service.IsLoginExist(userInfo));
            return isCorrect;
        }

        public bool LogIn(UserInfoTO userInfo)
        {
            bool isCorrect = false;
            ServiceChannel<IUserInfoService>.For(
                service => isCorrect = service.LogIn(userInfo));
            return isCorrect;
        }

        public void ChangePassword(string newPassword, UserInfoTO userInfo)
        {
            ServiceChannel<IUserInfoService>.For(
                service => service.ChangePassword(newPassword, userInfo));
        }

        public void ChangeLogin(string newLogin, UserInfoTO userInfo)
        {
            ServiceChannel<IUserInfoService>.For(
                service => service.ChangeLogin(newLogin, userInfo));
        }

        public bool IsLoginExistInDomain(string userDomainName)
        {
            var logger = LoggerFactory.Current.GetLogger<UserInfoService>();
            var domainLoginExist = false;
            try
            {
                using (var domainContext = new PrincipalContext(ContextType.Domain))
                {
                    using (var foundUser = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, userDomainName))
                    {
                        if (foundUser != null)
                        {
                            domainLoginExist = true;
                            logger.Trace("User {0} exist: true", userDomainName);
                        }
                        else
                        {
                            logger.Trace("User {0} exist: false", userDomainName);
                        }
                    }
                }
            }
            catch
            {
                logger.Trace("User {0} exist: serve is not in domain", userDomainName);
            }
            return domainLoginExist;
        }

    }
}
