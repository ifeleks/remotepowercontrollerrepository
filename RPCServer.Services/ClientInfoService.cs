﻿using System;
using System.Configuration;
using System.Net;
using System.Runtime.Remoting;
using System.ServiceModel;
using Client.Interface;
using Common.Availableness;
using Common.DTO;
using Common.DTO.Enums;
using Common.Logger.Impl;
using Common.Service.Proxy;
using Web.Interfaces;
using IClientInfoService = Web.Interfaces.IClientInfoService;

namespace RPCServer.Services
{
    public class ClientInfoService : Interfaces.IClientInfoService
    {
        public void AddNewClientInfoForUser(UserInfoTO user, ClientInfoTO clientInfo, SettingsTO settings)
        {
            var ex = new Exception("Web-server is unavailable");
            clientInfo.ServerSignature = ConfigurationManager.AppSettings["ServerSignature"];
            clientInfo.GetawayIp = "172.20.0.1";
            ServiceChannel<IClientInfoService>.For(
                service =>
                {
                    try
                    {
                        service.AddClientForUser(user, clientInfo, settings);
                        ex = new NotImplementedException();
                    }
                    catch (Exception exception)
                    {
                        ex = exception;
                    }
                });
            if (ex is NotImplementedException) return;
            throw ex;
        }

        public void UpdateClientState(State newState, ClientInfoTO clientInfo)
        {
            var logger = LoggerFactory.Current.GetLogger<ClientInfoService>();
            logger.Trace("Updating client {0} with state {1}", clientInfo.ClientName, newState.ToString());
            clientInfo.ServerSignature = ConfigurationManager.AppSettings["ServerSignature"];
            ServiceChannel<IClientInfoService>.For(
                service => service.UpdateClientState(newState, clientInfo));

        }

        public void ConfirmPowerAction(ClientInfoTO client, DefaultShutdownType powerAction)
        {
            var logger = LoggerFactory.Current.GetLogger<ClientInfoService>();
            logger.Trace("Client {0} asked about {1}. Confirming", client.ClientName, powerAction.ToString());

            var clientEndpoint = new EndpointAddress(
                string.Format("http://{0}:{1}/PowerManagementService/",
                    client.ClientIp, ConfigurationManager.AppSettings["ClientPort"]));

            if (ServiceAvailability.IsOnline(clientEndpoint.Uri.ToString(), 400))
            {
                switch (powerAction)
                {
                    case DefaultShutdownType.Off:
                        ServiceChannel<IPowerManagementService>.For(clientEndpoint,
                            powerManagementService => powerManagementService.TurnOff());
                        break;
                    case DefaultShutdownType.Sleep:
                        ServiceChannel<IPowerManagementService>.For(clientEndpoint,
                            powerManagementService => powerManagementService.Sleep());
                        break;
                }
            }
            else
            {
                throw new ServerException("PowerManagementService not responding");
            }
        }

        public void ChangeCommand(Command command, ClientInfoTO client)
        {
            ServiceChannel<IClientInfoService>.For(
                service => service.ChangeCommand(command, client));
        }

        public bool IsMacExist(string macAddress, UserInfoTO userCredentials)
        {
            bool isCorrect = false;
            ServiceChannel<IClientInfoService>.For(
                service => isCorrect = service.IsMacExist(macAddress, userCredentials));
            return isCorrect;
        }
    }
}
