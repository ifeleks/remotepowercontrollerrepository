﻿using Common.DTO;
using Common.Service.Proxy;
using ISettingsService = Web.Interfaces.ISettingsService;

namespace RPCServer.Services
{
    public class SettingsService : Interfaces.ISettingsService
    {
        public void AddNewSettings(SettingsTO settings, ClientInfoTO client)
        {
            ServiceChannel<ISettingsService>.For(
                service => service.AddSettings(settings, client));
        }

        public void UpdateSettings(ClientInfoTO client, SettingsTO settings)
        {
            ServiceChannel<ISettingsService>.For(
                service => service.UpdateSettings(client.MacAddress, settings));
        }

        public SettingsTO GetClientSettings(ClientInfoTO client)
        {
            var clientSettings = new SettingsTO();
            ServiceChannel<ISettingsService>.For(
                service => clientSettings = service.GetClientSettings(client));
            return clientSettings;
        }
    }
}
