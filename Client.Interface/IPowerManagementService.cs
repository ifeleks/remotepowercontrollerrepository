﻿using System.ServiceModel;

namespace Client.Interface
{
    [ServiceContract]
    public interface IPowerManagementService
    {
        [OperationContract(IsOneWay = true)]
        void TurnOff();

        [OperationContract(IsOneWay = true)]
        void Sleep();
    }
}
