﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Common.PluginsInterface;
using Microsoft.Office.Interop.Word;

namespace Common.WordPlugin
{
    [Export(typeof(IOpenerPlugin))]
    public class WordOpener : IOpenerPlugin
    {
        private readonly List<string> _applicationNames;
        private readonly List<string> _supportedExtensions;

        WordOpener()
        {
            _applicationNames = new List<string>
            {
                "WINWORD", "WORD", 
                "winword", "word"
            };
            _supportedExtensions = new List<string>
            {
                "doc", "docx", 
                "rtf", "docm", "dot", "dotx"
            };
        }

        public bool IsSuitableForProcess(string processName)
        {
            return _applicationNames.Any(processName.Equals);
        }

        public IEnumerable<string> GetSupportedExtensions()
        {
            return _supportedExtensions;
        }

        public string GetProcessLine(Process process)
        {
            var docList = new List<string>();
            try
            {
                Application WordObj;
                WordObj = (Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Word.Application");
                for (var i = 0; i < WordObj.Windows.Count; i++)
                {
                    object idx = i + 1;
                    var winObj = WordObj.Windows.get_Item(ref idx);
                    docList.Add(winObj.Document.FullName);
                }
            }
            catch
            {
                // No documents opened
            }


            var result = new StringBuilder();
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(process.MainModule.FileName);
            result.Append(fileVersionInfo.FileDescription + "\t" + process.ProcessName +
                "\t" + process.MainModule.FileName + "\t" + true + "\t");
            foreach (var supportedExtension in _supportedExtensions)
            {
                result.Append("." + supportedExtension);
            } 
            foreach (var doc in docList)
            {
                result.Append("\t" + doc);
            }
            return result.ToString();
        }

        public void OpenFiles(string processPath, List<string> filePaths)
        {
            if (filePaths.Any())
            {
                var arguments = filePaths.Aggregate(string.Empty,
                    (current, filePath) => current + ("\t\"" + filePath + "\""));
                Process.Start(processPath, arguments);
            }
            else
            {
                Process.Start(processPath);
            }
        }
    }
}
