﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Audio
{
    public interface ISoundNotifications
    {
        void PlayLullaby();
        void PlayTurnOff();
        void PlaySessionRestoring();
        void PlayExit();
        void PlayHello();
        void PlayError();
    }
}
