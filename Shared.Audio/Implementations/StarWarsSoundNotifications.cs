﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using Shared.Audio.Properties;

namespace Shared.Audio
{
    public class StarWarsSoundNotifications : ISoundNotifications
    {
        public void PlayLullaby()
        {
            var player = new SoundPlayer(Resources.palpatine_darkside);
            player.Play();
        }

        public void PlayTurnOff()
        {
            var player = new SoundPlayer(Resources.nooo);
            player.Play();
        }

        public void PlaySessionRestoring()
        {
            //var player = new SoundPlayer();
            //player.Play();
        }

        public void PlayExit()
        {
            var player = new SoundPlayer(Resources.exit);
            player.PlaySync();
        }

        public void PlayHello()
        {
            var player = new SoundPlayer(Resources.R2D2);
            player.Play();
        }

        public void PlayError()
        {
            var player = new SoundPlayer(Resources.nooo);
            player.Play();
        }
    }
}
