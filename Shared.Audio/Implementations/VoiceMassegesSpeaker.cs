﻿using System;
using System.Speech.Synthesis;

namespace Shared.Audio
{
    public class VoiceMassegesSpeaker : IDisposable, ISoundNotifications
    {
        private static readonly SpeechSynthesizer SSynthesizer;

        static VoiceMassegesSpeaker() 
        {
            SSynthesizer = new SpeechSynthesizer {Volume = 100, Rate = -1};
        }

        public void PlayLullaby()
        {
            SSynthesizer.Speak("good night");
        }

        public void PlayTurnOff()
        {
            SSynthesizer.Speak("shuting down");
        }

        public void PlaySessionRestoring()
        {
            SSynthesizer.Speak("your session will be restored in few moments");
        }

        public void PlayExit()
        {
            SSynthesizer.Speak("chao");
        }

        public void PlayHello()
        {
            SSynthesizer.Speak("aloha");
        }

        public void PlayError()
        {
            SSynthesizer.SpeakAsync("oops!");
        }

        public void Dispose()
        {
            SSynthesizer.Dispose();
        }
    }
}
