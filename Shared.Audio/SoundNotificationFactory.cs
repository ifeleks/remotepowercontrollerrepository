﻿namespace Shared.Audio
{
    public class SoundNotificationFactory
    {
        static SoundNotificationFactory()
        {
            Current = new StarWarsSoundNotifications();//todo:implement sound theme selection
        }
        public static ISoundNotifications Current { get; private set; }
    }
}