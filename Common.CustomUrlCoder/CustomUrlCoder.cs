﻿namespace Common.CustomUrlCoder
{
    public class CustomUrlCoder
    {
        public static string Encode(string text)
        {
            text = text.Replace("/", "_");
            text = text.Replace("+", "-");
            text = text.Replace("=", "@");
            text = text.Replace(":", "$");
            return text;
        }

        public static string Decode(string encoded)
        {
            encoded = encoded.Replace("_", "/");
            encoded = encoded.Replace("-", "+");
            encoded = encoded.Replace("@", "=");
            encoded = encoded.Replace("$", ":");
            return encoded;
        }
    }
}
