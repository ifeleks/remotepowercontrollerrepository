﻿using System;
using System.DirectoryServices.ActiveDirectory;
using System.Security.Authentication;
using System.Security.Principal;
using System.ServiceModel;
using System.Windows.Forms;
using Common.DTO;
using Common.Logger;
using Common.Logger.Impl;
using Common.Service.Proxy;
using DevExpress.XtraEditors;
using RPCClient.Enums;
using RPCServer.Interfaces;

namespace RPCClient.Authentication
{
    public class DomainAuthentification
    {
        private static SubmitType _submitType = SubmitType.UpdateSettings;

        public static void SignIn(EndpointAddress serverEndpoint, out SubmitType submitType)
        {
            var logger = LoggerFactory.Current.GetLogger<DomainAuthentification>();
            try
            {
                var domainName = Domain.GetComputerDomain(); //if current PC not in domain - go to standart authorization                
                logger.Trace("Current PC is in domain {0}", domainName);

                var exception = new Exception(string.Empty);
                ServiceChannel<IUserInfoService>.For(serverEndpoint, service =>
                {
                    try
                    {
                        RegisterOrLogin(service, logger);
                    }
                    catch (Exception ex)
                    {
                        exception = ex;
                    }
                });
                if (exception is AuthenticationException)
                {
                    logger.Trace("Domain authentification error: {0}", exception.Message);
                    throw exception;
                }
                if (exception.Message != string.Empty)
                {
                    XtraMessageBox.Show("Problem with domain login!", "Warning! - RemotePowerController", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    logger.Error("Problem with domain login", exception);
                    Environment.Exit(0);
                }
            }
            catch
            {
                logger.Trace("No domain on this PC. Starting normal authorization..");
                throw new AuthenticationException("No domain founded");
            }

            submitType = _submitType;
        }

        private static void RegisterOrLogin(IUserInfoService service, ILogger logger)
        {
            var currentIdentity = WindowsIdentity.GetCurrent();
            var currentDomainName = currentIdentity.Name;

            logger.Trace("Parsed domain name: {0}", currentDomainName);

            var login = new UserInfoTO
            {
                Login = currentDomainName,
                Password = Common.Keys.Passwords.DeafaultDomainUserPassword
            };

            var isLoginExistInDomain = service.IsLoginExistInDomain(currentDomainName);
            if (!isLoginExistInDomain) throw new AuthenticationException("Login " + login.Login + " is not exist in current domain");

            logger.Trace("User {0} exist in Domain. Domain Auth. started", currentDomainName);

            if (service.IsLoginExist(login))
            {
                var askUser = XtraMessageBox.Show("Would you like to login as Domain user?", "Login - RemotePowerController",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (askUser == DialogResult.No) throw new AuthenticationException("User cancel domain authentification");
                if (!AuthentificationValidator.ValidateLogin(login)) throw new AuthenticationException("Domain login validation fail");

                logger.Trace("Add new client to login {0}", currentDomainName);
                _submitType = SubmitType.AddNewClient;
            }
            else
            {
                var askUser = XtraMessageBox.Show("Would you like to register as Domain user?", "Registration - RemotePowerController",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (askUser == DialogResult.No) throw new AuthenticationException("User cancel domain authentification");
                if (!AuthentificationValidator.ValidateRegistration(login)) throw new AuthenticationException("Domain registration validation fail");

                logger.Trace("Registered new user {0}", currentDomainName);
                _submitType = SubmitType.RegisterNewUser;
            }

            login.IsDomain = true;
            Properties.Settings.Default.Login = login.Login;
            Properties.Settings.Default.Password = login.Password;
            Properties.Settings.Default.IsUserDomain = login.IsDomain;
        }
    }
}
