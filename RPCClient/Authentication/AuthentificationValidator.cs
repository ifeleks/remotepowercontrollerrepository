﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common.Availableness;
using Common.DTO;
using Common.Service.Proxy;
using DevExpress.XtraEditors;
using RPCClient.Availability;
using RPCServer.Interfaces;

namespace RPCClient.Authentication
{
    public class AuthentificationValidator
    {
        private AuthentificationValidator()
        {
        }

        public static bool ValidateLogin(UserInfoTO userInfoTO)
        {
            var loginCorrect = IsLoginCorrect(userInfoTO);
            var macExist = IsMacAdressExist(userInfoTO);
            if (!loginCorrect)
            {
                XtraMessageBox.Show("Username or password is incorrect", "Warning! - RemotePowerController",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (macExist)
            {
                XtraMessageBox.Show("That PC is already registered for another user", "Warning! - RemotePowerController",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        public static bool ValidateRegistration(UserInfoTO userInfoTO)
        {
            var loginExist = IsLoginExist(userInfoTO);
            var macExist = IsMacAdressExist(userInfoTO);
            if (loginExist)
            {
                XtraMessageBox.Show("This username is already exists", "Warning! - RemotePowerController",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (macExist)
            {
                XtraMessageBox.Show("This PC is already registered for another user", "Warning! - RemotePowerController",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        public static bool IsLoginExist(UserInfoTO userInfoTO)
        {
            var isLoginExist = false;
            var serverUserEndpoint = new EndpointAddress(
                string.Format("http://{0}:{1}/UserInfoService",
                    Properties.Settings.Default["ServerIP"],
                    Properties.Settings.Default["ServerPort"]));

            ServiceAvailability.DoWorkSafely(serverUserEndpoint.Uri.ToString(), () =>
            {
                ServiceChannel<IUserInfoService>.For(serverUserEndpoint,
                        service => isLoginExist = service.IsLoginExist(userInfoTO));
            });

            return isLoginExist;
        }

        public static bool IsLoginCorrect(UserInfoTO userInfoTO)
        {
            var isLoginCorrect = false;
            var serverUserEndpoint = new EndpointAddress(
                string.Format("http://{0}:{1}/UserInfoService",
                    Properties.Settings.Default["ServerIP"],
                    Properties.Settings.Default["ServerPort"]));

            ServiceAvailability.DoWorkSafely(serverUserEndpoint.Uri.ToString(), () =>
            {
                ServiceChannel<IUserInfoService>.For(serverUserEndpoint,
                        service => isLoginCorrect = service.LogIn(userInfoTO));
            });

            return isLoginCorrect;
        }

        public static bool IsMacAdressExist(UserInfoTO userCredentials)
        {
            var isMacExist = false;
            var serverUserEndpoint = new EndpointAddress(
                string.Format("http://{0}:{1}/ClientInfoService",
                    Properties.Settings.Default["ServerIP"],
                    Properties.Settings.Default["ServerPort"]));

            ServiceAvailability.DoWorkSafely(serverUserEndpoint.Uri.ToString(), () =>
            {
                ServiceChannel<IClientInfoService>.For(serverUserEndpoint,
                        service => isMacExist = service.IsMacExist(Properties.Settings.Default.ClientInfo.MacAddress, userCredentials));
            });

            return isMacExist;
        }
    }
}
