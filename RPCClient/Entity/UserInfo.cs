﻿using Common.DTO;

namespace RPCClient.Entity
{
    public class UserInfo
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public bool IsDomain { get; set; }

        public UserInfo(string login, string password, bool isDomain)
        {
            Login = login;
            Password = password;
            IsDomain = isDomain;
        }

        public UserInfoTO ConvertToTO()
        {
            var transferObject = new UserInfoTO()
            {
                Login = Login,
                Password = Password,
                IsDomain = IsDomain
            };

            return transferObject;
        }
    }
}
