﻿using Common.DTO;

namespace RPCClient.Entity
{
    public class ClientInfo
    {
        public string ClientName { get; set; }
        public string ClientFullName { get; set; }
        public string MacAddress { get; set; }
        public string IpAddress { get; set; }

        public ClientInfo(string clientName, string clientFullName, string macAddress, string ipAddress)
        {
            ClientName = clientName;
            ClientFullName = clientFullName;
            MacAddress = macAddress;
            IpAddress = ipAddress;
        }

        public ClientInfoTO ConvertToTO()
        {
            var transferObject = new ClientInfoTO
            {
                ClientName = ClientName,
                ClientFullName = ClientFullName,
                MacAddress = MacAddress,
                ClientIp = IpAddress
            };

            return transferObject;
        }
    }
}
