﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel.Description;
using Common.Logger.Impl;
using Shared.ProcessRestoring;

namespace RPCClient.Entity
{
    public class RestoringProcess
    {
        public string ProcessFullName { get; set; }
        public string ProcessName { get; set; }
        public string ProcessPath { get; set; }
        public bool PluginExist { get; set; }
        public List<string> FileExtensions { get; set; }
        public IList<FileToLoad> FilesToLoad { get; set; }

        public RestoringProcess(string processFullName, string processName, string processPath,
            bool pluginExist, List<string> fileExtensions , List<FileToLoad> filesToLoad)
        {
            ProcessFullName = processFullName;
            ProcessName = processName;
            ProcessPath = processPath;
            PluginExist = pluginExist;
            FileExtensions = fileExtensions;
            FilesToLoad = filesToLoad;
        } 
            
        public static RestoringProcess ParseFromString(string processLine)
        {
            try
            {
                var parsedLine = processLine.Split('\t');
                var processFullName = parsedLine[0];
                var processName = parsedLine[1];
                var processPath = parsedLine[2];
                var isPluginExist = Convert.ToBoolean(parsedLine[3]);
                List<string> fileExtensions = null;
                var filesToLoad = new List<FileToLoad>();
                if (parsedLine.Count() > 4)
                {
                    fileExtensions = parsedLine[4].Split('.').ToList();
                    fileExtensions.RemoveAt(0);
                    filesToLoad = GetProcessFileList(parsedLine);
                }
                return new RestoringProcess(
                    processFullName, processName, processPath,
                    isPluginExist, fileExtensions, filesToLoad);
            }
            catch (Exception ex)
            {
                var logger = LoggerFactory.Current.GetLogger<RestoringProcess>();
                logger.Error("Cant parse string", ex);
                return null;
            }        
        }

        public string ConvertToString()
        {
            if (PluginExist)
            {
                var fileFormatsString = FileExtensions
                    .Aggregate(string.Empty, (current, fe) => current + ("." + fe));
                var result = string.Format("{0}\t{1}\t{2}\t{3}\t{4}",
                    ProcessFullName, ProcessName, ProcessPath, PluginExist, fileFormatsString);
                result = FilesToLoad
                    .Aggregate(result, (current, fileToLoad) => current + ("\t" + fileToLoad.FilePath));
                return result;
            }
            return string.Format("{0}\t{1}\t{2}\t{3}",
                ProcessFullName, ProcessName, ProcessPath, PluginExist);
        }

        private static List<FileToLoad> GetProcessFileList(string[] parsedLine)
        {
            if (parsedLine.Count() < 6) return new List<FileToLoad>();
            var parsedFileLines = parsedLine.ToList()
                        .GetRange(5, parsedLine.Count() - 5);
            return (from filesline in parsedFileLines
                    let filedir = filesline.Split('\\')
                    let filename = filedir[filedir.Count() - 1]
                    select new FileToLoad(filename, filesline)).ToList();
        }

        public static RestoringProcess CreateFromPath(string filePath, string safeFileName)
        {
            var fullName = ProcessManager.GetAppName(filePath);
            var processName = safeFileName;
            if (safeFileName.Contains("."))
            {
                processName = safeFileName.Substring(0, safeFileName.LastIndexOf('.'));
            }
            var extensions = (new FileProcessRestorer()).GetExtensions(processName);
            var pluginExist = extensions != null;
            return new RestoringProcess(fullName, processName, filePath,
                    pluginExist, extensions, new List<FileToLoad>());
        }
    }
}
