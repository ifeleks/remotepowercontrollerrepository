﻿using System;
using Common.DTO;
using Common.DTO.Enums;

namespace RPCClient.Entity
{
    public class Settings
    {
        public DateTime WorkTimeStart { get; set; }
        public DateTime WorkTimeEnd { get; set; }
        public int IdleTime { get; set; }
        public DefaultShutdownType ShutDownMode { get; set; }

        public Settings(DateTime workTimeStart, DateTime workTimeEnd, int idleTime, DefaultShutdownType shutDownMode)
        {
            WorkTimeStart = workTimeStart;
            WorkTimeEnd = workTimeEnd;
            IdleTime = idleTime;
            ShutDownMode = shutDownMode;
        }

        public SettingsTO ConvertToTO()
        {
            var transferObject = new SettingsTO();
            transferObject.WorkTimeStart = WorkTimeStart.TimeOfDay;
            transferObject.WorkTimeEnd = WorkTimeEnd.TimeOfDay;
            transferObject.IdleTime = IdleTime;
            transferObject.ShutDownMode = ShutDownMode;
            return transferObject;
        }

        public SettingsTO ConvertFromTO()
        {
            var transferObject = new SettingsTO
            {
                WorkTimeStart = WorkTimeStart.TimeOfDay,
                WorkTimeEnd = WorkTimeEnd.TimeOfDay,
                IdleTime = IdleTime,
                ShutDownMode = ShutDownMode
            };

            return transferObject;
        }

    }
}
