﻿namespace RPCClient.Entity
{
    public class FileToLoad
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }

        public FileToLoad(string fileName, string filePath)
        {
            FileName = fileName;
            FilePath = filePath;
        }
    }
}
