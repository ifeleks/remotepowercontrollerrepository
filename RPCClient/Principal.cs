﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.ServiceModel;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using Client.Service;
using Common.Availableness;
using Common.DTO.Enums;
using Common.Logger;
using Common.Logger.Impl;
using Common.Service.Proxy;
using DevExpress.XtraEditors;
using RPCClient.Authentication;
using RPCClient.AutoDiscovery;
using RPCClient.Availability;
using RPCClient.Enums;
using RPCClient.Properties;
using RPCClient.UI;
using RPCClient.VoiceControl.VoiceCommands;
using RPCServer.Interfaces;
using Shared.Audio;
using Shared.Keyboard;
using Shared.ProcessRestoring;
using Timer = System.Timers.Timer;

namespace RPCClient
{
    public class Principal : IDisposable
    {
        readonly ILogger _logger = LoggerFactory.Current.GetLogger<TrayIconManager>();
        private readonly TrayIconManager _trayIcon;
        private readonly FormOpener _formOpener;

        private ServiceHost _powerManagementServiceHost;
        public PowerManagementService PowerManagementService;

        private readonly KeyboardHook _hook = new KeyboardHook();
        public SubmitType SubmitType = SubmitType.UpdateSettings;
        private IProcessRestorer _processRestorer;

        private readonly Timer _customTimer = new Timer();
        private Timer _idleTimer;
        private TimeSpan _turnOffTime;

        private volatile bool _listening;

        public Principal()
        {
            _trayIcon = new TrayIconManager(this);
            _formOpener = new FormOpener(this);
        }

        public void LoadAppModules()
        {
            NetworkAuthentification();

            OpenPowerManagementService();

            StartHotkeyListening(_hook);

            StartIdleTimer();

            _trayIcon.ChangeVisible(true);

            LoadAppsSession();
        }

        private void NetworkAuthentification()
        {
            if (IsFirstLoad())
            {
                _formOpener.ShowNetworkAdapterForm();
                ServerSearch();
                if (!OnLoadServerCheck())
                {
                    _trayIcon.ChangeIcon((Icon)Resources.ResourceManager.GetObject("power_offline"));
                    return;
                }

                try
                {
                    SignInDomain();
                }
                catch
                {
                    _formOpener.ShowAccountForm(out SubmitType);
                }

                _formOpener.ShowSettingsForm();
            }
            else
            {
                ServerSearch();
                if (!OnLoadServerCheck())
                {
                    _trayIcon.ChangeIcon((Icon)Resources.ResourceManager.GetObject("power_offline"));
                }
                if (Settings.Default.IsVoiceMessagesOn) SoundNotificationFactory.Current.PlayHello();
            }
        }

        public void ShutdownAfter(object sender, EventArgs e)
        {
            const int time = 60000;
            var senderMenuItem = sender as MenuItem;

            if (senderMenuItem != null)
                switch (senderMenuItem.Text)
                {
                    case "Half hour":
                        ShutdownAfterTime(time * 30);
                        break;
                    case "One hour":
                        ShutdownAfterTime(time * 60);
                        break;
                    case "Two hours":
                        ShutdownAfterTime(time * 120);
                        break;
                }
            _trayIcon.AddShutdownAfterNotification(_turnOffTime);
        }

        public void ShutdownAfterTime(int shutdownTime)
        {
            _customTimer.Interval = shutdownTime;
            _customTimer.Elapsed += Shutdown;
            _customTimer.Start();

            _turnOffTime = DateTime.Now.TimeOfDay.Add(new TimeSpan(0, shutdownTime / 60000, 0));

            if (_turnOffTime.TotalMinutes > 0)
            {
                _trayIcon.AddShutdownAfterNotification(_turnOffTime);
            }
        }

        private void Shutdown(object sender, ElapsedEventArgs e)
        {
            _customTimer.Stop();

            _trayIcon.DeleteShutdownAfterNotification();

            var powerManagementService = new PowerManagementService();
            if (Settings.Default.ShutdownType == DefaultShutdownType.Sleep)
            {
                powerManagementService.Sleep();
            }

            if (Settings.Default.ShutdownType == DefaultShutdownType.Off)
            {
                powerManagementService.TurnOff();
            }
        }

        public void StartIdleTimer()
        {
            _idleTimer = new Timer(1000);
            _idleTimer.Elapsed += CheckIdleTime;
            _idleTimer.Start();
        }

        private void CheckIdleTime(object sender, ElapsedEventArgs e)
        {
            var idleTime = Settings.Default.IdleTime;
            var workTimeEnd = Settings.Default.WorkTimeEnd;
            var isWorkTimeEnd = DateTime.Now.TimeOfDay.TotalSeconds >= (workTimeEnd.TotalSeconds + idleTime * 60);
            var isIdleTimePassed = IdleTimeCounter.IdleTimeCounter.GetLastInputTime() > idleTime * 60;
            var isCommandInProcess = Settings.Default.IsCommandInProcess;
            var idleTimerStarted = _idleTimer.Enabled;
            var timerStarted = _customTimer.Enabled;

            //CommandInProcess property is a flag, that tells application that wcf has some command in execution
            if (!isWorkTimeEnd || !isIdleTimePassed || !idleTimerStarted || timerStarted || isCommandInProcess) return;

            var serverEndpoint = new EndpointAddress(
                string.Format("http://{0}:{1}/ClientInfoService",
                    Settings.Default["ServerIP"],
                    Settings.Default["ServerPort"]));


            if (ServiceAvailability.IsOnline(serverEndpoint.Uri.ToString()))
            {

                var ex = new Exception(string.Empty);
                ServiceChannel<IClientInfoService>.For(serverEndpoint, service =>
                {
                    try
                    {
                        service.ConfirmPowerAction(Settings.Default.ClientInfo,
                            Settings.Default.ShutdownType);
                    }
                    catch (Exception exception)
                    {
                        ex = exception;
                    }
                });
                if (ex.Message != string.Empty)
                {
                    if (Settings.Default.IsVoiceMessagesOn) SoundNotificationFactory.Current.PlayError();
                    XtraMessageBox.Show("WinService on this PC is not responding. Can't turn off computer.",
                        "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                if (Settings.Default.IsVoiceMessagesOn) SoundNotificationFactory.Current.PlayError();
                XtraMessageBox.Show("Server is not responding. Cannot turn off computer.",
                    "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            _idleTimer.Enabled = true;
        }

        private void ServerSearch()
        {
            if (!Settings.Default.IsAutoDiscoveryOn) return;

            var identifyBytes = new byte[] { 0x1, 0x2, 0x3 };
            const int autoDiscoveryPort = 18500;
            const int timeout = 500;

            DialogResult mbResult;
            var discoveryClient = new AutoDiscoveryClient(identifyBytes, autoDiscoveryPort, timeout);

            do
            {
                var watch = new Stopwatch();
                watch.Start();
                discoveryClient.ServerSearch();
                watch.Stop();

                if (discoveryClient.IsServerFound)
                {
                    _logger.Trace(string.Format("Found server at {0}:{1} in {2}",
                        discoveryClient.ServerAddress, discoveryClient.ServerPort, watch.Elapsed));
                    AddServerToSettings(discoveryClient.ServerAddress, discoveryClient.ServerPort);
                    mbResult = DialogResult.OK;
                }
                else
                {
                    if (Settings.Default.IsVoiceMessagesOn) SoundNotificationFactory.Current.PlayError();
                    mbResult = XtraMessageBox.Show("Server not found. Try again?\n(No - exit, Cancel - continue in offline mode)",
                        "Warning! - RemotePowerController", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Error);
                    if (mbResult == DialogResult.No)
                    {
                        Environment.Exit(0);
                    }
                }
                if (mbResult == DialogResult.Cancel) return;
            } while (mbResult == DialogResult.Yes);
        }

        private bool IsFirstLoad()
        {
            return Settings.Default.Login == string.Empty ||
                   Settings.Default.Password == string.Empty;
        }

        private void OpenPowerManagementService()
        {
            try
            {
                _powerManagementServiceHost = new ServiceHost(typeof(PowerManagementService));
                _powerManagementServiceHost.Open();
            }
            catch (Exception ex)
            {
                _logger.Error("Cant start PowerManagementService. Possibly you have no admin rights", ex);
            }
        }

        private void ClosePowerManagementService()
        {
            if (_powerManagementServiceHost != null) _powerManagementServiceHost.Close();
        }

        private void AddServerToSettings(string ip, int port)
        {
            Settings.Default["ServerIP"] = ip;
            Settings.Default["ServerPort"] = port.ToString(CultureInfo.InvariantCulture);
            Settings.Default.Save();
        }

        private void SignInDomain()
        {
            var serverEndpoint = new EndpointAddress(
                    string.Format("http://{0}:{1}/UserInfoService",
                        Settings.Default["ServerIP"],
                        Settings.Default["ServerPort"]));

            DomainAuthentification.SignIn(serverEndpoint, out SubmitType);
        }

        private void LoadAppsSession()
        {
            if (!Settings.Default.IsRestoringOn) return;
            if (AdminAccess.IsAdminAccess())
            {
                if (Settings.Default.IsVoiceMessagesOn) SoundNotificationFactory.Current.PlaySessionRestoring();
                _processRestorer = new FileProcessRestorer();
                _processRestorer.LoadLastPoint();
            }
            else
            {
                XtraMessageBox.Show("Administrator rights is required for session restoring.\nRestart the program with administrator rights.",
                    "RemotePowerController", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void StartHotkeyListening(KeyboardHook keyboardHook)
        {
            // register the control + alt + L combination as hot key.
            keyboardHook.RegisterHotKey(Settings.Default.ListenModifierKeys,
                Settings.Default.ListenHotKey);
            // register the event that is fired after the key press.
            keyboardHook.KeyPressed += hook_KeyPressed;
        }

        private void hook_KeyPressed(object sender, KeyPressedEventArgs e)
        {
            if (!Settings.Default.IsVoiceCommandsOn || _listening) return;

            _trayIcon.AddBalloonTip("Listening for voice command...", ToolTipIcon.Info);

            var listenerThread = new Thread(() =>
            {
                _listening = true;
                var voiceCommandListener = new VoiceCommandsListener();
                voiceCommandListener.StartListening();
                var voiceCommand = voiceCommandListener.GetVoiceCommand();
                _listening = false;
                if (voiceCommand != null)
                {
                    ExecuteVoiceCommand(voiceCommand);
                }
                else
                {
                    _trayIcon.AddBalloonTip("Voice command is not recognized.", ToolTipIcon.Warning);
                }
                Thread.CurrentThread.Join();
            });
            listenerThread.Start();
        }

        private void ExecuteVoiceCommand(string voiceCommand)
        {
            switch (voiceCommand)
            {
                case "close":
                    Exit();
                    break;
                case "go to sleep":
                    new PowerManagementService().Sleep();
                    break;
                case "shutdown":
                    new PowerManagementService().TurnOff();
                    break;
                case "settings":
                    _formOpener.ShowSettingsForm();
                    break;
                case "session restoring":
                    _formOpener.ShowSessionRestoringForm();
                    break;
                case "advanced settings":
                    _formOpener.ShowAdvancedSettingsForm();
                    break;
            }
        }

        public bool ServerAvailblenessCheck()
        {
            var serverEndpoint = string.Format("http://{0}:{1}/ClientInfoService",
               Settings.Default["ServerIP"],
               Settings.Default["ServerPort"]);
            if (ServiceAvailability.IsOnline(serverEndpoint)) return true;
            XtraMessageBox.Show("No server connection. Settings save canceled.", "Warning! - RemotePowerController",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return false;
        }

        private bool OnLoadServerCheck()
        {
            var serverEndpoint = string.Format("http://{0}:{1}/ClientInfoService",
               Settings.Default["ServerIP"],
               Settings.Default["ServerPort"]);

            var availability = ServiceAvailability.IsOnline(serverEndpoint);
            if (!availability)
            {
                XtraMessageBox.Show(string.Format("There is no server listening on {0}:{1}.\nPlease, change server settings in \"Advanced Settings\" menu",
                    Settings.Default["ServerIP"], Settings.Default["ServerPort"]), "Warning. - RemotePowerController");
            }
            return availability;
        }

        public void ShowSettingsForm(object sender, EventArgs e)
        {
            _formOpener.ShowSettingsForm();
        }

        public void ShowShutdowmAfterForm(object sender, EventArgs e)
        {
            _formOpener.ShowShutdowmAfterForm();
        }

        public void ShowSessionManagerForm(object sender, EventArgs e)
        {
            _formOpener.ShowSessionManagerForm();
        }

        public void ShowSessionRestoringForm(object sender, EventArgs e)
        {
            _formOpener.ShowSessionRestoringForm();
        }

        public void ShowAdvancedSettingsForm(object sender, EventArgs e)
        {
            _formOpener.ShowAdvancedSettingsForm();
        }

        public void Exit()
        {
            if (XtraMessageBox.Show("Do you really want to exit?", "Remote Power Controller",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            if (Settings.Default.IsVoiceMessagesOn) SoundNotificationFactory.Current.PlayExit();
            Dispose();
            Environment.Exit(0);
        }

        public void Dispose()
        {
            ClosePowerManagementService();
            _trayIcon.Dispose();
            _hook.Dispose();
            _customTimer.Close();
            _customTimer.Dispose();
            _idleTimer.Close();
            _idleTimer.Dispose();
        }
    }
}
