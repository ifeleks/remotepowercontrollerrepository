﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Common.Logger;
using Common.Logger.Impl;

namespace RPCClient.AutoDiscovery
{
    public class AutoDiscoveryClient
    {
        public bool IsServerFound;

        private int _autoDiscoveryPort;
        private int _autoDiscoveryTimeout;

        private byte[] _identifyBytesPacket;

        public string ServerAddress;
        public int ServerPort;

        private ILogger _logger;

        public AutoDiscoveryClient(byte[] identifyBytes, int autoDiscoveryPort, int timeout)
        {
            _logger = LoggerFactory.Current.GetLogger<AutoDiscoveryClient>();
            //_worker = worker;
            _identifyBytesPacket = identifyBytes;
            _autoDiscoveryPort = autoDiscoveryPort;
            _autoDiscoveryTimeout = timeout;
            _logger.Trace("AutoDiscovery started at {0}", _autoDiscoveryPort);
        }

        public void ServerSearch(/*object sender, DoWorkEventArgs e*/)
        {
            try
            {
                _logger.Trace("Looking for server..", _autoDiscoveryPort);
                SendBroadcastSearchPacket();
            }
            catch (Exception ex)
            {
                _logger.Error("Error with sending broadcast request", ex);
            }
        }

        private void SendBroadcastSearchPacket()
        {
            IsServerFound = false;
            UdpClient udp;
            var currentIP = Properties.Settings.Default.ClientInfo.ClientIp; //use current NetworkAdapter
            udp = currentIP != null ?
                new UdpClient(new IPEndPoint(IPAddress.Parse(currentIP), 0))
                : new UdpClient();
            
            udp.EnableBroadcast = true;
            udp.Client.ReceiveTimeout = _autoDiscoveryTimeout;
            var groupEndPoint = new IPEndPoint(IPAddress.Parse("255.255.255.255"), _autoDiscoveryPort);

            try
            {
                udp.Send(_identifyBytesPacket, _identifyBytesPacket.Length, groupEndPoint);

                var receiveBytes = udp.Receive(ref groupEndPoint);

                var returnData = Encoding.ASCII.GetString(receiveBytes, 0, receiveBytes.Length);
                switch (returnData.Substring(0, 3))
                {
                    case "ACK":
                    {
                        var splitRcvd = returnData.Split(' ', ':');
                        _logger.Trace("Response Server is {0}:{1}", splitRcvd[1], splitRcvd[2]);

                        //ServiceIsReachable function takes time. Lets do it without check at first
                        ServerAddress = splitRcvd[1];
                        ServerPort = Convert.ToInt32(splitRcvd[2]);
                        IsServerFound = true;
                        _logger.Trace("Founded server is RemotePowerController Server. Application start");
                        //(sender as BackgroundWorker).ReportProgress(1, string.Format("{0}:{1}", splitRcvd[1], splitRcvd[2]));
                    }
                        break;
                    case "NAK":
                        _logger.Trace("INVALID REQUEST");
                        break;
                    default:
                        _logger.Trace("Garbage Received?");
                        break;
                }
                _logger.Trace("Sleeping. No work to do");
            }
            catch (SocketException ex)
            {
                _logger.Error("UDP timeout", ex);
                //(sender as BackgroundWorker).ReportProgress(1);
            }
            udp.Close();
        }

    }
}
