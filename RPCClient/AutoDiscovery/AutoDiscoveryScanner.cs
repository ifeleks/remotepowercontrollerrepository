﻿using System;
using System.ComponentModel;

namespace RPCClient.AutoDiscovery
{
    public class AutoDiscoveryScanner : IDisposable
    {
        public volatile bool IsServerFound;
        public volatile bool IsSearchCompleted;
 
        public string ServerIp = null;
        public string ServerPort = null;

        private BackgroundWorker _workerAD;
        private BackgroundWorker bgworker;
        private AutoDiscoveryClient _svcAutoDiscoveryClient;

        public AutoDiscoveryScanner(byte[] identifyBytes, int autoDiscoveryPort, int attempts, int timeout)
        {
            IsSearchCompleted = false;
            _svcAutoDiscoveryClient = new AutoDiscoveryClient(ref _workerAD, identifyBytes,
                autoDiscoveryPort, attempts, timeout);
            _workerAD = new BackgroundWorker { WorkerReportsProgress = true, WorkerSupportsCancellation = true };
            _workerAD.DoWork += _svcAutoDiscoveryClient.ServerSearch;
            _workerAD.ProgressChanged += new ProgressChangedEventHandler(logWorkers_ProgressChanged);
            _workerAD.RunWorkerAsync();
        }

        private void logWorkers_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
            {
                var result = e.UserState.ToString();
                var serverAdress = result.Split(':');
                ServerIp = serverAdress[0];
                ServerPort = serverAdress[1];
                IsServerFound = true;
            }
            else
            {
                IsServerFound = false;
            }
            _svcAutoDiscoveryClient.Stop();
            IsSearchCompleted = true;
        }

        public void TryAgain()
        {
            if (_svcAutoDiscoveryClient == null) return;
            IsSearchCompleted = false;
            _svcAutoDiscoveryClient.Resume();
        }

        public void Dispose()
        {
            if (_svcAutoDiscoveryClient == null) return;
            _svcAutoDiscoveryClient.Stop();
            _workerAD.Dispose();
        }
    }
}
