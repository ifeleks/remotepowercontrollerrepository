﻿using System;
using System.Runtime.InteropServices;

namespace RPCClient.IdleTimeCounter
{
    public static class IdleTimeCounter
    {
        [DllImport("user32.dll")]
        static extern bool GetLastInputInfo(ref Lastinputinfo plii);

        [StructLayout(LayoutKind.Sequential)]
        struct Lastinputinfo
        {
            private static readonly int SizeOf = Marshal.SizeOf(typeof(Lastinputinfo));

            [MarshalAs(UnmanagedType.U4)]
            public Int32 cbSize;
            [MarshalAs(UnmanagedType.U4)]
            public Int32 dwTime;
        }

        public static int GetLastInputTime()
        {
            var idleTime = 0;
            var lastInputInfo = new Lastinputinfo();
            lastInputInfo.cbSize = Marshal.SizeOf(lastInputInfo);
            lastInputInfo.dwTime = 0;

            var envTicks = Environment.TickCount;

            if (!GetLastInputInfo(ref lastInputInfo)) return ((idleTime > 0) ? (idleTime / 1000) : 0);
            var lastInputTick = lastInputInfo.dwTime;

            idleTime = envTicks - lastInputTick;

            return ((idleTime > 0) ? (idleTime / 1000) : 0);
        }
    }
}
