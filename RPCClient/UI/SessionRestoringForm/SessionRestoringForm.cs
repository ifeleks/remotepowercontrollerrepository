﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using DevExpress.Skins.XtraForm;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using RPCClient.Entity;

namespace RPCClient.UI.SessionRestoringForm
{
    public partial class SessionRestoringForm : XtraForm, ISessionRestoringView
    {
        BindingList<RestoringProcess> listDataSource = new BindingList<RestoringProcess>();
        private bool _currentIsOnValue;
        private bool _currentIsCustomValue;
        private BindingList<RestoringProcess> _currentDataSource;
        private bool _changeVerificationCancel;

        public SessionRestoringViewPresenter Presenter
        {
            get
            {
                return SessionRestoringViewPresenter.GetInstance(this);
            }
            set
            { }
        }

        public void ShowView()
        {
            ShowDialog();
        }

        public IList<RestoringProcess> GetGridData()
        {
            return listDataSource;
        }

        public void RefreshUI(bool isRestoringOn, bool isRestoringCustom)
        {
            tglSessionRestoring.IsOn = isRestoringOn;
            btnCustomSession.Checked = isRestoringCustom;
            btnLastSession.Checked = !btnCustomSession.Checked;
            btnCustomSession.Enabled = tglSessionRestoring.IsOn;
            btnLastSession.Enabled = tglSessionRestoring.IsOn;
            if (!(tglSessionRestoring.IsOn && btnCustomSession.Checked))
            {
                gridControlSessionRestoring.Visible = false;
                btnAddProcess.Visible = false;
                MinimumSize = new Size(450, 115);
                Height = 115;
                FormBorderStyle = FormBorderStyle.FixedSingle;
                if (Width > 450) Width = 450;
                if (WindowState == FormWindowState.Maximized)
                {
                    WindowState = FormWindowState.Normal;
                }
            }
            else
            {
                gridControlSessionRestoring.Visible = true;
                btnAddProcess.Visible = true;
                MinimumSize = new Size(450, 140);
                Height = 350;
                FormBorderStyle = FormBorderStyle.Sizable;
            }
        }

        public SessionRestoringForm()
        {
            InitializeComponent();
        }

        private void SessionRestoringForm_Load(object sender, System.EventArgs e)
        {
            RefreshUI(Presenter.GetIsOnSetting(), Presenter.GetIsCustomSetting());
            LoadDataToGrid();
        }

        private void SessionRestoringForm_Shown(object sender, System.EventArgs e)
        {
            gridControlSessionRestoring.DataSource = listDataSource;
            //save value on form_shown for changes moinitor:
            _currentIsOnValue = tglSessionRestoring.IsOn;
            _currentIsCustomValue = btnCustomSession.Checked;
            _currentDataSource = new BindingList<RestoringProcess>();
            foreach (var restoringProcess in listDataSource)
            {
                _currentDataSource.Add(restoringProcess);
            }
        }

        private void btnLastSession_CheckedChanged(object sender, System.EventArgs e)
        {
            btnCustomSession.Checked = !btnLastSession.Checked;
            RefreshUI(tglSessionRestoring.IsOn, btnCustomSession.Checked);
        }

        private void btnCustomSession_CheckedChanged(object sender, System.EventArgs e)
        {
            btnLastSession.Checked = !btnCustomSession.Checked;
            RefreshUI(tglSessionRestoring.IsOn, btnCustomSession.Checked);
        }

        private void tglSessionRestoring_Toggled(object sender, System.EventArgs e)
        {
            RefreshUI(tglSessionRestoring.IsOn, btnCustomSession.Checked);
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            Presenter.SavePropertiesAndSettings(tglSessionRestoring.IsOn, btnCustomSession.Checked);
            Presenter.SaveCustomProcesses(listDataSource);
            _changeVerificationCancel = true;
            Close();
        }

        private void btnDeleteProcess_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var view = gridControlSessionRestoring.FocusedView as ColumnView;
            if (view != null) view.DeleteRow(view.FocusedRowHandle);
        }

        private void btnDeleteFile_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var view = gridControlSessionRestoring.FocusedView as ColumnView;
            if (view != null) view.DeleteRow(view.FocusedRowHandle);
        }

        private void btnAddProcess_Click(object sender, System.EventArgs e)
        {
            var ofd = new OpenFileDialog { Filter = @"Executable file|*.exe" };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                if (!File.Exists(ofd.FileName)) XtraMessageBox.Show("File no longer exist");
                listDataSource.Add(RestoringProcess.CreateFromPath(ofd.FileName, ofd.SafeFileName));
            }
        }
        
        private void btnBrowseFile_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var processRowIndex = gridViewSessionRestoring.GetFocusedDataSourceRowIndex();

            var columnView = gridControlSessionRestoring.FocusedView as ColumnView;
            if (columnView == null) return;
            var fileToLoadRowIndex = columnView.FocusedRowHandle;
            var detailView = gridViewFilesToLoad.GetDetailView(gridViewFilesToLoad.FocusedRowHandle, fileToLoadRowIndex) as ColumnView;
            if (detailView != null) fileToLoadRowIndex = detailView.FocusedRowHandle;
            
            if (fileToLoadRowIndex < 0 || fileToLoadRowIndex > listDataSource[processRowIndex].FilesToLoad.Count)
            {
                var ofd = new OpenFileDialog {InitialDirectory = "::{20D04FE0-3AEA-1069-A2D8-08002B30309D}"};

                if (listDataSource[processRowIndex].FileExtensions != null)
                {
                    var filter = listDataSource[processRowIndex].FileExtensions.Aggregate("|", (current, extension) => 
                        current + ("*." + extension + ";"));
                    ofd.Filter = filter;
                }
                if (ofd.ShowDialog() != DialogResult.OK) return;
                listDataSource[processRowIndex].FilesToLoad.Add(new FileToLoad(ofd.SafeFileName, ofd.FileName));
                gridViewSessionRestoring.RefreshData();
            }
            else
            {
                var filePath = listDataSource[processRowIndex].FilesToLoad[fileToLoadRowIndex].FilePath.Trim('\"');
                if (File.Exists(filePath))
                {
                    var ofd = new OpenFileDialog {InitialDirectory = Path.GetDirectoryName(filePath)};
                    if (listDataSource[processRowIndex].FileExtensions != null)
                    {
                        var filter = listDataSource[processRowIndex].FileExtensions.Aggregate("|", (current, extension) =>
                            current + ("*." + extension + ";"));
                        ofd.Filter = filter;
                        
                    }
                    ofd.FileName = listDataSource[processRowIndex].FilesToLoad[fileToLoadRowIndex].FileName;

                    if (ofd.ShowDialog() != DialogResult.OK) return;
                    listDataSource[processRowIndex].FilesToLoad[fileToLoadRowIndex].FileName = ofd.SafeFileName;
                    listDataSource[processRowIndex].FilesToLoad[fileToLoadRowIndex].FilePath = ofd.FileName;
                    gridViewSessionRestoring.RefreshData();
                }
                else
                {
                    XtraMessageBox.Show("File no longer exist", "Remote Power Controler", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                
            }
        }

        private void gridViewSessionRestoring_MasterRowGetRelationCount(object sender, MasterRowGetRelationCountEventArgs e)
        {
            e.RelationCount = 1;
        }

        private void gridViewSessionRestoring_MasterRowGetRelationName(object sender, MasterRowGetRelationNameEventArgs e)
        {
            switch (e.RelationIndex)
            {
                case 0: 
                    e.RelationName = "FilesToLoadView";
                    break;
            }
        }

        private void gridViewSessionRestoring_MasterRowGetRelationDisplayCaption(object sender, MasterRowGetRelationNameEventArgs e)
        {
            switch (e.RelationIndex)
            {
                case 0:
                {
                    var process = gridViewSessionRestoring.GetRow(e.RowHandle) as RestoringProcess;

                    if (process != null && process.PluginExist)
                    {
                        if (process.ProcessName == "firefox" ||
                            process.ProcessName == "chrome" ||
                            process.ProcessName == "iexplore" ||
                            process.ProcessName == "opera")
                        {
                            e.RelationName = "URLs";
                        }
                        else
                        {
                            e.RelationName = "Files to load";
                        }
                    }
                    else
                    {
                        e.RelationName = "No plugin";
                    }
                    break;
                }
            }
        }

        private void gridViewSessionRestoring_MasterRowGetChildList(object sender, MasterRowGetChildListEventArgs e)
        {
            switch (e.RelationIndex)
            {
                case 0:
                {
                    var process = gridViewSessionRestoring.GetRow(e.RowHandle) as RestoringProcess;

                    if (process != null && process.PluginExist)
                    {
                        if (process.ProcessName == "firefox" ||
                            process.ProcessName == "chrome" ||
                            process.ProcessName == "iexplore" ||
                            process.ProcessName == "opera")
                        {
                            SetURLGridViewType();
                        }
                        else
                        {
                            SetFileGridViewType();
                        }

                        e.ChildList = (IList)process.FilesToLoad;
                    }
                    break;
                }
            }
        }

        private void SetURLGridViewType()
        {
            gridViewFilesToLoad.Columns["FileName"].Visible = false;
            gridViewFilesToLoad.Columns["FilePath"].Caption = @"URL";
            gridViewFilesToLoad.Columns["FilePath"].OptionsColumn.AllowEdit = true;
            gridViewFilesToLoad.Columns["FilePath"].VisibleIndex = 0;
            gridViewFilesToLoad.Columns["Browse"].Visible = false;
            gridViewFilesToLoad.Columns["Delete"].VisibleIndex = 1;
        }

        private void SetFileGridViewType()
        {
            gridViewFilesToLoad.Columns["FileName"].Visible = true;
            gridViewFilesToLoad.Columns["FileName"].VisibleIndex = 0;
            gridViewFilesToLoad.Columns["FilePath"].Caption = @"File path";
            gridViewFilesToLoad.Columns["FilePath"].OptionsColumn.AllowEdit = false;
            gridViewFilesToLoad.Columns["FilePath"].VisibleIndex = 1;
            gridViewFilesToLoad.Columns["Browse"].VisibleIndex = 2;
            gridViewFilesToLoad.Columns["Browse"].Visible = true;
            gridViewFilesToLoad.Columns["Delete"].VisibleIndex = 3;
        }
        
        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            _changeVerificationCancel = true;
            RefreshUI(tglSessionRestoring.IsOn, btnCustomSession.Checked);
            Close();
        }

        private void SessionRestoringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!FormWasChanged() || _changeVerificationCancel) return;
            var messageBox = XtraMessageBox.Show("Save changes?", "Remote Power Controller",
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            switch (messageBox)
            {
                case DialogResult.Yes:
                    Presenter.SavePropertiesAndSettings(tglSessionRestoring.IsOn, btnCustomSession.Checked);
                    Presenter.SaveCustomProcesses(listDataSource);
                    break;
                case DialogResult.Cancel:
                    e.Cancel = true;
                    break;
                case DialogResult.No:
                    Presenter.SavePropertiesAndSettings(_currentIsOnValue, _currentIsCustomValue);
                    break;
            }
        }

        private bool FormWasChanged()
        {
            if (listDataSource.Count != _currentDataSource.Count) return true;
            if (_currentDataSource.Any(data => !listDataSource.Contains(data))) return true;
            return _currentIsOnValue != tglSessionRestoring.IsOn || _currentIsCustomValue != btnCustomSession.Checked;
        }

        private void LoadDataToGrid()
        {
            listDataSource.Clear();
            var processes = Presenter.GetProcesses();
            processes = RefreshData(processes);
            foreach (var process in processes)
            {
                listDataSource.Add(process);
            }
        }

        //if file|plugin was deleted:
        private List<RestoringProcess> RefreshData(List<RestoringProcess> processes)
        {
            var refreshedProcesses = new List<RestoringProcess>();
            foreach (var restoringProcess in processes)
            {
                var refreshedProcInfo = RestoringProcess
                    .CreateFromPath(restoringProcess.ProcessPath, restoringProcess.ProcessName);
                refreshedProcesses.Add(refreshedProcInfo.PluginExist != restoringProcess.PluginExist
                    ? refreshedProcInfo
                    : restoringProcess);
            }
            return refreshedProcesses;
        }

        private void gridViewFilesToLoad_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            var processRowIndex = gridViewSessionRestoring.GetFocusedDataSourceRowIndex();

            var columnView = gridControlSessionRestoring.FocusedView as ColumnView;

            if (columnView != null)
            {
                var fileToLoadRowIndex = columnView.FocusedRowHandle;
                var detailView = gridViewFilesToLoad.GetDetailView(gridViewFilesToLoad.FocusedRowHandle, fileToLoadRowIndex) as ColumnView;
                if (detailView != null)
                    fileToLoadRowIndex = detailView.GetDataSourceRowIndex(e.RowHandle);

                if (fileToLoadRowIndex < 0 || fileToLoadRowIndex > listDataSource[processRowIndex].FilesToLoad.Count)
                {
                    listDataSource[processRowIndex].FilesToLoad.Add(new FileToLoad("", ((GridView)sender).EditingValue.ToString()));
                }
                else
                {
                    listDataSource[processRowIndex].FilesToLoad[fileToLoadRowIndex].FilePath = ((GridView)sender).EditingValue.ToString();
                }
            }
            gridViewSessionRestoring.RefreshData();
        }
    }
}
