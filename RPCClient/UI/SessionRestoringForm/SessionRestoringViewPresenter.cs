﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Client.Service;
using RPCClient.Entity;
using RPCClient.Infrastructure;
using Shared.ProcessRestoring;

namespace RPCClient.UI.SessionRestoringForm
{
    public class SessionRestoringViewPresenter : BasePresenter<ISessionRestoringView, SessionRestoringViewPresenter>
    {
        private static SessionRestoringViewPresenter _presenterInstance;
        readonly ISessionRestoringView _view;

        private IProcessRestorer _processManager;

        public static SessionRestoringViewPresenter GetInstance(ISessionRestoringView view)
        {
            return _presenterInstance ?? (_presenterInstance = new SessionRestoringViewPresenter(view));
        }

        public SessionRestoringViewPresenter(ISessionRestoringView view)
        {
            _view = view;
        }

        public void SavePropertiesAndSettings(bool isRestoringOn, bool isRestorinCustom)
        {
            Properties.Settings.Default.IsRestoringOn = isRestoringOn;
            Properties.Settings.Default.IsRestoringCustom = isRestorinCustom;
            Properties.Settings.Default.Save();
        }

        public bool GetIsOnSetting()
        {
            return Properties.Settings.Default.IsRestoringOn;
        }

        public bool GetIsCustomSetting()
        {
            return Properties.Settings.Default.IsRestoringCustom;
        }

        public List<RestoringProcess> GetProcesses()
        {
            _processManager = new FileProcessRestorer();
            List<RestoringProcess> processLines;
            try
            {
                processLines = _processManager.GetProcessList()
                    .Select(RestoringProcess.ParseFromString)
                    .ToList();
            }
            catch
            {
                processLines = new List<RestoringProcess>();
            }
            return processLines;
        }

        public void SaveCustomProcesses(BindingList<RestoringProcess> listDataSource)
        {
            _processManager = new FileProcessRestorer();
            var processList = listDataSource;
            var processStringList = processList
                .Select(restoringProcess => restoringProcess.ConvertToString())
                .ToList();
            _processManager.CreateSavePoint(processStringList);
        }
    }
}
