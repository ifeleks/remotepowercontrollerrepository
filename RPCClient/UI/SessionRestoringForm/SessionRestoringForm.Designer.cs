﻿namespace RPCClient.UI.SessionRestoringForm
{
    partial class SessionRestoringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SessionRestoringForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridViewFilesToLoad = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnFilePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnBrowseFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnBrowseFile = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumnDeleteFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDeleteFile = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridControlSessionRestoring = new DevExpress.XtraGrid.GridControl();
            this.gridViewSessionRestoring = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnProcessName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPlugin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkPlugin = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumnDeleteProcess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDeleteProcess = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.tglSessionRestoring = new DevExpress.XtraEditors.ToggleSwitch();
            this.btnLastSession = new DevExpress.XtraEditors.CheckButton();
            this.btnCustomSession = new DevExpress.XtraEditors.CheckButton();
            this.lblSessionRestoring = new DevExpress.XtraEditors.LabelControl();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddProcess = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewFilesToLoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBrowseFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSessionRestoring)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSessionRestoring)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPlugin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tglSessionRestoring.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewFilesToLoad
            // 
            this.gridViewFilesToLoad.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnFileName,
            this.gridColumnFilePath,
            this.gridColumnBrowseFile,
            this.gridColumnDeleteFile});
            this.gridViewFilesToLoad.GridControl = this.gridControlSessionRestoring;
            this.gridViewFilesToLoad.Name = "gridViewFilesToLoad";
            this.gridViewFilesToLoad.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewFilesToLoad.OptionsView.ShowGroupPanel = false;
            this.gridViewFilesToLoad.OptionsView.ShowIndicator = false;
            this.gridViewFilesToLoad.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewFilesToLoad_CellValueChanged);
            // 
            // gridColumnFileName
            // 
            this.gridColumnFileName.Caption = "File name";
            this.gridColumnFileName.FieldName = "FileName";
            this.gridColumnFileName.Name = "gridColumnFileName";
            this.gridColumnFileName.OptionsColumn.AllowEdit = false;
            this.gridColumnFileName.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumnFileName.Visible = true;
            this.gridColumnFileName.VisibleIndex = 0;
            this.gridColumnFileName.Width = 105;
            // 
            // gridColumnFilePath
            // 
            this.gridColumnFilePath.Caption = "Faile path";
            this.gridColumnFilePath.FieldName = "FilePath";
            this.gridColumnFilePath.Name = "gridColumnFilePath";
            this.gridColumnFilePath.OptionsColumn.AllowEdit = false;
            this.gridColumnFilePath.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumnFilePath.Visible = true;
            this.gridColumnFilePath.VisibleIndex = 1;
            this.gridColumnFilePath.Width = 199;
            // 
            // gridColumnBrowseFile
            // 
            this.gridColumnBrowseFile.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnBrowseFile.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnBrowseFile.Caption = "Browse";
            this.gridColumnBrowseFile.ColumnEdit = this.btnBrowseFile;
            this.gridColumnBrowseFile.FieldName = "Browse";
            this.gridColumnBrowseFile.Name = "gridColumnBrowseFile";
            this.gridColumnBrowseFile.OptionsColumn.FixedWidth = true;
            this.gridColumnBrowseFile.OptionsFilter.AllowFilter = false;
            this.gridColumnBrowseFile.Visible = true;
            this.gridColumnBrowseFile.VisibleIndex = 2;
            this.gridColumnBrowseFile.Width = 45;
            // 
            // btnBrowseFile
            // 
            this.btnBrowseFile.AutoHeight = false;
            this.btnBrowseFile.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnBrowseFile.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", null, null, true)});
            this.btnBrowseFile.Name = "btnBrowseFile";
            this.btnBrowseFile.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnBrowseFile.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnBrowseFile_ButtonClick);
            // 
            // gridColumnDeleteFile
            // 
            this.gridColumnDeleteFile.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnDeleteFile.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnDeleteFile.Caption = "Delete";
            this.gridColumnDeleteFile.ColumnEdit = this.btnDeleteFile;
            this.gridColumnDeleteFile.FieldName = "Delete";
            this.gridColumnDeleteFile.Name = "gridColumnDeleteFile";
            this.gridColumnDeleteFile.OptionsColumn.FixedWidth = true;
            this.gridColumnDeleteFile.OptionsFilter.AllowFilter = false;
            this.gridColumnDeleteFile.Visible = true;
            this.gridColumnDeleteFile.VisibleIndex = 3;
            this.gridColumnDeleteFile.Width = 49;
            // 
            // btnDeleteFile
            // 
            this.btnDeleteFile.AutoHeight = false;
            this.btnDeleteFile.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnDeleteFile.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "", null, null, true)});
            this.btnDeleteFile.Name = "btnDeleteFile";
            this.btnDeleteFile.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnDeleteFile.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnDeleteFile_ButtonClick);
            // 
            // gridControlSessionRestoring
            // 
            this.gridControlSessionRestoring.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlSessionRestoring.Cursor = System.Windows.Forms.Cursors.Default;
            gridLevelNode2.LevelTemplate = this.gridViewFilesToLoad;
            gridLevelNode2.RelationName = "FilesToLoadView";
            this.gridControlSessionRestoring.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gridControlSessionRestoring.Location = new System.Drawing.Point(12, 41);
            this.gridControlSessionRestoring.MainView = this.gridViewSessionRestoring;
            this.gridControlSessionRestoring.Name = "gridControlSessionRestoring";
            this.gridControlSessionRestoring.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnDeleteProcess,
            this.checkPlugin,
            this.btnDeleteFile,
            this.btnBrowseFile});
            this.gridControlSessionRestoring.Size = new System.Drawing.Size(413, 223);
            this.gridControlSessionRestoring.TabIndex = 4;
            this.gridControlSessionRestoring.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSessionRestoring,
            this.gridViewFilesToLoad});
            // 
            // gridViewSessionRestoring
            // 
            this.gridViewSessionRestoring.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnProcessName,
            this.gridColumnPlugin,
            this.gridColumnDeleteProcess});
            this.gridViewSessionRestoring.GridControl = this.gridControlSessionRestoring;
            this.gridViewSessionRestoring.Name = "gridViewSessionRestoring";
            this.gridViewSessionRestoring.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewSessionRestoring.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewSessionRestoring.OptionsDetail.AllowExpandEmptyDetails = true;
            this.gridViewSessionRestoring.OptionsDetail.AllowOnlyOneMasterRowExpanded = true;
            this.gridViewSessionRestoring.OptionsDetail.EnableDetailToolTip = true;
            this.gridViewSessionRestoring.OptionsDetail.ShowDetailTabs = false;
            this.gridViewSessionRestoring.OptionsNavigation.UseTabKey = false;
            this.gridViewSessionRestoring.OptionsView.ShowGroupPanel = false;
            this.gridViewSessionRestoring.OptionsView.ShowIndicator = false;
            this.gridViewSessionRestoring.MasterRowGetChildList += new DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventHandler(this.gridViewSessionRestoring_MasterRowGetChildList);
            this.gridViewSessionRestoring.MasterRowGetRelationName += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.gridViewSessionRestoring_MasterRowGetRelationName);
            this.gridViewSessionRestoring.MasterRowGetRelationDisplayCaption += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.gridViewSessionRestoring_MasterRowGetRelationDisplayCaption);
            this.gridViewSessionRestoring.MasterRowGetRelationCount += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventHandler(this.gridViewSessionRestoring_MasterRowGetRelationCount);
            // 
            // gridColumnProcessName
            // 
            this.gridColumnProcessName.Caption = "Process name";
            this.gridColumnProcessName.FieldName = "ProcessFullName";
            this.gridColumnProcessName.Name = "gridColumnProcessName";
            this.gridColumnProcessName.OptionsColumn.AllowEdit = false;
            this.gridColumnProcessName.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumnProcessName.Visible = true;
            this.gridColumnProcessName.VisibleIndex = 0;
            this.gridColumnProcessName.Width = 321;
            // 
            // gridColumnPlugin
            // 
            this.gridColumnPlugin.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnPlugin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnPlugin.Caption = "Plugin";
            this.gridColumnPlugin.ColumnEdit = this.checkPlugin;
            this.gridColumnPlugin.FieldName = "PluginExist";
            this.gridColumnPlugin.Name = "gridColumnPlugin";
            this.gridColumnPlugin.OptionsColumn.AllowEdit = false;
            this.gridColumnPlugin.OptionsColumn.FixedWidth = true;
            this.gridColumnPlugin.OptionsColumn.ReadOnly = true;
            this.gridColumnPlugin.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.gridColumnPlugin.Visible = true;
            this.gridColumnPlugin.VisibleIndex = 1;
            this.gridColumnPlugin.Width = 45;
            // 
            // checkPlugin
            // 
            this.checkPlugin.AutoHeight = false;
            this.checkPlugin.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.checkPlugin.Caption = "Check";
            this.checkPlugin.Name = "checkPlugin";
            this.checkPlugin.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.checkPlugin.ReadOnly = true;
            // 
            // gridColumnDeleteProcess
            // 
            this.gridColumnDeleteProcess.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumnDeleteProcess.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnDeleteProcess.Caption = "Delete";
            this.gridColumnDeleteProcess.ColumnEdit = this.btnDeleteProcess;
            this.gridColumnDeleteProcess.Name = "gridColumnDeleteProcess";
            this.gridColumnDeleteProcess.OptionsColumn.FixedWidth = true;
            this.gridColumnDeleteProcess.OptionsFilter.AllowFilter = false;
            this.gridColumnDeleteProcess.Visible = true;
            this.gridColumnDeleteProcess.VisibleIndex = 2;
            this.gridColumnDeleteProcess.Width = 50;
            // 
            // btnDeleteProcess
            // 
            this.btnDeleteProcess.AutoHeight = false;
            this.btnDeleteProcess.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("btnDeleteProcess.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, "", null, null, true)});
            this.btnDeleteProcess.Name = "btnDeleteProcess";
            this.btnDeleteProcess.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnDeleteProcess.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnDeleteProcess_ButtonClick);
            // 
            // tglSessionRestoring
            // 
            this.tglSessionRestoring.Location = new System.Drawing.Point(100, 12);
            this.tglSessionRestoring.Name = "tglSessionRestoring";
            this.tglSessionRestoring.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.tglSessionRestoring.Properties.OffText = "Off";
            this.tglSessionRestoring.Properties.OnText = "On";
            this.tglSessionRestoring.Size = new System.Drawing.Size(95, 24);
            this.tglSessionRestoring.TabIndex = 0;
            this.tglSessionRestoring.Toggled += new System.EventHandler(this.tglSessionRestoring_Toggled);
            // 
            // btnLastSession
            // 
            this.btnLastSession.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLastSession.Checked = true;
            this.btnLastSession.Location = new System.Drawing.Point(233, 12);
            this.btnLastSession.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.btnLastSession.Name = "btnLastSession";
            this.btnLastSession.Size = new System.Drawing.Size(96, 23);
            this.btnLastSession.TabIndex = 1;
            this.btnLastSession.Text = "Last session";
            this.btnLastSession.CheckedChanged += new System.EventHandler(this.btnLastSession_CheckedChanged);
            // 
            // btnCustomSession
            // 
            this.btnCustomSession.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCustomSession.Location = new System.Drawing.Point(329, 12);
            this.btnCustomSession.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.btnCustomSession.Name = "btnCustomSession";
            this.btnCustomSession.Size = new System.Drawing.Size(96, 23);
            this.btnCustomSession.TabIndex = 2;
            this.btnCustomSession.Text = "Custom session";
            this.btnCustomSession.CheckedChanged += new System.EventHandler(this.btnCustomSession_CheckedChanged);
            // 
            // lblSessionRestoring
            // 
            this.lblSessionRestoring.Location = new System.Drawing.Point(12, 16);
            this.lblSessionRestoring.Name = "lblSessionRestoring";
            this.lblSessionRestoring.Size = new System.Drawing.Size(82, 13);
            this.lblSessionRestoring.TabIndex = 3;
            this.lblSessionRestoring.Text = "Session restoring";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(269, 270);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(350, 270);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAddProcess
            // 
            this.btnAddProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddProcess.Location = new System.Drawing.Point(12, 270);
            this.btnAddProcess.Name = "btnAddProcess";
            this.btnAddProcess.Size = new System.Drawing.Size(86, 23);
            this.btnAddProcess.TabIndex = 7;
            this.btnAddProcess.Text = "Add process";
            this.btnAddProcess.Click += new System.EventHandler(this.btnAddProcess_Click);
            // 
            // SessionRestoringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 305);
            this.Controls.Add(this.btnAddProcess);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.gridControlSessionRestoring);
            this.Controls.Add(this.lblSessionRestoring);
            this.Controls.Add(this.btnCustomSession);
            this.Controls.Add(this.btnLastSession);
            this.Controls.Add(this.tglSessionRestoring);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(422, 198);
            this.Name = "SessionRestoringForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Session Restoring";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SessionRestoringForm_FormClosing);
            this.Load += new System.EventHandler(this.SessionRestoringForm_Load);
            this.Shown += new System.EventHandler(this.SessionRestoringForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewFilesToLoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBrowseFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSessionRestoring)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSessionRestoring)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPlugin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tglSessionRestoring.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ToggleSwitch tglSessionRestoring;
        private DevExpress.XtraEditors.CheckButton btnLastSession;
        private DevExpress.XtraEditors.CheckButton btnCustomSession;
        private DevExpress.XtraEditors.LabelControl lblSessionRestoring;
        private DevExpress.XtraGrid.GridControl gridControlSessionRestoring;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSessionRestoring;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnProcessName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPlugin;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDeleteProcess;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDeleteProcess;
        private DevExpress.XtraEditors.SimpleButton btnAddProcess;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit checkPlugin;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewFilesToLoad;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnFileName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnFilePath;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDeleteFile;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnDeleteFile;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnBrowseFile;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnBrowseFile;
    }
}