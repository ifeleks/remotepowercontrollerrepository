﻿using System.Collections.Generic;
using RPCClient.Entity;
using RPCClient.Infrastructure;

namespace RPCClient.UI.SessionRestoringForm
{
    public interface ISessionRestoringView : IView<SessionRestoringViewPresenter>
    {
        IList<RestoringProcess> GetGridData();
    }
}
