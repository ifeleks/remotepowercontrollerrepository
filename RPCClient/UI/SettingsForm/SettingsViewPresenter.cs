﻿using System;
using System.Threading;
using Common.Availableness;
using Common.DTO;
using RPCClient.Entity;
using RPCClient.Enums;
using RPCClient.Infrastructure;

namespace RPCClient.UI.SettingsForm
{
    public class SettingsViewPresenter : BasePresenter<ISettingsView, SettingsViewPresenter>
    {
        private static SettingsViewPresenter _presenterInstance;
        private Principal _principal;

        public static SettingsViewPresenter GetInstance(ISettingsView view, Principal principal)
        {
            return _presenterInstance ?? (_presenterInstance = new SettingsViewPresenter(view, principal));
        }

        readonly ISettingsView _view;
        public SettingsViewPresenter(ISettingsView view, Principal principal)
        {
            _view = view;
            _principal = principal;
        }

        public void SendInfo()
        {
            var sendInfoThread = new Thread(SendInfoWork);
            sendInfoThread.Start();
        }

        private void SendInfoWork()
        {
            var serverEndpoint = string.Format("http://{0}:{1}/ClientInfoService",
                Properties.Settings.Default["ServerIP"],
                Properties.Settings.Default["ServerPort"]);

            ServiceAvailability.DoWorkSafely(serverEndpoint, DetermineSaveButtonBehavior);

            Thread.CurrentThread.Join(); //terminate if succesfull
        }

        private void DetermineSaveButtonBehavior()
        {
            switch (_principal.SubmitType)
            {
                case SubmitType.RegisterNewUser:
                    RegisterNewUser();
                    break;
                case SubmitType.AddNewClient:
                    AddNewClient();
                    break;
                case SubmitType.UpdateSettings:
                    ChangeSettings();
                    break;
            }
            _principal.SubmitType = SubmitType.UpdateSettings;
        }

        public void RegisterNewUser()
        {
            var clientInfoTO = Properties.Settings.Default["ClientInfo"] as ClientInfoTO;
            var userInfo = new UserInfo(
                Properties.Settings.Default["Login"].ToString(),
                Properties.Settings.Default["Password"].ToString(),
                Properties.Settings.Default.IsUserDomain);
            var settings = GetSettingsFromConfig();

            var userInfoTO = userInfo.ConvertToTO();
            var settingsTO = settings.ConvertToTO();

            ServerConnection.SendDataToServer.RegisterNewUser(clientInfoTO, userInfoTO, settingsTO);
        }

        public void ChangeSettings()
        {
            var clientInfoTO = Properties.Settings.Default["ClientInfo"] as ClientInfoTO;
            var settings = GetSettingsFromConfig();

            var settingsTO = settings.ConvertToTO();

            ServerConnection.SendDataToServer.ChangeSettings(clientInfoTO, settingsTO);
        }

        public void AddNewClient()
        {
            var clientInfoTO = Properties.Settings.Default["ClientInfo"] as ClientInfoTO;
            var userInfo = new UserInfo(
                Properties.Settings.Default["Login"].ToString(),
                Properties.Settings.Default["Password"].ToString(),
                Properties.Settings.Default.IsUserDomain);
            var settings = GetSettingsFromConfig();

            var userInfoTO = userInfo.ConvertToTO();
            var settingsTO = settings.ConvertToTO();

            ServerConnection.SendDataToServer.AddNewClient(clientInfoTO, userInfoTO, settingsTO);
        }

        public void SaveSettings(Settings settings)
        {
            Properties.Settings.Default.WorkTimeStart = settings.WorkTimeStart.TimeOfDay;
            Properties.Settings.Default.WorkTimeEnd = settings.WorkTimeEnd.TimeOfDay;
            Properties.Settings.Default.IdleTime = settings.IdleTime;
            Properties.Settings.Default.ShutdownType = settings.ShutDownMode;
            Properties.Settings.Default.Save();
        }

        public Settings GetSettingsFromConfig()
        {
            var settings = new Settings(
                new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day,
                    Properties.Settings.Default.WorkTimeStart.Hours,
                    Properties.Settings.Default.WorkTimeStart.Minutes,
                    Properties.Settings.Default.WorkTimeStart.Seconds),
                new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day,
                    Properties.Settings.Default.WorkTimeEnd.Hours,
                    Properties.Settings.Default.WorkTimeEnd.Minutes,
                    Properties.Settings.Default.WorkTimeEnd.Seconds),
                Properties.Settings.Default.IdleTime,
                Properties.Settings.Default.ShutdownType);

            return settings;
        }
    }
}
