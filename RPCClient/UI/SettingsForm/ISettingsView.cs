﻿using System;
using RPCClient.Entity;
using RPCClient.Infrastructure;

namespace RPCClient.UI.SettingsForm
{
    public interface ISettingsView : IView<SettingsViewPresenter>
    {
    }
}
