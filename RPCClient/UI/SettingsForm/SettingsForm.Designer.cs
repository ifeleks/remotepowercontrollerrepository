﻿namespace RPCClient.UI.SettingsForm
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.lblWorkTime = new System.Windows.Forms.Label();
            this.lblDash = new System.Windows.Forms.Label();
            this.lblTimeFormat = new System.Windows.Forms.Label();
            this.lblOffMode = new System.Windows.Forms.Label();
            this.rtbWorkTime = new DevExpress.XtraEditors.RangeTrackBarControl();
            this.tmWorkTimeStart = new DevExpress.XtraEditors.TimeEdit();
            this.tmWorkTimeEnd = new DevExpress.XtraEditors.TimeEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.spnIdleTime = new DevExpress.XtraEditors.SpinEdit();
            this.lblMinute = new System.Windows.Forms.Label();
            this.lblIdleTime = new System.Windows.Forms.Label();
            this.cmbOffMode = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbWorkTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbWorkTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmWorkTimeStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmWorkTimeEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnIdleTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbOffMode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblWorkTime
            // 
            this.lblWorkTime.AutoSize = true;
            this.lblWorkTime.Location = new System.Drawing.Point(12, 43);
            this.lblWorkTime.Name = "lblWorkTime";
            this.lblWorkTime.Size = new System.Drawing.Size(59, 13);
            this.lblWorkTime.TabIndex = 4;
            this.lblWorkTime.Text = "Work time:";
            // 
            // lblDash
            // 
            this.lblDash.AutoSize = true;
            this.lblDash.Location = new System.Drawing.Point(156, 43);
            this.lblDash.Name = "lblDash";
            this.lblDash.Size = new System.Drawing.Size(11, 13);
            this.lblDash.TabIndex = 14;
            this.lblDash.Text = "-";
            // 
            // lblTimeFormat
            // 
            this.lblTimeFormat.AutoSize = true;
            this.lblTimeFormat.Location = new System.Drawing.Point(251, 43);
            this.lblTimeFormat.Name = "lblTimeFormat";
            this.lblTimeFormat.Size = new System.Drawing.Size(47, 13);
            this.lblTimeFormat.TabIndex = 31;
            this.lblTimeFormat.Text = "(hh:mm)";
            // 
            // lblOffMode
            // 
            this.lblOffMode.AutoSize = true;
            this.lblOffMode.Location = new System.Drawing.Point(12, 15);
            this.lblOffMode.Name = "lblOffMode";
            this.lblOffMode.Size = new System.Drawing.Size(56, 13);
            this.lblOffMode.TabIndex = 44;
            this.lblOffMode.Text = "Off mode:";
            // 
            // rtbWorkTime
            // 
            this.rtbWorkTime.AllowRangeDragging = true;
            this.rtbWorkTime.EditValue = new DevExpress.XtraEditors.Repository.TrackBarRange(540, 1140);
            this.rtbWorkTime.Location = new System.Drawing.Point(77, 66);
            this.rtbWorkTime.Name = "rtbWorkTime";
            this.rtbWorkTime.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.rtbWorkTime.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rtbWorkTime.Properties.Maximum = 1439;
            this.rtbWorkTime.Properties.TickStyle = System.Windows.Forms.TickStyle.None;
            this.rtbWorkTime.Size = new System.Drawing.Size(223, 45);
            this.rtbWorkTime.TabIndex = 3;
            this.rtbWorkTime.Value = new DevExpress.XtraEditors.Repository.TrackBarRange(540, 1140);
            this.rtbWorkTime.EditValueChanged += new System.EventHandler(this.rtbWorkTime_EditValueChanged);
            // 
            // tmWorkTimeStart
            // 
            this.tmWorkTimeStart.EditValue = new System.DateTime(2014, 6, 18, 9, 0, 0, 0);
            this.tmWorkTimeStart.Location = new System.Drawing.Point(77, 40);
            this.tmWorkTimeStart.Name = "tmWorkTimeStart";
            this.tmWorkTimeStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tmWorkTimeStart.Properties.Mask.EditMask = "t";
            this.tmWorkTimeStart.Size = new System.Drawing.Size(73, 20);
            this.tmWorkTimeStart.TabIndex = 1;
            this.tmWorkTimeStart.EditValueChanged += new System.EventHandler(this.tmWorkTimeStart_EditValueChanged);
            // 
            // tmWorkTimeEnd
            // 
            this.tmWorkTimeEnd.EditValue = new System.DateTime(2014, 6, 18, 19, 0, 0, 0);
            this.tmWorkTimeEnd.Location = new System.Drawing.Point(172, 40);
            this.tmWorkTimeEnd.Name = "tmWorkTimeEnd";
            this.tmWorkTimeEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tmWorkTimeEnd.Properties.Mask.EditMask = "t";
            this.tmWorkTimeEnd.Size = new System.Drawing.Size(73, 20);
            this.tmWorkTimeEnd.TabIndex = 2;
            this.tmWorkTimeEnd.EditValueChanged += new System.EventHandler(this.tmWorkTimeEnd_EditValueChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(192, 134);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(108, 30);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(77, 134);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(109, 30);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // spnIdleTime
            // 
            this.spnIdleTime.EditValue = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.spnIdleTime.Location = new System.Drawing.Point(77, 106);
            this.spnIdleTime.Name = "spnIdleTime";
            this.spnIdleTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnIdleTime.Properties.IsFloatValue = false;
            this.spnIdleTime.Properties.Mask.EditMask = "N00";
            this.spnIdleTime.Properties.MaxValue = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.spnIdleTime.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnIdleTime.Size = new System.Drawing.Size(51, 20);
            this.spnIdleTime.TabIndex = 4;
            this.spnIdleTime.EditValueChanged += new System.EventHandler(this.spnIdleTime_EditValueChanged);
            // 
            // lblMinute
            // 
            this.lblMinute.AutoSize = true;
            this.lblMinute.Location = new System.Drawing.Point(134, 109);
            this.lblMinute.Name = "lblMinute";
            this.lblMinute.Size = new System.Drawing.Size(15, 13);
            this.lblMinute.TabIndex = 54;
            this.lblMinute.Text = "m";
            // 
            // lblIdleTime
            // 
            this.lblIdleTime.AutoSize = true;
            this.lblIdleTime.Location = new System.Drawing.Point(12, 109);
            this.lblIdleTime.Name = "lblIdleTime";
            this.lblIdleTime.Size = new System.Drawing.Size(52, 13);
            this.lblIdleTime.TabIndex = 53;
            this.lblIdleTime.Text = "Idle time:";
            // 
            // cmbOffMode
            // 
            this.cmbOffMode.Location = new System.Drawing.Point(77, 12);
            this.cmbOffMode.Name = "cmbOffMode";
            this.cmbOffMode.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.cmbOffMode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbOffMode.Properties.Items.AddRange(new object[] {
            "Sleep",
            "Off"});
            this.cmbOffMode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbOffMode.Size = new System.Drawing.Size(223, 20);
            this.cmbOffMode.TabIndex = 0;
            this.cmbOffMode.SelectedIndexChanged += new System.EventHandler(this.cmbOffMode_SelectedIndexChanged);
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(312, 176);
            this.Controls.Add(this.cmbOffMode);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.spnIdleTime);
            this.Controls.Add(this.lblMinute);
            this.Controls.Add(this.lblIdleTime);
            this.Controls.Add(this.tmWorkTimeEnd);
            this.Controls.Add(this.tmWorkTimeStart);
            this.Controls.Add(this.rtbWorkTime);
            this.Controls.Add(this.lblOffMode);
            this.Controls.Add(this.lblTimeFormat);
            this.Controls.Add(this.lblDash);
            this.Controls.Add(this.lblWorkTime);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Work Time Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsForm_FormClosing);
            this.Shown += new System.EventHandler(this.SettingsForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.rtbWorkTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbWorkTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmWorkTimeStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tmWorkTimeEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnIdleTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbOffMode.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblWorkTime;
        private System.Windows.Forms.Label lblDash;
        private System.Windows.Forms.Label lblTimeFormat;
        private System.Windows.Forms.Label lblOffMode;
        private DevExpress.XtraEditors.RangeTrackBarControl rtbWorkTime;
        private DevExpress.XtraEditors.TimeEdit tmWorkTimeStart;
        private DevExpress.XtraEditors.TimeEdit tmWorkTimeEnd;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SpinEdit spnIdleTime;
        private System.Windows.Forms.Label lblMinute;
        private System.Windows.Forms.Label lblIdleTime;
        private DevExpress.XtraEditors.ComboBoxEdit cmbOffMode;
    }
}

