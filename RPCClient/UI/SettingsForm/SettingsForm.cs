﻿using System;
using System.Windows.Forms;
using Common.DTO.Enums;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using Settings = RPCClient.Entity.Settings;

namespace RPCClient.UI.SettingsForm
{
    public partial class SettingsForm : XtraForm, ISettingsView
    {
        private Principal _principal;

        private DefaultShutdownType _currentShutdownType;
        private DateTime _currentWorkTimeStart, _currentWorkTimeEnd;
        private int _currentIdleTime;

        private bool _firstLoad;

        public SettingsViewPresenter Presenter {
            get
            {
                return SettingsViewPresenter.GetInstance(this, _principal);
            }
            set
            { }
        }

        public void ShowView()
        {
            Show();
        }

        public SettingsForm(Principal principal)
        {
            _principal = principal;

            InitializeComponent();

            cmbOffMode.SelectedIndex = Top;
        }

        public SettingsForm(){}

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!_principal.ServerAvailblenessCheck()) return;

            Presenter.SaveSettings(GetSettingsFromGUI());
            Presenter.SendInfo();

            _principal.StartIdleTimer();

            _firstLoad = false;
            Close();
        }

        private Settings GetSettingsFromGUI()
        {
            var shutdownMode = new DefaultShutdownType();
            switch (cmbOffMode.SelectedItem.ToString())
            {
                case "Shutdown":
                    shutdownMode = DefaultShutdownType.Off;
                    break;
                case "Sleep":
                    shutdownMode = DefaultShutdownType.Sleep;
                    break;
            }

            var settings = new Settings(
                tmWorkTimeStart.Time,
                tmWorkTimeEnd.Time,
                int.Parse(spnIdleTime.Text),
                shutdownMode);

            return settings;
        }

        public void ShowSettings()
        {
            var settings = Presenter.GetSettingsFromConfig();

            switch (settings.ShutDownMode)
            {
                case DefaultShutdownType.Sleep:
                    cmbOffMode.SelectedText = DefaultShutdownType.Sleep.ToString();
                    break;
                case DefaultShutdownType.Off:
                    cmbOffMode.SelectedText = DefaultShutdownType.Off.ToString();
                    break;
            }

            tmWorkTimeEnd.Time = settings.WorkTimeEnd;
            tmWorkTimeStart.Time = settings.WorkTimeStart;
            spnIdleTime.Value = settings.IdleTime;

            SaveLocalCurrentSettings(settings);

            btnSave.Enabled = false;
        }

        private void SaveLocalCurrentSettings(Settings settings)
        {
            _currentShutdownType = settings.ShutDownMode;
            _currentWorkTimeStart = settings.WorkTimeStart;
            _currentWorkTimeEnd = settings.WorkTimeEnd;
            _currentIdleTime = settings.IdleTime;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_firstLoad)
            {
                var dialogResult = XtraMessageBox.Show("Save settings?", "Remote Power Controller",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                switch (dialogResult)
                {
                    case DialogResult.Yes:
                        _firstLoad = false;
                        if (!_principal.ServerAvailblenessCheck()) break;
                        Presenter.SaveSettings(GetSettingsFromGUI());
                        Presenter.SendInfo();
                        _principal.StartIdleTimer();
                        break;
                    case DialogResult.No:
                        Environment.Exit(0);
                        break;
                }
            }
        }

        private void tmWorkTimeStart_EditValueChanged(object sender, EventArgs e)
        {
            var workTimeEnd = rtbWorkTime.Value.Maximum;
            var totalMinutesWorkTimeStart = Convert.ToInt32(tmWorkTimeStart.Time.TimeOfDay.TotalMinutes);
            rtbWorkTime.Value = new TrackBarRange(totalMinutesWorkTimeStart, workTimeEnd);

            if (!_firstLoad)
            {
                btnSave.Enabled = ChangesOccurred();
            }
        }

        private void tmWorkTimeEnd_EditValueChanged(object sender, EventArgs e)
        {
            var workTimeStart = rtbWorkTime.Value.Minimum;
            var totalMinutesWorkTimeEnd = Convert.ToInt32(tmWorkTimeEnd.Time.TimeOfDay.TotalMinutes);
            rtbWorkTime.Value = new TrackBarRange(workTimeStart, totalMinutesWorkTimeEnd);

            if (!_firstLoad)
            {
                btnSave.Enabled = ChangesOccurred();
            }
        }

        private void rtbWorkTime_EditValueChanged(object sender, EventArgs e)
        {
            var workTimeStart = rtbWorkTime.Value.Minimum;
            var defaultWorkTimeStart = new DateTime();
            tmWorkTimeStart.Time = defaultWorkTimeStart.AddMinutes(workTimeStart);

            var workTimeEnd = rtbWorkTime.Value.Maximum;
            var defaultWorkTimeEnd = new DateTime();
            tmWorkTimeEnd.Time = defaultWorkTimeEnd.AddMinutes(workTimeEnd);
        }

        private void cmbOffMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_firstLoad)
            {
                btnSave.Enabled = ChangesOccurred();
            }
        }

        private void spnIdleTime_EditValueChanged(object sender, EventArgs e)
        {
            if (!_firstLoad)
            {
                btnSave.Enabled = ChangesOccurred();
            }
        }

        private bool ChangesOccurred()
        {
            return _currentShutdownType.ToString() != cmbOffMode.SelectedItem.ToString() ||
                _currentWorkTimeStart.TimeOfDay != tmWorkTimeStart.Time.TimeOfDay ||
                _currentWorkTimeEnd.TimeOfDay != tmWorkTimeEnd.Time.TimeOfDay ||
                _currentIdleTime != spnIdleTime.Value;
        }

        private void SettingsForm_Shown(object sender, EventArgs e)
        {
            ShowSettings();
        }
    }
}
