﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPCClient.Entity;
using RPCServer.DTO.Enums;

namespace RPCClient.UI.SettingsForm
{
    public partial class SettingsForm : Form, ISettingsView
    {

        public bool UserClosing { get; set; }

        public SettingsViewPresenter Presenter {
            get
            {
                return SettingsViewPresenter.GetInstance();
            }
            set
            { }
        }

        public void ShowView()
        {
            Show();
        }

        public SettingsForm()
        {
            InitializeComponent();

            notifyIcon.Visible = false; //disabling UI till server will be founded

            UserClosing = false;

            WindowState = FormWindowState.Minimized;
            ShowInTaskbar = false;

            CreateNotifyIcon();

            cmbOffMode.SelectedIndex = Top;
        }

        private void CreateNotifyIcon()
        {
            var contextMenu = new ContextMenu();

            contextMenu.MenuItems.Add(0, new MenuItem("Settings", ShowSettingsForm));
            contextMenu.MenuItems.Add(1, new MenuItem("Shutdown PC after..."));
            contextMenu.MenuItems[1].MenuItems.Add(0, new MenuItem("Half hour", ShutdownAfter));
            contextMenu.MenuItems[1].MenuItems.Add(1, new MenuItem("One hour", ShutdownAfter));
            contextMenu.MenuItems[1].MenuItems.Add(2, new MenuItem("Two hours", ShutdownAfter));
            contextMenu.MenuItems[1].MenuItems.Add("-");
            contextMenu.MenuItems[1].MenuItems.Add(4, new MenuItem("Enter the time...", ShowShutdownAfterForm));
            contextMenu.MenuItems.Add("-");
            contextMenu.MenuItems.Add(3, new MenuItem("Exit", Exit_Click));

            notifyIcon.ContextMenu = contextMenu; 
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            await SendInfoAsync();
        }

        async Task SendInfoAsync()
        {
            Presenter.SendInfo();
            
            WindowState = FormWindowState.Minimized;
            ShowInTaskbar = false;
        }

        public Settings GetSettings()
        {
            var shutdownMode = new DefaultShutdownType();
            if (cmbOffMode.SelectedItem.ToString() == "Shutdown")
            {
                shutdownMode = DefaultShutdownType.Off;
            }

            if (cmbOffMode.SelectedItem.ToString() == "Sleep")
            {
                shutdownMode = DefaultShutdownType.Sleep;
            }

            var settings = new Settings(
                dtmWorkTimeStart.Value,
                dtmWorkTimeEnd.Value,
                int.Parse(numIdleTime.Text),
                shutdownMode);

            return settings;
        }

        private void ShowSettingsForm(object sender, EventArgs e)
        {
            Presenter.ShowSettings();

            if (!btnCancel.Visible)
            {
                btnCancel.Visible = true;
            }

            WindowState = FormWindowState.Normal;
            ShowInTaskbar = true;
        }

        private void ShowShutdownAfterForm(object sender, EventArgs e)
        {
            Presenter.ShowShutdowmAfterForm();
            
            AddMenuNotification(Presenter.GetTurnOffTime());
        }

        private void ShutdownAfter(object sender, EventArgs e)
        {
            const int time = 60000;
            var senderMenuItem = sender as MenuItem;

            switch (senderMenuItem.Text)
            {
                case "Half hour":
                    Presenter.ShutdownAfter(time * 30);
                    break;
                case "One hour":
                    Presenter.ShutdownAfter(time * 60);
                    break;
                case "Two hours":
                    Presenter.ShutdownAfter(time * 120);
                    break;
            }

            AddMenuNotification(Presenter.GetTurnOffTime());
        }

        public void AddMenuNotification(TimeSpan powerActionTime)
        {
            MenuItem nextPAMenuItem;
            if (Presenter.GetShutdownType() == DefaultShutdownType.Off)
            {
                nextPAMenuItem = new MenuItem("Shutdown in:");
            }
            else
            {
                nextPAMenuItem = new MenuItem("Go to sleep in:");
            }

            var turnOffTimeMenuItem = new MenuItem(string.Format("{0}:{1}",
                powerActionTime.Hours.ToString("D2"), powerActionTime.Minutes.ToString("D2")));

            //remove if exist
            if (notifyIcon.ContextMenu.MenuItems[0].Text != @"Settings") //bad one todo: think harder
            {
                notifyIcon.ContextMenu.MenuItems.RemoveAt(2);
                notifyIcon.ContextMenu.MenuItems.RemoveAt(1);
                notifyIcon.ContextMenu.MenuItems.RemoveAt(0);
            }

            notifyIcon.ContextMenu.MenuItems.Add(0, nextPAMenuItem);
            notifyIcon.ContextMenu.MenuItems[0].Enabled = false;
            notifyIcon.ContextMenu.MenuItems.Add(1, turnOffTimeMenuItem);
            notifyIcon.ContextMenu.MenuItems[1].Enabled = false;
            notifyIcon.ContextMenu.MenuItems.Add(2, new MenuItem("-"));
        }

        protected void Exit_Click(Object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to exit?", "Remote Power Controller",
                MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
            {
                UserClosing = true;
                Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
            ShowInTaskbar = false;
        }

        public void ShowSettings(Settings settings)
        {
            foreach (var item in cmbOffMode.Items.Cast<object>().Where(item =>
                settings.ShutDownMode.ToString() == item.ToString()))
            {
                cmbOffMode.SelectedItem = item;
            }

            switch (settings.ShutDownMode)
            {
                case DefaultShutdownType.Sleep:
                    cmbOffMode.SelectedText = DefaultShutdownType.Sleep.ToString();
                    break;
                case DefaultShutdownType.Off:
                    cmbOffMode.SelectedText = DefaultShutdownType.Off.ToString();
                    break;
            }

            dtmWorkTimeStart.Text = settings.WorkTimeStart.ToString();
            dtmWorkTimeEnd.Text = settings.WorkTimeEnd.ToString();
            numIdleTime.Text = settings.IdleTime.ToString();
        }

        private void UserStartWindow()
        {
            if (Presenter.FirstLoad())
            {
                Presenter.ShowRegisterForm();

                WindowState = FormWindowState.Normal;
                ShowInTaskbar = true;
                btnCancel.Visible = false;
            }
        }

        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!UserClosing)
            {
                e.Cancel = true;

                WindowState = FormWindowState.Minimized;
                ShowInTaskbar = false;
            }
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            Presenter.ServerSearch();

            UserStartWindow();

            Presenter.OpenPowerManagementService();

            Presenter.StartIdleTimer();

            notifyIcon.Visible = true; //get back to the UI
        }

        private void SettingsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Presenter.ClosePowerManagementService();
        }

    }
}
