﻿using System;
using System.Diagnostics;
using System.DirectoryServices.ActiveDirectory;
using System.Security.Authentication;
using System.Security.Principal;
using System.ServiceModel;
using System.Timers;
using System.Windows.Forms;
using Client.Service;
using Common.DTO;
using Common.Logger;
using Common.Logger.Impl;
using Common.Service.Proxy;
using DevExpress.XtraEditors;
using RPCClient.AutoDiscovery;
using RPCClient.Entity;
using RPCClient.Enums;
using RPCClient.Infrastructure;
using RPCServer.DTO.Enums;
using RPCServer.Interfaces;
using Timer = System.Timers.Timer;

namespace RPCClient.UI.SettingsForm
{
    public class SettingsViewPresenter : BasePresenter<ISettingsView, SettingsViewPresenter>
    {
        private ServerAvailability.ServerAvailability _serverAvailability;
        private ServiceHost _powerManagementServiceHost;
        public PowerManagementService PowerManagementService;
        private SubmitType _submitType = SubmitType.UpdateSettings;

        private Timer _timer, _idleTimer;
        private DefaultShutdownType _shutdownType = DefaultShutdownType.Sleep;
        private bool _timerStart;
        private bool _idleTimerStart;
        private int _shudownAfterTime;
        private int _idleTime;
        private TimeSpan _workTimeEnd;
        private TimeSpan _turnOffTime;
        ILogger _logger = LoggerFactory.Current.GetLogger<SettingsViewPresenter>();

        private static SettingsViewPresenter _presenterInstance;

        public static SettingsViewPresenter GetInstance(ISettingsView view)
        {
            return _presenterInstance ?? (_presenterInstance = new SettingsViewPresenter(view));
        }

        readonly ISettingsView view;
        public SettingsViewPresenter(ISettingsView view)
        {
            this.view = view;
        }

        public bool FirstLoad()
        {
            //Properties.Settings.Default.Login = string.Empty;
            //Properties.Settings.Default.Password = string.Empty;
            //Properties.Settings.Default.Save();
            return Properties.Settings.Default.Login == string.Empty ||
                   Properties.Settings.Default.Password == string.Empty;
        }

        public void ShowAccountForm()
        {
            var accountForm = new AccountForm.AccountForm();
            accountForm.ShowDialog();
            accountForm.GetSubmitType(out _submitType);
            accountForm.Dispose();
        }

        public void ShowNetworkAdapterForm()
        {
            var networkAdapterForm = new NetworkAdapterForm();
            networkAdapterForm.ShowDialog();
            networkAdapterForm.Dispose();
        }

        public void OpenPowerManagementService()
        {
            _powerManagementServiceHost = new ServiceHost(typeof(PowerManagementService));
            _powerManagementServiceHost.Open();
            PowerManagementService.CurrentClient = Properties.Settings.Default.ClientInfo;
        }

        public void ClosePowerManagementService()
        {
            _powerManagementServiceHost.Close();
        }

        public void SendInfo()
        {
            switch (_submitType)
            {
                case SubmitType.RegisterNewUser:
                    RegisterNewUser();
                    break;
                case SubmitType.AddNewClient:
                    AddNewClient();
                    break;
                case SubmitType.UpdateSettings:
                    ChangeSettings();
                    break;
            }
        }

        public void RegisterNewUser()
        {
            var exception = new Exception(string.Empty);
            var clientInfoTO = Properties.Settings.Default["ClientInfo"] as ClientInfoTO;
            var userInfo = new UserInfo(
                Properties.Settings.Default["Login"].ToString(),
                Properties.Settings.Default["Password"].ToString(),
                Properties.Settings.Default.IsUserDomain);
            var settings = GetSettingsFromConfig();

            var userInfoTO = userInfo.ConvertToTO();
            var settingsTO = settings.ConvertToTO();

            var serverUserEndpoint = new EndpointAddress(
                string.Format("http://{0}:{1}/UserInfoService",
                    Properties.Settings.Default["ServerIP"],
                    Properties.Settings.Default["ServerPort"]));

            ServiceChannel<IUserInfoService>.For(serverUserEndpoint, service => 
            {
                try
                {
                    service.RegisterNewUser(userInfoTO, clientInfoTO, settingsTO);
                }
                catch (Exception ex)
                {
                    exception = ex;
                }
            });
            if (exception.Message != string.Empty)
            {
                Properties.Settings.Default.Login = string.Empty;
                Properties.Settings.Default.Password = string.Empty;
                Properties.Settings.Default.Save();

                XtraMessageBox.Show("Site error!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _logger.Error("Registration error", exception);
                Environment.Exit(1);
            }
        }

        public void ChangeSettings()
        {
            var clientInfoTO = Properties.Settings.Default["ClientInfo"] as ClientInfoTO;
            var settings = GetSettingsFromConfig();

            var settingsTO = settings.ConvertToTO();

            var serverSettingsEndpoint = new EndpointAddress(
                string.Format("http://{0}:{1}/SettingsService",
                Properties.Settings.Default["ServerIP"],
                Properties.Settings.Default["ServerPort"]));

            ServiceChannel<ISettingsService>.For(serverSettingsEndpoint,
                service => service.UpdateSettings(clientInfoTO, settingsTO));
        }

        public void AddNewClient()
        {
            var exception = new Exception(string.Empty);
            var clientInfoTO = Properties.Settings.Default["ClientInfo"] as ClientInfoTO;
            var userInfo = new UserInfo(
                Properties.Settings.Default["Login"].ToString(),
                Properties.Settings.Default["Password"].ToString(),
                Properties.Settings.Default.IsUserDomain);
            var settings = GetSettingsFromConfig();

            var userInfoTO = userInfo.ConvertToTO();
            var settingsTO = settings.ConvertToTO();

            var serverClientEndpoint = new EndpointAddress(
                string.Format("http://{0}:{1}/ClientInfoService",
                    Properties.Settings.Default["ServerIP"],
                    Properties.Settings.Default["ServerPort"]));

            ServiceChannel<IClientInfoService>.For(serverClientEndpoint, service =>
            {
                try
                {
                    service.AddNewClientInfoForUser(userInfoTO, clientInfoTO, settingsTO);
                }
                catch (Exception ex)
                {
                    exception = ex;
                }
            });
            if (exception.Message != string.Empty)
            {
                Properties.Settings.Default.Login = string.Empty;
                Properties.Settings.Default.Password = string.Empty;
                Properties.Settings.Default.Save();

                XtraMessageBox.Show("Site error!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _logger.Error("AddNewClient Error", exception);
                Environment.Exit(1);
            }
        }

        public void SaveDefaultSettings()
        {
            Properties.Settings.Default.WorkTimeStart = new TimeSpan(9, 0, 0);
            Properties.Settings.Default.WorkTimeEnd = new TimeSpan(19, 0, 0);
            Properties.Settings.Default.IdleTime = 15;
            Properties.Settings.Default.ShutdownType = DefaultShutdownType.Sleep;
            Properties.Settings.Default.Save();

            _shutdownType = Properties.Settings.Default.ShutdownType;

            StartIdleTimer();
        }

        public void SaveSettings(Settings settings)
        {
            Properties.Settings.Default.WorkTimeStart = settings.WorkTimeStart.TimeOfDay;
            Properties.Settings.Default.WorkTimeEnd = settings.WorkTimeEnd.TimeOfDay;
            Properties.Settings.Default.IdleTime = settings.IdleTime;
            Properties.Settings.Default.ShutdownType = settings.ShutDownMode;
            Properties.Settings.Default.Save();

            _shutdownType = Properties.Settings.Default.ShutdownType;

            StartIdleTimer();
        }

        public Settings GetSettingsFromConfig()
        {
            var settings = new Settings(
                new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day,
                    Properties.Settings.Default.WorkTimeStart.Hours,
                    Properties.Settings.Default.WorkTimeStart.Minutes,
                    Properties.Settings.Default.WorkTimeStart.Seconds),
                new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day,
                    Properties.Settings.Default.WorkTimeEnd.Hours,
                    Properties.Settings.Default.WorkTimeEnd.Minutes,
                    Properties.Settings.Default.WorkTimeEnd.Seconds),
                Properties.Settings.Default.IdleTime,
                Properties.Settings.Default.ShutdownType);

            return settings;
        }

        public void ShowSettings()
        {
            view.ShowSettings(GetSettingsFromConfig());
        }

        public void ShowShutdowmAfterForm()
        {
            var shutdownAfterForm = new ShutdownAfterForm();
            shutdownAfterForm.ShowDialog();
            shutdownAfterForm.GetTime(out _shudownAfterTime, out _timerStart);
            shutdownAfterForm.Dispose();

            if (_timerStart && _shudownAfterTime != 0)
            {
                ShutdownAfter(_shudownAfterTime);
            }
        }

        public void ShutdownAfter(int shutdownAfterTime)
        {
            _timer = new Timer(shutdownAfterTime);
            _timer.Elapsed += Shutdown;
            _timer.Start();

            _timerStart = true;

            _turnOffTime = DateTime.Now.TimeOfDay.Add(new TimeSpan(0, shutdownAfterTime/60000, 0));
        }

        public DefaultShutdownType GetShutdownType()
        {
            return _shutdownType;
        }

        public TimeSpan GetTurnOffTime()
        {
            return _turnOffTime;
        }

        private void Shutdown(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();

            _timerStart = false;

            view.DeleteMenuNotification();

            var powerManagementService = new PowerManagementService();
            if (_shutdownType == DefaultShutdownType.Sleep)
            {
                powerManagementService.Sleep();
            }

            if (_shutdownType == DefaultShutdownType.Off)
            {
                powerManagementService.TurnOff();
            }
        }
        
        public void StartIdleTimer()
        {
            _idleTime = Properties.Settings.Default.IdleTime;
            _workTimeEnd = Properties.Settings.Default.WorkTimeEnd;
            _shutdownType = Properties.Settings.Default.ShutdownType;

            _idleTimer = new Timer(1000);
            _idleTimer.Elapsed += CheckIdleTime;
            _idleTimer.Start();

            _idleTimerStart = true;
        }

        private void CheckIdleTime(object sender, ElapsedEventArgs e)
        {
            //CommandInProcess property is a flag, that tells application that it has some command in execution
            if (DateTime.Now.TimeOfDay.TotalSeconds >= (_workTimeEnd.TotalSeconds + _idleTime * 60) &&
                IdleTimer.IdleTimeCounter.GetLastInputTime() > _idleTime * 60 &&
                !_timerStart && 
                _idleTimerStart &&
                !PowerManagementService.CommandInProcess)
            {
                _idleTimerStart = false;

                var serverEndpoint = new EndpointAddress(
                    string.Format("http://{0}:{1}/ClientInfoService",
                        Properties.Settings.Default["ServerIP"],
                        Properties.Settings.Default["ServerPort"]));

                _serverAvailability = new ServerAvailability.ServerAvailability();

                if (_serverAvailability.IsServiceAvailble(500))
                {
                    ServiceChannel<IClientInfoService>.For(serverEndpoint, service =>
                    service.ConfirmPowerAction(Properties.Settings.Default["ClientInfo"] as ClientInfoTO,
                    _shutdownType));

                    _idleTimerStart = true;
                }
                else
                {
                    var mbResult = XtraMessageBox.Show("Server is not responding. Cannot turn off computer.",
                            "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    _idleTimerStart = mbResult == DialogResult.OK;
                }
            }
        }

        public void ServerSearch()
        {
            var identifyBytes = new byte[] { 0x1, 0x2, 0x3 };
            const int autoDiscoveryPort = 18500;
            const int timeout = 500;

            DialogResult mbResult;
            var discoveryClient = new AutoDiscoveryClient(identifyBytes, autoDiscoveryPort, timeout);

            do
            {
                var watch = new Stopwatch();
                watch.Start();
                discoveryClient.ServerSearch();
                watch.Stop();

                if (discoveryClient.IsServerFound)
                {
                    _logger.Trace(string.Format("Found server at {0}:{1} in {2}",
                        discoveryClient.ServerAddress, discoveryClient.ServerPort, watch.Elapsed));
                    AddServerToSettings(discoveryClient.ServerAddress, discoveryClient.ServerPort);
                    mbResult = DialogResult.OK;
                }
                else
                {
                    mbResult = XtraMessageBox.Show("Server not found. Try again?",
                        "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    if (mbResult == DialogResult.No)
                    {
                        Environment.Exit(1);
                    }
                }
            } while (mbResult == DialogResult.Yes);
        }

        private static void AddServerToSettings(string ip, int port)
        {
            Properties.Settings.Default["ServerIP"] = ip;
            Properties.Settings.Default["ServerPort"] = port.ToString();
            Properties.Settings.Default.Save();

            PowerManagementService.ServerIP = ip;
            PowerManagementService.ServerPort = port.ToString();
        }

        public void DomainLogin()
        {
            var serverEndpoint = new EndpointAddress(
                    string.Format("http://{0}:{1}/UserInfoService",
                        Properties.Settings.Default["ServerIP"],
                        Properties.Settings.Default["ServerPort"]));

            try
            {
                var domainName = Domain.GetComputerDomain(); //if current PC not in domain - go to standart authorization                
                _logger.Trace("Current PC is in domain {0}", domainName);

                var exception = new Exception(string.Empty);
                ServiceChannel<IUserInfoService>.For(serverEndpoint, service =>
                {
                    try
                    {
                        RegisterOrLogin(service);
                    }
                    catch (Exception ex)
                    {
                        exception = ex;
                    }
                });
                if (exception.Message != string.Empty)
                {
                    XtraMessageBox.Show("Site error!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _logger.Error("Problem with domain login", exception);

                    Environment.Exit(1);
                }
            }
            catch
            {
                _logger.Trace("No domain on this PC. Starting normal authorization..");
                throw new AuthenticationException();
            }
        }

        private void RegisterOrLogin(IUserInfoService service)
        {
            var currentIdentity = WindowsIdentity.GetCurrent();
            var currentDomainName = currentIdentity.Name;

            _logger.Trace("Parsed domain name: {0}", currentDomainName);

            var login = new UserInfoTO
            {
                Login = currentDomainName,
                Password = Common.Keys.Passwords.DeafaultDomainUserPassword
            };

            var isLoginExistInDomain = service.IsLoginExistInDomain(currentDomainName);
            if (!isLoginExistInDomain) return;

            _logger.Trace("User {0} exist in Domain. Domain Auth. started", currentDomainName);
            login.IsDomain = true;

            if (service.IsLoginExist(login))
            {
                _logger.Trace("Add new client to login {0}", currentDomainName);
                _submitType = SubmitType.AddNewClient;
            }
            else
            {
                _logger.Trace("Registered new user {0}", currentDomainName);
                _submitType = SubmitType.RegisterNewUser;
            }

            Properties.Settings.Default.Login = login.Login;
            Properties.Settings.Default.Password = login.Password;
            Properties.Settings.Default.IsUserDomain = login.IsDomain;
        }
    }
}
