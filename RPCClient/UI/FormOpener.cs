﻿using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RPCClient.Availability;
using RPCClient.Enums;

namespace RPCClient.UI
{
    public class FormOpener
    {
        private readonly Principal _principal;
        private int _shutdownTime;

        public FormOpener(Principal principal)
        {
            _principal = principal;
        }

        public void ShowAccountForm(out SubmitType submitType)
        {
            var accountForm = new AccountForm.AccountForm();
            accountForm.ShowDialog();
            accountForm.GetSubmitType(out submitType);
            accountForm.Dispose();
        }

        public void ShowNetworkAdapterForm()
        {
            var networkAdapterForm = new NetworkAdapterForm();
            networkAdapterForm.ShowDialog();
            networkAdapterForm.Dispose();
        }

        public void ShowSettingsForm()
        {
            OpenFormInstance<SettingsForm.SettingsForm>();
        }

        public void ShowSessionManagerForm()
        {
            if (AdminAccess.IsAdminAccess())
            {
                OpenFormInstance<SessionManagerForm>();
            }
            else
            {
                XtraMessageBox.Show("Administrator rights is required for this operation.\nRestart the program with administrator rights.",
                    "RemotePowerController", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void ShowSessionRestoringForm()
        {
            if (AdminAccess.IsAdminAccess())
            {
                OpenFormInstance<SessionRestoringForm.SessionRestoringForm>();
            }
            else
            {
                XtraMessageBox.Show("Administrator rights is required for this operation.\nRestart the program with administrator rights.",
                    "RemotePowerController", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void ShowAdvancedSettingsForm()
        {
            OpenFormInstance<AdvancedSettingsForm.AdvancedSettingsForm>();
        }

        public void ShowShutdowmAfterForm()
        {
            OpenFormInstance<ShutdownAfterForm>();

            if (_shutdownTime == 0) return;

            _principal.ShutdownAfterTime(_shutdownTime);
        }

        private void OpenFormInstance<T>() where T: Form, new()
        {
            if (Application.OpenForms.OfType<T>().Any())
            {
                Application.OpenForms.OfType<T>().First().TopMost = true;
                Application.OpenForms.OfType<T>().First().TopMost = false;
            }
            else
            {
                Form form;
                if (typeof (T) == typeof (SettingsForm.SettingsForm))
                {
                    form = new SettingsForm.SettingsForm(_principal) {ShowInTaskbar = true};
                }
                else
                {
                    form = new T {ShowInTaskbar = true};
                }
                form.ShowDialog();
                if (typeof (T) == typeof (ShutdownAfterForm))
                {
                    ((ShutdownAfterForm) form).GetTime(out _shutdownTime);
                }
                form.Dispose();
            }
        }
    }
}
