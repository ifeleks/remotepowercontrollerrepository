﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace RPCClient.UI
{
    public partial class ShutdownAfterForm : XtraForm
    {
        private int _shutdownAfterTime;
        public ShutdownAfterForm()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (tmShutdownTime.Time.TimeOfDay.TotalMinutes > 0)
            {
                var time = Convert.ToInt32(tmShutdownTime.Time.TimeOfDay.TotalMilliseconds);
                _shutdownAfterTime = time;

                Close();
            }
            else
            {
                XtraMessageBox.Show("Enter the time!", "Remote Power Controller", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void GetTime(out int shutdownAfterTime)
        {
            shutdownAfterTime = _shutdownAfterTime;
        }
    }
}
