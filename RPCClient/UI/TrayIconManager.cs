﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Common.DTO.Enums;
using RPCClient.Properties;

namespace RPCClient.UI
{
    public class TrayIconManager : IDisposable
    {
        private readonly Principal _principal;
        private readonly NotifyIcon _notifyIcon;

        public TrayIconManager(Principal principal)
        {
            _principal = principal;

            _notifyIcon = new NotifyIcon
            {
                Icon = (Icon) (Resources.ResourceManager.GetObject("power")),
                Text = @"Remote Power Controller",
                Visible = true
            };

            CreateNotifyIconMenu();

            _notifyIcon.MouseClick += notifyIcon_MouseClick;
            _notifyIcon.MouseDoubleClick += notifyIcon_MouseDoubleClick;
        }

        private void CreateNotifyIconMenu()
        {
            var contextMenu = new ContextMenu();
            contextMenu.MenuItems.Add(0, new MenuItem("Work Time", _principal.ShowSettingsForm));
            contextMenu.MenuItems.Add(1, new MenuItem("Shutdown PC after..."));
            contextMenu.MenuItems[1].MenuItems.Add(0, new MenuItem("Half hour", _principal.ShutdownAfter));
            contextMenu.MenuItems[1].MenuItems.Add(1, new MenuItem("One hour", _principal.ShutdownAfter));
            contextMenu.MenuItems[1].MenuItems.Add(2, new MenuItem("Two hours", _principal.ShutdownAfter));
            contextMenu.MenuItems[1].MenuItems.Add("-");
            contextMenu.MenuItems[1].MenuItems.Add(4, new MenuItem("Enter the time...", _principal.ShowShutdowmAfterForm));
            contextMenu.MenuItems.Add(2, new MenuItem("Session Restoring helper", _principal.ShowSessionManagerForm));
            contextMenu.MenuItems.Add(3, new MenuItem("Session Restoring", _principal.ShowSessionRestoringForm));
            contextMenu.MenuItems.Add(4, new MenuItem("Advanced Settings", _principal.ShowAdvancedSettingsForm));
            contextMenu.MenuItems.Add("-");
            contextMenu.MenuItems.Add(6, new MenuItem("Exit", Exit_Click));

            _notifyIcon.ContextMenu = contextMenu;
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            _principal.ShowSettingsForm(sender, e);
        }

        private void notifyIcon_MouseClick(object sender, MouseEventArgs e)
        {
            if (Application.OpenForms.Count <= 1) return;
            foreach (Form form in Application.OpenForms)
            {
                form.TopMost = true;
                form.TopMost = false;
            }
        }

        public void ChangeVisible(bool visibleValue)
        {
            _notifyIcon.Visible = visibleValue;
        }

        public void ChangeIcon(Icon icon)
        {
            _notifyIcon.Icon = icon;
        }

        public void AddShutdownAfterNotification(TimeSpan powerActionTime)
        {
            var tipText = Settings.Default.ShutdownType == DefaultShutdownType.Off ? "Shutdown in " : "Go to sleep in: ";
            tipText += string.Format("{0}:{1}", powerActionTime.Hours.ToString("D2"), powerActionTime.Minutes.ToString("D2"));
            _notifyIcon.Text = tipText;
            AddBalloonTip(tipText, ToolTipIcon.Info);
        }

        public void DeleteShutdownAfterNotification()
        {
            _notifyIcon.Text = string.Empty;
        }

        public void AddBalloonTip(string tipText, ToolTipIcon tipIcon)
        {
            _notifyIcon.ShowBalloonTip(500, "Remote Power Controller", tipText, tipIcon);
        }

        protected void Exit_Click(Object sender, EventArgs e)
        {
            _principal.Exit();
        }

        public void Dispose()
        {
            _notifyIcon.Dispose();
        }
    }
}
