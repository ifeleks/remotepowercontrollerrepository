﻿namespace RPCClient.UI
{
    partial class SessionManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SessionManagerForm));
            this.btnSavePoint = new DevExpress.XtraEditors.SimpleButton();
            this.btnLoadPoint = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // btnSavePoint
            // 
            this.btnSavePoint.Location = new System.Drawing.Point(12, 12);
            this.btnSavePoint.Name = "btnSavePoint";
            this.btnSavePoint.Size = new System.Drawing.Size(110, 41);
            this.btnSavePoint.TabIndex = 0;
            this.btnSavePoint.Text = "Save point";
            this.btnSavePoint.Click += new System.EventHandler(this.btnSavePoint_Click);
            // 
            // btnLoadPoint
            // 
            this.btnLoadPoint.Location = new System.Drawing.Point(128, 12);
            this.btnLoadPoint.Name = "btnLoadPoint";
            this.btnLoadPoint.Size = new System.Drawing.Size(110, 41);
            this.btnLoadPoint.TabIndex = 1;
            this.btnLoadPoint.Text = "Load point";
            this.btnLoadPoint.Click += new System.EventHandler(this.btnLoadPoint_Click);
            // 
            // SessionManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(250, 65);
            this.Controls.Add(this.btnLoadPoint);
            this.Controls.Add(this.btnSavePoint);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SessionManagerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Session manager";
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnSavePoint;
        private DevExpress.XtraEditors.SimpleButton btnLoadPoint;
    }
}