﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using RPCClient.Infrastructure;

namespace RPCClient.UI.AdvancedSettingsForm
{
    public interface IAdvancedSettingsView : IView<AdvancedSettingsViewPresenter>
    {
        void Close();
    }
}
