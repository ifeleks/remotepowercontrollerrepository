﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.ActiveDirectory;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RPCClient.Availability;

namespace RPCClient.UI.AdvancedSettingsForm
{
    public class AdvancedSettingsViewPresenter
    {
        private static AdvancedSettingsViewPresenter _presenterInstance;
        private IAdvancedSettingsView _view;

        public static AdvancedSettingsViewPresenter GetInstance(IAdvancedSettingsView view)
        {
            return _presenterInstance ?? (_presenterInstance = new AdvancedSettingsViewPresenter(view));
        }

        private AdvancedSettingsViewPresenter(IAdvancedSettingsView view)
        {
            _view = view;
        }

        public void ShowClientInfo()
        {
            Domain domain = null;
            try
            {
                domain = Domain.GetCurrentDomain();
            }
            catch { }
            XtraMessageBox.Show(
                string.Format("User name: {0}\nMachine name: {1}\nDomain name: {2}\n" +
                              "IP: {3}\nMAC: {4}\nClient installation date: {5}",
                Properties.Settings.Default.Login,
                Properties.Settings.Default.ClientInfo.ClientName,
                domain,
                Properties.Settings.Default.ClientInfo.ClientIp,
                Properties.Settings.Default.ClientInfo.MacAddress,
                Properties.Settings.Default.ClientInfo.InstallationDate),
                "Client info. - RemotePowerController");
        }

        public AdvancedSettingsList LoadSettings()
        {
            bool? RDPAvailable;
            try
            {
                RDPAvailable = Availability.RDPAvailability.IsRDPAvailable();
            }
            catch
            {
                RDPAvailable = null;
            }
            return new AdvancedSettingsList
            {
                AllowAutoDiscovery = Properties.Settings.Default.IsAutoDiscoveryOn,
                AllowPowerActionMessage = Properties.Settings.Default.IsConfirmPowerActionOn,
                AllowVoiceCommands = Properties.Settings.Default.IsVoiceCommandsOn,
                AllowVoiceMessages = Properties.Settings.Default.IsVoiceMessagesOn,
                HotKey = Properties.Settings.Default.ListenHotKey,
                ModifierHotKey = Properties.Settings.Default.ListenModifierKeys,
                PowerActionMessageCountdown = Properties.Settings.Default.ConfirmPowerActionTime,
                ServerIP = Properties.Settings.Default.ServerIP,
                ServerPort = Properties.Settings.Default.ServerPort,
                AllowRemoteDesktop = RDPAvailable
            };   
        }

        public void SaveSettings(AdvancedSettingsList advancedSettings)
        {
            var afterRebootSettings = Properties.Settings.Default.IsAutoDiscoveryOn != advancedSettings.AllowAutoDiscovery ||
                Properties.Settings.Default.IsConfirmPowerActionOn != advancedSettings.AllowPowerActionMessage ||
                Properties.Settings.Default.ListenHotKey != advancedSettings.HotKey ||
                Properties.Settings.Default.ListenModifierKeys != advancedSettings.ModifierHotKey;
            Properties.Settings.Default.IsAutoDiscoveryOn = advancedSettings.AllowAutoDiscovery;
            Properties.Settings.Default.IsConfirmPowerActionOn = advancedSettings.AllowPowerActionMessage;
            Properties.Settings.Default.IsVoiceCommandsOn = advancedSettings.AllowVoiceCommands;
            Properties.Settings.Default.IsVoiceMessagesOn = advancedSettings.AllowVoiceMessages;
            Properties.Settings.Default.ListenHotKey = advancedSettings.HotKey;
            Properties.Settings.Default.ListenModifierKeys = advancedSettings.ModifierHotKey;
            Properties.Settings.Default.ConfirmPowerActionTime = advancedSettings.PowerActionMessageCountdown;
            if (!advancedSettings.AllowAutoDiscovery)
            {
                Properties.Settings.Default.ServerIP = advancedSettings.ServerIP;
                Properties.Settings.Default.ServerPort = advancedSettings.ServerPort;
            }
            Properties.Settings.Default.Save();

            if (advancedSettings.AllowRemoteDesktop != null)
            {
                if ((bool)advancedSettings.AllowRemoteDesktop)
                    RDPAvailability.EnableRDP();
                else RDPAvailability.DisableRDP();
            }

            if (afterRebootSettings) XtraMessageBox.Show("Changes will take affect after restart", "Information. - RemotePowerController",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public bool IsSettingsChanged(AdvancedSettingsList newSettings)
        {
            bool? RDPAvailable;
            try
            {
                RDPAvailable = Availability.RDPAvailability.IsRDPAvailable();
            }
            catch
            {
                RDPAvailable = null;
            }
            var oldSettings = new AdvancedSettingsList
            {
                AllowAutoDiscovery = Properties.Settings.Default.IsAutoDiscoveryOn,
                AllowPowerActionMessage = Properties.Settings.Default.IsConfirmPowerActionOn,
                AllowVoiceCommands = Properties.Settings.Default.IsVoiceCommandsOn,
                AllowVoiceMessages = Properties.Settings.Default.IsVoiceMessagesOn,
                HotKey = Properties.Settings.Default.ListenHotKey,
                ModifierHotKey = Properties.Settings.Default.ListenModifierKeys,
                PowerActionMessageCountdown = Properties.Settings.Default.ConfirmPowerActionTime,
                ServerIP = Properties.Settings.Default.ServerIP,
                ServerPort = Properties.Settings.Default.ServerPort,
                AllowRemoteDesktop = RDPAvailable
            };

            return oldSettings.AllowAutoDiscovery != newSettings.AllowAutoDiscovery ||
                             oldSettings.AllowPowerActionMessage != newSettings.AllowPowerActionMessage ||
                             oldSettings.AllowVoiceCommands != newSettings.AllowVoiceCommands ||
                             oldSettings.AllowVoiceMessages != newSettings.AllowVoiceMessages ||
                             oldSettings.AllowRemoteDesktop != newSettings.AllowRemoteDesktop ||
                             oldSettings.HotKey != newSettings.HotKey ||
                             oldSettings.ModifierHotKey != newSettings.ModifierHotKey ||
                             oldSettings.PowerActionMessageCountdown != newSettings.PowerActionMessageCountdown ||
                             oldSettings.ServerIP != newSettings.ServerIP ||
                             oldSettings.ServerPort != newSettings.ServerPort;

        }

        public bool ValidateIP(string IP, string port)
        {
            IPAddress ipadress;
            if (!string.IsNullOrEmpty(IP) && !IPAddress.TryParse(IP, out ipadress))
            {
                XtraMessageBox.Show("Entered IP adress doesnt pass validation", "Warning", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }
            int n;
            if (!string.IsNullOrEmpty(port) && port.Length > 5 && !int.TryParse(port, out n))
            {

                XtraMessageBox.Show("Entered server port doesnt pass validation", "Warning", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }
    }

    public struct AdvancedSettingsList
    {
        public bool AllowVoiceMessages;
        public bool AllowVoiceCommands;
        public Shared.Keyboard.ModifierKeys ModifierHotKey;
        public Keys HotKey;
        public bool AllowAutoDiscovery;
        public string ServerIP;
        public string ServerPort;
        public bool AllowPowerActionMessage;
        public int PowerActionMessageCountdown;
        public bool? AllowRemoteDesktop;
    }
}
