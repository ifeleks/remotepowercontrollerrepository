﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Shared.Keyboard;

namespace RPCClient.UI.AdvancedSettingsForm
{
    public partial class AdvancedSettingsForm : XtraForm, IAdvancedSettingsView
    {
        public AdvancedSettingsViewPresenter Presenter
        {
            get
            {
                return AdvancedSettingsViewPresenter.GetInstance(this);
            }
            set
            { }
        }

        public void ShowView()
        {
            Show();
        }

        public AdvancedSettingsForm()
        {
            InitializeComponent();
        }

        private void checkVoiceCommands_CheckedChanged(object sender, EventArgs e)
        {
            checkShift.Enabled = checkVoiceCommands.Checked;
            checkCtrl.Enabled = checkVoiceCommands.Checked;
            checkAlt.Enabled = checkVoiceCommands.Checked;
            txtHotKey.Enabled = checkVoiceCommands.Checked;
            lblHotKey.Enabled = checkVoiceCommands.Checked;
        }

        private void checkCustomServer_CheckedChanged(object sender, EventArgs e)
        {
            lblServerAddress.Enabled = checkCustomServer.Checked;
            txtServerAddress.Enabled = checkCustomServer.Checked;
            lblServerPort.Enabled = checkCustomServer.Checked;
            txtServerPort.Enabled = checkCustomServer.Checked;
        }

        private void checkShowConfirmActionMessage_CheckedChanged(object sender, EventArgs e)
        {
            lblCountdownTime.Enabled = checkShowConfirmActionMessage.Checked;
            spnCountdowmTime.Enabled = checkShowConfirmActionMessage.Checked;
        }

        private void btnClientInfo_Click(object sender, EventArgs e)
        {
            Presenter.ShowClientInfo();
        }

        private void AdvancedSettingsForm_Load(object sender, EventArgs e)
        {
            LoadSettings();
        }

        private void LoadSettings()
        {
            var advancedSettings = Presenter.LoadSettings();
            checkVoiceMessages.Checked = advancedSettings.AllowVoiceMessages;
            checkVoiceCommands.Checked = advancedSettings.AllowVoiceCommands;
            checkCtrl.Checked = (advancedSettings.ModifierHotKey == Shared.Keyboard.ModifierKeys.Control);
            switch (advancedSettings.ModifierHotKey)
            {
                case Shared.Keyboard.ModifierKeys.Control:
                    checkCtrl.Checked = true;
                    break;
                case Shared.Keyboard.ModifierKeys.Shift:
                    checkShift.Checked = true;
                    break;
                case Shared.Keyboard.ModifierKeys.Alt:
                    checkAlt.Checked = true;
                    break;
                case Shared.Keyboard.ModifierKeys.Control | Shared.Keyboard.ModifierKeys.Shift:
                    checkCtrl.Checked = true;
                    checkShift.Checked = true;
                    break;
                case Shared.Keyboard.ModifierKeys.Control | Shared.Keyboard.ModifierKeys.Alt:
                    checkCtrl.Checked = true;
                    checkAlt.Checked = true;
                    break;
                case Shared.Keyboard.ModifierKeys.Shift | Shared.Keyboard.ModifierKeys.Alt:
                    checkShift.Checked = true;
                    checkAlt.Checked = true;
                    break;
                case Shared.Keyboard.ModifierKeys.Shift | Shared.Keyboard.ModifierKeys.Alt | Shared.Keyboard.ModifierKeys.Control:
                    checkCtrl.Checked = true;
                    checkShift.Checked = true;
                    checkAlt.Checked = true;
                    break;
            }
            txtHotKey.Text = advancedSettings.HotKey.ToString();
            checkAutoDiscovery.Checked = advancedSettings.AllowAutoDiscovery;
            checkCustomServer.Checked = !advancedSettings.AllowAutoDiscovery;
            txtServerAddress.Text = advancedSettings.ServerIP;
            txtServerPort.Text = advancedSettings.ServerPort;
            checkShowConfirmActionMessage.Checked = advancedSettings.AllowPowerActionMessage;
            checkPowerActionImmediately.Checked = !advancedSettings.AllowPowerActionMessage;
            if (advancedSettings.AllowRemoteDesktop == null)
            {
                groupRemoteDesktopSettings.Enabled = false;
            }
            else
            {
                checkAllowRDP.Checked = (bool)advancedSettings.AllowRemoteDesktop;
                checkDisableRDP.Checked = (bool)!advancedSettings.AllowRemoteDesktop;
            }
            spnCountdowmTime.Text = advancedSettings.PowerActionMessageCountdown.ToString();
        }

        private void txtHotKey_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
            if (e.KeyCode != Keys.ControlKey && e.KeyCode != Keys.Alt && e.KeyCode != Keys.ShiftKey && e.KeyCode != Keys.Menu)
            {
                txtHotKey.Text = e.KeyCode.ToString();
            }
        }

        private void txtHotKey_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!Presenter.ValidateIP(txtServerAddress.Text, txtServerPort.Text)) return;
            Presenter.SaveSettings(GatherSettings());
            Close();
        }

        private void AdvancedSettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            var newSettings = GatherSettings();
            if (!Presenter.IsSettingsChanged(newSettings)) return;
            var mbResult = XtraMessageBox.Show("Settings was changed. Would you like to save changes?",
                "Question. - RemotePowerController",
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            switch (mbResult)
            {
                case DialogResult.Yes:
                    Presenter.SaveSettings(newSettings);
                    break;
                case DialogResult.Cancel:
                    e.Cancel = true;
                    break;
            }
        }

        private AdvancedSettingsList GatherSettings()
        {
            var advancedSettings = new AdvancedSettingsList
            {
                AllowVoiceMessages = checkVoiceMessages.Checked,
                AllowVoiceCommands = checkVoiceCommands.Checked,
                AllowAutoDiscovery = checkAutoDiscovery.Checked,
                AllowRemoteDesktop = checkAllowRDP.Checked,
                AllowPowerActionMessage = checkShowConfirmActionMessage.Checked,
                PowerActionMessageCountdown = int.Parse(spnCountdowmTime.Text)
            };
            Keys.TryParse(txtHotKey.Text, out advancedSettings.HotKey);
            advancedSettings.ModifierHotKey = 0;
            if (checkCtrl.Checked)
                advancedSettings.ModifierHotKey = advancedSettings.ModifierHotKey | Shared.Keyboard.ModifierKeys.Control;
            if (checkShift.Checked)
                advancedSettings.ModifierHotKey = advancedSettings.ModifierHotKey | Shared.Keyboard.ModifierKeys.Shift;
            if (checkAlt.Checked)
                advancedSettings.ModifierHotKey = advancedSettings.ModifierHotKey | Shared.Keyboard.ModifierKeys.Alt;
            advancedSettings.ServerIP = txtServerAddress.Text;
            advancedSettings.ServerPort = txtServerPort.Text;
            return advancedSettings;
        }
    }
}
