﻿namespace RPCClient.UI.AdvancedSettingsForm
{
    partial class AdvancedSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdvancedSettingsForm));
            this.groupVoiceSettings = new DevExpress.XtraEditors.GroupControl();
            this.lblHotKey = new DevExpress.XtraEditors.LabelControl();
            this.txtHotKey = new DevExpress.XtraEditors.TextEdit();
            this.checkAlt = new DevExpress.XtraEditors.CheckEdit();
            this.checkShift = new DevExpress.XtraEditors.CheckEdit();
            this.checkCtrl = new DevExpress.XtraEditors.CheckEdit();
            this.checkVoiceCommands = new DevExpress.XtraEditors.CheckEdit();
            this.checkVoiceMessages = new DevExpress.XtraEditors.CheckEdit();
            this.groupServerSettings = new DevExpress.XtraEditors.GroupControl();
            this.lblServerPort = new DevExpress.XtraEditors.LabelControl();
            this.txtServerPort = new DevExpress.XtraEditors.TextEdit();
            this.lblServerAddress = new DevExpress.XtraEditors.LabelControl();
            this.txtServerAddress = new DevExpress.XtraEditors.TextEdit();
            this.checkCustomServer = new DevExpress.XtraEditors.CheckEdit();
            this.checkAutoDiscovery = new DevExpress.XtraEditors.CheckEdit();
            this.groupConfirmPowerActionMessage = new DevExpress.XtraEditors.GroupControl();
            this.lblCountdownTime = new DevExpress.XtraEditors.LabelControl();
            this.spnCountdowmTime = new DevExpress.XtraEditors.SpinEdit();
            this.checkPowerActionImmediately = new DevExpress.XtraEditors.CheckEdit();
            this.checkShowConfirmActionMessage = new DevExpress.XtraEditors.CheckEdit();
            this.groupRemoteDesktopSettings = new DevExpress.XtraEditors.GroupControl();
            this.checkDisableRDP = new DevExpress.XtraEditors.CheckEdit();
            this.checkAllowRDP = new DevExpress.XtraEditors.CheckEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnClientInfo = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupVoiceSettings)).BeginInit();
            this.groupVoiceSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHotKey.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAlt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkShift.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCtrl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkVoiceCommands.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkVoiceMessages.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupServerSettings)).BeginInit();
            this.groupServerSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtServerPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServerAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCustomServer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAutoDiscovery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupConfirmPowerActionMessage)).BeginInit();
            this.groupConfirmPowerActionMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spnCountdowmTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPowerActionImmediately.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkShowConfirmActionMessage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupRemoteDesktopSettings)).BeginInit();
            this.groupRemoteDesktopSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkDisableRDP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAllowRDP.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupVoiceSettings
            // 
            this.groupVoiceSettings.Controls.Add(this.lblHotKey);
            this.groupVoiceSettings.Controls.Add(this.txtHotKey);
            this.groupVoiceSettings.Controls.Add(this.checkAlt);
            this.groupVoiceSettings.Controls.Add(this.checkShift);
            this.groupVoiceSettings.Controls.Add(this.checkCtrl);
            this.groupVoiceSettings.Controls.Add(this.checkVoiceCommands);
            this.groupVoiceSettings.Controls.Add(this.checkVoiceMessages);
            this.groupVoiceSettings.Location = new System.Drawing.Point(12, 12);
            this.groupVoiceSettings.Name = "groupVoiceSettings";
            this.groupVoiceSettings.Size = new System.Drawing.Size(321, 118);
            this.groupVoiceSettings.TabIndex = 0;
            this.groupVoiceSettings.Text = "Sound settings";
            // 
            // lblHotKey
            // 
            this.lblHotKey.Location = new System.Drawing.Point(22, 74);
            this.lblHotKey.Name = "lblHotKey";
            this.lblHotKey.Size = new System.Drawing.Size(223, 13);
            this.lblHotKey.TabIndex = 6;
            this.lblHotKey.Text = "Hot key for activate listening voice commands:";
            // 
            // txtHotKey
            // 
            this.txtHotKey.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtHotKey.Location = new System.Drawing.Point(178, 92);
            this.txtHotKey.Name = "txtHotKey";
            this.txtHotKey.Size = new System.Drawing.Size(82, 20);
            this.txtHotKey.TabIndex = 5;
            this.txtHotKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHotKey_KeyDown);
            this.txtHotKey.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHotKey_KeyPress);
            // 
            // checkAlt
            // 
            this.checkAlt.Location = new System.Drawing.Point(126, 93);
            this.checkAlt.Name = "checkAlt";
            this.checkAlt.Properties.Caption = "Alt";
            this.checkAlt.Size = new System.Drawing.Size(46, 19);
            this.checkAlt.TabIndex = 4;
            // 
            // checkShift
            // 
            this.checkShift.Location = new System.Drawing.Point(22, 93);
            this.checkShift.Name = "checkShift";
            this.checkShift.Properties.Caption = "Shift";
            this.checkShift.Size = new System.Drawing.Size(46, 19);
            this.checkShift.TabIndex = 2;
            // 
            // checkCtrl
            // 
            this.checkCtrl.Location = new System.Drawing.Point(74, 93);
            this.checkCtrl.Name = "checkCtrl";
            this.checkCtrl.Properties.Caption = "Ctrl";
            this.checkCtrl.Size = new System.Drawing.Size(46, 19);
            this.checkCtrl.TabIndex = 3;
            // 
            // checkVoiceCommands
            // 
            this.checkVoiceCommands.EditValue = true;
            this.checkVoiceCommands.Location = new System.Drawing.Point(5, 49);
            this.checkVoiceCommands.Name = "checkVoiceCommands";
            this.checkVoiceCommands.Properties.Caption = "Allow voice commands";
            this.checkVoiceCommands.Size = new System.Drawing.Size(135, 19);
            this.checkVoiceCommands.TabIndex = 1;
            this.checkVoiceCommands.CheckedChanged += new System.EventHandler(this.checkVoiceCommands_CheckedChanged);
            // 
            // checkVoiceMessages
            // 
            this.checkVoiceMessages.EditValue = true;
            this.checkVoiceMessages.Location = new System.Drawing.Point(5, 24);
            this.checkVoiceMessages.Name = "checkVoiceMessages";
            this.checkVoiceMessages.Properties.Caption = "Audio notifications";
            this.checkVoiceMessages.Size = new System.Drawing.Size(135, 19);
            this.checkVoiceMessages.TabIndex = 0;
            // 
            // groupServerSettings
            // 
            this.groupServerSettings.Controls.Add(this.lblServerPort);
            this.groupServerSettings.Controls.Add(this.txtServerPort);
            this.groupServerSettings.Controls.Add(this.lblServerAddress);
            this.groupServerSettings.Controls.Add(this.txtServerAddress);
            this.groupServerSettings.Controls.Add(this.checkCustomServer);
            this.groupServerSettings.Controls.Add(this.checkAutoDiscovery);
            this.groupServerSettings.Location = new System.Drawing.Point(12, 136);
            this.groupServerSettings.Name = "groupServerSettings";
            this.groupServerSettings.Size = new System.Drawing.Size(321, 125);
            this.groupServerSettings.TabIndex = 1;
            this.groupServerSettings.Text = "Server settings";
            // 
            // lblServerPort
            // 
            this.lblServerPort.Enabled = false;
            this.lblServerPort.Location = new System.Drawing.Point(22, 103);
            this.lblServerPort.Name = "lblServerPort";
            this.lblServerPort.Size = new System.Drawing.Size(59, 13);
            this.lblServerPort.TabIndex = 5;
            this.lblServerPort.Text = "Server port:";
            // 
            // txtServerPort
            // 
            this.txtServerPort.Enabled = false;
            this.txtServerPort.Location = new System.Drawing.Point(105, 100);
            this.txtServerPort.Name = "txtServerPort";
            this.txtServerPort.Size = new System.Drawing.Size(185, 20);
            this.txtServerPort.TabIndex = 4;
            // 
            // lblServerAddress
            // 
            this.lblServerAddress.Enabled = false;
            this.lblServerAddress.Location = new System.Drawing.Point(22, 77);
            this.lblServerAddress.Name = "lblServerAddress";
            this.lblServerAddress.Size = new System.Drawing.Size(77, 13);
            this.lblServerAddress.TabIndex = 3;
            this.lblServerAddress.Text = "Server address:";
            // 
            // txtServerAddress
            // 
            this.txtServerAddress.Enabled = false;
            this.txtServerAddress.Location = new System.Drawing.Point(105, 74);
            this.txtServerAddress.Name = "txtServerAddress";
            this.txtServerAddress.Size = new System.Drawing.Size(185, 20);
            this.txtServerAddress.TabIndex = 2;
            // 
            // checkCustomServer
            // 
            this.checkCustomServer.Location = new System.Drawing.Point(5, 49);
            this.checkCustomServer.Name = "checkCustomServer";
            this.checkCustomServer.Properties.Caption = "Custom server settings";
            this.checkCustomServer.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkCustomServer.Properties.RadioGroupIndex = 0;
            this.checkCustomServer.Size = new System.Drawing.Size(135, 19);
            this.checkCustomServer.TabIndex = 1;
            this.checkCustomServer.TabStop = false;
            this.checkCustomServer.CheckedChanged += new System.EventHandler(this.checkCustomServer_CheckedChanged);
            // 
            // checkAutoDiscovery
            // 
            this.checkAutoDiscovery.EditValue = true;
            this.checkAutoDiscovery.Location = new System.Drawing.Point(5, 24);
            this.checkAutoDiscovery.Name = "checkAutoDiscovery";
            this.checkAutoDiscovery.Properties.Caption = "Auto Discovery";
            this.checkAutoDiscovery.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkAutoDiscovery.Properties.RadioGroupIndex = 0;
            this.checkAutoDiscovery.Size = new System.Drawing.Size(217, 19);
            this.checkAutoDiscovery.TabIndex = 0;
            // 
            // groupConfirmPowerActionMessage
            // 
            this.groupConfirmPowerActionMessage.Controls.Add(this.lblCountdownTime);
            this.groupConfirmPowerActionMessage.Controls.Add(this.spnCountdowmTime);
            this.groupConfirmPowerActionMessage.Controls.Add(this.checkPowerActionImmediately);
            this.groupConfirmPowerActionMessage.Controls.Add(this.checkShowConfirmActionMessage);
            this.groupConfirmPowerActionMessage.Location = new System.Drawing.Point(12, 267);
            this.groupConfirmPowerActionMessage.Name = "groupConfirmPowerActionMessage";
            this.groupConfirmPowerActionMessage.Size = new System.Drawing.Size(321, 99);
            this.groupConfirmPowerActionMessage.TabIndex = 2;
            this.groupConfirmPowerActionMessage.Text = "Confirm power action message";
            // 
            // lblCountdownTime
            // 
            this.lblCountdownTime.Location = new System.Drawing.Point(22, 77);
            this.lblCountdownTime.Name = "lblCountdownTime";
            this.lblCountdownTime.Size = new System.Drawing.Size(132, 13);
            this.lblCountdownTime.TabIndex = 5;
            this.lblCountdownTime.Text = "Countdown time (seconds):";
            // 
            // spnCountdowmTime
            // 
            this.spnCountdowmTime.EditValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spnCountdowmTime.Location = new System.Drawing.Point(160, 74);
            this.spnCountdowmTime.Name = "spnCountdowmTime";
            this.spnCountdowmTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnCountdowmTime.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.spnCountdowmTime.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnCountdowmTime.Size = new System.Drawing.Size(40, 20);
            this.spnCountdowmTime.TabIndex = 4;
            // 
            // checkPowerActionImmediately
            // 
            this.checkPowerActionImmediately.Location = new System.Drawing.Point(5, 24);
            this.checkPowerActionImmediately.Name = "checkPowerActionImmediately";
            this.checkPowerActionImmediately.Properties.Caption = "Perform power action immediately";
            this.checkPowerActionImmediately.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkPowerActionImmediately.Properties.RadioGroupIndex = 0;
            this.checkPowerActionImmediately.Size = new System.Drawing.Size(285, 19);
            this.checkPowerActionImmediately.TabIndex = 3;
            this.checkPowerActionImmediately.TabStop = false;
            // 
            // checkShowConfirmActionMessage
            // 
            this.checkShowConfirmActionMessage.EditValue = true;
            this.checkShowConfirmActionMessage.Location = new System.Drawing.Point(5, 49);
            this.checkShowConfirmActionMessage.Name = "checkShowConfirmActionMessage";
            this.checkShowConfirmActionMessage.Properties.Caption = "Show \"Confirm power action\" message";
            this.checkShowConfirmActionMessage.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkShowConfirmActionMessage.Properties.RadioGroupIndex = 0;
            this.checkShowConfirmActionMessage.Size = new System.Drawing.Size(217, 19);
            this.checkShowConfirmActionMessage.TabIndex = 2;
            this.checkShowConfirmActionMessage.CheckedChanged += new System.EventHandler(this.checkShowConfirmActionMessage_CheckedChanged);
            // 
            // groupRemoteDesktopSettings
            // 
            this.groupRemoteDesktopSettings.Controls.Add(this.checkDisableRDP);
            this.groupRemoteDesktopSettings.Controls.Add(this.checkAllowRDP);
            this.groupRemoteDesktopSettings.Location = new System.Drawing.Point(12, 372);
            this.groupRemoteDesktopSettings.Name = "groupRemoteDesktopSettings";
            this.groupRemoteDesktopSettings.Size = new System.Drawing.Size(321, 73);
            this.groupRemoteDesktopSettings.TabIndex = 3;
            this.groupRemoteDesktopSettings.Text = "Remote Desktop settings";
            // 
            // checkDisableRDP
            // 
            this.checkDisableRDP.Location = new System.Drawing.Point(5, 49);
            this.checkDisableRDP.Name = "checkDisableRDP";
            this.checkDisableRDP.Properties.Caption = "Don\'t allow connections to this computer";
            this.checkDisableRDP.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkDisableRDP.Properties.RadioGroupIndex = 0;
            this.checkDisableRDP.Size = new System.Drawing.Size(217, 19);
            this.checkDisableRDP.TabIndex = 3;
            this.checkDisableRDP.TabStop = false;
            // 
            // checkAllowRDP
            // 
            this.checkAllowRDP.EditValue = true;
            this.checkAllowRDP.Location = new System.Drawing.Point(5, 24);
            this.checkAllowRDP.Name = "checkAllowRDP";
            this.checkAllowRDP.Properties.Caption = "Allow connections to this computer";
            this.checkAllowRDP.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkAllowRDP.Properties.RadioGroupIndex = 0;
            this.checkAllowRDP.Size = new System.Drawing.Size(217, 19);
            this.checkAllowRDP.TabIndex = 2;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(258, 451);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(177, 451);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClientInfo
            // 
            this.btnClientInfo.Location = new System.Drawing.Point(12, 451);
            this.btnClientInfo.Name = "btnClientInfo";
            this.btnClientInfo.Size = new System.Drawing.Size(120, 23);
            this.btnClientInfo.TabIndex = 6;
            this.btnClientInfo.Text = "Client information...";
            this.btnClientInfo.Click += new System.EventHandler(this.btnClientInfo_Click);
            // 
            // AdvancedSettingsForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(345, 486);
            this.Controls.Add(this.btnClientInfo);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupRemoteDesktopSettings);
            this.Controls.Add(this.groupConfirmPowerActionMessage);
            this.Controls.Add(this.groupServerSettings);
            this.Controls.Add(this.groupVoiceSettings);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AdvancedSettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Advanced Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdvancedSettingsForm_FormClosing);
            this.Load += new System.EventHandler(this.AdvancedSettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupVoiceSettings)).EndInit();
            this.groupVoiceSettings.ResumeLayout(false);
            this.groupVoiceSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHotKey.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAlt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkShift.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCtrl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkVoiceCommands.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkVoiceMessages.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupServerSettings)).EndInit();
            this.groupServerSettings.ResumeLayout(false);
            this.groupServerSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtServerPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServerAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCustomServer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAutoDiscovery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupConfirmPowerActionMessage)).EndInit();
            this.groupConfirmPowerActionMessage.ResumeLayout(false);
            this.groupConfirmPowerActionMessage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spnCountdowmTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPowerActionImmediately.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkShowConfirmActionMessage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupRemoteDesktopSettings)).EndInit();
            this.groupRemoteDesktopSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkDisableRDP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAllowRDP.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupVoiceSettings;
        private DevExpress.XtraEditors.CheckEdit checkVoiceMessages;
        private DevExpress.XtraEditors.TextEdit txtHotKey;
        private DevExpress.XtraEditors.CheckEdit checkAlt;
        private DevExpress.XtraEditors.CheckEdit checkCtrl;
        private DevExpress.XtraEditors.CheckEdit checkShift;
        private DevExpress.XtraEditors.CheckEdit checkVoiceCommands;
        private DevExpress.XtraEditors.GroupControl groupServerSettings;
        private DevExpress.XtraEditors.CheckEdit checkCustomServer;
        private DevExpress.XtraEditors.CheckEdit checkAutoDiscovery;
        private DevExpress.XtraEditors.LabelControl lblServerAddress;
        private DevExpress.XtraEditors.TextEdit txtServerAddress;
        private DevExpress.XtraEditors.LabelControl lblServerPort;
        private DevExpress.XtraEditors.TextEdit txtServerPort;
        private DevExpress.XtraEditors.LabelControl lblHotKey;
        private DevExpress.XtraEditors.GroupControl groupConfirmPowerActionMessage;
        private DevExpress.XtraEditors.LabelControl lblCountdownTime;
        private DevExpress.XtraEditors.SpinEdit spnCountdowmTime;
        private DevExpress.XtraEditors.CheckEdit checkPowerActionImmediately;
        private DevExpress.XtraEditors.CheckEdit checkShowConfirmActionMessage;
        private DevExpress.XtraEditors.GroupControl groupRemoteDesktopSettings;
        private DevExpress.XtraEditors.CheckEdit checkDisableRDP;
        private DevExpress.XtraEditors.CheckEdit checkAllowRDP;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnClientInfo;
    }
}