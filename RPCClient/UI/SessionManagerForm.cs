﻿using DevExpress.XtraEditors;
using Shared.ProcessRestoring;

namespace RPCClient.UI
{
    public partial class SessionManagerForm : XtraForm
    {
        private IProcessRestorer _processRestorer;

        public SessionManagerForm()
        {
            InitializeComponent();
        }

        private void btnSavePoint_Click(object sender, System.EventArgs e)
        {
            _processRestorer = new FileProcessRestorer();
            _processRestorer.CreateSavePoint();
        }

        private void btnLoadPoint_Click(object sender, System.EventArgs e)
        {
            _processRestorer = new FileProcessRestorer();
            _processRestorer.LoadLastPoint();
        }
    }
}
