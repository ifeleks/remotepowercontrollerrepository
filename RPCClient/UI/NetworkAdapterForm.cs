﻿using System;
using System.Linq;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace RPCClient.UI
{
    public partial class NetworkAdapterForm : XtraForm
    {
        private bool _memoVisible;
        private bool _userClosing = true;
        
        public NetworkAdapterForm()
        {
            InitializeComponent();
        }

        private void NetworkAdapterForm_Load(object sender, EventArgs e)
        {
            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces().Where(nic =>
                nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet ||
                nic.NetworkInterfaceType == NetworkInterfaceType.FastEthernetFx ||
                nic.NetworkInterfaceType == NetworkInterfaceType.FastEthernetT ||
                nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet3Megabit ||
                nic.NetworkInterfaceType == NetworkInterfaceType.GigabitEthernet))
            {
                cmbNetworkAdapter.Properties.Items.Add(nic.Name);
            }
            cmbNetworkAdapter.SelectedIndex = 0;

            Height = 124;
            memoWOLInfo.Visible = false;
        }

        private void btnShowHide_Click(object sender, EventArgs e)
        {
            if (_memoVisible)
            {
                memoWOLInfo.Visible = false;
                Height = 124;
                btnShowHide.Text = @"Show ↓";
                _memoVisible = false;
            }
            else
            {
                memoWOLInfo.Visible = true;
                Height = 339;
                btnShowHide.Text = @"Hide ↑";
                _memoVisible = true;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            SaveClientInfo();

            _userClosing = false;
            Close();
        }

        private void SaveClientInfo()
        {
            var clientInfo = ClientInformation.ClientInformation.GetClientInfo(cmbNetworkAdapter.SelectedItem.ToString());
            var clientInfoTO = clientInfo.ConvertToTO();

            Properties.Settings.Default.ClientInfo = clientInfoTO;
        }
        
        private void NetworkAdapterForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!_userClosing) return;
            if (XtraMessageBox.Show(
                "Registration process is not complete, you want to cancel it?", "Remote Power Controller",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                Environment.Exit(0);
            }
        }
    }
}
