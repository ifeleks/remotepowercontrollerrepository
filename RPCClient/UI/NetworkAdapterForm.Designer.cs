﻿namespace RPCClient.UI
{
    partial class NetworkAdapterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NetworkAdapterForm));
            this.cmbNetworkAdapter = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblNetowrkAdapter = new System.Windows.Forms.Label();
            this.lblMoreInfo = new System.Windows.Forms.Label();
            this.btnShowHide = new DevExpress.XtraEditors.SimpleButton();
            this.memoWOLInfo = new DevExpress.XtraEditors.MemoEdit();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.cmbNetworkAdapter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoWOLInfo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbNetworkAdapter
            // 
            this.cmbNetworkAdapter.Location = new System.Drawing.Point(12, 25);
            this.cmbNetworkAdapter.Name = "cmbNetworkAdapter";
            this.cmbNetworkAdapter.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.cmbNetworkAdapter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbNetworkAdapter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbNetworkAdapter.Size = new System.Drawing.Size(291, 20);
            this.cmbNetworkAdapter.TabIndex = 0;
            // 
            // lblNetowrkAdapter
            // 
            this.lblNetowrkAdapter.AutoSize = true;
            this.lblNetowrkAdapter.Location = new System.Drawing.Point(12, 9);
            this.lblNetowrkAdapter.Name = "lblNetowrkAdapter";
            this.lblNetowrkAdapter.Size = new System.Drawing.Size(290, 13);
            this.lblNetowrkAdapter.TabIndex = 48;
            this.lblNetowrkAdapter.Text = "Choose your local network adapter with WOL functionality:";
            // 
            // lblMoreInfo
            // 
            this.lblMoreInfo.AutoSize = true;
            this.lblMoreInfo.Location = new System.Drawing.Point(12, 56);
            this.lblMoreInfo.Name = "lblMoreInfo";
            this.lblMoreInfo.Size = new System.Drawing.Size(129, 13);
            this.lblMoreInfo.TabIndex = 50;
            this.lblMoreInfo.Text = "Info about set up WOL...";
            // 
            // btnShowHide
            // 
            this.btnShowHide.Location = new System.Drawing.Point(147, 51);
            this.btnShowHide.Name = "btnShowHide";
            this.btnShowHide.Size = new System.Drawing.Size(75, 23);
            this.btnShowHide.TabIndex = 1;
            this.btnShowHide.Text = "Show ↓";
            this.btnShowHide.Click += new System.EventHandler(this.btnShowHide_Click);
            // 
            // memoWOLInfo
            // 
            this.memoWOLInfo.EditValue = resources.GetString("memoWOLInfo.EditValue");
            this.memoWOLInfo.Location = new System.Drawing.Point(12, 80);
            this.memoWOLInfo.Name = "memoWOLInfo";
            this.memoWOLInfo.Properties.ReadOnly = true;
            this.memoWOLInfo.Size = new System.Drawing.Size(291, 208);
            this.memoWOLInfo.TabIndex = 3;
            this.memoWOLInfo.UseOptimizedRendering = true;
            this.memoWOLInfo.Visible = false;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(228, 50);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // NetworkAdapterForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(315, 85);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.memoWOLInfo);
            this.Controls.Add(this.btnShowHide);
            this.Controls.Add(this.lblMoreInfo);
            this.Controls.Add(this.cmbNetworkAdapter);
            this.Controls.Add(this.lblNetowrkAdapter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "NetworkAdapterForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Network adapter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NetworkAdapterForm_FormClosing);
            this.Load += new System.EventHandler(this.NetworkAdapterForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cmbNetworkAdapter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoWOLInfo.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit cmbNetworkAdapter;
        private System.Windows.Forms.Label lblNetowrkAdapter;
        private System.Windows.Forms.Label lblMoreInfo;
        private DevExpress.XtraEditors.SimpleButton btnShowHide;
        private DevExpress.XtraEditors.MemoEdit memoWOLInfo;
        private DevExpress.XtraEditors.SimpleButton btnOK;
    }
}