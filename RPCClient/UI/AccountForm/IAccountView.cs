﻿using RPCClient.Entity;
using RPCClient.Infrastructure;

namespace RPCClient.UI.AccountForm
{
    public interface IAccountView : IView<AccountViewPresenter>
    {
        UserInfo GetRegisterUserInfo();
        UserInfo GetLoginUserInfo();
    }
}
