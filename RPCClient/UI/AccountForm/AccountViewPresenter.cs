﻿using System;
using Common.DTO;
using RPCClient.Authentication;
using RPCClient.Entity;
using RPCClient.Infrastructure;

namespace RPCClient.UI.AccountForm
{
    public class AccountViewPresenter : BasePresenter<IAccountView, AccountViewPresenter>
    {
        private static AccountViewPresenter _presenterInstance;

        public static AccountViewPresenter GetInstance(IAccountView view)
        {
            return _presenterInstance ?? (_presenterInstance = new AccountViewPresenter());
        }

        public void SaveInfo(UserInfo userInfo)
        {
            Properties.Settings.Default.Login = userInfo.Login;
            Properties.Settings.Default.Password = userInfo.Password;
            Properties.Settings.Default.IsUserDomain = false;
        }

        public void SetInstallationDate()
        {
            Properties.Settings.Default.ClientInfo.InstallationDate = DateTime.Now;
            Properties.Settings.Default.Save();
        }

        public bool IsLoginExist(UserInfoTO userInfoTO)
        {
            return AuthentificationValidator.IsLoginExist(userInfoTO);
        }

        public bool IsMacAdressExist(UserInfoTO userInfoTO)
        {
            return AuthentificationValidator.IsMacAdressExist(userInfoTO);
        }

        public bool IsLoginCorrect(UserInfoTO userInfoTO)
        {
            return AuthentificationValidator.IsLoginCorrect(userInfoTO);
        }
    }
}
