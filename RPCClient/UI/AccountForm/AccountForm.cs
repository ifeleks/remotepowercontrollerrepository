﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RPCClient.Authentication;
using RPCClient.Entity;
using RPCClient.Enums;

namespace RPCClient.UI.AccountForm
{
    public partial class AccountForm : XtraForm, IAccountView
    {
        private SubmitType _submitType = SubmitType.UpdateSettings;

        private bool _userClosing = true;

        public AccountViewPresenter Presenter
        {
            get
            {
                return AccountViewPresenter.GetInstance(this);
            }
            set
            { }
        }

        public void ShowView()
        {
            ShowDialog();
        }
        public AccountForm()
        {
            InitializeComponent();
        }

        private void btnCreateNewUser_Click(object sender, EventArgs e)
        {
            if (txtRegisterUsername.Text.Length >= 3 &&
                txtRegisterPassword.Text.Length >= 6)
            {
                if (txtRegisterConfirmPassword.Text == txtRegisterPassword.Text)
                {
                    var inputUserCredentials = GetRegisterUserInfo();
                    if (!AuthentificationValidator.ValidateRegistration(inputUserCredentials.ConvertToTO()))
                        return;

                    Presenter.SaveInfo(inputUserCredentials);

                    _submitType = SubmitType.RegisterNewUser;

                    _userClosing = false;
                    Close();
                }
                else
                {
                    XtraMessageBox.Show("Passwords not match", "Warning! - RemotePowerController",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                XtraMessageBox.Show("Username should not be shorter than 3 symbols and password should not be shorter than 6 symbols",
                    "Warning! - RemotePowerController",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnCreateNewClient_Click(object sender, EventArgs e)
        {
            var inputUserCredentials = GetLoginUserInfo();
            if (!AuthentificationValidator.ValidateLogin(inputUserCredentials.ConvertToTO()))
                return;

            Presenter.SaveInfo(GetLoginUserInfo());

            _submitType = SubmitType.AddNewClient;

            _userClosing = false;
            Close();
        }

        public UserInfo GetRegisterUserInfo()
        {
            var userInfo = new UserInfo(
                  txtRegisterUsername.Text,
                  txtRegisterPassword.Text,
                  false);

            return userInfo;
        }

        public UserInfo GetLoginUserInfo()
        {
            var userInfo = new UserInfo(
                  txtLoginUsername.Text,
                  txtLoginPassword.Text,
                  false);

            return userInfo;
        }

        public void GetSubmitType(out SubmitType submitType)
        {
            submitType = _submitType;
        }

        private void AccountForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!_userClosing) return;
            if (XtraMessageBox.Show(
                "Registration process is not complete, you want to cancel it?", "Closing. - RemotePowerController",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                Environment.Exit(0);
            }
        }

        private void tabsAccount_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            AcceptButton = tabsAccount.SelectedTabPage.Text.Equals("Register") ? btnCreateNewUser : btnCreateNewClient;
        }

        private void AccountForm_Shown(object sender, EventArgs e)
        {
            Presenter.SetInstallationDate();
        }
    }
}
