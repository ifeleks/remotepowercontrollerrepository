﻿namespace RPCClient.UI.AccountForm
{
    partial class AccountForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AccountForm));
            this.tabsAccount = new DevExpress.XtraTab.XtraTabControl();
            this.tabRegister = new DevExpress.XtraTab.XtraTabPage();
            this.btnCreateNewUser = new DevExpress.XtraEditors.SimpleButton();
            this.txtRegisterUsername = new DevExpress.XtraEditors.TextEdit();
            this.txtRegisterConfirmPassword = new DevExpress.XtraEditors.TextEdit();
            this.txtRegisterPassword = new DevExpress.XtraEditors.TextEdit();
            this.lblRegisterUsername = new System.Windows.Forms.Label();
            this.lblRegisterPassword = new System.Windows.Forms.Label();
            this.lblRegisterConfirmPassword = new System.Windows.Forms.Label();
            this.tabLogin = new DevExpress.XtraTab.XtraTabPage();
            this.btnCreateNewClient = new DevExpress.XtraEditors.SimpleButton();
            this.txtLoginPassword = new DevExpress.XtraEditors.TextEdit();
            this.txtLoginUsername = new DevExpress.XtraEditors.TextEdit();
            this.lblLoginPassword = new System.Windows.Forms.Label();
            this.lblLoginUsername = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tabsAccount)).BeginInit();
            this.tabsAccount.SuspendLayout();
            this.tabRegister.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegisterUsername.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegisterConfirmPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegisterPassword.Properties)).BeginInit();
            this.tabLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoginPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoginUsername.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tabsAccount
            // 
            this.tabsAccount.Location = new System.Drawing.Point(0, 0);
            this.tabsAccount.Name = "tabsAccount";
            this.tabsAccount.SelectedTabPage = this.tabRegister;
            this.tabsAccount.Size = new System.Drawing.Size(350, 162);
            this.tabsAccount.TabIndex = 4;
            this.tabsAccount.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabRegister,
            this.tabLogin});
            this.tabsAccount.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tabsAccount_SelectedPageChanged);
            // 
            // tabRegister
            // 
            this.tabRegister.Controls.Add(this.btnCreateNewUser);
            this.tabRegister.Controls.Add(this.txtRegisterUsername);
            this.tabRegister.Controls.Add(this.txtRegisterConfirmPassword);
            this.tabRegister.Controls.Add(this.txtRegisterPassword);
            this.tabRegister.Controls.Add(this.lblRegisterUsername);
            this.tabRegister.Controls.Add(this.lblRegisterPassword);
            this.tabRegister.Controls.Add(this.lblRegisterConfirmPassword);
            this.tabRegister.Name = "tabRegister";
            this.tabRegister.Size = new System.Drawing.Size(344, 134);
            this.tabRegister.Text = "Register";
            // 
            // btnCreateNewUser
            // 
            this.btnCreateNewUser.Location = new System.Drawing.Point(114, 93);
            this.btnCreateNewUser.Name = "btnCreateNewUser";
            this.btnCreateNewUser.Size = new System.Drawing.Size(116, 30);
            this.btnCreateNewUser.TabIndex = 3;
            this.btnCreateNewUser.Text = "Create new user";
            this.btnCreateNewUser.Click += new System.EventHandler(this.btnCreateNewUser_Click);
            // 
            // txtRegisterUsername
            // 
            this.txtRegisterUsername.Location = new System.Drawing.Point(114, 15);
            this.txtRegisterUsername.Name = "txtRegisterUsername";
            this.txtRegisterUsername.Size = new System.Drawing.Size(219, 20);
            this.txtRegisterUsername.TabIndex = 0;
            // 
            // txtRegisterConfirmPassword
            // 
            this.txtRegisterConfirmPassword.Location = new System.Drawing.Point(114, 67);
            this.txtRegisterConfirmPassword.Name = "txtRegisterConfirmPassword";
            this.txtRegisterConfirmPassword.Properties.PasswordChar = '●';
            this.txtRegisterConfirmPassword.Size = new System.Drawing.Size(219, 20);
            this.txtRegisterConfirmPassword.TabIndex = 2;
            // 
            // txtRegisterPassword
            // 
            this.txtRegisterPassword.Location = new System.Drawing.Point(114, 41);
            this.txtRegisterPassword.Name = "txtRegisterPassword";
            this.txtRegisterPassword.Properties.PasswordChar = '●';
            this.txtRegisterPassword.Size = new System.Drawing.Size(219, 20);
            this.txtRegisterPassword.TabIndex = 1;
            // 
            // lblRegisterUsername
            // 
            this.lblRegisterUsername.AutoSize = true;
            this.lblRegisterUsername.Location = new System.Drawing.Point(11, 18);
            this.lblRegisterUsername.Name = "lblRegisterUsername";
            this.lblRegisterUsername.Size = new System.Drawing.Size(59, 13);
            this.lblRegisterUsername.TabIndex = 35;
            this.lblRegisterUsername.Text = "Username:";
            // 
            // lblRegisterPassword
            // 
            this.lblRegisterPassword.AutoSize = true;
            this.lblRegisterPassword.Location = new System.Drawing.Point(11, 44);
            this.lblRegisterPassword.Name = "lblRegisterPassword";
            this.lblRegisterPassword.Size = new System.Drawing.Size(57, 13);
            this.lblRegisterPassword.TabIndex = 37;
            this.lblRegisterPassword.Text = "Password:";
            // 
            // lblRegisterConfirmPassword
            // 
            this.lblRegisterConfirmPassword.AutoSize = true;
            this.lblRegisterConfirmPassword.Location = new System.Drawing.Point(11, 70);
            this.lblRegisterConfirmPassword.Name = "lblRegisterConfirmPassword";
            this.lblRegisterConfirmPassword.Size = new System.Drawing.Size(97, 13);
            this.lblRegisterConfirmPassword.TabIndex = 41;
            this.lblRegisterConfirmPassword.Text = "Confirm password:";
            // 
            // tabLogin
            // 
            this.tabLogin.Controls.Add(this.btnCreateNewClient);
            this.tabLogin.Controls.Add(this.txtLoginPassword);
            this.tabLogin.Controls.Add(this.txtLoginUsername);
            this.tabLogin.Controls.Add(this.lblLoginPassword);
            this.tabLogin.Controls.Add(this.lblLoginUsername);
            this.tabLogin.Name = "tabLogin";
            this.tabLogin.Size = new System.Drawing.Size(344, 134);
            this.tabLogin.Text = "Login";
            // 
            // btnCreateNewClient
            // 
            this.btnCreateNewClient.Location = new System.Drawing.Point(114, 93);
            this.btnCreateNewClient.Name = "btnCreateNewClient";
            this.btnCreateNewClient.Size = new System.Drawing.Size(116, 30);
            this.btnCreateNewClient.TabIndex = 51;
            this.btnCreateNewClient.Text = "Add new client";
            this.btnCreateNewClient.Click += new System.EventHandler(this.btnCreateNewClient_Click);
            // 
            // txtLoginPassword
            // 
            this.txtLoginPassword.Location = new System.Drawing.Point(114, 53);
            this.txtLoginPassword.Name = "txtLoginPassword";
            this.txtLoginPassword.Properties.PasswordChar = '●';
            this.txtLoginPassword.Size = new System.Drawing.Size(219, 20);
            this.txtLoginPassword.TabIndex = 50;
            // 
            // txtLoginUsername
            // 
            this.txtLoginUsername.Location = new System.Drawing.Point(114, 27);
            this.txtLoginUsername.Name = "txtLoginUsername";
            this.txtLoginUsername.Size = new System.Drawing.Size(219, 20);
            this.txtLoginUsername.TabIndex = 49;
            // 
            // lblLoginPassword
            // 
            this.lblLoginPassword.AutoSize = true;
            this.lblLoginPassword.Location = new System.Drawing.Point(16, 56);
            this.lblLoginPassword.Name = "lblLoginPassword";
            this.lblLoginPassword.Size = new System.Drawing.Size(57, 13);
            this.lblLoginPassword.TabIndex = 42;
            this.lblLoginPassword.Text = "Password:";
            // 
            // lblLoginUsername
            // 
            this.lblLoginUsername.AutoSize = true;
            this.lblLoginUsername.Location = new System.Drawing.Point(16, 30);
            this.lblLoginUsername.Name = "lblLoginUsername";
            this.lblLoginUsername.Size = new System.Drawing.Size(59, 13);
            this.lblLoginUsername.TabIndex = 39;
            this.lblLoginUsername.Text = "Username:";
            // 
            // AccountForm
            // 
            this.AcceptButton = this.btnCreateNewUser;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 158);
            this.Controls.Add(this.tabsAccount);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AccountForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Account";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AccountForm_FormClosing);
            this.Shown += new System.EventHandler(this.AccountForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.tabsAccount)).EndInit();
            this.tabsAccount.ResumeLayout(false);
            this.tabRegister.ResumeLayout(false);
            this.tabRegister.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegisterUsername.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegisterConfirmPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegisterPassword.Properties)).EndInit();
            this.tabLogin.ResumeLayout(false);
            this.tabLogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoginPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoginUsername.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl tabsAccount;
        private DevExpress.XtraTab.XtraTabPage tabRegister;
        private System.Windows.Forms.Label lblRegisterUsername;
        private System.Windows.Forms.Label lblRegisterPassword;
        private System.Windows.Forms.Label lblRegisterConfirmPassword;
        private DevExpress.XtraTab.XtraTabPage tabLogin;
        private System.Windows.Forms.Label lblLoginPassword;
        private System.Windows.Forms.Label lblLoginUsername;
        private DevExpress.XtraEditors.TextEdit txtRegisterPassword;
        private DevExpress.XtraEditors.TextEdit txtRegisterConfirmPassword;
        private DevExpress.XtraEditors.TextEdit txtRegisterUsername;
        private DevExpress.XtraEditors.TextEdit txtLoginUsername;
        private DevExpress.XtraEditors.TextEdit txtLoginPassword;
        private DevExpress.XtraEditors.SimpleButton btnCreateNewUser;
        private DevExpress.XtraEditors.SimpleButton btnCreateNewClient;

    }
}