﻿namespace RPCClient.UI
{
    partial class ShutdownAfterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShutdownAfterForm));
            this.lblShutownAfter = new System.Windows.Forms.Label();
            this.tmShutdownTime = new DevExpress.XtraEditors.TimeEdit();
            this.lblTimeFormat = new System.Windows.Forms.Label();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.tmShutdownTime.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblShutownAfter
            // 
            this.lblShutownAfter.AutoSize = true;
            this.lblShutownAfter.Location = new System.Drawing.Point(12, 14);
            this.lblShutownAfter.Name = "lblShutownAfter";
            this.lblShutownAfter.Size = new System.Drawing.Size(86, 13);
            this.lblShutownAfter.TabIndex = 39;
            this.lblShutownAfter.Text = "Shutdown after:";
            // 
            // tmShutdownTime
            // 
            this.tmShutdownTime.EditValue = new System.DateTime(2014, 6, 18, 0, 5, 0, 0);
            this.tmShutdownTime.Location = new System.Drawing.Point(104, 11);
            this.tmShutdownTime.Name = "tmShutdownTime";
            this.tmShutdownTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tmShutdownTime.Properties.Mask.EditMask = "t";
            this.tmShutdownTime.Size = new System.Drawing.Size(68, 20);
            this.tmShutdownTime.TabIndex = 0;
            // 
            // lblTimeFormat
            // 
            this.lblTimeFormat.AutoSize = true;
            this.lblTimeFormat.Location = new System.Drawing.Point(178, 14);
            this.lblTimeFormat.Name = "lblTimeFormat";
            this.lblTimeFormat.Size = new System.Drawing.Size(47, 13);
            this.lblTimeFormat.TabIndex = 50;
            this.lblTimeFormat.Text = "(hh:mm)";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(39, 38);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(120, 37);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ShutdownAfterForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(237, 72);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblTimeFormat);
            this.Controls.Add(this.tmShutdownTime);
            this.Controls.Add(this.lblShutownAfter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ShutdownAfterForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shutdown after...";
            ((System.ComponentModel.ISupportInitialize)(this.tmShutdownTime.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblShutownAfter;
        private DevExpress.XtraEditors.TimeEdit tmShutdownTime;
        private System.Windows.Forms.Label lblTimeFormat;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
    }
}