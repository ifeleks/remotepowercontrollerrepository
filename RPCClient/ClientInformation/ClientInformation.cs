﻿using System;
using System.DirectoryServices.ActiveDirectory;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using RPCClient.Entity;

namespace RPCClient.ClientInformation
{
    static class ClientInformation
    {
        public static ClientInfo GetClientInfo(string selectedNetworkAdapterName)
        {
            return new ClientInfo(
                ClientName(),
                ClientFullName(),
                MacAddress(selectedNetworkAdapterName),
                IpAddress(selectedNetworkAdapterName));
        }

        private static string ClientName()
        {
            return Environment.MachineName;
        }

        private static string ClientFullName()
        {
            var fullName = string.Empty;
            try
            {
                Domain.GetComputerDomain();
                var strHostName = Dns.GetHostName();
                var ipEntry = Dns.GetHostEntry(strHostName);
                fullName = ipEntry.HostName;
            }
            catch { }

            return fullName;
        }

        private static string MacAddress(string networkAdapter)
        {
            var macAddress = string.Empty;
            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces().Where(nic =>
                nic.Name == networkAdapter))
            {
                macAddress = nic.GetPhysicalAddress().ToString();
            }

            return macAddress;
        }

        private static string IpAddress(string networkAdapter)
        {
            var localIP = string.Empty;
            foreach (var ip in from nic in NetworkInterface.GetAllNetworkInterfaces()
                               where nic.Name == networkAdapter
                               from ip in nic.GetIPProperties().UnicastAddresses.Where(ip =>
                                   ip.Address.AddressFamily == AddressFamily.InterNetwork)
                               select ip)
            {
                localIP = ip.Address.ToString();
            }

            return localIP;
        }
    }
}
