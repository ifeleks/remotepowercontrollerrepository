﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Threading;
using Shared.Audio;

namespace RPCClient
{
    static class Program
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool createdNew;
            using (new Mutex(true, "Remote Power Controller", out createdNew))
            {
                if (createdNew)
                {
                    StartApplication();
                }
                else
                {
                    var current = Process.GetCurrentProcess();
                    foreach (var process in Process.GetProcessesByName(current.ProcessName)
                        .Where(process => process.Id != current.Id))
                    {
                        SetForegroundWindow(process.MainWindowHandle);
                        MessageBox.Show(@"Another instance of Remote Power Controller is already running.", @"Remote Power Controller",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                }
            }
        }

        private static void StartApplication()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var mainEntrance = new Principal();
            mainEntrance.LoadAppModules();

            Application.Run();       
        }
    }
}
