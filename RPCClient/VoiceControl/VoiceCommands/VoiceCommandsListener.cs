﻿using System;
using System.Speech.Recognition;
using DevExpress.XtraEditors;

namespace RPCClient.VoiceControl.VoiceCommands
{
    class VoiceCommandsListener
    {
        SpeechRecognitionEngine sRecognizer = new SpeechRecognitionEngine();
        private string _voiceCommand;

        public void StartListening()
        {
            var commandsList = new Choices();
            commandsList.Add(new[] { "shutdown", "go to sleep", "close", "settings", "session restoring", "advanced settings" });
            var grammar = new Grammar(new GrammarBuilder(commandsList));
            try
            {
                sRecognizer.RequestRecognizerUpdate();
                sRecognizer.LoadGrammar(grammar);
                sRecognizer.SpeechRecognized += sRecognizer_SpeechRecognized;
                sRecognizer.SetInputToDefaultAudioDevice();
                sRecognizer.Recognize(TimeSpan.FromSeconds(5));
            }
            catch
            {
                return;
            }
        }

        private void sRecognizer_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            //sRecognizer.RecognizeAsyncStop();
            sRecognizer.Dispose();
            _voiceCommand = e.Result.Text;
            //XtraMessageBox.Show("Speech recognized: " + e.Result.Text);
        }

        public string GetVoiceCommand()
        {
            return _voiceCommand;
        }
    }
}
