﻿namespace RPCClient.Infrastructure
{
     public abstract class BasePresenter<TView, TPresenter>
        where TView : IView<TPresenter>
        where TPresenter : class
    {
        private TView _view;

        public void ShowView()
        {
            _view.ShowView();
        }
         
        public TView View
        {
            get { return _view; }
            set
            {
                _view = value;
                _view.Presenter = this as TPresenter;
            }
        }
    }
}
