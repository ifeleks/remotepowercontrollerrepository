﻿using RPCClient.UI.AdvancedSettingsForm;
using RPCClient.UI.SettingsForm;

namespace RPCClient.Infrastructure
{
    public interface IView<TPresenter>
    {
        TPresenter Presenter { get; set; }

        void ShowView();
    }
}
