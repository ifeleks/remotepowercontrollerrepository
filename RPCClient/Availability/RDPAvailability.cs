﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Microsoft.Win32;

namespace RPCClient.Availability
{
    static class RDPAvailability
    {
        private static RegistryKey _registryKey;
        public static bool IsRDPAvailable()
        {
            try
            {
                _registryKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\Terminal Server", true);
                if (_registryKey == null) return false;
                var rdpValue = _registryKey.GetValue("fDenyTSConnections");
                _registryKey.Close();
                _registryKey.Dispose();
                return rdpValue.Equals(0);
            }
            catch (Exception ex)
            {
                NoAdminAccess();
                throw ex;
            }
        }

        public static void NoAdminAccess()
        {
            var mbResult = XtraMessageBox.Show(
                "Remote Desktop Settings\n\n" +
                "Cannot read Remote Desktop Settings.\n" +
                "You must run appliation with admin permission, to be able to change Remote Settings.\n" +
                "(OK - Skip this step; Cancel - Exit application)",
                "Remote Power Controller", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

            if (mbResult == DialogResult.Cancel)
            {
                Environment.Exit(0);
            }
        }

        public static void EnableRDP()
        {
            SwitchRDP(0);
        }

        public static void DisableRDP()
        {
            SwitchRDP(1);
        }

        private static void SwitchRDP(int value)
        {
            _registryKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\Terminal Server", true);
            if (_registryKey == null) return;
            _registryKey.SetValue("fDenyTSConnections", value);
            _registryKey.Close();
            _registryKey.Dispose();
        }
    }
}
