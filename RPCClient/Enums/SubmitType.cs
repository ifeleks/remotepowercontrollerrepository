﻿namespace RPCClient.Enums
{
    public enum SubmitType
    {
        RegisterNewUser, AddNewClient, UpdateSettings
    }
}
