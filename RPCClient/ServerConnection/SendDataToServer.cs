﻿using System;
using System.ServiceModel;
using System.Windows.Forms;
using Common.DTO;
using Common.Logger;
using Common.Logger.Impl;
using Common.Service.Proxy;
using DevExpress.XtraEditors;
using RPCClient.UI.SettingsForm;
using RPCServer.Interfaces;

namespace RPCClient.ServerConnection
{
    static class SendDataToServer
    {
        static readonly ILogger Logger = LoggerFactory.Current.GetLogger<SettingsViewPresenter>();

        public static void RegisterNewUser(ClientInfoTO clientInfoTO, UserInfoTO userInfoTO, SettingsTO settingsTO)
        {
            var exception = new Exception(string.Empty);

            var serverUserEndpoint = new EndpointAddress(
                string.Format("http://{0}:{1}/UserInfoService",
                    Properties.Settings.Default["ServerIP"],
                    Properties.Settings.Default["ServerPort"]));

            ServiceChannel<IUserInfoService>.For(serverUserEndpoint, service =>
            {
                try
                {
                    service.RegisterNewUser(userInfoTO, clientInfoTO, settingsTO);
                }
                catch (Exception ex)
                {
                    exception = ex;
                }
            });
            if (exception.Message == string.Empty)
            {
                XtraMessageBox.Show(
                    string.Format("User {0} registered succesfull!", userInfoTO.Login),
                    "Succes!. - RemotePowerController", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Properties.Settings.Default.Login = string.Empty;
            Properties.Settings.Default.Password = string.Empty;
            Properties.Settings.Default.Save();

            switch (exception.Message)
            {
                case "MACAddres already exists":
                    XtraMessageBox.Show(
                        "This PC is currently linked to another user.\nPlease login or contact administrator if you lost your password",
                        "Warning. - RemotePowerController", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case "Login already exists":
                    XtraMessageBox.Show(
                        "That username already exist. Try another",
                        "Warning. - RemotePowerController", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                default:
                    XtraMessageBox.Show("Error occured during registration! Try again or contact service center",
                        "Error - RemotePowerController", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Logger.Error("Registration error", exception);
                    break;
            }
            Environment.Exit(0);
        }

        public static void AddNewClient(ClientInfoTO clientInfoTO, UserInfoTO userInfoTO, SettingsTO settingsTO)
        {
            var exception = new Exception(string.Empty);
          
            var serverClientEndpoint = new EndpointAddress(
                string.Format("http://{0}:{1}/ClientInfoService",
                    Properties.Settings.Default["ServerIP"],
                    Properties.Settings.Default["ServerPort"]));

            ServiceChannel<IClientInfoService>.For(serverClientEndpoint, service =>
            {
                try
                {
                    service.AddNewClientInfoForUser(userInfoTO, clientInfoTO, settingsTO);
                }
                catch (Exception ex)
                {
                    exception = ex;
                }
            });
            if (exception.Message == string.Empty)
            {
                XtraMessageBox.Show(
                    string.Format("Login for {0} went succesfull!", userInfoTO.Login),
                    "Succes!. - RemotePowerController", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Properties.Settings.Default.Login = string.Empty;
            Properties.Settings.Default.Password = string.Empty;
            Properties.Settings.Default.Save();

            if (exception.Message == "MACAddres already exists for another user")
            {
                XtraMessageBox.Show(
                    "This PC is currently linked to another user.\nPlease login or contact administrator if you lost your password",
                    "Warning. - RemotePowerController", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }
            else
            {
                XtraMessageBox.Show("Error occured during login! Try again or contact service center", "Error. - RemotePowerController", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Logger.Error("AddNewClient Error", exception);
                Environment.Exit(0);
            }
        }

        public static void ChangeSettings(ClientInfoTO clientInfoTO, SettingsTO settingsTO)
        {
            var serverSettingsEndpoint = new EndpointAddress(
                string.Format("http://{0}:{1}/SettingsService",
                Properties.Settings.Default["ServerIP"],
                Properties.Settings.Default["ServerPort"]));

            ServiceChannel<ISettingsService>.For(serverSettingsEndpoint,
                service => service.UpdateSettings(clientInfoTO, settingsTO));
        }
    }
}
